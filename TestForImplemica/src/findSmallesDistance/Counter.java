package findSmallesDistance;

/*
 * A helper class that calculates the minimum distance, getting the desired point
 * 
 * @author Shvec.K.
 * @version 1.0
 */
class Counter {
	private int[][] distances;

	public Counter(int city) {
		distances = new int[city][city];
	}

	/*
	 * Method calculates an array of distances from the origin to the other and
	 * return the minimum distance
	 * 
	 */
	public Integer[] distancesFrom(int startingPoint) {
		Integer[] result = new Integer[distances.length];
		java.util.Arrays.fill(result, 10000);// Set max point
		result[startingPoint] = startingPoint;// Assigns the resulting search point
		boolean[] factOfVisit = new boolean[distances.length];// Establishes the fact of visiting a specific point
		for (int x = 0; x < distances.length; x++) {
			int City = findMinIndexDistance(result, factOfVisit);// Select the top of which has a minimum mark
			factOfVisit[City] = true;
			for (int y = 0; y < distances.length; y++) {
				result[y] = Math.min(result[y], result[City] + getDistance(City, y));
			}
		}
		return result;
	}

	/*
	 * Checks if there is a neighborhood between cities. If not, it returns 10000
	 */
	public int getDistance(int x, int y) {
		if (x == y) {
			return 0;
		}
		if (distances[x][y] == 0) {
			return 10000;
		}
		return distances[x][y];
	}

	/*
	 * The method finds the index of the element to which the distance is the
	 * smallest
	 */
	protected int findMinIndexDistance(Integer[] output, boolean[] factOfVisit) {
		int indexSmallestDistance = -1;
		for (int i = 0; i < distances.length; i++) {
			if (!factOfVisit[i] && ((indexSmallestDistance < 0) || (output[i] < output[indexSmallestDistance]))) {
				indexSmallestDistance = i;
			}
		}
		return indexSmallestDistance;
	}

	/*
	 * Set the cost between the nearest points
	 * 
	 */
	public void setDistance(int x, int y, int cost) {
		distances[x][y] = cost;
	}

}