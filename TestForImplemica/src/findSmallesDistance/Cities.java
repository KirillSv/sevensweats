package findSmallesDistance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * 2. You are given a list of cities. Each direct connection between two cities has its
 *  transportation cost (an integer bigger than 0). The goal is to find the paths of minimum 
 *  cost between pairs of cities. Assume that the cost of each path (which is the sum of 
 *  costs of all direct connections belonging to this path) is at most 200000.
 *  The name of a city is a string containing characters a,...,z and is at most 10 characters long.2) 
 * 
 * @author Shvec.K.
 * @version 1.0
 */

public class Cities {
	public static void main(String[] args) throws IOException {
		final String PATH = new File("src\\findSmallesDistance\\test.txt").getAbsolutePath();
		final int MAX_ARRAY_LENGTH = 10000;

		try (BufferedReader input = new BufferedReader(new FileReader(PATH))) {
			String nextline = input.readLine();
			int data = Integer.parseInt(nextline);
			for (int i = 0; i < data; i++) {
				String[] cityArray = new String[MAX_ARRAY_LENGTH];// create an array for data
				nextline = input.readLine();
				int countCities = Integer.parseInt(nextline);// get the number of cities
				int sityArraySize = countCities + 1; // shift the index by one because arrays are counted from 0
				Counter counter = new Counter(sityArraySize);
				for (int t = 0; t < countCities; t++) {
					nextline = input.readLine();// get the city`s name
					cityArray[t] = nextline;
					nextline = input.readLine();// get the next number of neighbors
					int nextNumber = Integer.parseInt(nextline);
					for (int j = 0; j < nextNumber; j++) {
						nextline = input.readLine();
						String[] stopMarker = nextline.split(" ");
						int cityConnect = Integer.parseInt(stopMarker[0]);// code city to connect
						int weightOfConnectionCityConnect = Integer.parseInt(stopMarker[1]);
						counter.setDistance(t, cityConnect, weightOfConnectionCityConnect);
					}
				}
				nextline = input.readLine();
				int directionsToFind = Integer.parseInt(nextline);// number of possible directions
				for (int r = 0; r < directionsToFind; r++) {
					nextline = input.readLine();// get the path
					String[] nameOfSity = nextline.split(" ");
					String startSity = nameOfSity[0];
					String destinationSity = nameOfSity[1];
					List<String> listPath = new ArrayList<String>();
					for (String str : cityArray) { // removing empty options
						if (str != null) {
							listPath.add(str);
						}
					}
					cityArray = listPath.toArray(new String[listPath.size()]);
					int startIndex = 0; // index of the start city
					int finishIndex = 0; // index of the finish city
					for (int x = 0; x < cityArray.length; x++) {
						if (startSity.equals(cityArray[x])) {
							startIndex = x;
							break;
						}
					}
					for (int y = 0; y < cityArray.length; y++) {
						if (destinationSity.equals(cityArray[y])) {
							finishIndex = y;
							break;
						}
					}
					Integer[] howlongDistance = counter.distancesFrom(startIndex);
					int destinationDistance = howlongDistance[finishIndex];
					System.out.println(destinationDistance);
				}

			}
		}

	}

}