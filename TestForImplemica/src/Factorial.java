import java.math.BigInteger;

/*
 * Find the sum of the digits in the number 100! (i.e. 100 factorial) 
 * 
 * @author Shvec.K.
 * @version 1.0
 */

public class Factorial {

	public static void main(String[] args) {
		final int X = 100;
		BigInteger factorial = getFactorial(X);

		System.out.println(findSum(factorial));

	}

	/*
	 * The method takes a number as input and finds its factorial
	 * 
	 */
	public static BigInteger getFactorial(int x) {
		if (x <= 1) {
			return BigInteger.valueOf(1);
		} else {
			return BigInteger.valueOf(x).multiply(getFactorial(x - 1));
		}
	}

	/*
	 * The method takes a number as input and finds the sum of its numbers
	 * 
	 */
	private static int findSum(BigInteger number) {
		final String stringFromBigInteger = String.valueOf(number);
		final String charArray = stringFromBigInteger.trim();

		int sum = charArray.chars().map(a -> Character.digit(a, 10)).sum();

		return sum;
	}
}