import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/*
 * Find the number of correct parenthesis expressions
 * 
 * @author Shvec.K.
 * @version 1.0
 */

public class Bracket {

	public static void main(String[] args) {
		final String START_STRING = "()"; // starting state for a set of minimum length
		final String INPUT_MESSAGE = "Input your N: ";
		int minimumLengthBrackets = 1;
		int countBracketsInputUser;

		Set<String> finallySet = new HashSet<>();
		Set<String> tempSet = new HashSet<>();

		try (Scanner scr = new Scanner(System.in)) {
			System.out.print(INPUT_MESSAGE);
			countBracketsInputUser = scr.nextInt();
		}

		finallySet.add(START_STRING);
		while (minimumLengthBrackets < countBracketsInputUser) {
			// find all location options by shifting "()" along the entire length of the
			// option
			for (String str : finallySet) {
				for (int t = 0; t < str.length(); t++) {
					// temporary set stores only unique variants
					tempSet.add(str.substring(0, t) + "()" + str.substring(t, str.length()));
				}
			}

			finallySet = tempSet;
			tempSet = new HashSet<>();
			minimumLengthBrackets++;
		}

		System.out.println("For the entered number N=" + countBracketsInputUser + "-" + finallySet.size());
		for (String bracketExample : finallySet) {
			System.out.println(bracketExample);
		}

	}

}
