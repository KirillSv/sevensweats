package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import entity.User;
import processor.dao.FactoryDao;

@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 9L;
	private FactoryDao factoryDao = FactoryDao.getInstance();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		HttpSession session = request.getSession();
//
//		String userName = request.getParameter("username");
//		String password = request.getParameter("password");
//
//		User user = factoryDao.getUserDao().loginChecker(userName, password);
//
//		session.setAttribute("role", user.getRole());
//	

		request.getRequestDispatcher("/startServlet").forward(request, response);
	}
}
