package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Rabbit;
import processor.dao.FactoryDao;


@WebServlet("/EditRabbitServlet")
public class EditRabbitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("selectionMenu", "editRabbit");
		
		String id = request.getParameter("id");
		
		@SuppressWarnings("unchecked")
		ArrayList<Rabbit> rabbits=(ArrayList<Rabbit>) session.getAttribute("listRabbit");
		
		for (Rabbit rabbit: rabbits) {
			if(rabbit.getId()==Integer.parseInt(id)) {
				session.setAttribute("rabbit", rabbit);
				break;
			}
		}
		
		response.sendRedirect("jsp/start.jsp");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		Rabbit rabbit= (Rabbit) session.getAttribute("rabbit");
        int id = rabbit.getId();
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		String data = request.getParameter("data");
		String count = request.getParameter("count");
		String errorMessage = FactoryDao.getInstance().getRabbitDao().editRabbit(id, name, sex, data, count);
		request.setAttribute("errorMessage", errorMessage);

		response.sendRedirect("ControllerServlet");
	}

}
