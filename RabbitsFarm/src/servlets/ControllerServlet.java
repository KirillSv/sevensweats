package servlets;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import entity.Note;
import entity.Rabbit;
import processor.dao.FactoryDao;
@WebServlet("/ControllerServlet")
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 3L;
	private FactoryDao factoryDao = FactoryDao.getInstance();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		ArrayList<Rabbit> rabbitList = factoryDao.getRabbitDao().readRabbitFromBase();
		ArrayList<Note> noteList = factoryDao.getNoteDao().readNoteFromBase();

		int[] sumRabbit = findSumRabbit(rabbitList);
		double sumFood = findSumFood(rabbitList);
		double sumAddMeat = findSumAddMeat(rabbitList);

		HttpSession session = request.getSession();

		session.setAttribute("role", "ADMIN"); // delete!

		session.setAttribute("listRabbit", rabbitList);
		session.setAttribute("listNote", noteList);
		session.setAttribute("sumAddMeat", sumAddMeat);
		session.setAttribute("sumFood", sumFood);
		session.setAttribute("sumOldRabbit", sumRabbit[0]);
		session.setAttribute("sumLitleRabbit", sumRabbit[1]);

		response.sendRedirect("jsp/start.jsp");
	}
	
	private int[] findSumRabbit(ArrayList<Rabbit> rabbitList) {// result [count old rabbit , count little rabbit]
		int[] sum = { 0, 0 };
		for (Rabbit rabbit : rabbitList) {

			if (rabbit.getAge() > 4) {
				sum[0] += rabbit.getCount();
			} else {
				sum[1] += rabbit.getCount();
			}
		}
		return sum;
	}

	private double findSumFood(ArrayList<Rabbit> rabbitList) {
		double sum = 0;
		for (Rabbit rabbit : rabbitList) {
			if (rabbit.getAge() > 4) {
				sum += (5 * rabbit.getCount());
			}
			if (rabbit.getAge() > 1 | rabbit.getAge() < 4) {
				sum += (2 * rabbit.getCount());
			}

		}
		sum = sum * 0.1;
		return Math.round(sum * 100d) / 100d;
	}

	private double findSumAddMeat(ArrayList<Rabbit> rabbitList) {
		double sum = 0;
		for (Rabbit rabbit : rabbitList) {
			if (rabbit.getAge() < 4) {
				sum += (rabbit.getAge() * rabbit.getCount());
			}
		}
		sum = (sum * 0.4) / 30;
		return Math.round(sum * 100d) / 100d;
	}

}
