package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.Rectangle;
import java.awt.AWTException;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import javax.swing.filechooser.FileSystemView;

@WebServlet("/scrinServlet")
public class scrinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String FILE_PATH = "screenRabbit.jpg";

	protected void doPost(HttpServletRequest request, HttpServletResponse response)

			throws ServletException, IOException {
		try {
			ImageIO.write(grabScreen(), "jpg", new File(getHomeDir(), FILE_PATH));
		} catch (IOException e) {
			System.out.println("IO exception" + e);
		}

		response.sendRedirect("ControllerServlet");
	}

	private static File getHomeDir() {
		FileSystemView fsv = FileSystemView.getFileSystemView();
		return fsv.getHomeDirectory();
	}

	private static BufferedImage grabScreen() {
		try {
			return new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		} catch (SecurityException e) {
		} catch (AWTException e) {
		}
		return null;
	}

}
