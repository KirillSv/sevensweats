package servlets;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import entity.Rabbit;
import processor.dao.FactoryDao;

@WebServlet("/personDiaryServlet")
public class personDiaryServlet extends HttpServlet {
	private static final long serialVersionUID = 8L;
	private FactoryDao factoryDao = FactoryDao.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("selectionMenu", "personDiary");
		String id = request.getParameter("id");
		String listDataRabbit = null;
        String name="��� ������ ������� � ����";
        
		@SuppressWarnings("unchecked")
		ArrayList<Rabbit> listRabbit = (ArrayList<Rabbit>) session.getAttribute("listRabbit");

		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getId()==Integer.valueOf(id)) {
				 name=rabbit.getName();
				listDataRabbit = rabbit.getPersonDiary();
				break;
			}
		}
		
		String[] listDataRabbitArr = listDataRabbit.split("#");

		session.setAttribute("name", name);
		session.setAttribute("listDataRabbit", listDataRabbitArr);

		response.sendRedirect("ControllerServlet");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("GUEST")) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

		}
		String newMessage = request.getParameter("text");
		String name = (String) session.getAttribute("name");
		factoryDao.getRabbitDao().addMessageInPersonDiary(name, newMessage);

		//���� ���� ��� ���� ���� ������ ���� ����������� ��� ���������� ��������
		
		ArrayList<Rabbit> listRabbit = factoryDao.getRabbitDao().readRabbitFromBase();

		String listDataRabbit="";
		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getName().equals(name)) {
				listDataRabbit = rabbit.getPersonDiary();
				String[] listDataRabbitArr = listDataRabbit.split("#");
				session.setAttribute("listDataRabbit", listDataRabbitArr);
				break;
			}
		}
		//*
		
		response.sendRedirect("ControllerServlet");
	}

}
