package entity;

import java.io.Serializable;

public class User implements Serializable{

	private static final long serialVersionUID = -4358258769190744328L;
	private int id;
	private String login;
	private String password;
	private String role;

	public User(int id, String login, String password, String role) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.role = role;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	 @Override
	    public boolean equals(Object obj) {
	    if (obj == this) {
	        return true;
	    }
	    if (obj == null || obj.getClass() != this.getClass()) {
	        return false;
	    }

	    User user = (User) obj;
	    return login.equals(user.login)
	        && (login.equals(user.login)
	            || (login != null && login.equals(user.getLogin())))&& (password.equals(user.password)
	            || (password != null && password.equals(user.getPassword())
	            ));
	    }
	 
	@Override
	public String toString() {
		return "User [id=" + id + " login=" + login + ", role=" + role + ", password=" + password + "]";
	}

}
