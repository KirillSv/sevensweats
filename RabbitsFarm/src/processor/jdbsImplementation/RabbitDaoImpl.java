package processor.jdbsImplementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entity.Rabbit;
import processor.ConnectionPool.ConnectionPool;
import processor.dao.RabbitDao;

public class RabbitDaoImpl implements RabbitDao {

	private final String READ_RABBIT = "SELECT id, name, sex, date_of_birth, count, person_diary FROM rabbitbase.rabbit_table";
    private final String UPDATE_RABBIT_DIARY_BY_NAME= "UPDATE rabbit_table SET person_diary=? WHERE  name=?";
	private final String ADD_RABBIT = "INSERT rabbitbase.rabbit_table (name, sex, date_of_birth, count, person_diary) VALUE (?,?,?,?,'0')";
    private final String DEL_RABBIT_BY_NAME= "DELETE FROM rabbitbase.rabbit_table WHERE name=?";
	private final String EDIT_RABBIT_BY_ID= "UPDATE rabbit_table SET name=?, sex=?, date_of_birth=?, count=? WHERE id=?";
	
    @Override
	public ArrayList<Rabbit> readRabbitFromBase() {
		ArrayList<Rabbit> listRabbit = new ArrayList<Rabbit>();
		try {
			Connection con = null;
			ResultSet rs = null;
			PreparedStatement ps = null;
			try {
				con = ConnectionPool.getInstance().getConnection();
				ps = con.prepareStatement(READ_RABBIT);
				rs = ps.executeQuery();
				while (rs.next()) {
					String sex;
					int intSex = Integer.valueOf(rs.getString(3));
					switch (intSex) {
					case (1):
						sex = "Самка";
						break;
					case (3):
						sex = "Выводок";
						break;
					default:
						sex = "Самец";
					}
					listRabbit.add(new Rabbit(rs.getInt(1), rs.getString(2), sex, rs.getDate(4), rs.getInt(5),
							rs.getString(6)));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return listRabbit;
	}

    @Override
	public String addRabbit(String name, String sex, String dateOfBirth, int count) {		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = con.prepareStatement(ADD_RABBIT);
			ps.setString(1, name);
			ps.setString(2, sex);
			ps.setString(3, dateOfBirth);
			ps.setInt(4, count);
			ps.executeUpdate();
		} catch (SQLException error) {
			error.printStackTrace();
			return "Error when was  record from base SQL";
		} finally {
			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		}
		return "Rabbit succesfull added to base";
	}

    @Override
	public String addMessageInPersonDiary(String nameRabbit, String newMessage) {
		String oldMessage=null;
		
		ArrayList<Rabbit> listRabbit = readRabbitFromBase();
		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getName().equals(nameRabbit)) {
				oldMessage=rabbit.getPersonDiary();
			}
		}
		oldMessage=oldMessage + "#" + newMessage;
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = con.prepareStatement(UPDATE_RABBIT_DIARY_BY_NAME);
			ps.setString(1, oldMessage);
			ps.setString(2, nameRabbit);
			ps.executeUpdate();
		} catch (SQLException error) {
			error.printStackTrace();
			return "Error connection SQL";
		} finally {
			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		}
		return "Everything OK";
	}
	
    @Override
	public String deleteRabbit(String nameRabbit) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = con.prepareStatement(DEL_RABBIT_BY_NAME);
			ps.setString(1, nameRabbit);
			ps.executeUpdate();
		} catch (SQLException error) {
			error.printStackTrace();
			return "Error connection SQL";
		} finally {
			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		}
		return "Everything OK";
	}

	@Override                
	public String editRabbit(int id, String name, String sex, String newDate, String count) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = con.prepareStatement(EDIT_RABBIT_BY_ID);
			ps.setString(1, name);
			ps.setString(2, sex);
			ps.setString(3, newDate);
			ps.setString(4, count);
			ps.setInt(5, id);
			ps.executeUpdate();
		} catch (SQLException error) {
			error.printStackTrace();
			return "Error connection SQL";
		} finally {
			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		}
		return "Everything OK";
	}

}
