package processor.jdbsImplementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entity.User;
import processor.ConnectionPool.ConnectionPool;
import processor.dao.UserDao;

public class UserDaoImpl implements UserDao {

	private final String SELECT_USER_BY_LOGIN = "Select * from rabbitbase.access_table where login=? and password=?";

	@Override
	public User loginChecker(String username, String password) {
		User user = null;
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = con.prepareStatement(SELECT_USER_BY_LOGIN);
			ps.setString(1, username);
			ps.setString(2, password);
			rs = ps.executeQuery();
			while (rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ConnectionPool.close(rs);
		ConnectionPool.close(ps);
		ConnectionPool.close(con);
		if (user == null) {
			user = new User(0, "0", "0", "GUEST");
		}
		return user;
	}

}
