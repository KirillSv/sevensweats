package processor.jdbsImplementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import entity.Note;
import processor.ConnectionPool.ConnectionPool;
import processor.dao.NoteDao;

public class NoteDaoImpl implements NoteDao {

	private final String DELETE_NOTE_BY_ID = "DELETE FROM rabbitbase.note_table WHERE id=?";
	private final String ADD_NOTE = "INSERT rabbitbase.note_table (name, text, date_note) VALUE ((SELECT id FROM rabbit_table WHERE name=?),?,?)";
    private final String READ_ALL_NOTE= "SELECT n.id, r.name, text, date_note FROM note_table n JOIN rabbit_table r ON n.name=r.id";
	private final String EDIT_NOTE_BY_ID= "UPDATE note_table SET text=?, date_note=? WHERE id=?";
    @Override
	public String deleteNote(String numberNote) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = con.prepareStatement(DELETE_NOTE_BY_ID);
			ps.setString(1, numberNote);
			ps.executeUpdate();
		} catch (SQLException error) {
			error.printStackTrace();
			return "Error connection SQL";
		} finally {
			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		}
		return "Everything OK";
	}

    @Override
	public String addNote(String name, String text, String dateOfNote) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = con.prepareStatement(ADD_NOTE);
			ps.setString(1, name);
			ps.setString(2, text);
			ps.setString(3, dateOfNote);
			ps.executeUpdate();
		} catch (SQLException error) {
			error.printStackTrace();
			return "Error connection SQL";
		} finally {
			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		}
		return "Everything OK";
	}

    @Override
	public ArrayList<Note> readNoteFromBase() {
		ArrayList<Note> listNote = new ArrayList<Note>();
		try {
			Connection con = null;
			ResultSet rs = null;
			PreparedStatement ps = null;
			try {
				con = ConnectionPool.getInstance().getConnection();
				ps = con.prepareStatement(READ_ALL_NOTE);
				rs = ps.executeQuery();

				while (rs.next()) {
					listNote.add(new Note(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4)));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		Collections.sort(listNote);
		return listNote;
	}

	@Override
	public String editNote(int id, String text, String data) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = con.prepareStatement(EDIT_NOTE_BY_ID);
			ps.setString(1, text);
			ps.setString(2, data);
			ps.setInt(3, id);
			ps.executeUpdate();
		} catch (SQLException error) {
			error.printStackTrace();
			return "Error connection SQL";
		} finally {
			ConnectionPool.close(rs);
			ConnectionPool.close(ps);
			ConnectionPool.close(con);
		}
		return "Everything OK";
	}

}
