package processor.dao;

import java.util.ArrayList;

import entity.Rabbit;

public interface RabbitDao {

	String addRabbit(String name, String sex, String dateOfBirth, int count);

	String addMessageInPersonDiary(String mothersName, String string);

	ArrayList<Rabbit> readRabbitFromBase();

	String deleteRabbit(String name);

	String editRabbit(int id, String name, String sex, String newDate, String count);

}
