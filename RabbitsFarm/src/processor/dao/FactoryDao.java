package processor.dao;

import processor.jdbsImplementation.NoteDaoImpl;
import processor.jdbsImplementation.RabbitDaoImpl;
import processor.jdbsImplementation.UserDaoImpl;

public class FactoryDao {
	private static FactoryDao factoryDao;

	private FactoryDao() {
	}

	public static FactoryDao getInstance() {
		if (factoryDao == null) {
			factoryDao = new FactoryDao();
		}
		return factoryDao;
	}
	
	public UserDao getUserDao() {
		return new UserDaoImpl();
	}

	public NoteDao getNoteDao() {
		return new NoteDaoImpl();
	}
	
	public RabbitDao getRabbitDao() {
		return new RabbitDaoImpl();
	}
	
}
