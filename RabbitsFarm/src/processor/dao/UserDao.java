package processor.dao;

import entity.User;

public interface UserDao {

	User loginChecker(String username, String password);

}
