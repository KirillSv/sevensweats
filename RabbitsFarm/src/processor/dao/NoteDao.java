package processor.dao;

import java.util.ArrayList;

import entity.Note;

public interface NoteDao {

	String addNote(String name, String string, String dateOfNoteZaboi);

	ArrayList<Note> readNoteFromBase();

	String deleteNote(String parameter);

	String editNote(int id, String text, String data);

}
