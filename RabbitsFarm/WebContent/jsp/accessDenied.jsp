<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Доступ запрещен</title>
</head>
<body>
	<c:import url="start.jsp"></c:import>
	<div class='accessDenied'>
	
	 <h1>Ваш уровень доступа недостаточный для этой операции.</h1>
	 
	 <form action="index.jsp">
			<input type="submit" 
				value=" Изменить уровень доступа " style="width: 400px; " class="button"/>
    </form>
	 	
	</div>
</body>
</html>