<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/jstl/imageInTable.tld" prefix="xx"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Ферма "Веселый кролик"</title>
<c:import url="style.jsp"></c:import>

<script type="text/javascript">
<%@include file="../JS/script.js"%>
</script>

</head>

<body>

	<%-- Footer --%>
	<%@ include file="/jspf/header.jspf"%>
	<%-- Footer --%>

    <%-- Menu  --%>
	<%@ include file="/jspf/menu.jspf"%>
	<%--  Menu --%>
	
    <%-- Menu & 2 tables  --%>
	<%@ include file="/jspf/twoTables.jspf"%>
	<%--  Menu & 2 tables --%>
	
	 ${sessionScope.selectionMenu}
	 <c:if test="${not empty sessionScope.selectionMenu}">    
	
	 <c:if test="${sessionScope.selectionMenu eq 'addNote'}">
		<%@ include file="/jspf/addNoteMenu.jspf"%>
	</c:if>
		
		 <c:if test="${sessionScope.selectionMenu eq 'addRabbit'}">
		<%@ include file="/jspf/addRabbitMenu.jspf"%>
	</c:if>
	
	 <c:if test="${sessionScope.selectionMenu eq 'addKombo'}">
		<%@ include file="/jspf/komboMenu.jspf"%>
	</c:if>
	
	 <c:if test="${sessionScope.selectionMenu eq 'delRabbit'}">
		<%@ include file="/jspf/delRabbitMenu.jspf"%>
	</c:if>
	
	 <c:if test="${sessionScope.selectionMenu eq 'personDiary'}">
		<%@ include file="/jspf/personDiaryMenu.jspf"%>
	</c:if>
	
	 <c:if test="${sessionScope.selectionMenu eq 'editRabbit'}">
		<%@ include file="/jspf/editRabbitMenu.jspf"%>
	</c:if>
	
	 <c:if test="${sessionScope.selectionMenu eq 'editNote'}">
		<%@ include file="/jspf/editNoteMenu.jspf"%>
	</c:if>
	
	</c:if>    
	
	
</body>
</html>