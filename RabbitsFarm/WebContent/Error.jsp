<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true" %>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Error page</title>
<c:import url="jsp/style.jsp"></c:import>
</head>
<body>
    <center>
        <img src="img/error.jpg" width="260" height="260" class="center-img">
        <h2>По причине :  <%=exception.fillInStackTrace() %><br/> </h2>
    </center>
</body>
</html>