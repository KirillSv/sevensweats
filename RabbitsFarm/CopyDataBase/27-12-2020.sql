CREATE DATABASE  IF NOT EXISTS `rabbitbase` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `rabbitbase`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: rabbitbase
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_table`
--

DROP TABLE IF EXISTS `access_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `access_table` (
  `id` int(11) NOT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_table`
--

LOCK TABLES `access_table` WRITE;
/*!40000 ALTER TABLE `access_table` DISABLE KEYS */;
INSERT INTO `access_table` VALUES (1,'admin','admin','ADMIN'),(2,'1','1','USER'),(3,'0','0','GUEST');
/*!40000 ALTER TABLE `access_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note_table`
--

DROP TABLE IF EXISTS `note_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `note_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `text` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `date_note` date NOT NULL,
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note_table`
--

LOCK TABLES `note_table` WRITE;
/*!40000 ALTER TABLE `note_table` DISABLE KEYS */;
INSERT INTO `note_table` VALUES (30,30,'забой','2019-11-23'),(31,31,'забой','2019-11-23'),(38,32,'забой','2020-02-01'),(50,3,'Роды(последняя попытка)','2019-12-08'),(56,100,'textTest','2019-10-10'),(58,104,'textTest','2019-10-10'),(65,110,'textTest','2019-10-10'),(67,114,'textTest','2019-10-10'),(69,118,'textTest','2019-10-10'),(71,3,'Забой если не родила','2019-12-20'),(86,128,'забой','2020-07-22'),(91,132,'забой','2020-08-04'),(95,136,'забой','2022-05-01'),(96,133,'случка','2022-02-01'),(107,4,'забой если не купили','2020-07-17'),(112,204,'забой','2020-10-13'),(114,205,'забой','2020-10-14'),(120,208,'забой','2020-12-16'),(122,209,'забой','2020-12-19'),(124,210,'забой','2020-12-21'),(126,237,'Забой','2020-12-30'),(128,238,'Забой','2021-01-12'),(131,254,'ZABOI','2021-02-18'),(132,131,'РОДЫ','2021-01-28'),(135,256,'ZABOI','2021-02-22'),(136,2,'РОДЫ','2021-01-27'),(137,258,'Забой','2020-12-30'),(139,270,'ZABOI','2021-03-26'),(140,130,'РОДЫ','2021-01-26'),(141,271,'ZABOI','2021-03-27'),(142,123,'РОДЫ','2021-01-27'),(143,272,'ZABOI','2021-04-05'),(144,1,'SLUCHKA','2021-01-05'),(145,273,'ZABOI','2021-04-10'),(146,239,'SLUCHKA','2021-01-10'),(147,274,'ZABOI','2021-04-27'),(148,131,'SLUCHKA','2021-01-27');
/*!40000 ALTER TABLE `note_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rabbit_table`
--

DROP TABLE IF EXISTS `rabbit_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rabbit_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sex` tinyint(10) DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `count` tinyint(50) NOT NULL,
  `person_diary` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rabbit_table`
--

LOCK TABLES `rabbit_table` WRITE;
/*!40000 ALTER TABLE `rabbit_table` DISABLE KEYS */;
INSERT INTO `rabbit_table` VALUES (1,'Зина',1,'2019-03-07',1,'0#344#03,11 родила 11 штук#18-02 пропустила случку была в отпуске#18,03 случка#20200416родила8штук, вязка по плану:  20200516#20200621 родила Зина6 12 штук, вязка по плану:  20200721#20200830 СЂРѕРґРёР»Р° Зина7 5 С€С‚СѓРє, РІСЏР·РєР° РїРѕ РїР»Р°РЅСѓ:  20200930#20201031 RODILA Зина8 8 SLEDUYUSHA VYAZKA PO PLANU:  20201131#выводок кто-то сожрал. Свели 03,11,2020#20201205 RODILA Зина9 8 SLEDUYUSHA VYAZKA PO PLANU:  20210105'),(2,'Зоя',1,'2019-03-07',1,'0#свели 2 го ноября#прохолостила.#сводили с 15 по 22го декабря#родила 22,03#свели 01,05 с Паддишах#20200601родилаЗоя5 10штук, вязка по плану:  20200701#20200819 родила Зоя6 8 штук, вязка по плану:  20200919#20201020 RODILA  8 SLEDUYUSHA VYAZKA PO PLANU:  20201120#20201022 RODILA Зоя7 8 SLEDUYUSHA VYAZKA PO PLANU:  20201122#25/12/2020 родила крольчат все поогибли'),(123,'Альфа',1,'2019-09-09',1,'0#дочка Зины#01.01.2020 свели с папой#04,01- точно свели.#04,02- родила#0404 - родила 6шт#20200622 родила Альфовцы3 7 штук, вязка по плану:  20200722#МЕСЯЦ КАНИКУЛЫ -август#20200912 СЂРѕРґРёР»Р° Альфавцы4 10 С€С‚СѓРє, РІСЏР·РєР° РїРѕ РїР»Р°РЅСѓ:  20201012#20201127 RODILA АльфаВыводок 9 SLEDUYUSHA VYAZKA PO PLANU:  20201227'),(130,'Афина',1,'2020-01-01',1,'0#20200614 родила Афина1 9 штук, вязка по плану:  20200714#20200821 родила Афинавцы2 7 штук, вязка по плану:  20200921#20201126 RODILA Афинины1 8 SLEDUYUSHA VYAZKA PO PLANU:  20201226'),(131,'Люция',1,'2020-01-01',1,'0#свели 13-мая -2020#20200613родилаЛюция-1 5штук, вязка по плану:  20200713#20200816 родила Люция1-3шт 3 штук, вязка по плану:  20200916#20201018 RODILA Люция1 8 SLEDUYUSHA VYAZKA PO PLANU:  20201118#20201227 RODILA Люция2 8 SLEDUYUSHA VYAZKA PO PLANU:  20210127'),(209,'Зоя6',3,'2020-08-19',8,'0'),(210,'Афинавцы2',3,'2020-08-21',7,'0'),(238,'Альфавцы4',3,'2020-09-12',10,'0'),(239,'Лили',1,'2020-06-01',1,'0#20201210 RODILA Лили 1 8 SLEDUYUSHA VYAZKA PO PLANU:  20210110'),(254,'Люция1',3,'2020-10-18',8,'0'),(257,'Зина8',3,'2020-10-31',8,'0'),(270,'Афинины1',3,'2020-11-26',8,'0'),(271,'АльфаВыводок',3,'2020-11-27',9,'0'),(272,'Зина9',3,'2020-12-05',8,'0'),(273,'Лили 1',3,'2020-12-10',8,'0'),(274,'Люция2',3,'2020-12-27',8,'0');
/*!40000 ALTER TABLE `rabbit_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'rabbitbase'
--

--
-- Dumping routines for database 'rabbitbase'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-27 22:16:45
