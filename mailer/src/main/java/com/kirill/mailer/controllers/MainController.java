package com.kirill.mailer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    @Autowired
    public JavaMailSender emailSender;
    
	@GetMapping("/")
	public String mainPage(Model model) {
		return "index";
	}
	
	@GetMapping("/send")
	public String sendMail(@RequestParam String title, @RequestParam String address, @RequestParam String full_text, Model model) {
		
		try {  	 
	        SimpleMailMessage message = new SimpleMailMessage();
	        message.setTo(address);
	        message.setSubject(title);
	        message.setText(full_text);
	        this.emailSender.send(message);
		} catch (Exception ex) {
			model.addAttribute("errorMessage",  ex.fillInStackTrace());
			return "index";
		}
		
	    model.addAttribute("errorMessage",  "Email Send!");
		
		return "index";
	}
	
}
