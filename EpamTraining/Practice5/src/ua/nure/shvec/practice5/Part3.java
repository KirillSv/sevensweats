package ua.nure.shvec.practice5;

public class Part3 {
	private int counter;
	private int counter2;
	private int countRepeat;
	private int timeSleep;

	private Thread[] tr;

	public Part3(int countThreads, int countRepeat, int timeSleep) {
		this.tr = new Thread[countThreads];
		this.countRepeat = countRepeat;
		this.timeSleep = timeSleep;
	}

	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}

	public int getCounter2() {
		return counter2;
	}

	public void setCounter2(int counter2) {
		this.counter2 = counter2;
	}

	
	public static void main(String[] args) {
		Part3 p = new Part3(3, 5, 100);
		
		p.test();
		
		p.reset();
		
		p.testSync();
		
	}

	private void testSync() {
		for (int t = 0; t < tr.length; t++) {
			tr[t] = new ThreadImp2();
			tr[t].start();
		}
		for (int t = 0; t < tr.length; t++) {
			try {
				tr[t].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void test() {
		for (int t = 0; t < tr.length; t++) {
			tr[t] = new ThreadImp();
			tr[t].start();

		}
		for (int t = 0; t < tr.length; t++) {

			try {
				tr[t].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

	}

	public void reset() {
		setCounter(0);
		setCounter2(0);
	}

	public void printResult() {
		System.out.println(getCounter() + " " + getCounter2());
		setCounter(getCounter() + 1);
		try {
			Thread.sleep(timeSleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setCounter2(getCounter2() + 1);

	}

	public class ThreadImp extends Thread {
		@Override
		public void run() {
			for (int t = 0; t < Part3.this.countRepeat; t++) {
				printResult();
			}
		}
	}

	public class ThreadImp2 extends Thread {
		@Override
		public void run() {
			synchronized (Part3.this) {
				for (int t = 0; t < Part3.this.countRepeat; t++) {
					printResult();
				}
			}

		}
	}

}
