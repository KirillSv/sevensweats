package ua.nure.shvec.practice5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.Scanner;

public class Part5 {

	private static final String FILENAME = "part5.txt";
	private static final String ENCODING = "Cp1251";
	private static final int COUNT = 20;
	private int countThread;

	public Part5(int countThread) {
		this.countThread = countThread;
	}

	public int getCountThread() {
		return countThread;
	}

	public void setCountThread(int countThread) {
		this.countThread = countThread;
	}

	public static void main(String[] args) {
		Part5 part5 = new Part5(10);
		fileMaker();
		part5.fileWriter();
		consoleOutputContentFile();

	}

	public static void fileMaker() {
		try {
			Files.deleteIfExists(new File(FILENAME).toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		File file = new File(FILENAME);

		if (!file.exists()) {

			try {
				if (file.createNewFile()) {
					System.out.println("File created");
				} else {
					System.out.println("File already exists");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public void fileWriter() {
		Thread[] arrThreads = new Thread[getCountThread()];

		for (int i = 0; i < getCountThread(); i++) {
			final int nString = i;

			arrThreads[i] = new Thread() {
				@Override
				public void run() {
					try {
						writerFromThread(nString);
					} catch (IOException e) {
						System.err.print("Write in file- ERROR");
						e.printStackTrace();
					}
				}
			};
			arrThreads[i].start();
		}

		try {
			for (Thread thread : arrThreads) {
				if (thread != null) {
					thread.join();

				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	synchronized void writerFromThread(int nString) throws IOException {

		try (RandomAccessFile file = new RandomAccessFile(FILENAME, "rw")) {
			int position = nString * (COUNT + System.lineSeparator().length());

			file.seek(position);
			String st;
			for (int i = 0; i < COUNT; i++) {
				st = String.valueOf(nString);

				file.write(st.getBytes(ENCODING));
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					System.err.print("thread interrupted");
					e.printStackTrace();
				}
			}
			st = System.lineSeparator();
			file.write(st.getBytes(ENCODING));

		}
	}

	public static void consoleOutputContentFile() {
		try {
			Scanner in = new Scanner(new File(FILENAME));

			while (in.hasNextLine()) {
				System.out.println(in.nextLine());
			}
			in.close();
		} catch (FileNotFoundException e) {
			System.err.print("file not found for read");
			e.printStackTrace();
		}
	}

}
