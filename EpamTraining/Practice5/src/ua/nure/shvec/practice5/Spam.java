package ua.nure.shvec.practice5;

import java.awt.event.KeyEvent;
import java.io.IOException;

public class Spam {
	private Thread[] threads;

	public Spam(String[] reports, int[] times) {
		if (reports.length == times.length) {
			threads = new Thread[reports.length];
			for (int t = 0; t < reports.length; t++) {
				threads[t] = new Worker(reports[t], times[t]);
			}
		}
	}

	public void start() {
		for (int t = 0; t < threads.length; t++) {
			threads[t].start();
		}

	}

	public void stop() {
		for (int t = 0; t < threads.length; t++) {
			threads[t].interrupt();
		}
	}

	private static class Worker extends Thread {
		private String report;
		private int time;

		public Worker(String report, int time) {
			this.report = report;
			this.time = time;
		}

		@Override
		public void run() {
			while (true) {
				System.out.println(report);
				try {
					Worker.sleep(time);
				} catch (InterruptedException e) {
					break;
				}
			}
		}
	}

	public static void main(String[] args) {
		String[] reports = new String[] { "@@@", "bbbbbbb" };
		int[] times = new int[] { 333, 222 };
		Spam spam = new Spam(reports, times);
		spam.start();
		while (true) {
			try {
				if (System.in.read() == KeyEvent.VK_ENTER) {
					spam.stop();
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
