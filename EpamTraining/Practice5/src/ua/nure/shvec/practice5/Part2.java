package ua.nure.shvec.practice5;

import java.io.ByteArrayInputStream;

public class Part2 {

	public static void main(String[] args) {

		ByteArrayInputStream myStream = new ByteArrayInputStream(System.lineSeparator().getBytes());
		System.setIn(myStream);

		Thread thread = new Thread() {
			@Override
			public void run() {
				Spam.main(args);
			}
		};
		
		thread.start();
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.setIn(System.in);

	}

}
