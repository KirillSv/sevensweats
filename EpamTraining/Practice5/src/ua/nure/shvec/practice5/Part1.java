package ua.nure.shvec.practice5;

public class Part1 {

	public static void main(String[] arg) {
		Thread threadextend = new Threadextend();
		threadextend.start();

		try {
			threadextend.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Thread threadRunnable = new Thread(new ThreadRunnable());
		threadRunnable.start();

		try {
			threadRunnable.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Thread.currentThread();
	}

	static class ThreadRunnable implements Runnable {

		@Override
		public void run() {
			for (int i = 0; i < 3; i++) {
				System.out.println(Thread.currentThread().getName());
				try {
					Thread.sleep(333);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}

	}

	static class Threadextend extends Thread {

		@Override
		public void run() {
			for (int i = 0; i < 3; i++) {
				System.out.println(Thread.currentThread().getName());
				try {
					Thread.sleep(333);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}
	}

}
