package ua.nure.shvec.practice5;

public class Part6 {

	private static final Object M = new Object();

	private static boolean waiter = true;

	public static void main(String[] args) {
		Thread t = new Thread() {
			@Override
			public void run() {
				synchronized (M) {

					while (waiter) {
						try {
							M.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					Thread.currentThread().interrupt();

				}
			}
		};

		t.start();

		synchronized (M) {
			M.notifyAll();
			while (t.getState() != Thread.State.BLOCKED) {
				waiter = true;
			}
			System.out.println(t.getState());

			try {
				while (t.getState() != Thread.State.WAITING) {
					M.wait(1);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.out.println(t.getState());
			M.notifyAll();
		}

		waiter = false;
		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(t.getState());
		waiter = true;
	}
}