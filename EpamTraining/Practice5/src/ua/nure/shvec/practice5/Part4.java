package ua.nure.shvec.practice5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Part4 {
	private static int x;
	private static int y;
	private static final String FILENAME = "part4.txt";

	public static void main(String[] args) {
		String[] arrayStrings;
		try {
			arrayStrings = readArrayStringFromFile();

			x = (arrayStrings[0].split(" ")).length;
			y = arrayStrings.length;

			int[][] matrix = new int[y][x];

			for (int i = 0; i < y; i++) {
				int[] matrixOneString = fromEveryStringtoIntArray(arrayStrings[i]);

				for (int t = 0; t < x; t++) {
					matrix[i][t] = matrixOneString[t];
				}

			}

			long startTime = System.currentTimeMillis();
			System.out.println(findMaxThreads(matrix));
			System.out.println(System.currentTimeMillis() - startTime);

			long start = System.currentTimeMillis();
			System.out.println(findMaxWithoutThreads(matrix));
			System.out.println(System.currentTimeMillis() - start);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static int findMaxThreads(int[][] array) {

		int[] returnedMaxValues = new int[y];
		Thread[] allThreads = new Thread[y];

		for (int i = 0; i < y; i++) {
			final int threadId = i;

			allThreads[i] = new Thread() {
				@Override
				public void run() {
					returnedMaxValues[threadId] = findMaximum(array[threadId]);
				}
			};

			allThreads[i].start();
		}

		try {
			for (Thread thread : allThreads) {
				if (thread != null) {
					thread.join();

				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return findMaximum(returnedMaxValues);
	}

	public static int findMaxWithoutThreads(int[][] matrix) {
		int max = 0;

		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (matrix[i][j] > max) {
					max = matrix[i][j];
				}
			}
		}
		return max;
	}

	public static int[] fromEveryStringtoIntArray(String array) {
		String[] fromInt = array.split(" ");
		int[] intArray = new int[fromInt.length];
		for (int i = 0; i < fromInt.length; i++) {
			intArray[i] = Integer.parseInt(fromInt[i]);
		}
		return intArray;
	}

	public static String[] readArrayStringFromFile() throws IOException {

		File file = new File(FILENAME);
		FileReader fr = new FileReader(file);
		try (BufferedReader reader = new BufferedReader(fr)) {

			List<String> list = new ArrayList<>();

			String line;
			while ((line = reader.readLine()) != null) {
				list.add(line);
			}
			
			return list.toArray(new String[list.size()]);
		}

	}

	public static int findMaximum(int[] arr) {
		int max = 0;
		for (int i = 0; i < arr.length; i++) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (arr[i] > max) {
				max = arr[i];
			}
		}
		return max;
	}
}