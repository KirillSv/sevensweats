package ua.nure.shvec.practice5;

import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;

public class MatrixMakerForPart4 {

	public static void main(String[] args) {
		maker(4, 100, "part4.txt");
	}

	private static void maker(int x, int y, String fileName) {

		StringBuilder finalSbl = new StringBuilder();
		String str;
		SecureRandom rand = new SecureRandom();
		final int max = 111;
		final int min = 888;
		
		for (int i = 0; i < x; i++) {
			StringBuilder sbl = new StringBuilder();
			int digit= rand.nextInt(max);
			for (int j = 0; j < y; j++) {
				
				digit= rand.nextInt(max);

				while (digit < min) {

					digit = rand.nextInt(max);
				}
				sbl.append(rand + " ");
			}
			str = sbl.toString().trim();
			finalSbl.append(str + System.lineSeparator());
		}

		try (FileWriter writer = new FileWriter(fileName, false)) {

			writer.write(finalSbl.toString());
			writer.flush();

		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}

	}
}
