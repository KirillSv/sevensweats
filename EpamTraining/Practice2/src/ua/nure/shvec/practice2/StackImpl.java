package ua.nure.shvec.practice2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class StackImpl implements Stack {

	private Node top;
	private int size;
	private Object[] newArr = {};

	public StackImpl() {
		top = null;
		size = 0;
	}

	public StackImpl(int n) {
		this.size = n;
		newArr = new Object[size];
	}

	@Override
	public void clear() {
		newArr = new Object[] {};
		size = 0;

	}

	@Override
	public int size() {
		return toArray().length;
	}

	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();
	}

	public Object[] toArray() {
		return this.newArr.clone();

	}

	@Override
	public String toString() {
		StringBuilder sbl = new StringBuilder();
		sbl.append("[");
		for (int i = 0; i < toArray().length; i++) {
			if (toArray()[i] == null) {
				sbl.append("");
				continue;
			}
			sbl.append("" + toArray()[i].toString() + "");
			if (i != size() - 1) {
				sbl.append(", ");
			}
		}
		sbl.append("]");
		return sbl.toString();

	}

	@Override
	public void push(Object element) {
		Object[] temp = new Object[newArr.length + 1];
		System.arraycopy(newArr, 0, temp, 0, newArr.length);
		temp[temp.length - 1] = element;
		newArr = temp;

		Node node = new Node();
		node.setElement(element);
		node.setNext(top);
		top = node;
		size++;

	}

	@Override
	public Object pop() {
		if (isEmpty()) {
			throw new NoSuchElementException("Error next element");
		}
		Object obj = top.getElement();
		top = top.getNext();
		size--;
		return obj;
	}

	public boolean isEmpty() {
		return (top == null);
	}

	@Override
	public Object top() {
		if (isEmpty()) {
			throw new NoSuchElementException("Error such element ");
		}
		return top.getElement();
	}

	private class IteratorImpl implements Iterator<Object> {
		private boolean nextOrPrevious;
		private boolean marker = true;
		private int pointer = toArray().length;

		public boolean isMarker() {
			return marker;
		}

		public boolean isNextOrPrevious() {
			return nextOrPrevious;
		}

		public void setNextOrPrevious(boolean nextOrPrevious) {
			this.nextOrPrevious = nextOrPrevious;
		}

		public boolean hasNext() {
			return (pointer + 1 >= toArray().length - 1);

		}

		@Override
		public void remove() {
			if (isMarker()) {
				throw new IllegalStateException();
			}
			int x = 0;
			if (!isNextOrPrevious()) {
				x = pointer--;

			} else {
				x = ++pointer;
			}
			Object[] temp = new Object[newArr.length - 1];
			System.arraycopy(newArr, 0, temp, 0, x);
			System.arraycopy(newArr, x + 1, temp, x, temp.length - x);
			newArr = temp;
			marker = true;
			++pointer;
			if (isNextOrPrevious()) {
				--pointer;
			}
		}
		
		public Object next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			marker = false;
			nextOrPrevious = false;
			return toArray()[--pointer];
		}

	}

	private static class Node {

		private Node next;
		private Object element;
		
		public Node() {
		}

		public Node(Object element, Node next) {
			this.next = next;
			this.element = element;
		}

		public Node getNext() {
			return next;
		}

		public void setElement(Object element) {
			this.element = element;
		}

		public void setNext(Node next) {
			this.next = next;
		}

		public Object getElement() {
			return element;
		}

	}

	public static void main(String[] args) {

		Stack stack = new StackImpl();
		stack.push("10");
		stack.push("20");
		stack.push("30");
		System.out.println(stack);

		Iterator<Object> iter = stack.iterator();

		System.out.println(iter.next());
		iter.remove();
		System.out.println(iter.next());
		iter.remove();

		System.out.println(stack);

		stack.pop();
		stack.top();
		stack.clear();
		System.out.println(stack.size());
		System.out.println(stack.toString());

		System.out.println("\n show " + stack);

	}
}
