package ua.nure.shvec.practice2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListImpl implements List {

	private Node first;
	private Node end;
	private int size;
	private Object[] newArr = {};

	public ListImpl() {
		this.first = this.end = null;
		this.size = 0;
	}

	public Node setLast(Node end) {
		this.end = end;
		return end;
	}

	@Override
	public void clear() {
		newArr = new Object[] {};
		this.size = 0;
	}

	@Override
	public void addFirst(Object element) {
		Object[] temp = new Object[newArr.length + 1];
		System.arraycopy(newArr, 0, temp, 1, size);
		temp[0] = element;
		newArr = temp;
		size++;
	}

	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();
	}

	@Override
	public void addLast(Object element) {
		Object[] temp = new Object[newArr.length + 1];
		System.arraycopy(newArr, 0, temp, 0, newArr.length);
		temp[temp.length - 1] = element;
		newArr = temp;
		size++;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void removeFirst() {
		if (this.first == null) {
			this.first = this.end = null;
		}
		removed(0);
	}

	@Override
	public void removeLast() {
		int count = newArr.length - 1;
		removed(count);

	}

	public void removed(int index) {
		Object[] temp = new Object[newArr.length - 1];
		System.arraycopy(newArr, 0, temp, 0, index);
		System.arraycopy(newArr, index + 1, temp, index, temp.length - index);
		newArr = temp;
		size--;
	}

	@Override
	public Object getFirst() {
		return newArr[0];

	}

	public void isEmpty() {
		if (this.first != null) {
			System.out.println("arr no empty");
		} else {
			System.out.println("arr empty");
		}
	}

	@Override
	public Object getLast() {
		return newArr[newArr.length - 1];

	}

	@Override
	public Object search(Object element) {
		Object result = "";
		for (int index = 0; index < newArr.length; index++) {
			if (newArr[index] == element) {
				result = newArr[index];
			}
		}
		return result;
	}

	public boolean remove(Object element) {
		Object[] temp = new Object[newArr.length - 1];
		System.arraycopy(newArr, 0, temp, 0, 0);
		System.arraycopy(newArr, 0 + 1, temp, 0, temp.length - 0);
		newArr = temp;
		return true;
	}

	public String toString() {
		Iterator<Object> iter = iterator();
		if (!iter.hasNext()) {
			return "[]";
		}
		StringBuilder sbl = new StringBuilder();
		sbl.append('[');
		for (;;) {
			Object e = iter.next();
			sbl.append(e == this ? "(this List)" : e);
			if (!iter.hasNext()) {
				return sbl.append(']').toString();
			}
			sbl.append(", ");
		}
	}

	public Object[] toArray() {
		return this.newArr.clone();
	}

	private static class Node {
		private Object element;
		private Node next;
		private Node prev;

		public Node(Object element) {
			this.element = element;
			this.next = null;
		}

		public Node(Object element, Node next) {
			this.next = next;
			this.element = element;
		}

		public void setNext(Node next) {
			this.next = next;
		}

		public void setData(Object element) {
			this.element = element;
		}

		public Object getData() {
			return this.element;
		}

		public Node getNext() {
			return this.next;
		}

		public Node getPrev() {
			return prev;
		}

		public void setPrev(Node prev) {
			this.prev = prev;
		}
	}

	private class IteratorImpl implements Iterator<Object> {

		private boolean marker = true;
		private boolean nextOrPrevious;
		private int pointer = -1;

		public boolean isMarker() {
			return marker;
		}

		public void setFlag(boolean marker) {
			this.marker = marker;
		}

		public int getPointer() {
			return pointer;
		}

		public boolean isNextOrPrevious() {
			return nextOrPrevious;
		}

		public void setPointer(int pointer) {
			this.pointer = pointer;
		}

		public boolean hasNext() {
			return (pointer < toArray().length - 1);
		}

		public void setNextOrPrevious(boolean nextOrPrevious) {
			this.nextOrPrevious = nextOrPrevious;
		}

		public Object next() {
			if (pointer >= size()) {
				throw new NoSuchElementException("Error: no next item");
			}

			marker = false;
			nextOrPrevious = false;
			return toArray()[++pointer];
		}

		@Override
		public void remove() {
			if (isMarker()) {
				throw new IllegalStateException();
			}
			int i = 0;
			if (!isNextOrPrevious()) {
				i = pointer--;
			} else {
				i = ++pointer;
			}

			Object[] temp = new Object[newArr.length - 1];
			System.arraycopy(newArr, 0, temp, 0, i);
			System.arraycopy(newArr, i + 1, temp, i, temp.length - i);
			newArr = temp;

			marker = true;
			if (isNextOrPrevious()) {
				--pointer;
			}
		}
	}

	public static void main(String[] args) {

		List list = new ListImpl();
		list.addLast("10");
		list.addLast("20");
		list.addLast("30");

		System.out.println(list.size());
		System.out.println(list);

		list.clear();
		System.out.println(list);
		System.out.println(list.size());

		list.addLast("10");
		list.addLast("20");
		list.addLast("30");

		System.out.println(list.size());
		System.out.println(list);
		list.addFirst("10");
		list.addLast("20");
		list.addFirst("30");

		System.out.println(list);
		System.out.print(list.getLast());
		System.out.print(list.getFirst());

		list.addLast("1");
		list.addLast("2");
		list.addLast("3");

		System.out.println(list);

		System.out.println("\nIterator:");
		Iterator<Object> it = list.iterator();
		System.out.println(it.next());
		it.remove();
		System.out.println(it.next());
		it.remove();
		System.out.println(it.next());
		it.remove();
		System.out.println(list);

		System.out.println(list);
		System.out.println(list.getLast());
		System.out.println(list.search(1));
		System.out.println(list.search(2));
		System.out.println(list);

		list.addLast(1);
		list.addLast(2);

		list.remove(2);
		System.out.println(list);

	}

}