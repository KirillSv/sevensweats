package ua.nure.shvec.practice2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class QueueImpl implements Queue {

	private Object[] newArr = new Object[3];
	private int head;
	private int tail;
	private Node top;
	private Node last;
	private int size;

	public QueueImpl() {
		this.last = null;
		this.size = 0;
	}

	public QueueImpl(int capac) {
		newArr = new Object[capac];
	}

	@Override
	public String toString() {
		Iterator<Object> iter = iterator();
		StringBuilder sbl = new StringBuilder();

		if (!iter.hasNext()) {
			return "[]";
		}

		sbl.append('[');
		for (;;) {
			Object ob = iter.next();
			if (ob == null) {
				return "[]";
			}
			sbl.append(ob == this ? "(this Collection)" : ob);
			if (!iter.hasNext()) {
				return sbl.append(']').toString();
			}
			sbl.append(", ");
		}
	}

	private static class Node {
		private Object element;
		private Node next;
		private Node prev;

		public Node() {
			// constructor
		}

		public Node getPrev() {
			return prev;
		}

		public void setPrev(Node prev) {
			this.prev = prev;
		}

		public Object getElement() {
			return element;
		}

		public void setElement(Object element) {
			this.element = element;
		}

	}

	@Override
	public void enqueue(Object element) {
		Node oldlast = last;
		last = new Node();
		newArr[tail] = element;
		tail = (tail + 1) % newArr.length;
		last.next = null;
		if (!isEmpty()) {
			oldlast.next = last;
		}
		size++;
	}

	public Node getTop() {
		return top;
	}

	@Override
	public void clear() {
		for (int i = 0; i < newArr.length; i++) {
			newArr[i] = null;
			size = 0;
		}
	}

	@Override
	public int size() {
		return size;

	}

	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();
	}

	@Override
	public Object dequeue() {
		if (size == 0) {
			throw new NoSuchElementException("Queue is empty");
		}
		Object item = newArr[head];
		newArr[head] = null;
		head = (head + 1) % newArr.length;
		size--;
		return item;
	}

	@Override
	public Object top() {
		if (size == 0) {
			throw new NoSuchElementException("Queue is empty");
		}
		return newArr[0];

	}

	public boolean isEmpty() {
		return size == 0;
	}

	public Object[] toArray() {
		return this.newArr.clone();

	}

	private class IteratorImpl implements Iterator<Object> {

		private boolean marker = true;
		private int pointer = -1;
		private boolean nextOrPrevious;

		private boolean isNextOrPrevious() {
			return nextOrPrevious;
		}

		private boolean isMarker() {
			return marker;
		}

		@Override
		public boolean hasNext() {
			return (pointer < toArray().length - 1);

		}

		@Override
		public void remove() {
			if (isMarker()) {
				throw new IllegalStateException();
			}
			int t = 0;
			if (!isNextOrPrevious()) {
				t = pointer--;
			} else {
				t = ++pointer;
			}
			Object[] temp = new Object[newArr.length - 1];
			System.arraycopy(newArr, 0, temp, 0, t);
			System.arraycopy(newArr, t + 1, temp, t, temp.length - t);
			newArr = temp;
			marker = true;
			if (isNextOrPrevious()) {
				--pointer;
			}
		}

		@Override
		public Object next() {
			if (pointer >= size()) {
				throw new NoSuchElementException("Error no next element");
			}

			marker = false;
			nextOrPrevious = false;
			return toArray()[++pointer];

		}

	}

	public static void main(String[] args) {
		Queue queue = new QueueImpl(3);
		queue.enqueue("1");
		queue.enqueue("2");
		queue.enqueue("3");

		System.out.println(queue);
		System.out.println(queue.size());

		queue.clear();
		System.out.println(queue);
		System.out.println(queue.size());

		Queue queue1 = new QueueImpl(3);
		queue1.enqueue("1");
		queue1.enqueue("2");
		queue1.enqueue("3");

		System.out.println(queue1);
		System.out.println(queue1.size());

		queue1.enqueue("1");
		queue1.enqueue("2");
		queue1.enqueue("3");

		System.out.println(queue1);
		System.out.println(queue1.top());

	}

}
