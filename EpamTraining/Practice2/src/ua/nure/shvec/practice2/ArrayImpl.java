package ua.nure.shvec.practice2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayImpl implements Array {
	private int modCount;
	private Object[] newArr = {};

	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();

	}

	@Override
	public int size() {
		return toArray().length;
	}

	@Override
	public void add(Object element) {
		Object[] temp = new Object[newArr.length + 1];
		System.arraycopy(newArr, 0, temp, 0, newArr.length);
		temp[temp.length - 1] = element;
		newArr = temp;
		setModCount(getModCount() + 1);
	}

	@Override
	public void set(int index, Object element) {
		newArr[index] = element;
	}

	@Override
	public Object get(int index) {
		return newArr[index];
	}

	@Override
	public int indexOf(Object element) {
		int result = -1;
		for (int i = 0; i < newArr.length; ++i) {
			if (element == newArr[i]) {
				result = i;
				break;
			}
		}
		return result;
	}

	@Override
	public void remove(int index) {
		Object[] temp = new Object[newArr.length - 1];
		System.arraycopy(newArr, 0, temp, 0, index);
		System.arraycopy(newArr, index + 1, temp, index, temp.length - index);
		newArr = temp;
	}

	@Override
	public void clear() {
		newArr = new Object[] {};
		setModCount(0);
	}

	public Object[] toArray() {
		return this.newArr.clone();

	}

	@Override
	public String toString() {
		StringBuilder sbl = new StringBuilder();
		sbl.append("[");
		for (int x = 0; x < toArray().length; x++) {
			if (toArray()[x] == null) {
				sbl.append("[" + "null" + "]");
				continue;
			}
			sbl.append("" + toArray()[x].toString() + "");
			if (x != size() - 1) {
				sbl.append(", ");
			}
		}
		sbl.append("]");
		return sbl.toString();

	}

	public int getModCount() {
		return modCount;
	}

	public void setModCount(int modCount) {
		this.modCount = modCount;
	}

	private class IteratorImpl implements Iterator<Object> {

		private boolean nextOrPrevious;
		private boolean marker = true;
		private int currentIndex;
		private int pointer = -1;

		public boolean isNextOrPrevious() {
			return nextOrPrevious;
		}

		public boolean isMarker() {
			return marker;
		}

		@Override
		public boolean hasNext() {
			return (pointer < toArray().length - 1);
		}

		@Override
		public Object next() {
			if (pointer >= size()) {
				throw new NoSuchElementException("Error next item is missing");
			}
			setMarker(false);
			setNextOrPrevious(false);
			return toArray()[++pointer];
		}

		@Override
		public void remove() {
			if (pointer < 0) {
				throw new IllegalStateException("remove: not indicate which item");
			}
			ArrayImpl.this.remove(pointer);
			if (pointer < currentIndex) {
				currentIndex--;
			}
			pointer = -1;
		}

		public void setNextOrPrevious(boolean nextOrPrevious) {
			this.nextOrPrevious = nextOrPrevious;
		}

		public void setMarker(boolean marker) {
			this.marker = marker;
		}
	}

	public static void main(String[] args) {

		// Array demo
		Array arr = new ArrayImpl();
		arr.add("1");
		arr.add("2");
		arr.add("3");
		System.out.println(arr);

		System.out.println("set");
		arr.set(1, "1");
		System.out.println(arr);
		arr.set(0, "2");
		System.out.println(arr);
		arr.size();
		arr.set(0, "3");
		arr.get(1);
		arr.add(new Object());

		arr.remove(0);
		System.out.println(arr + " remove ");

		System.out.println(arr.toString());
		String str = "word";
		arr.add(str);
		System.out.println(arr);
		arr.clear();

		Array arr2 = new ArrayImpl();
		arr2.add("1");
		arr2.add("2");
		arr2.add("3");

		arr2.remove(0);
		arr2.remove(arr2.size() - 1);

		str += "privet";
		arr2.add(str);
		System.out.println(arr);
		System.out.println(arr2);

		// iterator demo
		for (Object ob : arr) {
			System.out.print(ob + " ");
		}

		Iterator<Object> it = arr.iterator();
		while (it.hasNext()) {
			System.out.print(it.next() + " ");
		}

		System.out.println("\nshow: " + arr2);
		Iterator<Object> it1 = arr2.iterator();
		while (it1.hasNext()) {
			it1.next();
		}

	}
}
