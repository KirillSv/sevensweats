

@echo off

javac -d ..\bin ua\nure\schvec\practice1\Demo.java 
javac -d ..\bin ua\nure\schvec\practice1\Part1.java 
javac -d ..\bin ua\nure\schvec\practice1\Part2.java  
javac -d ..\bin ua\nure\schvec\practice1\Part3.java  
javac -d ..\bin ua\nure\schvec\practice1\Part4.java  
javac -d ..\bin ua\nure\schvec\practice1\Part5.java  

java -cp ..\bin ua.nure.schvec.practice1.Demo

del ..\bin\ua\nure\schvec\practice1\Demo.class
del ..\bin\ua\nure\schvec\practice1\Part1.class
del ..\bin\ua\nure\schvec\practice1\Part2.class
del ..\bin\ua\nure\schvec\practice1\Part3.class
del ..\bin\ua\nure\schvec\practice1\Part4.class
del ..\bin\ua\nure\schvec\practice1\Part5.class
pause

