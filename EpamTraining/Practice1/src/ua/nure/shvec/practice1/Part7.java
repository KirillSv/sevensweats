package ua.nure.shvec.practice1;

public class Part7 {

	public static void main(String[] args) {
		String arrow = " ==> ";
		System.out.println("A" + arrow + "1" + arrow + "A");
		System.out.println("B" + arrow + "2" + arrow + "B");
		System.out.println("Z" + arrow + "26" + arrow + "Z");
		System.out.println("AA" + arrow + "27" + arrow + "AA");
		System.out.println("AZ" + arrow + "52" + arrow + "AZ");
		System.out.println("BA" + arrow + "53" + arrow + "BA");
		System.out.println("ZZ" + arrow + "702" + arrow + "ZZ");
		System.out.println("AAA" + arrow + "703" + arrow + "AAA");

	}

	public static int str2int(String lettersNumber) {
		final int constntSystem = 64;
		final int constAlphabet = 26;

		int digitalNumber = 0;
		for (int i = 0, j = lettersNumber.length() - 1; i < lettersNumber.length(); i++, j--) {
			digitalNumber += (lettersNumber.charAt(j) - constntSystem) * Math.pow(constAlphabet, i);
		}
		return digitalNumber;
	}

	public static String int2str(int number) {
		final int constntSystem = 64;
		final int constAlphabet = 26;

		StringBuilder hereWillBeNeedLetters = new StringBuilder();

		while (number != 0) {
			int letters = number % constAlphabet;
			if (letters == 0) {
				hereWillBeNeedLetters.append("Z");
				number = (number - 1) / constAlphabet;
			} else {
				hereWillBeNeedLetters.append((char) (letters + constntSystem));
				number = (number - letters) / constAlphabet;
			}
		}
		StringBuilder resultLetter = new StringBuilder();
		for (int i = 0; i < hereWillBeNeedLetters.length(); ++i) {
			resultLetter.append(hereWillBeNeedLetters.charAt(hereWillBeNeedLetters.length() - i - 1));
		}
		return resultLetter.toString();

	}

	public static String rightColumn(String number) {
		int num = str2int(number);
		return int2str(++num);
	}

}
