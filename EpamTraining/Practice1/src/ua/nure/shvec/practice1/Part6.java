package ua.nure.shvec.practice1;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Part6 {

	public static void main(String[] args) {
		int countElement = Integer.parseInt(args[0]);
		int[] arr;
		arr = IntStream.iterate(2, i -> i + 1).filter(j -> IntStream.range(2, j).noneMatch(e -> j % e == 0))
				.limit(countElement).toArray();
		String clearArr = Arrays.toString(arr).replaceAll("[^0-9 ]", "").replaceAll(",", "");
		System.out.println(clearArr);

	}
}
