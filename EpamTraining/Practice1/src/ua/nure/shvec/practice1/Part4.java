package ua.nure.shvec.practice1;

public class Part4 {

	public static void main(String[] args) {

		System.out.println(findNOD(Integer.parseInt(args[0]), Integer.parseInt(args[1])));

	}

	public static int findNOD(int a, int b) {
		if (b == 0) {
			return Math.abs(a);
		}
		return findNOD(b, a % b);
	}

}