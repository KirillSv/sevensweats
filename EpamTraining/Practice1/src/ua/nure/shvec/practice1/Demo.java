package ua.nure.shvec.practice1;

public class Demo {

	public static void main(String[] args) {
		Part1.main(new String[] { null });
		Part2.main(new String[] { "10", "20" });
		Part3.main(new String[] { "start", "middle", "end" });
		Part4.main(new String[] { "15", "26" });
		Part5.main(new String[] { "12345" });
		Part6.main(new String[] { "1" });

		System.out.println(Part7.rightColumn("C") + " expected " + "D");

		String[] letters = { "A", "B", "Z", "AA", "AZ", "BA", "ZZ", "AAA" };
		int[] numbers = { 1, 2, 26, 27, 52, 53, 702, 703 };

		for (int i = 0; i < 8; i++) {
			System.out.println("expected " + numbers[i] + " get=> " + Part7.str2int(letters[i]));
			System.out.println("expected " + letters[i] + " get=> " + Part7.int2str(numbers[i]));
		}

	}

}
