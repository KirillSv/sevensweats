package ua.nure.shvec.practice6.part4;

public class Part4 {

	public static void main(String[] args) {
		Range r = new Range(3, 10);
		for (Integer e : r) {
			System.out.printf("%d ", e);
		}
		System.out.println();
		 
		r = new Range(3, 10, true);
		for (Integer e : r) {
			System.out.printf("%d ", e);
		}
		System.out.println();
		
	}

}
