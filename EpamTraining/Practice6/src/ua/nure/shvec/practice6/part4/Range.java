package ua.nure.shvec.practice6.part4;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Range implements Iterable<Integer> {

	private int[] arr;

	public Range(int n, int m) {
		this(n, m, false);
	}

	public Range(int n, int m, boolean reverse) {
		arr = new int[m - n + 1];
		if (reverse) {		
			int index = 0;
			for (int i = m; i >= n; i--) {
				arr[index] = i;
				index++;
			}
		} else {		
			for (int i = 0; i < (m - n + 1); i++) {
				arr[i] = n + i;
			}
		}

	}

	@Override
	public Iterator<Integer> iterator() {
		return new RangeIterator();
	}

	class RangeIterator implements Iterator<Integer> {

		private int count = arr.length;
		private int index;

		@Override
		public boolean hasNext() {
			return index < count;
		}

		@Override
		public Integer next() {
			if (index < count) {
				return arr[index++];
			} else {
				throw new NoSuchElementException("No such element.");
			}
		}
	}

}
