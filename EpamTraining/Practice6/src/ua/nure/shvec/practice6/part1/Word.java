package ua.nure.shvec.practice6.part1;
import java.io.Serializable;
import java.util.Comparator;

public class Word {
	
	private int frequency;
	private String content;
	

	Word(String word) {
		frequency = 1;
		this.content = word;
	}

	int getFrequency() {
		return frequency;
	}

	String getValue() {
		return content;
	}

	public void setFrequency(int a) {
		this.frequency = a;
	}

	@Override
	public int hashCode() {
		return 31 * content.hashCode() + frequency;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Word word1 = (Word) o;

		return frequency == word1.frequency && content.equals(word1.content);
	}


	static class FrequencyCompare implements Comparator<Word>, Serializable {
	
		private static final long serialVersionUID = 6054239939815570021L;

		@Override
		public int compare(Word o1, Word o2) {
			int comp = o2.getFrequency()-o1.getFrequency();
			if (comp == 0) {
				return new WordCompare().compare(o1, o2);
			}
			return comp;
		}
	}
	

	static class WordCompare implements Comparator<Word>, Serializable{
	
		private static final long serialVersionUID = -4600964749725884138L;

		@Override
		public int compare(Word a1, Word a2) {
			return a1.getValue().compareTo(a2.getValue());
		}
	}
}
