package ua.nure.shvec.practice6.part1;

import java.util.Iterator;
import java.util.Scanner;
import java.io.InputStream;
import java.io.PrintStream;

public class Part1 {

	public static void main(String[] args) {

		wordTakes(System.in, System.out);

	}

	public static void wordTakes(InputStream input, PrintStream output) {
		try (Scanner s = new Scanner(input)) {
			WordContainer container = new WordContainer(new Word.WordCompare());
			while (s.hasNext()) {
				String w = s.next();

				if ("stop".equals(w)) {
					break;
				}

				container.add(new Word(w));

			}

			Iterator<Word> i = container.iteratorFrequency();
			while (i.hasNext()) {
				Word word = i.next();

				output.printf("%s : %s%n", word.getValue(), word.getFrequency());
			}
		}
	}

}
