package ua.nure.shvec.practice6.part1;

import java.util.Iterator;
import java.util.TreeSet;
import java.util.Comparator;

public class WordContainer extends TreeSet<Word> {

	private static final long serialVersionUID = 2L;

	public WordContainer(Comparator<? super Word> c) {
		super(c);
	}

	public static void main(String[] args) {
		Part1.wordTakes(System.in, System.out);
	}

	public Iterator<Word> iteratorFrequency() {
		TreeSet<Word> tset = new TreeSet<>(new Word.FrequencyCompare());
		for (Iterator<Word> i = iterator(); i.hasNext();) {
			tset.add(i.next());
		}

		return tset.iterator();
	}

	@Override
	public boolean add(Word w) {
		if (!contains(w)) {
			super.add(w);
			return false;
		}
		Iterator<Word> i = iterator();
		while (i.hasNext()) {
			Word n = i.next();
			if (comparator().compare(n, w) == 0) {
				n.setFrequency(n.getFrequency() + 1);
				return true;
			}
		}
		return false;
	}

}
