package ua.nure.shvec.practice6.part6;

import java.util.Comparator;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part6 {

	private String textfileName;
	private String[] words;

	public static void main(String[] arg) {
		new Part6().consoleInput(arg[0], arg[1], arg[2], arg[3]);
	}

	private boolean consoleInput(String text, String textfileName, String comand, String choice) {
		if (!("--input".equals(text) || "-i".equals(text))) {
			System.err.println("Error input");
			return false;
		}
		if (!("--task".equals(comand) || "-t".equals(comand))) {
			System.err.println("Error input");
			return false;
		}

		this.textfileName = textfileName;
		getSeparatedWord();

		switch (choice) {
		case "duplicates":
			getWordDubl();
			break;
		case "length":
			getWordLength();
			break;
		case "frequency":
			getWordFrequecy();
			break;
		default:
			return false;
		}
		return true;
	}

	static class CountWordLengthPlace {

		private int length;
		private int place;

		public CountWordLengthPlace(int length, int place) {
			this.length = length;
			this.place = place;
		}

		public int getLength() {
			return length;
		}

	}

	private void getWordLength() {

		HashMap<String, CountWordLengthPlace> wordes = new HashMap<>();
		for (int place = 0; place < words.length; place++) {
			String w = words[place];

			wordes.put(w, new CountWordLengthPlace(w.length(), place));

		}
		TreeMap<String, CountWordLengthPlace> sortedWord = new TreeMap<>(new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				CountWordLengthPlace countWordLengthPlaceA = wordes.get(a);
				CountWordLengthPlace countWordLengthPlaceB = wordes.get(b);
				int lengh = countWordLengthPlaceB.length - countWordLengthPlaceA.length;
				if (lengh == 0) {
					return countWordLengthPlaceA.place - countWordLengthPlaceB.place;
				} else {
					return lengh;
				}
			}
		});
		sortedWord.putAll(wordes);

		int r = 0;
		for (Entry<String, CountWordLengthPlace> counter : sortedWord.entrySet()) {
			if (r == 3) {
				break;
			}
			r++;
			System.out.println(counter.getKey() + " ==> " + sortedWord.get(counter.getKey()).getLength());
		}

	}

	private String getInputtext() {
		StringBuilder sbl = new StringBuilder();
		try (Scanner f = new Scanner(new File(textfileName), "Cp1251")) {
			while (f.hasNext()) {
				sbl.append(f.next()).append(" ");
			}
		} catch (FileNotFoundException e) {
			System.err.println(String.format("Error not found %s", textfileName));
		}
		return sbl.toString();
	}

	public void getSeparatedWord() {
		StringBuilder sbl = new StringBuilder();
		Pattern pat = Pattern.compile("[a-zA-Z]+");
		Matcher match = pat.matcher(getInputtext());
		while (match.find()) {
			sbl.append(match.group() + " ");
		}
		words = sbl.toString().split(" ");
	}

	private void getWordDubl() {
		Map<String, Integer> counterWord = new LinkedHashMap<>();
		for (int place = 0; place < words.length; place++) {
			String oneWord = words[place];
			Integer countWithPlace = counterWord.get(oneWord);
			if (countWithPlace == null) {
				counterWord.put(oneWord, 1);
			} else {
				countWithPlace++;
				counterWord.put(oneWord, countWithPlace);
			}
		}
		int t = 0;
		for (Map.Entry<String, Integer> counter : counterWord.entrySet()) {
			if (t == 3) {
				break;
			}
			if (counter.getValue() > 1) {
				t++;
				System.out
						.println(new StringBuilder(counter.getKey()).reverse().toString().toUpperCase(Locale.ENGLISH));
			}
		}

	}

	static class CountPlace {
		private int repeat = 1;
		private int numberOfplace;

		public CountPlace(int numberOfplace) {
			this.numberOfplace = numberOfplace;
		}

		public int getRepeat() {
			return repeat;
		}

		public CountPlace setCount(int repeat) {
			this.repeat = repeat;
			return this;
		}
	}

	private void getWordFrequecy() {

		HashMap<String, CountPlace> wordCount = new HashMap<>();
		for (int countPlace = 0; countPlace < words.length; countPlace++) {
			String oneWord = words[countPlace];
			CountPlace countPl = wordCount.get(oneWord);
			if (countPl == null) {
				wordCount.put(oneWord, new CountPlace(countPlace));
			} else {
				countPl.setCount(countPl.getRepeat() + 1);
			}
		}
		TreeMap<String, CountPlace> sortedWord = new TreeMap<>(new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				CountPlace countPlaceA = wordCount.get(a);
				CountPlace countPlaceB = wordCount.get(b);
				int output = countPlaceB.repeat - countPlaceA.repeat;
				if (output == 0) {
					return countPlaceA.numberOfplace - countPlaceB.numberOfplace;
				} else {
					return output;
				}
			}
		});
		sortedWord.putAll(wordCount);

		TreeSet<String> firstString = new TreeSet<>(new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				return b.compareTo(a);
			}
		});

		int t = 0;
		for (Entry<String, CountPlace> counter : sortedWord.entrySet()) {
			if (t == 3) {
				break;
			}
			t++;
			firstString.add(counter.getKey());
		}

		for (String str : firstString) {
			System.out.println(str + " ==> " + sortedWord.get(str).getRepeat());
		}
	}

}
