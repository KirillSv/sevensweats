package ua.nure.shvec.practice6.part6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part62 {

	private String textfileName;
	private String[] words;

	public static void main(String[] args) {
		new Part62().consoleInput("--input", "part6.txt", "--task", "length");
	}

	private boolean consoleInput(String text, String textfileName, String comand, String choice) {
		if (!("--input".equals(text) || "-i".equals(text))) {
			System.err.println("Input Error");
			return false;
		}
		if (!("--task".equals(comand) || "-t".equals(comand))) {
			System.err.println("Input Error");
			return false;
		}

		this.textfileName = textfileName;
		getSeparatedWord();

		if ("length".equals(choice)) {
			getResultLength();
		} else {
			return false;
		}
		return true;
	}

	private String getInputtext() {
		StringBuilder sbl = new StringBuilder();
		try (Scanner f = new Scanner(new File(textfileName), "Cp1251")) {
			while (f.hasNext()) {
				sbl.append(f.next()).append(" ");
			}
		} catch (FileNotFoundException e) {
			System.err.println(String.format("Error. Not found %s", textfileName));
		}
		return sbl.toString();
	}

	public void getSeparatedWord() {
		StringBuilder sbl = new StringBuilder();
		Pattern pat = Pattern.compile("[a-zA-Z]+");
		Matcher match = pat.matcher(getInputtext());
		while (match.find()) {
			sbl.append(match.group() + " ");
		}
		words = sbl.toString().split(" ");
	}

	static class CountPlace {
		private int length;
		private int place;

		public CountPlace(int length, int place) {
			this.length = length;
			this.place = place;
		}

		public int getLength() {
			return length;
		}

	}

	private void getResultLength() {

		HashMap<String, CountPlace> wordCount = new HashMap<>();
		for (int place = 0; place < words.length; place++) {
			String w = words[place];

			wordCount.put(w, new CountPlace(w.length(), place));
		}
		TreeMap<String, CountPlace> sortedWord = new TreeMap<>(new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				CountPlace countPlaceA = wordCount.get(a);
				CountPlace countPlaceB = wordCount.get(b);
				int output = countPlaceB.length - countPlaceA.length;
				if (output == 0) {
					return countPlaceA.place - countPlaceB.place;
				} else {
					return output;
				}
			}
		});
		sortedWord.putAll(wordCount);

		int r = 0;
		for (Entry<String, CountPlace> counter : sortedWord.entrySet()) {
			if (r == 3) {
				break;
			}
			r++;
			System.out.println(counter.getKey() + " ==> " + sortedWord.get(counter.getKey()).getLength());
		}

	}

}
