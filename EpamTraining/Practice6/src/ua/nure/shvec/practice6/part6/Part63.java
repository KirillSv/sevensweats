package ua.nure.shvec.practice6.part6;

import java.io.File;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.FileNotFoundException;
import java.util.LinkedHashMap;
import java.util.Locale;


public class Part63 {

	private String textfileName;
	private String[] words;

	public static void main(String[] arg) {

		new Part63().consoleInput("--input", "part6.txt", "--task", "duplicates");
	}

	private boolean consoleInput(String text, String textfileName, String comand, String choice) {
		if (!("--input".equals(text) || "-i".equals(text))) {
			System.err.println("Input Error");
			return false;
		}
		if (!("--task".equals(comand) || "-t".equals(comand))) {
			System.err.println("Input Error");
			return false;
		}

		this.textfileName = textfileName;
		getSeparatedWord();

		if("duplicates".equals(choice)) {
			getResultDuplicates();
		}else {
			return false;
		}
		return true;
	}

	private String getInputtext() {
		StringBuilder s = new StringBuilder();
		try (Scanner scn = new Scanner(new File(textfileName), "Cp1251")) {
			while (scn.hasNext()) {
				s.append(scn.next()).append(" ");
			}
		} catch (FileNotFoundException e) {
			System.err.println(String.format("Error. Not found %s", textfileName));
		}
		return s.toString();
	}

	public void getSeparatedWord() {
		StringBuilder s = new StringBuilder();
		Pattern patt = Pattern.compile("[a-zA-Z]+");
		Matcher match = patt.matcher(getInputtext());
		while (match.find()) {
			s.append(match.group() + " ");
		}
		words = s.toString().split(" ");
	}

	private void getResultDuplicates() {
		Map<String, Integer> counts = new LinkedHashMap<>();
		for (int place = 0; place < words.length; place++) {
			String w = words[place];
			Integer countWithPlace = counts.get(w);
			if (countWithPlace == null) {
				counts.put(w, 1);
			} else {
				countWithPlace++;
				counts.put(w, countWithPlace);
			}
		}
		int f = 0;
		for (Map.Entry<String, Integer> newWords : counts.entrySet()) {
			if (f == 3) {
				break;
			}
			if (newWords.getValue() > 1) {
				f++;
				
				System.out.println(new StringBuilder(newWords.getKey()).reverse().toString().toUpperCase(Locale.ENGLISH));
			}
		}
	}
}
