package ua.nure.shvec.practice6.part6;

import java.util.TreeSet;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Comparator;
import java.util.TreeMap;
import java.io.File;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part61 {

	private String textfileName;
	private String[] words;

	public static void main(String[] arg) {
		new Part61().consoleInput("--input", "part6.txt", "--task", "frequency");
	}

	private boolean consoleInput(String text, String textfileName, String comand, String choice) {
		if (!("--input".equals(text) || "-i".equals(text))) {
			System.err.println("Input Error");
			return false;
		}
		if (!("--task".equals(comand) || "-t".equals(comand))) {
			System.err.println("Input Error");
			return false;
		}

		this.textfileName = textfileName;
		getSeparatedWord();

		if ("frequency".equals(choice)) {
			getWordFrequecy();
		}
		return true;
	}

	public void getSeparatedWord() {
		StringBuilder sbld = new StringBuilder();
		Pattern patter = Pattern.compile("[a-zA-Z]+");
		Matcher match = patter.matcher(getInputtext());
		while (match.find()) {
			sbld.append(match.group() + " ");
		}
		words = sbld.toString().split(" ");
	}

	private String getInputtext() {
		StringBuilder sbld = new StringBuilder();
		try (Scanner fscan = new Scanner(new File(textfileName), "Cp1251")) {
			while (fscan.hasNext()) {
				sbld.append(fscan.next()).append(" ");
			}
		} catch (FileNotFoundException e) {
			System.err.println(String.format("Error. Not found %s", textfileName));
		}
		return sbld.toString();
	}

	static class CountPlace {
		private int repeat = 1;
		private int numberOfplace;

		public CountPlace(int numberOfplace) {
			this.numberOfplace = numberOfplace;
		}

		public int getRepeat() {
			return repeat;
		}

		public CountPlace setCount(int repeat) {
			this.repeat = repeat;
			return this;
		}
	}

	private void getWordFrequecy() {

		HashMap<String, CountPlace> wordCount = new HashMap<>();
		for (int place = 0; place < words.length; place++) {
			String word = words[place];
			CountPlace countPlace = wordCount.get(word);
			if (countPlace == null) {
				wordCount.put(word, new CountPlace(place));
			} else {
				countPlace.setCount(countPlace.getRepeat() + 1);
			}
		}
		TreeMap<String, CountPlace> sortedWord = new TreeMap<>(new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				CountPlace countPlaceA = wordCount.get(a);
				CountPlace countPlaceB = wordCount.get(b);
				int output = countPlaceB.repeat - countPlaceA.repeat;
				if (output == 0) {
					return countPlaceA.numberOfplace - countPlaceB.numberOfplace;
				} else {
					return output;
				}
			}
		});
		sortedWord.putAll(wordCount);

		TreeSet<String> firstString = new TreeSet<>(new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				return b.compareTo(a);
			}
		});
		int r = 0;
		for (String str : sortedWord.keySet()) {
			if (r == 3) {
				break;
			}
			r++;
			firstString.add(str);
		}
		for (String str : firstString) {
			System.out.println(str + " ==> " + sortedWord.get(str).getRepeat());
		}
	}
}
