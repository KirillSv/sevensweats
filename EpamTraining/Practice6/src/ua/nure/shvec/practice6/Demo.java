package ua.nure.shvec.practice6;

import java.io.ByteArrayInputStream;

import ua.nure.shvec.practice6.part1.Part1;
import ua.nure.shvec.practice6.part2.Part2;
import ua.nure.shvec.practice6.part3.Part3;
import ua.nure.shvec.practice6.part4.Part4;
import ua.nure.shvec.practice6.part5.Part5;
import ua.nure.shvec.practice6.part6.Part6;
import ua.nure.shvec.practice6.part6.Part61;
import ua.nure.shvec.practice6.part6.Part62;
import ua.nure.shvec.practice6.part6.Part63;

public class Demo {

	public static void main(String[] args) {
		System.out.println("----------Part1----------");
        ByteArrayInputStream inStream = new ByteArrayInputStream("Du hast mich Du hast mich Du hast mich gefragt stop".getBytes());
        System.setIn(inStream);
        Part1.main(args);
        System.setIn(System.in);
	
        System.out.println("----------Part2----------");
		Part2.main(args);
		System.out.println("----------Part3----------");
		Part3.main(args);
		System.out.println("----------Part4----------");
		Part4.main(args);
		System.out.println("----------Part5----------");
		Part5.main(args);
		System.out.println("----------Part6----------");
		Part61.main(args);
		Part62.main(args);
		Part63.main(args);
		System.out.println("----------Part61------frequency----");
		Part6.main(new String[] {"--input", "part6.txt", "--task", "frequency"});
		System.out.println("----------Part62------length----");
		Part6.main(new String[] {"--input", "part6.txt", "--task", "length"});
		System.out.println("----------Part63-----duplicates-----");
		Part6.main(new String[] {"--input", "part6.txt", "--task", "duplicates"});

	
	}
}
