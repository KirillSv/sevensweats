package ua.nure.shvec.practice6.part3;

import java.util.ArrayList;
import java.util.List;

public class Parking {

	private int countCar;
	private List<Integer> list= new ArrayList<>();

	public Parking(int parkingSize) {

		list = new ArrayList<>();
		for (int i = 0; i < parkingSize; i++) {
			list.add(i, 0);
		}
	}

	public boolean arrive(int place) {
		if (countCar == list.size()) {
			return false;
		}

		if (place < 0 || place > (list.size() - 1)) {
			throw new IllegalArgumentException();
		}

		if (list.get(place) == 1) {
			place++;
			if (place == (list.size())) {
				place = 0;
			}

			arrive(place);
		} else {
			list.set(place, 1);
			countCar++;
			return true;
		}

		return true;
	}

	public boolean depart(int k) {
		if (k < 0 || k > (list.size() - 1)) {
			throw new IllegalArgumentException();
		}
		if (list.get(k) == 1) {
			list.set(k, 0);
			countCar--;
			return true;
		}
		return false;
	}

	public void print() {
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i));
		}
		System.out.println("");

	}

}