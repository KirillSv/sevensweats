package ua.nure.shvec.practice6.part5;

public class Part5 {

	public static void main(String[] args) {
		Tree<Integer> m = new Tree<>();

		System.out.println(m.add(3));
		System.out.println(m.add(3));

		System.out.println("~~~~~~~");
		Integer[] a = new Integer[] { 1, 2, 5, 4, 6, 0 };
		m.add(a);
		m.print();

		System.out.println("~~~~~~~");
		System.out.println(m.remove(5));
		System.out.println(m.remove(5));

		System.out.println("~~~~~~~");
		m.print();
	}

}
