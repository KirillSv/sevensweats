package ua.nure.shvec.practice6.part5;

public class Tree<E extends Comparable<E>> {

	private Node<E> treeRoot;
	private Node<E> currentNode;
	private Node<E> parentNode;

	public E get() {
		return treeRoot.elem;
	}

	public void setParentNode(Node<E> parentNode) {
		this.parentNode = parentNode;
	}

	public void setCurrentNode(Node<E> currentNode) {
		this.currentNode = currentNode;
	}

	public static class Node<E> {

		private E elem;
		private Node<E> rightBranch;
		private Node<E> leftBranch;

		Node(E elem, Node<E> leftBranch, Node<E> rightBranch) {
			this.elem = elem;
			this.rightBranch = rightBranch;
			this.leftBranch = leftBranch;

		}

	}

	public boolean add(E e) {
		if (treeRoot == null) {
			treeRoot = new Node<>(e, null, null);
			return true;
		}
		return add(treeRoot, e);
	}

	public void add(E[] elements) {
		for (int t = 0; t < elements.length; t++) {
			add(elements[t]);
		}
	}

	public void print() {
		print(treeRoot, "");
	}

	public boolean remove(E element) {
		int toRight = 1;
		int toLeft = -1;

		setParentNode(null);
		setCurrentNode(treeRoot);
		int endBranch = 0;

		endBranch = removeNodeCurrent(element, toLeft, toRight, endBranch);
		if (currentNode == null) {
			return false;
		}
		if (currentNode.leftBranch == null) {
			if (endBranch == toRight) {
				parentNode.rightBranch = currentNode.rightBranch;
			} else if (endBranch == toLeft) {
				parentNode.leftBranch = currentNode.rightBranch;
			} else {
				treeRoot = currentNode.rightBranch;
			}
		} else if (currentNode.rightBranch == null) {
			getRightNode(toLeft, toRight, endBranch);
		} else {
			Node<E> replaceNode = currentNode.rightBranch;
			parentNode = currentNode;
			while (replaceNode.leftBranch != null) {
				parentNode = replaceNode;
				replaceNode = replaceNode.leftBranch;
			}
			currentNode.elem = replaceNode.elem;
			getNode(replaceNode);
		}
		return true;
	}

	private void getRightNode(int toLeft, int toRight, int endBranch) {
		if (endBranch == toRight) {
			parentNode.rightBranch = currentNode.leftBranch;
		} else if (endBranch == toLeft) {
			parentNode.leftBranch = currentNode.leftBranch;
		} else {
			treeRoot = currentNode.leftBranch;
		}
	}

	private void getNode(Node<E> n) {
		if (parentNode.equals(currentNode)) {
			parentNode.rightBranch = n.rightBranch;
		} else {
			parentNode.leftBranch = n.rightBranch;
		}
	}

	private int removeNodeCurrent(E el, int left, int right, int endBranch) {
		int comparisonResult;
		while (currentNode != null && (comparisonResult = currentNode.elem.compareTo(el)) != 0) {
			parentNode = currentNode;
			if (comparisonResult < 0) {
				endBranch = right;
				currentNode = currentNode.rightBranch;
			} else {
				endBranch = left;
				currentNode = currentNode.leftBranch;
			}
		}
		return endBranch;
	}

	public boolean add(Node<E> node, E a) {
		if (a.compareTo(node.elem) < 0) {
			if (node.leftBranch == null) {
				node.leftBranch = new Node<>(a, null, null);
				return true;
			}
			return add(node.leftBranch, a);
		}
		if (a.compareTo(node.elem) > 0) {
			if (node.rightBranch == null) {
				node.rightBranch = new Node<>(a, null, null);
				return true;
			}
			return add(node.rightBranch, a);
		}
		return false;
	}

	private void print(Node<E> n, String str) {
		if (n == null) {
			return;
		}
		print(n.leftBranch, str + "  ");
		System.out.println(str + n.elem);
		print(n.rightBranch, str + "  ");
	}

}
