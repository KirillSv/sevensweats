package ua.nure.shvec.practice7.candy;

/**
 * @author Shvec
 *
 */
public enum TypeName {

	CHOCOLATE("Chocolate"), CARAMEL("Caramel"), IRIS("Iris"), CHOCOLATE_WITH_FILLING("Chocolate_with_filling");

	private final String value;

	TypeName(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static TypeName fromValue(String v) {
		for (TypeName c : TypeName.values()) {
			if (c.value.equals(v)) {

				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
