package ua.nure.shvec.practice7.candy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author Shvec.
 *
 */

public class Candies {

	private List<Candy> candy = new ArrayList<>();

	public void setCandyList(List<Candy> list) {
		this.candy = list;
	}

	public List<Candy> getCandy() {
		return candy;
	}

	public void sortCandyListByName() {
		Comparator<Candy> compareByStringOne = new Comparator<Candy>() {
			@Override
			public int compare(Candy str1, Candy str2) {
				return str1.getCandyname().compareTo(str2.getCandyname());
			}
		};
		Collections.sort(this.candy, compareByStringOne);
	}

}