package ua.nure.shvec.practice7;

import ua.nure.shvec.practice7.constants.XMLConstants;
import ua.nure.shvec.practice7.htmltransform.TransformerHTML;
import ua.nure.shvec.practice7.parsers.DOMparserClass;
import ua.nure.shvec.practice7.parsers.SAXParserClass;
import ua.nure.shvec.practice7.parsers.STAXParserClass;

public class Main {

	public static void main(String[] args) throws Exception {

		DOMparserClass dOMparserClass = new DOMparserClass(args[0]);
		dOMparserClass.parseXML(true);
		dOMparserClass.saveToXML();

		SAXParserClass sAXParserClass = new SAXParserClass(args[0]);
		sAXParserClass.parse(true);
		sAXParserClass.writeXML();

		STAXParserClass sTAXParserClass = new STAXParserClass(args[0]);
		sTAXParserClass.parse();
		sTAXParserClass.writeXML();
		
		TransformerHTML.transformToHTML(XMLConstants.VALID_XML_FILE, XMLConstants.VALID_XSL_FILE);
	}
}