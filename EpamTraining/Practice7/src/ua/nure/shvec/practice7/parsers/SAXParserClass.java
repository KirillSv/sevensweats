package ua.nure.shvec.practice7.parsers;

import java.io.FileNotFoundException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.xml.sax.Attributes;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLStreamException;

import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.helpers.DefaultHandler;

import ua.nure.shvec.practice7.candy.Candy;
import ua.nure.shvec.practice7.candy.CandyType;
import ua.nure.shvec.practice7.candy.Ingredient;
import ua.nure.shvec.practice7.candy.TypeName;
import ua.nure.shvec.practice7.candy.Value;
import ua.nure.shvec.practice7.constants.XMLConstants;
import ua.nure.shvec.practice7.util.MakerObjectFromSaveClass;
/**
 * @author Shvec SAX Parser
 *
 */
public class SAXParserClass extends DefaultHandler {

	private List<Candy> candysList;

	private String xmlFile;
	private Candy candy;
	private CandyType candyType;
	private Ingredient ingredient;
	private Value value;
	private String cont;

	public SAXParserClass(String xmlFile) {
		this.xmlFile = xmlFile;
		candysList = new ArrayList<>();
	}

	/**
	 * @param valida validation XML file and parse XML file
	 */
	public void parse(boolean valida) {
		SAXParserFactory f = SAXParserFactory.newInstance();
		f.setNamespaceAware(true);

		if (valida) {
			f.setValidating(true);
			try {
				f.setFeature(XMLConstants.FEATURE_TURN_VALIDATION_ON, true);
				f.setFeature(XMLConstants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
			} catch (SAXNotRecognizedException | SAXNotSupportedException | ParserConfigurationException e) {
				e.printStackTrace();
			}
		}
		javax.xml.parsers.SAXParser parse;

		try {
			parse = f.newSAXParser();
			parse.parse(xmlFile, this);
		} catch (ParserConfigurationException e) {
			System.err.println("Error with Parser Config");
		} catch (IOException e) {
			System.err.println("Error Filepath");
		} catch (SAXException e) {
			System.err.println("Error SAXException");
		} catch (Exception e) {
			System.err.println("Error Exception");
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attrs) {
		switch (qName) {
		case "candies":
			candysList = new ArrayList<>();
			break;
		case "candy":
			candy = new Candy();
			break;
		case "ingredient":
			ingredient = new Ingredient();
			break;
		case "value":
			value = new Value();
			break;
		default:
			break;
		}
	}

	@Override
	public void endElement(String url, String localName, String qNames) {
		switch (qNames) {
		case "candy":
			candysList.add(candy);
			break;
		case "candyname":
			candy.setCandyname(cont);
			break;

		case "type":
			candyType = new CandyType();
			candyType.setValue(TypeName.fromValue(cont));
			break;
		case "energy":
			candy.setEnergy(Integer.valueOf(cont));
			break;
		case "name":
			ingredient.setName(cont);
			break;
		case "quantity":
			ingredient.setQuantity(Integer.valueOf(cont));
			candy.setIngredient(ingredient);
			break;
		case "proteins":
			value.setProteins(Integer.valueOf(cont));
			break;
		case "fats":
			value.setFats(Integer.valueOf(cont));
			break;
		case "carbohydrates":
			value.setCarbohydrates(Integer.valueOf(cont));
			break;
		case "production":
			candy.setProduction(cont);
			break;
		default:
			break;
		}
		candy.setValue(value);
		candy.setCandyType(candyType);
	}

	public void characters(char[] cha, int first, int lengt) {
		cont = String.copyValueOf(cha, first, lengt).trim();
	}

	public void writeXML() {
		candysList.sort(new Comparator<Candy>() {
			@Override
			public int compare(Candy o1, Candy o2) {
				return o2.compareTo(o1);
			}
		});
		try {
			MakerObjectFromSaveClass.writerSaxStax(candysList, XMLConstants.SAX_F);
		} catch (IOException | XMLStreamException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param arg
	 * @throws FileNotFoundException
	 * @throws JAXBException
	 */
	public static void main(String[] arg){
		SAXParserClass saxPars = new SAXParserClass("input.xml");
		saxPars.parse(true);
		saxPars.writeXML();
	}

}
