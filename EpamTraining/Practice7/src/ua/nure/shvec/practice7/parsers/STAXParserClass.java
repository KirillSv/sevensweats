package ua.nure.shvec.practice7.parsers;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;

import ua.nure.shvec.practice7.candy.Candy;
import ua.nure.shvec.practice7.candy.CandyType;
import ua.nure.shvec.practice7.candy.Ingredient;
import ua.nure.shvec.practice7.candy.TypeName;
import ua.nure.shvec.practice7.candy.Value;
import ua.nure.shvec.practice7.constants.XMLConstants;
import ua.nure.shvec.practice7.util.MakerObjectFromSaveClass;

/**
 * @author Shvec
 *
 */
public class STAXParserClass {

	private Candy candy;
	private Ingredient ingredient;
	private CandyType candyType;
	private Value value;
	private String tag;
	private List<Candy> candysList;
	private String xmlFile;

	public STAXParserClass(String xmlFilePath) {
		this.xmlFile = xmlFilePath;
		candysList = new ArrayList<>();
	}

	/**
	 * Write output XML File
	 */
	public void writeXML() {
		candysList.sort(new Comparator<Candy>() {
			@Override
			public int compare(Candy o1, Candy o2) {
				return o2.compareTo(o1);
			}
		});
		try {
			MakerObjectFromSaveClass.writerSaxStax(candysList, XMLConstants.STAX_F);
		} catch (IOException | XMLStreamException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] arg) {
		STAXParserClass pars = new STAXParserClass("input.xml");
		pars.parse();
		pars.writeXML();
	}

	/**
	 * parsed xml file
	 */
	public void parse() {
		try {

			XMLInputFactory factoryXML = XMLInputFactory.newInstance();
			XMLStreamReader reader = factoryXML.createXMLStreamReader(new StreamSource(xmlFile));
			while (reader.hasNext()) {
				int evType = reader.next();
				switch (evType) {
				case XMLStreamReader.START_ELEMENT:
					checkFirstElement(reader);
					break;
				case XMLStreamReader.CHARACTERS:
					tag = reader.getText().trim();
					break;
				case XMLStreamReader.END_ELEMENT:
					checkEndElement(reader);
					break;
				default:
					break;
				}
			}
		} catch (XMLStreamException e) {
			System.err.println("XML Exception");
		}
	}

	private void checkFirstElement(XMLStreamReader reader) {
		switch (reader.getLocalName()) {
		case "candies":
			candysList = new ArrayList<>();
			break;
		case "candy":
			candy = new Candy();
			break;
		case "ingredient":
			ingredient = new Ingredient();
			break;
		case "value":
			value = new Value();
			break;
		default:
			break;
		}
	}

	private void checkEndElement(XMLStreamReader reader) {
		switch (reader.getLocalName()) {
		case "candy":
			candysList.add(candy);
			break;
		case "candyname":
			candy.setCandyname(tag);
			break;
		case "energy":
			candy.setEnergy(Integer.valueOf(tag));
			break;
		case "type":
			candyType = new CandyType();
			candyType.setValue(TypeName.fromValue(tag));
			break;
		case "name":
			ingredient.setName(tag);
			break;
		case "quantity":
			ingredient.setQuantity(Integer.valueOf(tag));
			candy.setIngredient(ingredient);
			break;
		case "proteins":
			value.setProteins(Integer.valueOf(tag));
			break;
		case "fats":
			value.setFats(Integer.valueOf(tag));
			break;
		case "carbohydrates":
			value.setCarbohydrates(Integer.valueOf(tag));
			break;
		case "production":
			candy.setProduction(tag);
			break;
		default:
			break;
		}
		candy.setValue(value);
		candy.setCandyType(candyType);
	}
}
