package ua.nure.shvec.practice7.constants;

/**
 * @author Shvec class-storage for used constants
 */
public final class XMLConstants {

	private XMLConstants() {
		throw new IllegalStateException("Utility class");
	}

	public static final String VALID_XML_FILE = "input.xml";
	public static final String INVALID_XML_FILE = "input-invalid.xml";
	public static final String XSD_FILE = "input.xsd";
	public static final String SAX_F = "output.sax.xml";
	public static final String STAX_F = "output.stax.xml";
	public static final String DOM_F = "output.dom.xml";

	public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
	public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";

	public static final String CANDIES = "tns:candies";
	public static final String CANDY = "candy";
	public static final String CANDYNAME = "candyname";
	public static final String ENERGY = "energy";
	public static final String TYPE = "type";
	public static final String INGREDIENTS = "ingredients";
	public static final String INGREDIENT = "ingredient";
	public static final String VALUE = "value";
	public static final String PRODUCTION = "production";
	public static final String NAME = "name";
	public static final String QUANTITY = "quantity";
	public static final String VALID_XSL_FILE = "input.xsl";

}
