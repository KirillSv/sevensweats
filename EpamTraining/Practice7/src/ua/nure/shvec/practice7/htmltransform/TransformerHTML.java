package ua.nure.shvec.practice7.htmltransform;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * @author Shvec
 *class Transformed XML to HTML.
 */
public final class TransformerHTML {
	
	private TransformerHTML() {
		throw new IllegalStateException("Utility class");
	}
	
	
    public static void transformToHTML(String xmlFilePath, String xslFilePath) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            javax.xml.transform.Transformer transformer = transformerFactory
                    .newTransformer(new StreamSource(xslFilePath));
            transformer.transform(new StreamSource(xmlFilePath), new StreamResult("candy.html"));
            System.out.println("Transform to HTML is successful.");
        } catch (TransformerException e) {
            System.out.println("Transform to HTML is not successful.");
        }
    }
}