package ua.nure.shvec.practice7.parsers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import ua.nure.shvec.practice7.candy.Candy;
import ua.nure.shvec.practice7.candy.CandyType;
import ua.nure.shvec.practice7.candy.Ingredient;
import ua.nure.shvec.practice7.candy.TypeName;
import ua.nure.shvec.practice7.candy.Value;
import ua.nure.shvec.practice7.constants.XMLConstants;
/**
 * @author Shvec
 * DOM Parser
 */
public class DOMparserClass {
	private List<Candy> candysList = new ArrayList<>();
	private String xmlFile;
	private Document outputDocument;

	public DOMparserClass(String xmlFile) {
		this.xmlFile = xmlFile;
		candysList = new ArrayList<>();
	}

	
	private String getValue(Element e, String name) {
		NodeList n = e.getElementsByTagName(name);
		Element elem = (Element) n.item(0);
		return ((Text) elem.getFirstChild()).getNodeValue();
	}

	/**
	 * @param filePath
	 * @return list with candy for recording
	 */
	public List<Candy> dOMParser(String filePath) {
		try {
			DocumentBuilderFactory bf = DocumentBuilderFactory.newInstance();
			bf.setNamespaceAware(true);
			DocumentBuilder db = bf.newDocumentBuilder();
			Document document = db.newDocument();
			Element rootElement = document.createElement(XMLConstants.CANDIES);
			rootElement.setAttribute("xmlns:tns", "http://www.example.org/input");
			rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootElement.setAttribute("xsi:schemaLocation", "http://www.example.org/input input.xsd");
			document.appendChild(rootElement);
			
			NodeList n = outputDocument.getElementsByTagName("candy");

			for (int t = 0; t < n.getLength(); t++) {
				Node node = n.item(t);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Candy candy = new Candy();
					Element eElem = (Element) node;
					Node name = eElem.getAttributes().item(0);
					candy.setCandyname(name.getNodeValue());
					candy.setEnergy(Integer.parseInt(getValue(eElem, "energy")));

					CandyType candyType = new CandyType();
					candyType.setValue(TypeName.fromValue(getValue(eElem, "type")));
					candy.setCandyType(candyType);

					Element ing = (Element) eElem.getElementsByTagName("ingredients").item(0);
					NodeList ingredList = ing.getElementsByTagName("ingredient");
					Element ingredEl;
					for (int g = 0; g < ingredList.getLength(); g++) {
						ingredEl = (Element) ingredList.item(g);
						Ingredient ingred = new Ingredient();
						ingred.setName(getValue(ingredEl, "name"));
						ingred.setQuantity(Integer.parseInt(getValue(ingredEl, "quantity")));
						candy.setIngredient(ingred);
					}
					Element values = (Element) eElem.getElementsByTagName("value").item(0);

					Value candyV = new Value();
					candyV.setFats(Integer.parseInt(getValue(values, "fats")));
					candyV.setProteins(Integer.parseInt(getValue(values, "proteins")));
					candyV.setCarbohydrates(Integer.parseInt(getValue(values, "carbohydrates")));
					candy.setValue(candyV);
					candy.setProduction(getValue(eElem, "production"));
					candysList.add(candy);
				}
			}
		} catch (ParserConfigurationException e) {
			System.err.println("Pars Config Error");
		} catch (Exception e) {
			System.err.println("Error Exception");
		}
		return candysList;
	}

	/**
	 * @param validationPath
     *Parse XML for Candies objects executing
	 */
	public void parseXML(boolean valid) {
		DocumentBuilderFactory dF = DocumentBuilderFactory.newInstance();
		dF.setNamespaceAware(true);
		if (valid) {
			try {
				dF.setFeature("http://xml.org/sax/features/validation", true);
				dF.setFeature("http://apache.org/xml/features/validation/schema", true);
			} catch (ParserConfigurationException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		try {
			outputDocument = dF.newDocumentBuilder().parse(xmlFile);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			System.err.println(e.getLocalizedMessage());

		}

	}

	/**
	 * @throws TransformerException
	 * @throws ParserConfigurationException 
	 * record output XML file
	 */
	public void saveToXML() throws TransformerException{

		StreamResult output = new StreamResult(new File(XMLConstants.DOM_F));

		TransformerFactory trf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer tr = trf.newTransformer();
		tr.setOutputProperty(OutputKeys.INDENT, "yes");

		tr.transform(new DOMSource(outputDocument), output);
	}

	public static void main(String[] arg) throws TransformerException{
		DOMparserClass pars = new DOMparserClass("input.xml");
		pars.parseXML(true);
		pars.saveToXML();
	}
}
