package ua.nure.shvec.practice7.candy;

import java.util.ArrayList;

import java.util.List;
import java.util.Objects;

/**
 * @author Shvec.
 *
 */
public class Candy implements Comparable<Candy> {

	private String candyname;
	private int energy;

	private CandyType candyType;

	private List<Ingredient> ingredients = new ArrayList<>();

	private Value value;
	private String production;

	public Candy() {
		candyType = new CandyType();
		value = new Value();
		ingredients = new ArrayList<>();

	}

	public int getEnergy() {
		return energy;
	}

	public void setEnergy(int value) {
		this.energy = value;
	}

	public CandyType getCandyType() {
		return candyType;
	}

	public void setCandyType(CandyType value) {
		this.candyType = value;
	}

	public Value getValue() {
		return value;
	}

	public void setValue(Value value) {
		this.value = value;
	}

	public String getProduction() {
		return production;
	}

	public void setProduction(String value) {
		this.production = value;
	}

	public String getCandyname() {
		return candyname;
	}

	public void setCandyname(String candyname) {
		this.candyname = candyname;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public void setIngredient(Ingredient ingredient) {
		ingredients.add(ingredient);
	}

	public List<Ingredient> getIngredients() {
		if (ingredients == null) {
			ingredients = new ArrayList<>();
		}
		return this.ingredients;
	}

	@Override
	public int hashCode() {
		return Objects.hash(candyType, candyname, energy, ingredients, production, value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;

		}
		Candy other = (Candy) obj;
		return Objects.equals(candyType, other.candyType) && Objects.equals(candyname, other.candyname)
				&& energy == other.energy;
	}

	@Override
	public int compareTo(Candy o) {
		int result = candyname.compareTo(o.candyname);
		if (result != 0) {
			return result;
		}

		result = Double.compare(energy, o.energy);
		if (result != 0) {
			return result;
		}

		return 0;
	}

	@Override
	public String toString() {
		return "Candy [candyname=" + candyname + ", energy=" + energy + ", candyType=" + candyType + ", ingredients="
				+ ingredients + ", value=" + value + ", production=" + production + "]";
	}

}
