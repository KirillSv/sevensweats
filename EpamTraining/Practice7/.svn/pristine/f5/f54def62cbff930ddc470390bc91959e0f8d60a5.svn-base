package ua.nure.shvec.practice7.util;

import java.io.FileWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import ua.nure.shvec.practice7.candy.Candy;
import ua.nure.shvec.practice7.candy.Ingredient;
import ua.nure.shvec.practice7.candy.Value;
import ua.nure.shvec.practice7.constants.XMLCandyConstants;

/**
 * @author Shvec
 *
 */
public class MakerObjectFromSaveClass {

	public static List<Object> candyToList(Candy candy) {
		List<Object> outputList = new ArrayList<>();
		outputList.add(candy.getName());
		outputList.add(candy.getEnergy());
		outputList.add(candy.getCandyType());
		outputList.add(candy.getIngredients());

		List<Ingredient> ingredient = candy.getIngredients();
		for (Ingredient i : ingredient) {
			outputList.add(i);
		}

		Value val = candy.getValue();
		outputList.add(val.getProteins());
		outputList.add(val.getFats());
		outputList.add(val.getCarbohydrates());

		outputList.add(candy.getProduction());

		return outputList;
	}

	/**
	 * @param candies
	 * @param path
	 * @throws IOException
	 * @throws XMLStreamException
	 * Forms and write result file for SAX and STAX parsers
	 */
	public static void writerSaxStax(List<Candy> candies, String path) throws IOException, XMLStreamException {

		XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter xmlStrWriter = null;
		try (FileWriter fWriter = new FileWriter(path)) {
			xmlStrWriter = outputFactory.createXMLStreamWriter(fWriter);
			xmlStrWriter.writeStartDocument();
			xmlStrWriter.writeStartElement(XMLCandyConstants.CANDIES);
			xmlStrWriter.writeAttribute("xmlns:tns", "http://www.example.org/input");
			xmlStrWriter.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			xmlStrWriter.writeAttribute("xsi:schemaLocation", "http://www.example.org/input input.xsd");
			for (Candy c : candies) {
				List<Object> listedCandy = MakerObjectFromSaveClass.candyToList(c);

				List<Object> listedIngredient = listedCandy.subList(4, 7);

				List<Object> listedValue = listedCandy.subList(7, listedCandy.size() - 1);
				xmlStrWriter.writeStartElement(XMLCandyConstants.CANDY);

				xmlStrWriter.writeStartElement(XMLCandyConstants.NAME);
				xmlStrWriter.writeCharacters(listedCandy.get(0).toString());
				xmlStrWriter.writeEndElement();

				xmlStrWriter.writeStartElement(XMLCandyConstants.ENERGY);
				xmlStrWriter.writeCharacters(listedCandy.get(1).toString());
				xmlStrWriter.writeEndElement();

				xmlStrWriter.writeStartElement(XMLCandyConstants.TYPE);
				xmlStrWriter.writeCharacters(listedCandy.get(2).toString());
				xmlStrWriter.writeEndElement();

				XMLStreamWriter writer = xmlStrWriter;

				writer.writeStartElement(XMLCandyConstants.INGREDIENTS);
				for (int k = 0; k < listedIngredient.size(); k++) {

					writer.writeStartElement(XMLCandyConstants.INGREDIENT);

					String[] temp = listedIngredient.get(k).toString().split(" ");

					writer.writeStartElement(XMLCandyConstants.NAME);

					writer.writeCharacters(temp[0]);
					writer.writeEndElement();
					writer.writeStartElement(XMLCandyConstants.QUANTITY);

					writer.writeCharacters(temp[1]);
					writer.writeEndElement();

					writer.writeEndElement();
				}
				writer.writeEndElement();

				writeValue(xmlStrWriter, listedValue);

				xmlStrWriter.writeStartElement(XMLCandyConstants.PRODUCTION);
				xmlStrWriter.writeCharacters(listedCandy.get(listedCandy.size() - 1).toString());
				xmlStrWriter.writeEndElement();
				xmlStrWriter.writeEndElement();

			}
			xmlStrWriter.writeEndElement();

		}
	}

	/**
	 * @param write
	 * @param Value
	 * @throws XMLStreamException
	 * form part "Value" for XML
	 */
	private static void writeValue(XMLStreamWriter write, List<Object> Value) throws XMLStreamException {
		write.writeStartElement(XMLCandyConstants.VALUE);
		for (int t = 0; t < Value.size(); t++) {
			write.writeStartElement(XMLCandyConstants.VALUE_ARRAY[t]);
			write.writeCharacters(Value.get(t).toString());
			write.writeEndElement();
		}
		write.writeEndElement();

	}

}
