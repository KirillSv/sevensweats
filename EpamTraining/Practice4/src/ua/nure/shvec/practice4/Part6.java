package ua.nure.shvec.practice4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part6 {
	private static String encoding = "Cp1251";
	private static String fileName = "part6.txt";

	public static void main(String[] args) {

		String fileContent;
		try {
			fileContent = readerFile(fileName, encoding);
			scanConsole(fileContent);
		} catch (FileNotFoundException e) {
			System.out.println("Not found file");
			e.printStackTrace();
		}

	}

	public static void scanConsole(String fileContent) {

		Scanner in = new Scanner(System.in, encoding);

		while (in.hasNext()) {
			String input = in.next();
			String addInput = input.substring(0, 1).toLowerCase(Locale.ENGLISH).concat(input.substring(1, input.length()));
			switch (addInput) {
			case "stop":
				return;
			case "cyrl":
			case "latn":
				printResultInConsole(input, fileContent);
				continue;
			default:
				System.out.println(input + ": " + "Incorrect input");
				continue;
			}

		}
		in.close();

	}

	public static void printResultInConsole(String pattern, String fileContent) {
		System.out.println(analiticFile(pattern, fileContent));
	}

	public static String readerFile(String fileName, String encoding) throws FileNotFoundException {
		StringBuilder sbl = new StringBuilder();
		try (Scanner scan = new Scanner(new FileInputStream(new File(fileName)), encoding)) {

			while (scan.hasNextLine()) {
				sbl.append(scan.nextLine()).append(System.lineSeparator());
			}

		}
		return sbl.toString();
	}

	public static String analiticFile(String language, String fileContent) {

		StringBuilder sbl = new StringBuilder();

		Pattern pattern = Pattern.compile("[^\\s|\\d|\\.|\\-]{1,}");
		Matcher match = pattern.matcher(fileContent);

		while (match.find()) {
			if (defineLanguage(language, match.group())) {
				sbl.append(" " + match.group().trim());
			}
		}
		if (sbl.length() == 0) {
			sbl.append("No such values");
		}
		return language + ": " + sbl.toString().trim();
	}

	private static boolean defineLanguage(String language, String word) {
		StringBuilder sbl = new StringBuilder();
		Pattern pattern = Pattern.compile("[a-zA-Z]+");
		Matcher match = pattern.matcher(word);
		language = language.substring(0, 1).toLowerCase(Locale.ENGLISH)
				.concat(language.substring(1, language.length()));
		while (match.find()) {
			sbl.append(match.group());
		}

		return (("latn".equals(language) && sbl.length() > 0) || ("cyrl".equals(language) && sbl.length() == 0));

	}

}
