package ua.nure.shvec.practice4;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;

public class Part2 {

	public static void main(String[] arg) throws Exception {
		String fileName = "part2.txt";
		String fileNameSecond = "part2_sorted.txt";
		File myFile = new File(fileName);

		if (!myFile.exists()) {

			if (myFile.createNewFile()) {
				System.out.println("File created");
			} else {
				System.out.println("File already exists");
			}

		}

		saveContentInFile(generatorContenta(), fileName);
		saveContentInFile(sortContents(readerFile(fileName)), fileNameSecond);
		System.out.println("Array start: " + readerFile(fileName));
		System.out.println("Array finish: " + Part2.readerFile(fileNameSecond));

	}

	public static void saveContentInFile(int[] content, String fileName) {
		StringBuilder sbl = new StringBuilder();
		for (int i = 0; i < content.length; i++) {
			sbl.append(String.valueOf(content[i] + " "));
		}

		try (FileWriter writer = new FileWriter(fileName, false)) {

			writer.write(sbl.toString().trim());
			writer.flush();

		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}

	public static int[] generatorContenta() {
		SecureRandom random = new SecureRandom();

		int[] array = new int[10];
		final int max = 50;
		final int min = 0;

		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(max);

			while (array[i] < min) {
				array[i] = random.nextInt(max);

			}

		}

		return array;

	}

	public static int[] sortContents(String content) {
		String[] arrayString = content.split(" ");

		int[] arrayInt = new int[arrayString.length];
		int i = 0;
		for (String a : arrayString) {
			arrayInt[i] = Integer.parseInt(a);
			i++;
		}

		for (int x = arrayInt.length - 1; x > 0; x--) {
			for (int j = 0; j < x; j++) {
				if (arrayInt[j] > arrayInt[j + 1]) {
					int tmp = arrayInt[j];
					arrayInt[j] = arrayInt[j + 1];
					arrayInt[j + 1] = tmp;
				}
			}
		}

		return arrayInt;
	}

	public static String readerFile(String fileName) {
		StringBuilder sbl = new StringBuilder();
		try (FileReader reader = new FileReader(fileName)) {
			int letter;
			while ((letter = reader.read()) != -1) {
				sbl.append((char) letter);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return sbl.toString();
	}

}