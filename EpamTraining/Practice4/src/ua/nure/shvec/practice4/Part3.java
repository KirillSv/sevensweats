package ua.nure.shvec.practice4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part3 {
	private static String encoding = "Cp1251";
	private static String fileName = "part3.txt";

	public static void main(String[] args) {

		String fileContent;
		try {
			fileContent = readerFile(fileName, encoding);
			scanConsole(fileContent);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public static void scanConsole(String fileContent) {

		Scanner in = new Scanner(System.in, encoding);
		String pattern = "";

		while (in.hasNext()) {
			String input = in.next();

			switch (input) {
			case "stop":
				return;
			case "String":
				pattern = "[^\\s|\\d|\\.]{2,}";
				printResultInConsole(pattern, fileContent);
				continue;
			case "int":
				pattern = "[^\\.]\\b[0-9]+\\b[^\\.]";
				printResultInConsole(pattern, fileContent);
				continue;
			case "double":
				pattern = "([0-9]+)?[\\.]([0-9]+)?";
				printResultInConsole(pattern, fileContent);
				continue;
			case "char":
				pattern = "\\b[^\\s|\\d|\\.]{1}\\b";
				printResultInConsole(pattern, fileContent);
				continue;
			default:
				System.out.println("Incorrect input");
				continue;
			}

		}
		in.close();

	}

	public static void printResultInConsole(String pattern, String fileContent) {
		System.out.println(analiticFile(pattern, fileContent));
	}

	public static String readerFile(String fileName, String encoding) throws FileNotFoundException {
		StringBuilder sbl = new StringBuilder();
		try (Scanner scan = new Scanner(new FileInputStream(new File(fileName)), encoding)) {

			while (scan.hasNextLine()) {
				sbl.append(scan.nextLine()).append(System.lineSeparator());
			}

		}
		return sbl.toString();
	}

	public static String analiticFile(String patternFromScanner, String fileContent) {

		StringBuilder sbl = new StringBuilder();

		Pattern pattern = Pattern.compile(patternFromScanner);
		Matcher match = pattern.matcher(fileContent);

		while (match.find()) {
			sbl.append(" " + match.group().trim());
		}
		if (sbl.length() == 0) {
			sbl.append("No such values");
		}
		return sbl.toString().trim();
	}

}
