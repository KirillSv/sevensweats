package ua.nure.shvec.practice4;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part1 {
	public static void main(String[] args) throws IOException {
		System.out.println(Part1.runer());
	}

	public static String runer() throws IOException {
		String result = readFile("part1.txt", "Cp1251");
		return findAndReplaceWord(result);
	}

	public static String readFile(String file, String encoding) throws IOException {

		StringBuilder sbl = new StringBuilder();

		try (Scanner sc = new Scanner(new FileInputStream(new File(file)), encoding)) {

			while (sc.hasNextLine()) {
				sbl.append(sc.nextLine()).append(System.lineSeparator());
			}
		}

		return sbl.toString();
	}

	public static String findAndReplaceWord(String input) {

		Pattern pattern = Pattern.compile("(?U)[\\w]+");
		Matcher m = pattern.matcher(input);
		String newWord = "";
		while (m.find()) {
			if (m.group().length() > 3) {
				newWord = invertWord(m.group());
				input = input.replaceAll(m.group(), newWord);
			}

		}
		return input;

	}

	private static String invertWord(String group) {
		StringBuilder sbl = new StringBuilder();

		for (int i = 0; i < group.length(); i++) {

			if (Character.isLowerCase(group.charAt(i))) {
				sbl.append(Character.toUpperCase(group.charAt(i)));
			} else {
				sbl.append(Character.toLowerCase(group.charAt(i)));
			}
		}

		return sbl.toString();
	}

}
