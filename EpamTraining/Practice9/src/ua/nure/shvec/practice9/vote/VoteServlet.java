package ua.nure.shvec.practice9.vote;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/VoteServlet")
public class VoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public VoteServlet() {
		super();
	}

	public void init() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String[] arr = new String[3];
		arr = getParametersFromWebxmlTOContext();

		ServletContext sc = getServletContext();
		sc.getInitParameter("Sportlist");

		HttpSession session = request.getSession();
		String sessionNameUser = (String) session.getAttribute("sessionNameUser");

		if (sessionNameUser == null) {
			session.setAttribute("sessionNameUser", "firstUser");
		}
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<html><head>");
		out.println("<title>Vote Servlet</title>");
		out.println("</head><body>");
		out.println("<form name=\"test\" method=\"get\" action=\"VoteServlet\">");
		out.println("<p><b>���� ���:</b><br>");
		out.println("<input type=\"text\" size=\"40\" name=\"nameUser\"></p>");
		out.println("<p><b>�����:</b><Br>");
		out.println("<input type=\"radio\" name=\"sport\" value=\"" + arr[0] + "\">" + arr[0] + "<Br>");
		out.println("<input type=\"radio\" name=\"sport\" value=\"" + arr[1] + "\">" + arr[1] + "<Br>");
		out.println("<input type=\"radio\" name=\"sport\" value=\"" + arr[2] + "\">" + arr[2] + "<Br>");
		out.println("</p>");
		out.println("<p><input type=\"submit\" value=\"���������\">");
		out.println("<input type=\"reset\" value=\"��������\"></p>");
		out.println("<a href = \"http://localhost:8080/Practice9/\">�� �������</a>");
		out.println("</form></body></html>");

		String userReqest = request.getParameter("nameUser");
		String sportReqest = request.getParameter("sport");

		if (!(userReqest.isEmpty()) && !(sportReqest.isEmpty())) {

			if (findUserinThisSession(userReqest, request)) {
				response.sendRedirect("DoubleVote");
			} else {
				addUserNameToSession(userReqest, request);
				setResultInSource(userReqest, sportReqest);
				response.sendRedirect("Finalable");

			}
		}

	}

	private String[] getParametersFromWebxmlTOContext() {
		ServletContext sc = getServletContext();
		String str = sc.getInitParameter("Sportlist");
		return str.split(" ");
	}

	@SuppressWarnings("unchecked")
	private void addUserNameToSession(String userReqest, HttpServletRequest request) {
		HttpSession session = request.getSession();
		List<String> userSource = new ArrayList<String>();

		if (session.getAttribute("UserSource") == null) {
			session.setAttribute("UserSource", userSource);
		}
		userSource = (ArrayList<String>) session.getAttribute("UserSource");
		userSource.add(userReqest);
		session.setAttribute("UserSource", userSource);

	}

	@SuppressWarnings("unchecked")
	private boolean findUserinThisSession(String userReqest, HttpServletRequest request) {
		HttpSession session = request.getSession();
		List<String> userSource = new ArrayList<String>();

		if (session.getAttribute("UserSource") == null) {
			session.setAttribute("UserSource", userSource);
		}

		userSource = (List<String>) session.getAttribute("UserSource");

		if (userSource.indexOf(userReqest) != -1) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private void setResultInSource(String userReqest, String sportReqest) {
		ServletContext sc = getServletContext();

		HashMap<String, Integer> map = new HashMap<String, Integer>();
		String str = sc.getInitParameter("Sportlist");
		String[] arr = str.split(" ");

		if (sc.getAttribute("FinalMap") == null) {
			sc.setAttribute("FinalMap", map);

			for (String a : arr) {
				map.put(a, 0);
			}

			sc.setAttribute("" + arr[0], "");
			sc.setAttribute("" + arr[1], "");
			sc.setAttribute("" + arr[2], "");

		}

		map = (HashMap<String, Integer>) sc.getAttribute("FinalMap");

		if (map.keySet().contains(sportReqest)) {
			map.put(sportReqest, map.get(sportReqest) + 1);
		} else {
			map.put(sportReqest, 1);
		}

		for (int i = 0; i < arr.length; i++) {
			if (sportReqest.equals(arr[i])) {
				sc.setAttribute("" + arr[i], sc.getAttribute(arr[i]) + ", " + userReqest);
				String temp = (String) sc.getAttribute(arr[i]);
				if (temp.startsWith(", ")) {
					temp = temp.substring(2);
					System.out.print(temp);
					sc.setAttribute(arr[i], temp);
				}
				break;
			}
		}

		sc.setAttribute("FinalMap", map);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}
