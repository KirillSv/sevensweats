package ua.nure.shvec.practice9.vote;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Finalable")
public class Finalable extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext sc = getServletContext();
		String str = sc.getInitParameter("Sportlist");
		String[] arr = str.split(" ");

		HashMap<String, Integer> map = new HashMap<String, Integer>();
		sc = getServletContext();
		map = (HashMap<String, Integer>) sc.getAttribute("FinalMap");

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<html><head>");
		out.println("<title>Result table</title>");
		out.println("</head><body>");
		out.println("<p><b>��������� ������ ����������� �����:</b><br></b><br>");
		out.println("	  <style type=\"text/css\">\r\n" + "   TABLE {\r\n" + "    width: 300px; \r\n"
				+ "	 border-collapse: collapse;\r\n" + "	 text-align:center;\r\n" + "   }\r\n" + "   \r\n"
				+ "   TD{\r\n" + "    padding: 10px;\r\n" + "    border: 1px solid #00FFFF; \r\n" + "   }\r\n"
				+ "   table td:first-child {text-align: left;}\r\n" + "   table td:last-child {text-align: left;}"
				+ "</style>\r\n" + "<table>\r\n" + "<tr height=25>\r\n" + "	        <td width=\"30%\"></td>	   \r\n"
				+ "	        <td width=\"50\"></td>\r\n" + "			<td></td>\r\n" + "	    </tr>\r\n"
				+ "		</table>\r\n" + "	<table>\r\n" + "	    <tr>\r\n" + "	        <td width=\"30%\">" + arr[0]
				+ "</td>\r\n" + "			<td width=\"50\">" + map.get(arr[0]) + "</td>\r\n" + "	        <td>"
				+ sc.getAttribute(arr[0]) + "</td>\r\n" + "	    </tr>\r\n" + "	      <tr>\r\n"
				+ "	        <td width=\"30%\">" + arr[1] + "</td>\r\n" + "			<td width=\"50\">" + map.get(arr[1])
				+ "</td>\r\n" + "	        <td >" + sc.getAttribute(arr[1]) + "</td>\r\n" + "	    </tr>\r\n"
				+ "	     <tr>\r\n" + "	        <td width=\"30%\">" + arr[2] + "</td>\r\n"
				+ "			<td width=\"50\">" + map.get(arr[2]) + "</td>\r\n" + "	        <td>"
				+ sc.getAttribute(arr[2]) + "</td>\r\n" + "	    </tr>\r\n" + "	</table>\r\n" + "	");
		out.println("<br></br>");
		out.println("<a href = \"http://localhost:8080/Practice9/\">�� �������</a>");
		out.println("</body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
