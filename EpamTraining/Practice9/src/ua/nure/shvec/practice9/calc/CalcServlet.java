package ua.nure.shvec.practice9.calc;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/CalcServlet")
public class CalcServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public CalcServlet() {
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter outs = response.getWriter();
		outs.println("<html>");
		outs.println("<head>");
		outs.println("<title>Calculator</title>");
		outs.println("</head>");
		outs.println("<body>");
		try {
			int x = Integer.valueOf(request.getParameter("x"));
			int y = Integer.valueOf(request.getParameter("y"));
			String operation = request.getParameter("oper");
			outs.println("<p>Result: " + calc(x, y, operation) + "</p>");
		} catch (NumberFormatException e) {
			outs.println("<h1>�������� ������ !</h1>");
		} finally {
			outs.println("<a href = \"http://localhost:8080/Practice9/Calc.html\"> ����� </a><br />");
			outs.println("<a href = \"http://localhost:8080/Practice9/\"> �� ������� </a>");
			outs.println("</body>");
			outs.println("</html>");
		}
	}
	
	private int calc(int x, int y, String operation) {
		if ("+".equals(operation)) {
			return x + y;
		} else if ("-".equals(operation)) {
			return x - y;
		} else {
			throw new NumberFormatException();
		}
	}
}
