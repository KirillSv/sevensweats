package ua.nure.shvec.practice3;

import java.security.SecureRandom;

public final class Part1 {
	private Part1() {
	}

	public static String convert1(String input) {
		String[] inputText = input.split(System.lineSeparator());
		StringBuilder sbl = new StringBuilder();
		for (int t = 1; t < inputText.length; t++) {
			String[] str = inputText[t].split(";");
			sbl.append(str[0] + ": " + str[2] + System.lineSeparator());
		}
		return sbl.toString();
	}

	public static String convert2(String input) {
		String[] inputText = input.split(System.lineSeparator());
		StringBuilder sbl = new StringBuilder();
		for (int t = 1; t < inputText.length; t++) {
			String[] str = inputText[t].split("[\\s;]");
			sbl.append(str[2] + " " + str[1] + " (email: " + str[3] + ")" + System.lineSeparator());

		}
		return sbl.toString();

	}

	public static String convert3(String input) {
		String[] inputText = input.split(System.lineSeparator());
		StringBuilder sbl = new StringBuilder();
		String[] mail = new String[inputText.length - 1];

		for (int i = 1; i < inputText.length; i++) {
			String[] a = inputText[i].split("((?U)[^\\w]+)");

			for (int f = 0; f < mail.length; f++) {

				if (mail[f] == null) {
					mail[f] = "";
				}

				if (!(("").equals(mail[f])) && mail[f].contains(a[4])) {
					mail[f] = mail[f] + ", " + a[0];
					break;
				}

				if (("").equals(mail[f])) {

					mail[f] = a[4] + "." + a[5] + " ==> " + a[0];
					break;
				}

			}

		}

		for (int t = 0; t < mail.length; t++) {
			if (mail[t] != null) {
				sbl.append(mail[t] + System.lineSeparator());
			}
		}

		return sbl.toString();
	}

	public static String convert4(String input) {
		StringBuilder sbl = new StringBuilder();
		SecureRandom random = new SecureRandom();
		final int max = 9999;
		final int min = 1000;
		String[] inputText = input.split(System.lineSeparator());

		sbl.append(inputText[0] + ";Password" + System.lineSeparator());

		for (int t = 1; t < inputText.length; t++) {
			int password = random.nextInt(max);

			while (password < min) {

				password = random.nextInt(max);
			}

			inputText[t] = inputText[t] + ";" + password;
			sbl.append(inputText[t] + System.lineSeparator());

		}
		return sbl.toString();

	}

}
