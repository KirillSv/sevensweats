package ua.nure.shvec.practice3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Part3 {
	
	private Part3(){}
	
	public static String convert(String input) {
		StringBuilder sbl = new StringBuilder(input);
		Pattern pattern = Pattern.compile("(?U)[\\W&&[\\d]]?[\\w&&[\\D]]+");
		Matcher match = pattern.matcher(input);
		while (match.find()) {
			if (match.group().length() > 2) {
				if (Character.isUpperCase(sbl.charAt(match.start()))) {
					sbl.setCharAt(match.start(), Character.toLowerCase(sbl.charAt(match.start())));
				} else {
					sbl.setCharAt(match.start(), Character.toUpperCase(sbl.charAt(match.start())));
				}

			}
		}
		return sbl.toString();
	}
}