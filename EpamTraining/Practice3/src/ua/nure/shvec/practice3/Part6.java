package ua.nure.shvec.practice3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Part6 {
	
	private Part6(){}
	
	public static String convert(String input) {
		StringBuilder doubleWords = new StringBuilder();
		String []word = input.split("(?U)[^\\w]+");

		for (String a : word) {
			int count = 0;
			for (String s : word) {
				if (a.equals(s)) {
					count++;
				}
			}
			if (count > 1) {
				doubleWords.append(a + "#");
			}
		}

		word = (doubleWords.toString()).split("#");

		for (String a : word) {
			Pattern pattern = Pattern.compile("\\b" + a + "\\b");
			Matcher matcher = pattern.matcher(input);

			while (matcher.find()) {
				input = matcher.replaceAll("_" + a);
			}
		}
		return input;
	}

}