package ua.nure.shvec.practice3;

public class Demo {

	public static void main(String[] arg) throws Exception {
		
		String fileName = "part1.txt";
		
		System.out.println("convert1");
		System.out.println(Part1.convert1(Util.readFile(fileName)) + System.lineSeparator());
		System.out.println("convert2");
		System.out.println(Part1.convert2(Util.readFile(fileName)) + System.lineSeparator());
		System.out.println("convert3");
		System.out.println(Part1.convert3(Util.readFile(fileName)) + System.lineSeparator());
		System.out.println("convert4");
		System.out.println(Part1.convert4(Util.readFile(fileName)) + System.lineSeparator());

		System.out.println("Part2");
		fileName = "part2.txt";
		System.out.println(Part2.convert(Util.readFile(fileName)));

		System.out.println("Part3");
		fileName = "part3.txt";
		System.out.println(Part3.convert(Util.readFile(fileName)));

		System.out.println("Part4");
		System.out.println(Part4.hash("password", "SHA-256"));

		System.out.println("Part5");
		System.out.println("DECIMAL -decimal2Roman-> ROMAN -roman2Decimal-> DECIMAL");
		System.out.println("-------------------------------------------------------");
		for (int i = 1; i < 101; i++) {
			System.out.println(
					i + " --> " + Part5.decimal2Roman(i) + " --> " + Part5.roman2Decimal(Part5.decimal2Roman(i)));
		}
		System.out.println("-------------------------------------------------------");

		System.out.println("Part6");
		fileName = "part6.txt";
		System.out.println(Part6.convert(Util.readFile(fileName)));
	}

}
