package ua.nure.shvec.practice3;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

public class Part4 {

	public static String hash(String input, String algorithm) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");

		try {
			digest.update(input.getBytes("Cp1251"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		byte[] hashArr = digest.digest();

		StringBuilder finale = new StringBuilder();
		StringBuilder hash = new StringBuilder();

		for (byte bt : hashArr) {
			char everyByte = 0xFF;
			finale.append(Integer.toHexString(everyByte & bt));
			if (finale.length() == 1) {
				hash.append('0');
			}
			hash.append(finale);
			finale.setLength(0);
		}

		return hash.toString().toUpperCase(Locale.ENGLISH);
	}

}