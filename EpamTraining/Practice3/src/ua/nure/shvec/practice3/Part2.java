package ua.nure.shvec.practice3;

public final class Part2 {
	private Part2(){}
	
	public static String convert(String input) {
	
		StringBuilder finaleMax = new StringBuilder();
		StringBuilder finaleMin = new StringBuilder();

		String[] a = null;
		int max = 0;
		int min = Integer.MAX_VALUE;
		a = input.split("[\\s-,']");
		for (int d = 0; d < a.length; d++) {
			if (a[d].length() > max) {
				max = a[d].length();
			}
			if (a[d].length() < min && a[d].length() >= 1) {
				min = a[d].length();
			}
		}

		for (int d = 0; d < a.length; d++) {
			if (a[d].length() == max && !(finaleMax.toString()).contains(a[d])) {
				finaleMax.append(a[d] + ", ");
			}
			if (a[d].length() == min && !finaleMin.toString().contains(a[d])) {
				finaleMin.append(a[d] + ", ");
			}

		}
		String tempMax = finaleMax.toString();
		String temp = tempMax.substring(0, tempMax.length() - 2);

		String tempMin = finaleMin.toString();
		String temp2 = tempMin.substring(0, tempMin.length() - 2);

		return "Min: " + temp2 + "\n" + "Max: " + temp;

	}
}
