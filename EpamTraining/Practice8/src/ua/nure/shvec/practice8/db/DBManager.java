package ua.nure.shvec.practice8.db;

import ua.nure.shvec.practice8.db.entity.Team;
import ua.nure.shvec.practice8.db.entity.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

public class DBManager {
	private Connection con;

	public static Connection getConnection() {
		ResourceBundle r = ResourceBundle.getBundle("app");
		try {
			return DriverManager.getConnection(r.getString("connection.url"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private DBManager() {
		con = getConnection();
	}

	public static DBManager getInstance() {
		return new DBManager();
	}

	public boolean insertUser(User user) {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement("INSERT INTO users VALUES (DEFAULT ,?)", Statement.RETURN_GENERATED_KEYS);
			p.setString(1, user.getLogin());
			if (p.executeUpdate() != 1) {
				return false;
			}
			rs = p.getGeneratedKeys();
			if (rs.next()) {
				user.setId(rs.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("insertUser() error" + e.getMessage());
			return false;
		} finally {
			close(rs);
			close(p);
		}
		return true;
	}

	public boolean insertTeam(Team team) {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement("INSERT INTO teams VALUES (DEFAULT ,?)", Statement.RETURN_GENERATED_KEYS);
			p.setString(1, team.getName());
			if (p.executeUpdate() != 1) {
				return false;
			}
			rs = p.getGeneratedKeys();
			if (rs.next()) {
				team.setId(rs.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("insertTeam() error" + e.getMessage());
			return false;
		} finally {
			close(rs);
			close(p);
		}
		return true;
	}

	public List<User> findAllUsers() {
		Statement s = null;
		ResultSet rs = null;
		List<User> users = new ArrayList<>();
		try {
			s = con.createStatement();
			rs = s.executeQuery("SELECT * FROM users");
			while (rs.next()) {
				User us = new User();
				users.add(us);
				us.setId(rs.getInt(1));
				us.setLogin(rs.getString(2));
			}
		} catch (SQLException e) {
			System.out.println("findAllUsers() error " + e.getMessage());
			return Collections.emptyList();
		} finally {
			close(rs);
			close(s);
		}
		return users;
	}

	public List<Team> findAllTeams() {
		Statement s = null;
		ResultSet rs = null;
		List<Team> teams = new ArrayList<>();
		try {
			s = con.createStatement();
			rs = s.executeQuery("SELECT * FROM teams");
			while (rs.next()) {
				Team t = new Team();
				teams.add(t);
				t.setId(rs.getInt(1));
				t.setName(rs.getString(2));
			}
		} catch (SQLException e) {
			System.out.println("findAllTeams() error" + e.getMessage());
			return Collections.emptyList();
		} finally {
			close(rs);
			close(s);
		}
		return teams;
	}

	public User getUser(String login) {
		PreparedStatement p = null;
		ResultSet rs = null;
		User us = null;
		try {
			p = con.prepareStatement("SELECT * FROM users WHERE login=?");
			p.setString(1, login);
			rs = p.executeQuery();
			if (rs.next()) {
				us = new User();
				us.setId(rs.getInt("id"));
				us.setLogin(rs.getString("login"));
			}
		} catch (SQLException ex) {
			System.out.println("getUser() error");
		} finally {
			close(rs);
			close(p);
		}
		return us;
	}

	public Team getTeam(String name) {
		PreparedStatement p = null;
		ResultSet rs = null;
		Team t = null;
		try {
			p = con.prepareStatement("SELECT * FROM teams WHERE name=?");
			p.setString(1, name);
			rs = p.executeQuery();
			if (rs.next()) {
				t = new Team();
				t.setId(rs.getInt("id"));
				t.setName(rs.getString("name"));
			}
		} catch (SQLException ex) {
			System.out.println("getTeam() error");
		} finally {
			close(rs);
			close(p);
		}
		return t;
	}

	public List<Team> getUserTeams(User user) {
		PreparedStatement p = null;
		ResultSet rs = null;
		List<Team> teams = new ArrayList<>();
		try {
			p = con.prepareStatement("SELECT t.id, t.name FROM users_teams usts JOIN users u ON "
					+ "usts.user_id = u.id JOIN teams t ON usts.team_id = t.id WHERE u.id = ?");
			p.setInt(1, user.getId());
			rs = p.executeQuery();
			while (rs.next()) {
				Team tem = new Team();
				teams.add(tem);
				tem.setId(rs.getInt(1));
				tem.setName(rs.getString(2));
			}
		} catch (SQLException e) {
			System.out.println("getUserTeams() error" + e.getMessage());
			return Collections.emptyList();
		} finally {
			close(rs);
			close(p);
		}
		return teams;
	}

	public boolean setTeamsForUser(User user, Team... teams) {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			con.setAutoCommit(false);
			p = con.prepareStatement("INSERT INTO users_teams VALUES (?, ?)");
			for (Team tm : teams) {
				p.setInt(1, user.getId());
				p.setInt(2, tm.getId());
				p.addBatch();
			}
			p.executeBatch();
			con.commit();
			return true;
		} catch (SQLException ex) {
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} finally {
			close(rs);
			close(p);
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	public boolean deleteTeam(Team team) {
		PreparedStatement p = null;
		try {
			p = con.prepareStatement("DELETE FROM teams WHERE name=?");
			p.setString(1, team.getName());
			if (p.executeUpdate() != 1) {
				return false;
			}
		} catch (SQLException e) {
			System.out.println("deleteTeam() error " + e.getMessage());
			return false;
		} finally {
			close(p);
		}
		return true;
	}

	public boolean updateTeam(Team team) {
		PreparedStatement p = null;
		ResultSet rs = null;
		try {
			p = con.prepareStatement("UPDATE teams SET name=? WHERE id=?");
			p.setString(1, team.getName());
			p.setInt(2, team.getId());
			if (p.executeUpdate() != 1) {
				return false;
			}
		} catch (SQLException e) {
			System.out.println("updateTeam() error" + e.getMessage());
			return false;
		} finally {
			close(rs);
			close(p);
		}
		return true;
	}

	private static void close(ResultSet r) {
		if (r != null) {
			try {
				r.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static void close(Statement s) {
		if (s != null) {
			try {
				s.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
