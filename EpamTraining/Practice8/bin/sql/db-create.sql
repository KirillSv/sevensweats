DROP DATABASE IF EXISTS practic8;

CREATE DATABASE practic8;
USE practic8;
CREATE TABLE `users` (
  `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ;

INSERT INTO `users` VALUES (1,'ivanov');

CREATE TABLE `teams` (
  `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ;

INSERT INTO `teams` VALUES (1,'teamA');

CREATE TABLE `users_teams` (
  `user_id` INTEGER(10) NOT NULL,
  `team_id` INTEGER(10) NOT NULL,
  PRIMARY KEY (`user_id`,`team_id`),
  FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE
) ;

INSERT INTO `users_teams` VALUES (1,1);