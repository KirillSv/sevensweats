package ua.nure.shvec.practice10.task3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.shvec.practice10.db.DBWorker;

/**
 * Servlet implementation class Task3Servlet
 */
@WebServlet("/Task3Servlet")
public class Task3Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<String> userList = new ArrayList<String>();
		
		String name = request.getParameter("userName");

        if(name.length()>1) {
        	
		if (session.getAttribute("names") != null) {

		userList = (List<String>) session.getAttribute("names");
		
		}

		userList.add(name);
		
		session.setAttribute("names", userList);
       }
        
		response.sendRedirect("./task3.jsp");
	}

}
