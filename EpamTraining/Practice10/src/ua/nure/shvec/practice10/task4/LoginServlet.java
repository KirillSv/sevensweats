package ua.nure.shvec.practice10.task4;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ua.nure.shvec.practice10.db.DBWorker;
import ua.nure.shvec.practice10.entity.Role;
import ua.nure.shvec.practice10.entity.User;
import ua.nure.shvec.practice10.exception.MyExceptionLogin;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String login = request.getParameter("login");
		String password = request.getParameter("password");

		if (login != null) {

			User user = new User();
            Role role =new Role();
			DBWorker dBWorker = new DBWorker();
			try {
				user = dBWorker.findUserByLogin(login, password);
				role =  dBWorker.findRoleById(user.getRoleId());
			} catch (MyExceptionLogin e) {
				session.setAttribute("errorMessage", e.getLocalizedMessage());
			}

			if (user.getRoleId() > 0) {
				session.setAttribute("errorMessage", " ");
				session.setAttribute("user", user);
				session.setAttribute("role", role.getName());
				response.sendRedirect("./task3.jsp");
			} else {
				response.sendRedirect("./registration.jsp");
			}
		}

	}

}
