package ua.nure.shvec.practice10.exception;

public class MyExceptionLogin extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public MyExceptionLogin() {

	}

	public MyExceptionLogin(String message) {
		super(message);
	}

	public MyExceptionLogin(Throwable cause) {
		super(cause);
	}

	public MyExceptionLogin(String message, Throwable cause) {
		super(message, cause);
	}

}
