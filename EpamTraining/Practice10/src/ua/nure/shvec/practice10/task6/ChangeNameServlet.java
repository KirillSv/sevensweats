package ua.nure.shvec.practice10.task6;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.shvec.practice10.db.DBWorker;
import ua.nure.shvec.practice10.entity.User;
import ua.nure.shvec.practice10.exception.MyExceptionLogin;

/**
 * Servlet implementation class ChangeNameServlet
 */
@WebServlet("/ChangeNameServlet")
public class ChangeNameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		String newName = request.getParameter("newName");

		DBWorker dBWorker = new DBWorker();
		User user = new User();
		user = (User) session.getAttribute("user");

		String login = user.getLogin();
		try {
			if (!dBWorker.updateUserName(login, newName)) {
				session.setAttribute("ChangeNameMessage", "Name change was successful!");
				user.setName(newName);

				session.setAttribute("user", user);

			} else {
				session.setAttribute("ChangeNameMessage", " ");
			}
		} catch (MyExceptionLogin e) {
			session.setAttribute("errorMessage", e.getLocalizedMessage());
		}
		response.sendRedirect("./changeName.jsp");
	}

}
