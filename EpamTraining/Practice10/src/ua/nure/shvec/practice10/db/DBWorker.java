package ua.nure.shvec.practice10.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import ua.nure.shvec.practice10.entity.Role;
import ua.nure.shvec.practice10.entity.User;
import ua.nure.shvec.practice10.exception.MyExceptionLogin;

public class DBWorker {

	private static final String SELECT_ROLE = "select * from roles where id = ?;";
	private static final String SELECT_USER = "select * from users where login = ? and password = ?;";
	private static final String UPDATE_USER = "update users set name = ? where login = ?;";

	public User findUserByLogin(String login, String password) {
		PreparedStatement st = null;
		Connection con = null;
		try {
			con = Connector.getConnection();
			User user = new User();
			st = con.prepareStatement(SELECT_USER);
			st.setString(1, login);
			st.setString(2, password);
			ResultSet resSet = st.executeQuery();
			if (resSet.next()) {
				user.setId(resSet.getInt("id"));
				user.setLogin(resSet.getString("login"));
				user.setPassword(resSet.getString("password"));
				user.setName(resSet.getString("name"));
				user.setRoleId(resSet.getInt("role_id"));
				return user;
			} else {
				throw new SQLException();
			}
		} catch (SQLException e) {
			throw new MyExceptionLogin("User hasn`t been finded!", e);
		} finally {
			close(st);
		}
	}

	public boolean updateUserName(String login, String newName) {
		PreparedStatement st = null;
		Connection con = null;
		try {
			con = Connector.getConnection();
			st = con.prepareStatement(UPDATE_USER);
			st.setString(1, newName);
			st.setString(2, login);
			return st.execute();
		} catch (SQLException e) {
			throw new MyExceptionLogin("user hasn`t been finded!", e);
		} finally {
			close(st);
		}
	}

	public Role findRoleById(int id) {
		PreparedStatement st = null;
		Connection con = null;
		try {
			con = Connector.getConnection();
			Role role = new Role();
			st = con.prepareStatement(SELECT_ROLE);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				role.setId(rs.getInt("id"));
				role.setName(rs.getString("name"));
				return role;
			} else {
				throw new SQLException();
			}
		} catch (SQLException e) {
			throw new MyExceptionLogin("Role hasn't been finded!", e);
		} finally {
			close(st);
		}
	}

	public void close(Statement statement) {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

}
