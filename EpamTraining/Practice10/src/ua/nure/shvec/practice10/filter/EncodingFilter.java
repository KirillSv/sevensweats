/**
* @author Shvec K.
* @version 1.0
*/
package ua.nure.shvec.practice10.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * EncodingFilter sets UTF-8 encoding.
 * 
 * @author Shvec K.
 * @version 1.0
 */
public class EncodingFilter implements Filter {

	public void destroy() {
	}

	/**
	 * Method sets UTF-8 encoding.
	 * 
	 * @author Shvec K.
	 * @version 1.0
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}