--------------------------------------------------------
-- DB creation script for MySQL
----------------------------------------------------
DROP DATABASE IF EXISTS practice10;
CREATE DATABASE practice10;
USE practice10;
----------------------------------------------------------------
-- users
----------------------------------------------------------------
CREATE TABLE users(
`id` INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
`login` VARCHAR(30) NOT NULL UNIQUE,
`password` VARCHAR(30) NOT NULL,	
`name` VARCHAR(30) NOT NULL,	
`role_id` INTEGER(10) NOT NULL
);

INSERT users value(1, "admin", "12345","Petya", 1);
INSERT users value(2, "юзер", "12345","Вася", 2);
----------------------------------------------------------------
-- roles
----------------------------------------------------------------
CREATE TABLE roles(
`id` INTEGER(10) NOT NULL UNIQUE PRIMARY KEY,
`name` VARCHAR(30) NOT NULL
);
INSERT roles value(1,"admin"); 
INSERT roles value(2, "client");
