<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
</head>
<body>
	<h3>Welcome to Practice 10  (login=admin pass=12345)</h3>
	<form method="post" action="LoginServlet">
		<input name="login" type="text" placeholder="Login"  pattern="[^.]{2,10}" title="min length 2.  max length 10 letters."/><br> 
		 <input name="password" type="password" placeholder="password" pattern="[^.]{2,10}" title="min length 2.  max length 10 letters."/><br>
		<button type="submit" value="reg">Enter</button>
	</form>
	${errorMessage} 
</body>
</html>