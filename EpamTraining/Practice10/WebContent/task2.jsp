<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Task2</title>
</head>
<body>
	<c:set var="i" value="1,2,3,4,5,6,7,8,9" />
	<c:set var="j" value="1,2,3,4,5,6,7,8,9" />

	<table border="1" cellpadding="5" cellspacing=false>
		<tr>
			<td width='20'></td>

			<c:forEach var="i" items="${i}">
				<td align="center" width="20">
					<p>${i}</p>
				</td>
			</c:forEach>

		</tr>
	</table>


	<table border='1' cellpadding='5' cellspacing=false>
		<tr>
			<%
				for (int i = 1; i < 10; i++) {

					out.print("<td align='center' width='20'></b>" + i + "</b></td>");

					for (int j = 1; j < 10; j++) {
						out.print("<td align='center' width='20'></b>" + (i * j) + "</b></td>");
					}
					out.print("<tr>");
				}
			%>
		</tr>
	</table>

</body>
</html>