<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Task4</title>
</head>
<body>
	<div align="right">
		<b>you are logged as ${role} ${user.name} </b>
	</div>

	<%-- HEADER --%>
	<%@ include file="jspf/header.jspf"%>
	<%-- HEADER --%>
	<br>
	<br>

	<form action="ChangeNameServlet" method="post">
		<input type="text" name="newName" pattern="[^.]{2,10}"
			title="min length 2.  max length 10 letters.">
		<button name="newName">NEW NAME :</button>
	</form>
	${ChangeNameMessage}
</body>
</html>