<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Task3</title>
</head>
<body>
	<div align="right">
		<b>you are logged as ${role} ${user.name} </b>
	</div>

	<%-- HEADER --%>
	<%@ include file="jspf/header.jspf"%>
	<%-- HEADER --%>
	<br>
	<br>

	<form action="Task3Servlet" method="post">
		<input type="text" name="userName" pattern="[^.]{2,10}"
			title="min length 1.  max length 10 letters.">
		<button name="userName">Input user name:</button>
	</form>

	<c:forEach var="name" items="${names}">
		<c:out value="${name}" />
		<br>
	</c:forEach>

	<c:if test="${not empty sessionScope.names}">
		<p>
			<a href="Task3CleanerServlet">remove</a>
		</p>
	</c:if>

</body>
</html>