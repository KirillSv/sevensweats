package com.hillel.hw10(Generic&MyList);

public class Main {
	public static void main(String[] args) {

		List<String> a = new List<String>();
		a.addBack("0-Nulevoi");
		a.addBack("1-First");
		a.addBack("2-Two");
		a.addBack("3-Three");
		a.addBack("4-Fourth");
		a.addBack("5-Fifth");
		System.out.println("\r\n" + "size- " + a.sizeList() + "\r\n" + a);

		a.addFront("newFrontElement");
		System.out.println("\r\n" + "size- " + a.sizeList() + "\r\n" + a);

		a.addBack("newBackElement");
		System.out.println("\r\n" + "size- " + a.sizeList() + "\r\n" + a);

		a.addMiddle(3, "newMiddleElement#3");
		System.out.println("\r\n" + "size- " + a.sizeList() + "\r\n" + a);

		
		a.deleteElementFromHead();
		System.out.println("\r\n" + "size- " + a.sizeList() + "\r\n" + a);

		a.deleteElementFromEnd();
		System.out.println("\r\n" + "size- " + a.sizeList() + "\r\n" + a);

		a.deleteElementByIndex(3);
		a.deleteElement("5-Fifth");
		System.out.println("\r\n" + "size- " + a.sizeList() + "\r\n" + a);

		a.interchangeTwoListNodes(0, 3);
		a.interchangeTwoListNodes(3, 0);
		a.interchangeTwoListNodes(3, 2);
		a.interchangeTwoListNodes(1, 4);
		a.interchangeTwoListNodes(10, 5000);

		System.out.println("\r\n" + "size- " + a.sizeList() + "\r\n" + a +" \r\nChangeTwoListNodes");

		System.out.println("\r\n" + "Check on Empty- " + a.checkListOnEmpty());

		System.out.println("\r\nWith other data types also works!  10 balov. THE END :) !");
	}
}