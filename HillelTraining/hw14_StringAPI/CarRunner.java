import cars.*;
public class CarRunner {
	public static void main(String[] args) {

		Car[] cars = { new BossCar("Ferrary", 10000, 18, 8, 250, true),
				new BossCar("Mersedes", 5000, 28, 8, 250, false), 
				new BossCar("Pobeda", 500000, 18, 8, 250, true),
				new ClientCar("Lanos", 5000, 6, 8, 5, 10), 
				new ClientCar("Reno", 1000, 6, 8, 5, 12),
				new ClientCar("Ford", 6000, 6, 8, 5, 8), 
				new SpecificCars("T-34", 7000, 6, 8, 5, true),
				new SpecificCars("IS-7", 9000, 6, 8, 5, false), 
				new TechnicalCar("Kamaz", 5000, 2, 8, 20, 2),
				new TechnicalCar("Ural", 4000, 3, 8, 20, 5) };

		TaxoPark tp = new TaxoPark(cars);
		System.out.println("Cost all taxopark = " + tp.calculateCostAllCar() + " $");

		tp.sortingFuelConsumption();
		System.out.println("\r\n" + "Sorted by fuel consumption taxopark looks like this: ");
		for (int i = 0; i < cars.length; i++)
			System.out.println(cars[i]);

		System.out.println("\r\n" + "The following cars fall in the range of maximum speed from 1 to 50 km / h: ");
		Car maxMinDiapazonGood[] = tp.matchingSpeedRange(1, 50);
		for (int i = 0; i < maxMinDiapazonGood.length; i++) {
			System.out.println(maxMinDiapazonGood[i]);

		}
	}
}
