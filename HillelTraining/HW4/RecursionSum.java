
public class RecursionSum {

	public static int sum(int n) {
		return (n > 1) ? n + sum(n - 1) : n;
	}

	public static void main(String[] args) {
		int n = 5;
		System.out.println("the sum of the number " + n + " = " + RecursionSum.sum(n));
	}
}