
public class Triangle {
	private Point A, B, C;
    private double AB, BC, AC;
 
    public Triangle(Point A, Point B, Point C) {
        this.A = A;
        this.B = B;
        this.C = C;
 
        AB = findSide(A, B);
        BC = findSide(B, C);
        AC = findSide(A, C);
    }
 
    public double findSide(Point first, Point second) {
        return Math.sqrt((second.getX() - first.getX()) * (second.getX() - first.getX())
                + (second.getY() - first.getY()) * (second.getY() - first.getY()));
    }
 
    public double getPerimetr() {
        return AB + BC + AC;
    }
 
    public double getSquare() {
        return Math.sqrt(getPerimetr()/2*(getPerimetr()/2-AB)*(getPerimetr()/2-BC)*(getPerimetr()/2-AC));
    }
 
    public boolean isEquilateral() { // ravnostoronniy
        return Double.compare(AB, BC) == 0 && Double.compare(BC, AC) == 0;
    }
 
    public boolean isIsosceles() { // ravnobedrenniy
        return !isEquilateral() && (Double.compare(AB, BC) == 0 || Double.compare(BC, AC) == 0);
    }
 
    public boolean isRectangular() { // pramougolniy
        return (AB * AB == BC * BC + AC * AC) || (BC * BC == AB * AB + BC * BC) || (AC * AC == AB * AB + BC * BC);
    }
 
    public boolean isArbitrary() { // proizvolniy
        return AB != BC && BC != AC;
    }
 
    public Point getA() {
        return A;
    }
 
    public void setA(Point p) {
        A = p;
    }
 
    public Point getB() {
        return B;
    }
 
    public void setB(Point p) {
        B = p;
    }
 
    public Point getC() {
        return C;
    }
 
    public void setC(Point p) {
        C = p;
    }
 
    public double getSideAB() {
        return AB;
    }
    
    public double getSideBC() {
        return BC;
    }
    
    public double getSideAC() {
        return AC;
    }
 
    public void setSideAB(Point first, Point second) {
        AB = findSide(first, second);
    }
 
    public void setSideBC(Point first, Point second) {
        BC = findSide(first, second);
    }
 
    public void setSideAC(Point first, Point second) {
        AC = findSide(first, second);
    }
}
