
public class StartTriangles {
	public static void main(String[] args) {
		Triangle[] triangles = new Triangle[10];

		triangles[0] = new Triangle(new Point(15, 11), new Point(18, 125), new Point(17, 13));
		triangles[1] = new Triangle(new Point(4, 5), new Point(8, 5), new Point(1, 4));
		triangles[2] = new Triangle(new Point(10, 15), new Point(8, 25), new Point(7, 4));
		triangles[3] = new Triangle(new Point(6, 9), new Point(8, 5), new Point(5, 34));
		triangles[4] = new Triangle(new Point(8, 5), new Point(8, 6), new Point(7, 4));
		triangles[5] = new Triangle(new Point(4, 18), new Point(8, 25), new Point(3, 4));
		triangles[6] = new Triangle(new Point(3, 5), new Point(8, 4), new Point(7, 34));
		triangles[7] = new Triangle(new Point(2, 4), new Point(8, 5), new Point(4, 4));
		triangles[8] = new Triangle(new Point(6, 16), new Point(8, 23), new Point(7, 4));
		triangles[9] = new Triangle(new Point(5, 7), new Point(8, 5), new Point(39, 4));

		double[] minMaxP = countMaxAndMinPerimetr(triangles);
		System.out.printf("minPerimetr=%.2f\n", minMaxP[0]);
		System.out.printf("maxPerimetr=%.2f\n\n", minMaxP[1]);

		double[] minMaxS = countMaxAndMinSqare(triangles);
		System.out.printf("minSquare=%.2f\n", minMaxS[0]);
		System.out.printf("maxSquare=%.2f\n\n", minMaxS[1]);

		int[] typesCount = countDifferentTriangleType(triangles);
		System.out.println("equilateral=" + typesCount[0]);
		System.out.println("isosceles=" + typesCount[1]);
		System.out.println("rectangular=" + typesCount[2]);
		System.out.println("arbitrary=" + typesCount[3]);
	}

	public static double[] countMaxAndMinPerimetr(Triangle[] triangles) {
		double min = 0;
		double max = 0;

		for (Triangle triangle : triangles) {
			if (triangle.getPerimetr() > max) {
				max = triangle.getPerimetr();
			}
			if (triangle.getPerimetr() < min) {
				min = triangle.getPerimetr();
			}
		}

		return new double[] { min, max };
	}

	public static double[] countMaxAndMinSqare(Triangle[] triangles) {
		double min = 0;
		double max = 0;

		for (Triangle triangle : triangles) {
			if (triangle.getSquare() > max) {
				max = triangle.getSquare();
			}
			if (triangle.getSquare() < min) {

			}
			min = triangle.getSquare();
		}

		return new double[] { min, max };
	}

	public static int[] countDifferentTriangleType(Triangle[] triangles) {
		int equilateral = 0;
		int isosceles = 0;
		int rectangular = 0;
		int arbitrary = 0;

		for (Triangle triangle : triangles) {
			if (triangle.isEquilateral())
				equilateral++;
			if (triangle.isIsosceles())
				isosceles++;
			if (triangle.isRectangular())
				rectangular++;
			if (triangle.isArbitrary())
				arbitrary++;
		}

		return new int[] { equilateral, isosceles, rectangular, arbitrary };
	}
}
