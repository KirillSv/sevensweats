package com.hillel;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbContainer.DBWorker;
import com.hillel.film.Film;

public class FindFilmsForCurrentAndPreviousYear extends HttpServlet {
	private static final long serialVersionUID = 2L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("FindFilmsForCurrentAndPreviousYear.jsp").forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DBWorker dbWorker = new DBWorker();
		List<Film> listFilmsForCurrentAndPreviousYear = dbWorker.findFilmsForCurrentAndPreviousYear();
		request.setAttribute("listFilmsForCurrentAndPreviousYear", listFilmsForCurrentAndPreviousYear);
		request.getRequestDispatcher("FindFilmsForCurrentAndPreviousYear.jsp").forward(request, response);
	}

}
