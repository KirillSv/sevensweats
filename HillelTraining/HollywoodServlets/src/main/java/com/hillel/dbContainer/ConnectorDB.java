package com.hillel.dbContainer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectorDB {
    public static Connection getConnection() throws SQLException {

        String url = "jdbc:mysql://localhost:3306/hollywood?allowPublicKeyRetrieval=true&useSSL=false";

        Properties prop = new Properties();
        prop.put("user", "root");
        prop.put("password", "123456");
        prop.put("useSSL", "false");
        prop.put("serverTimezone", "UTC");
        prop.put("characterEncoding", "UTF-8");
        return DriverManager.getConnection(url, prop);

    }

}
