package com.hillel;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbContainer.DBWorker;

public class DeleteFilmsOlderGivenYears extends HttpServlet {
	private static final long serialVersionUID = 6L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("DeleteFilmsOlderGivenYears.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int year = Integer.valueOf(request.getParameter("year"));

		DBWorker dbWorker = new DBWorker();
		boolean success = dbWorker.deleteFilmsOlderGivenYears(year);
		request.setAttribute("success", success);
		request.getRequestDispatcher("DeleteFilmsOlderGivenYears.jsp").forward(request, response);
	}

}
