package com.hillel;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbContainer.DBWorker;
import com.hillel.person.Actor;

public class FindActorsForNFilms extends HttpServlet {
	private static final long serialVersionUID = 4L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("FindActorsForNFilms.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int count = Integer.valueOf(request.getParameter("count"));

		DBWorker dbWorker = new DBWorker();
		List<Actor> listActorsForNFilms = dbWorker.findActorsForNFilms(count);
		request.setAttribute("listActorsForNFilms", listActorsForNFilms);
		request.getRequestDispatcher("FindActorsForNFilms.jsp").forward(request, response);
	}

}
