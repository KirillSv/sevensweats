package com.hillel;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbContainer.DBWorker;
import com.hillel.person.Actor;

public class FindActorsForFilm extends HttpServlet {
	private static final long serialVersionUID = 5L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("FindActorsForFilm.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String filmName = request.getParameter("filmName");

		DBWorker dbWorker = new DBWorker();
		List<Actor> listActorsFromSpecificFilm = dbWorker.findActorsForFilm(filmName);
		request.setAttribute("listActorsFromSpecificFilm", listActorsFromSpecificFilm);
		request.getRequestDispatcher("FindActorsForFilm.jsp").forward(request, response);
	}

}
