package com.hillel;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbContainer.DBWorker;
import com.hillel.person.Actor;

public class FindActorsAsDirector extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBWorker dbWorker = new DBWorker();
		List<Actor> listActorsAsDirector=dbWorker.findActorsAsDirector();
		request.setAttribute("listActorsAsDirector", listActorsAsDirector);
		request.getRequestDispatcher("FindActorsAsDirector.jsp").forward(request, response);
	}

}
