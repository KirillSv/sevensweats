<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Delete films older given year</title>
</head>
<body>
<body>
	Delete films older given year:
	<br>
	<br>

	<form name="Form1" method="post"
		action="http://localhost:8080/DeleteFilmsOlderGivenYears">
		<table>
			<tr>
				<td class="text">Input year:</td>
				<td><input name="year" size="25"></td>
			</tr>
		</table>
		<input type=submit value=" Go! "> <br>
	</form>
	<br>


	<c:choose>
		<c:when test="${success = true}">
        Process of removing older films has passed  successful!    
    </c:when>
		<c:otherwise>
       Process of removing ERROR. Try again. May be later....
    </c:otherwise>
	</c:choose>

</body>
</body>
</html>