<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Find Films for current and previous year</title>
</head>


<body>
    <br>
<c:forEach var="film" items="${listFilmsForCurrentAndPreviousYear}">
      ${film} <br>
</c:forEach>

</body>
</html>