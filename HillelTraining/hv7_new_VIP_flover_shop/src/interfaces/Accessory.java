package interfaces;

public class Accessory {
	private String name;
	private int price;

	public Accessory(String name, int price) {
		this.setName(name);
		this.setPrice(price);

	}

	@Override
	public String toString() {
		setName(getName() + " price- " + getPrice() + "$");
		return getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
