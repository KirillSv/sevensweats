package interfaces;

public class Flover {
	private String name;
	private int price;
	private int stemLength;
	private int daysFresh;

	public Flover(String name, int price, int stemLength, int daysFresh) {
		this.name = name;
		this.price = price;
		this.stemLength = stemLength;
		this.daysFresh = daysFresh;
	}

	@Override
	public String toString() {
		setName(getName() + " price- " + getPrice() + "$ " + " Stem length- " + getStemLength() + "cm " + " DaysFresh- "
				+ getDaysFresh());
		return getName();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getStemLength() {
		return stemLength;
	}

	public void setStemLength(int stemLength) {
		this.stemLength = stemLength;
	}

	public int getDaysFresh() {
		return daysFresh;
	}

	public void setDaysFresh(int daysFresh) {
		this.daysFresh = daysFresh;
	}
}
