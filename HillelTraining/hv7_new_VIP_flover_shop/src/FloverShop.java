package com.hillel.hv7_new_VIP_flover_shop.src;

import java.util.Scanner;
import exceptions.MyPersonalException;
import processors.BouquetMaker;

public class FloverShop {
	private static Scanner sc;

	public static void main(String[] args) {
		BouquetMaker bouquetMaker = new BouquetMaker();
		menuStart1(bouquetMaker);

	}

	public static void menuStart1(BouquetMaker bouquetMaker) {
		System.out.println("\n1. Collect a bouquet\r\n" + "2. Print a bouquet and perform operations\r\n" + "0.Exit ");
		boolean notExit = true;
		while (notExit) {
			int ur = userChoice();
			switch (ur) {
			case 1:
				menuBouquetFolding2(bouquetMaker);
				break;
			case 2:
				bouquetMaker.makingFinalBouquets();
				menuBouquetOperations3(bouquetMaker);
				break;
			case 0:
				notExit = false;
				System.out.println("Hasta la vista, baby!");
				break;
			}
		}
	}

	public static void menuBouquetFolding2(BouquetMaker bouquetMaker) {
		System.out.println("" + "\r\n 1. Enter flovers " + "\r\n 2. Enter accessorys " + "\r\n 0. Exit ");
		while (true) {
			int ur = userChoice();
			switch (ur) {
			case 1:
				try {
					bouquetMaker.flowerMaker();
				} catch (java.util.InputMismatchException e) {
					System.out.println(
							"Horrible! You have entered something wrong and an error has occurred! Enter again!");
				}
				menuBouquetFolding2(bouquetMaker);
				break;
			case 2:
				try {
					bouquetMaker.accessorysMaker();
				} catch (java.util.InputMismatchException e) {
					System.out.println(
							"Horrible! You have entered something wrong and an error has occurred! Enter again!");
				}
				menuBouquetFolding2(bouquetMaker);
				break;
			case 0:
				menuStart1(bouquetMaker);
				break;
			}
		}
	}

	public static void menuBouquetOperations3(BouquetMaker bouquetMaker) {
		System.out.println("\r\n" + "1. Fresh sorting \r\n" + "2. Choose flowers by stem length \r\n" + "0.Exit ");
		while (true) {
			int ur = userChoice();
			switch (ur) {
			case 1:
				bouquetMaker.sortBouquetFreshness();
				break;
			case 2:

				try {
					bouquetMaker.findFlowerRangeStemLengths();
				} catch (MyPersonalException e) {
					System.out.println("A terrible critical error! The length of the stem can't  be negative!");

				}

			case 0:
				menuStart1(bouquetMaker);
				break;
			}
		}
	}

	public static int userChoice() {
		sc = new Scanner(System.in);
		int scan = sc.nextInt();
		return scan;
	}

}
