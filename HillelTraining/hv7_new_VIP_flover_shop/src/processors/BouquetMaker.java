package processors;

import java.util.Scanner;

import exceptions.MyPersonalException;
import interfaces.Accessory;
import interfaces.Flover;

public class BouquetMaker {
	private static Scanner sc;
	Flover[] result;
	Flover[] flovers = new Flover[10];
	Accessory[] accessorys = new Accessory[10];

	public void flowerMaker() throws java.util.InputMismatchException {
		sc = new Scanner(System.in);
		int i = 0;

		System.out.println("add Flover?  1- Yes     0 - Exit ");
		int exit = sc.nextInt();
		while (exit == 1) {

			System.out.println("Enter Flover name: ");
			String name = sc.next();
			System.out.println("Enter Flover price: ");
			int price = sc.nextInt();
			System.out.println("Enter Flover stem length: ");
			int stemLength = sc.nextInt();
			System.out.println("Enter Flover daysFresh: ");
			int daysFresh = sc.nextInt();

			if (flovers[i] == null) {
				flovers[i] = new Flover(name, price, stemLength, daysFresh);
				i++;
			} else
				i++;
			System.out.println("add more Flover?  1- Yes     0 - Exit ");
			exit = sc.nextInt();
		}
	}

	public void accessorysMaker() throws java.util.InputMismatchException {
		sc = new Scanner(System.in);
		int i = 0;

		System.out.println("add accessory?  1- Yes     0 - Exit ");
		int exit = sc.nextInt();
		while (exit == 1) {

			System.out.println("Enter accessory name: ");
			String name = sc.next();
			System.out.println("Enter accessory price: ");
			int price = sc.nextInt();
			if (accessorys[i] == null) {
				accessorys[i] = new Accessory(name, price);
				i++;
			} else
				i++;
			System.out.println("add more accessorys?  1- Yes     0 - Exit ");
			exit = sc.nextInt();

		}
	}

	public void makingFinalBouquets() {
		int tempArrIndex = 0;
		int count = 0;
		int floversCost = 0;
		int accessorysCost = 0;
		for (int n = 0; n < flovers.length; n++) {
			if (flovers[n] != null) {
				tempArrIndex++;
			}
		}
		result = new Flover[tempArrIndex];

		for (int k = 0; k < result.length; k++) {
			if (flovers[k] != null) {
				result[count++] = flovers[k];
			}
		}

		System.out.println("\r\n" + "Your bouquet is ready:");

		for (int i = 0; i < result.length; i++) {
			System.out.println(result[i]);
			floversCost += result[i].getPrice();
		}

		for (int k = 0; k < accessorys.length; k++) {
			if (accessorys[k] != null) {
				System.out.println(accessorys[k]);
				accessorysCost = accessorysCost + accessorys[k].getPrice();
			}
		}

		System.out.println("All cost flovers and accessorys =  " + (floversCost + accessorysCost) + "$");
	}

	public void sortBouquetFreshness() {
		Flover temp;
		for (int i = 0; i <= result.length; i++) {
			for (int j = i + 1; j < result.length; j++) {
				if (result[j].getDaysFresh() < result[i].getDaysFresh()) {
					temp = result[i];
					result[i] = result[j];
					result[j] = temp;
				}
			}
		}
		for (int i = 0; i < result.length; i++) {
			System.out.println(result[i]);
		}
	}

	public void findFlowerRangeStemLengths() throws MyPersonalException {
		sc = new Scanner(System.in);
		System.out.println("\r\n" + "Enter the minimum length of the stem: ");
		int min = sc.nextInt();
		if (min < 0)
			throw new MyPersonalException();
		System.out.println("\r\n" + "Enter the maximum length of the stem: ");
		int max = sc.nextInt();
		if (max < 0)
			throw new MyPersonalException();

		for (int i = 0; i < result.length; i++) {
			if (min <= result[i].getStemLength() && max >= result[i].getStemLength()) {
				System.out.println(result[i]);
			}
		}

	}
}
