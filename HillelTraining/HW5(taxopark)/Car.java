
public class Car {
	public static void main(String[] args) {

		Taxopark[] car = new Taxopark[10];

		car[0] = new BossCar("Ferrary", 10000, 18, 8, 250, true);
		car[1] = new BossCar("Mersedes", 5000, 28, 8, 250, false);
		car[2] = new BossCar("Pobeda", 500000, 18, 8, 250, true);
		car[3] = new ClientCar("Lanos", 5000, 6, 8, 5, 10);
		car[4] = new ClientCar("Reno", 1000, 6, 8, 5, 12);
		car[5] = new ClientCar("Ford", 6000, 6, 8, 5, 8);
		car[6] = new ClientCar("Ford", 7000, 6, 8, 5, 8);
		car[7] = new ClientCar("Opel", 9000, 6, 8, 5, 5);
		car[8] = new TechnicalCar("Kamaz", 5000, 2, 8, 20, 2);
		car[9] = new TechnicalCar("Ural", 4000, 3, 8, 20, 5);

		System.out.println("Cost all taxopark = " + Processor.calculateCostAllCar(car) + " $");

		Processor.sortingFuelConsumption(car);
		System.out.println("\r\n" + "Sorted by fuel consumption taxopark looks like this: ");
		for (int i = 0; i < car.length; i++)
			System.out.println(car[i]);

		System.out.println("\r\n" + "The following cars fall in the range of maximum speed from 1 to 50 km / h: ");
		Taxopark maxMinDiapazonGood[] = Processor.matchingSpeedRange(car, 1, 50);
		for (int i = 0; i < maxMinDiapazonGood.length; i++) {
			System.out.println(maxMinDiapazonGood[i]);
		}
	}
}
