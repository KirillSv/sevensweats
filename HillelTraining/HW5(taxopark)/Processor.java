public class Processor {

	public static double calculateCostAllCar(Taxopark[] car) {
		double commonPrice = 0;
		for (int i = 0; i < car.length; i++) {
			commonPrice = commonPrice + car[i].priceOfAuto;

		}
		return commonPrice;
	}

	public static void sortingFuelConsumption(Taxopark[] car) {
		Taxopark temp;
		for (int i = 0; i < car.length; i++) {
			for (int j = i + 1; j < car.length; j++) {
				if (car[j].fuelConsumption < car[i].fuelConsumption) {
					temp = car[i];
					car[i] = car[j];
					car[j] = temp;
				}
			}
		}
	}

	public static Taxopark[] matchingSpeedRange(Taxopark[] cars, int min, int max) {
		int count = 0, index = 0;
		for (Taxopark car : cars) {
			if (min <= car.speedMin && max >= car.speedMax) {
				count++;
			}
		}
		Taxopark[] result = new Taxopark[count];
		for (Taxopark car : cars) {
			if (min <= car.speedMin && max >= car.speedMax) {
				result[index++] = car;
			}
		}
		return result;
	}

}
