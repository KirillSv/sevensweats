
class Taxopark {
	int priceOfAuto;
	int speedMax;
	int speedMin;
	double fuelConsumption;
	String model;
	String name;

	Taxopark(String model, int priceOfAuto, double fuelConsumption, int speedMin, int speedMax) {
		this.model = model;
		this.priceOfAuto = priceOfAuto;
		this.fuelConsumption = fuelConsumption;
		this.speedMin = speedMin;
		this.speedMax = speedMax;
	}

}

class BossCar extends Taxopark {
	boolean insurance;

	public BossCar(String model, int priceOfAuto, double fuelConsumption, int speedMin, int speedMax,
			boolean insurance) {
		super(model, priceOfAuto, fuelConsumption, speedMin, speedMax);
		this.insurance = insurance;
	}

	@Override
	public String toString() {
		name = " This is a private car our dear boss! " + model + " " + priceOfAuto + " $ " + " Fuel Consumption = "
				+ fuelConsumption + " Max speed= " + speedMax + " Min speed= " + speedMin + " Insurance:" + insurance;
		return name;
	}
}

class ClientCar extends Taxopark {
	int capacity;

	public ClientCar(String model, int priceOfAuto, double fuelConsumption, int speedMin, int speedMax, int capacity) {
		super(model, priceOfAuto, fuelConsumption, speedMin, speedMax);
		this.capacity = capacity;
	}

	@Override
	public String toString() {
		name = " This is a client car                 " + model + " " + priceOfAuto + " $ " + " Fuel Consumption = "
				+ fuelConsumption + " Max speed= " + speedMax + " Min speed= " + speedMin + " capacity- " + capacity
				+ " persons";
		return name;
	}
}

class TechnicalCar extends Taxopark {
	int loadCapacity;

	TechnicalCar(String model, int priceOfAuto, double fuelConsumption, int speedMin, int speedMax, int loadCapacity) {
		super(model, priceOfAuto, fuelConsumption, speedMin, speedMax);
		this.loadCapacity = loadCapacity;
	}

	@Override
	public String toString() {
		name = " This is a technical service machine  " + model + " " + priceOfAuto + " $ " + " Fuel Consumption = "
				+ fuelConsumption + " Max speed= " + speedMax + " Min speed= " + speedMin + " load capacity- "
				+ loadCapacity + " tons";
		return name;
	}

}
