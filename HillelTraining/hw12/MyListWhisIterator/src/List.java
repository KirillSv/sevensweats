package com.hillel.hw12.MyListWhisIterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

class Node<E> {
    E data;
    Node<E> next;

    @Override
    public String toString() {
        return (String) data;
    }

}

class List<E> implements Iterable<E> {
    private Node<E> head;

    void addFront(E data) {
        Node<E> a = new Node<E>();
        a.data = data;

        if (head == null) {
            head = a;

        } else {
            a.next = head;
            head = a;
        }
    }

    void addMiddle(int index, E data) {
        Node<E> a = new Node<E>();

        if (index > (sizeList() - 1)) {
            index = (sizeList());
        }
        if (index == 0) {
            addFront((E) data);
            return;
        }
        a.data = data;
        if (head == null) {
            head = a;
        } else {
            Node<E> t = head;
            for (int i = 0;; i++) {

                if (i == index - 1) {

                    Node<E> temp = t.next;
                    t.next = a;
                    a.next = temp;
                    return;

                }
                t = t.next;
            }
        }
    }

    void addBack(E data) {
        Node<E> a = new Node<E>();
        a.data = data;
        Node<E> t = head;

        if (head == null) {
            head = a;
            return;
        }
        while (t != null) {

            if (t.next == null) {
                t.next = a;
                return;
            }
            t = t.next;
        }

    }

    void deleteElementFromHead() {
        head = head.next;
    }

    void deleteElementByIndex(int index) {
        Node<E> t = head;
        if (index > (sizeList() - 1)) {
            System.out.println("Index " + index + "too big. ");
        }
        if (index == 0) {
            head = head.next;
            return;
        }

        for (int i = 0;; i++) {

            if (i == index - 1) {
                t.next = t.next.next;
                return;
            }
            t = t.next;

        }
    }

    void deleteElement(E data) {
        if (head == null)
            return;

        if (head.data == data) {
            head = head.next;
            return;
        }
        Node<E> t = head;
        while (t.next != null) {

            if (t.next.data == data) {
                t.next = t.next.next;
                return;
            }
            t = t.next;
        }
    }

    void deleteElementFromEnd() {
        Node<?> t = head;
        while (t != null) {
            if (t.next.next == null) {
                t.next = null;
                return;
            }
            t = t.next;

        }

    }

    void interchangeTwoListNodes(int x, int y) {
        Node<E> prevNode1 = null, prevNode2 = null, node1 = head, node2 = head;
        if (x > (sizeList() - 1) || y > (sizeList() - 1)) {
            System.out.println("Index too big.");
            return;
        }
        if (head == null || (x == y)) {
            System.out.println("Index is wrong.");
            return;
        }

        if (y == 0 || (y < x)) {
            int temp = x;
            x = y;
            y = temp;
        }

        if (x != 0) {
            prevNode1 = lookingForNodePrevThatNeedsChanged(x);
            node1 = prevNode1.next;
            prevNode2 = lookingForNodePrevThatNeedsChanged(y);
            node2 = prevNode2.next;
        } else {
            prevNode2 = lookingForNodePrevThatNeedsChanged(y);
            node2 = prevNode2.next;
        }

        if (prevNode1 != null) {
            prevNode1.next = node2;
        } else
            head = node2;

        prevNode2.next = node1;

        Node<E> temp = node1.next;
        node1.next = node2.next;
        node2.next = temp;
    }

    Node<E> lookingForNodePrevThatNeedsChanged(int x) {
        Node<E> t = head;
        for (int i = 0;; i++) {
            if (i == (x - 1)) {
                return t;
            } else
                t = t.next;
        }
    }

    int sizeList() {
        Node<E> b = head;

        for (int i = 0;; i++) {
            if (b != null) {
                b = b.next;

            } else
                return i;
        }
    }

    boolean checkListOnEmpty() {
        return head == null;
    }

    @Override
    public String toString() {
        Node<E> t = head;
        String text = "";
        while (t != null) {
            text += t.data + "\r\n";
            t = t.next;
        }
        return text;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyListIterator();
    }

    class MyListIterator implements Iterator<E> {
        Node<E> current = null;

        public boolean hasNext() {
            if (current == null && head != null) {

                return true;
            } else if (current != null) {
                return current.next != null;
            }
            return false;
        }

   
        public E next() {
            if (current == null && head != null) {
                current = head;
                return head.data;
            } else if (current != null) {
              
                current = current.next;
                return current.data;
            }
            throw new NoSuchElementException();
        }
    }
}
