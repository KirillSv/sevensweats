package com.hillel.hw12.ReversStringInFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ReadFile {
    private static BufferedReader br;

    public static void main(String[] args) throws IOException {
        ArrayList<String> arr = new ArrayList<String>();

        File someFile = new File("src/textFile");
        if (!someFile.exists()) {
            someFile.createNewFile();

        }

        try {
            FileInputStream fstream = new FileInputStream("src/textFile");
            br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                arr.add(strLine);
            }

            try (FileWriter writer = new FileWriter("src/ReversStringFile", true)) {

                for (int j = arr.size() - 1; j >= 0; j--) {
                    writer.write(arr.get(j) + "\r\n");

                }

            }
            br.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}