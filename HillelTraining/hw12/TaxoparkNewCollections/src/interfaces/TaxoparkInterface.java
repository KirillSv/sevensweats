package interfaces;

import java.util.ArrayList;

import cars.*;

public interface TaxoparkInterface {

    public double calculateCostAllCar();

    public void sortingFuelConsumption();

    public ArrayList<Car> matchingSpeedRange(int min, int max);

    public ArrayList<Car> getCars();

    public void setCars(ArrayList<Car> cars);

    public String toString();
}
