package interfaces;

public interface CarInterface {
    public int getPriceOfAuto();

    public void setPriceOfAuto(int priceOfAuto);
}