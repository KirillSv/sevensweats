package cars;

public class BossCar extends Car {
    private boolean insurance;

    public BossCar(String model, int priceOfAuto, double fuelConsumption, int speedMin, int speedMax,
            boolean insurance) {
        super(model, priceOfAuto, fuelConsumption, speedMin, speedMax);
        this.insurance = insurance;
    }

    public boolean getInsurance() {
        return insurance;
    }

    public void insurance(boolean insurance) {
        this.insurance = insurance;
    }

    @Override
    public String toString() {
        setName(" This is a private car our dear boss! " + getModel() + " " + getpriceOfAuto() + " $ "
                + " Fuel Consumption = " + getFuelConsumption() + " Max speed= " + getSpeedMax() + " Min speed= "
                + getSpeedMin() + " Insurance:" + getInsurance());
        return getName();
    }
}
