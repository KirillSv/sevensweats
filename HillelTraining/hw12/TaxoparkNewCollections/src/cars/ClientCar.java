package cars;

public class ClientCar extends Car {
    private int capacity;

    public ClientCar(String model, int priceOfAuto, double fuelConsumption, int speedMin, int speedMax, int capacity) {
        super(model, priceOfAuto, fuelConsumption, speedMin, speedMax);
        this.capacity = capacity;
    }

    public int getLoadCapacity() {
        return capacity;
    }

    public void setLoadCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        setName(" This is a client car                 " + getModel() + " " + getpriceOfAuto() + " $ "
                + " Fuel Consumption = " + getFuelConsumption() + " Max speed= " + getSpeedMax() + " Min speed= "
                + getSpeedMin() + " capacity- " + getLoadCapacity() + " persons");
        return getName();
    }
}
