package com.hillel.hw12.TaxoparkNewCollections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cars.*;
import interfaces.*;

public class TaxoPark implements TaxoparkInterface {

    private ArrayList<Car> cars;

    public TaxoPark(ArrayList<Car> cars) {
        this.cars = cars;
    }

    public double calculateCostAllCar() {
        double commonPrice = 0;
        for (int i = 0; i < cars.size(); i++) {
            commonPrice = commonPrice + cars.get(i).getpriceOfAuto();

        }
        return commonPrice;
    }

    public void sortingFuelConsumption() {
        Collections.sort(cars, new Comparator<Car>() {
            public int compare(Car cars, Car cars1) {
                return (int) (cars.getFuelConsumption() - cars1.getFuelConsumption());
            }
        });
    }

    public ArrayList<Car> matchingSpeedRange(int min, int max) {

        ArrayList<Car> result = new ArrayList<Car>();
        for (Car car : cars) {
            if (min <= car.getSpeedMin() && max >= car.getSpeedMax()) {
                result.add(car);
            }
        }
        return result;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        String result = "";

        for (Car car : cars) {
            result += (car.toString() + "\n");
        }

        return result;
    }

}
