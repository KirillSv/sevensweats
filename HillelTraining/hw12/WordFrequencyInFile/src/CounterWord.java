package com.hillel.hw12.WordFrequencyInFile;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

public class CounterWord {

    public static void main(String[] args) throws Exception {
        List<Character> list = new ArrayList<Character>();

        FileReader fr = new FileReader(new File("src/texter"));
        int ch;
        while ((ch = fr.read()) != -1) {
            Character temp = new Character((char) ch);
            list.add(temp);
        }
        fr.close();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == '.' || list.get(i) == ',' || list.get(i) == '!' || list.get(i) == '') {
                list.remove(i);
            }
        }

        String textWithautPoints = "";
        for (int i = 0; i < list.size(); i++) {
            textWithautPoints += list.get(i);

        }

        textWithautPoints = textWithautPoints.replaceAll("\n|\r\n|\b|\t|\f|\r|\n\r", " ");
        String[] splitWords = textWithautPoints.split(" ");

        HashMap<String, Integer> map = new HashMap<>();

        for (int i = 0; i < splitWords.length; i++) { //    
            if (map.get(splitWords[i]) == null) {
                map.put(splitWords[i], 1);
            } else
                map.put(splitWords[i], map.get(splitWords[i]) + 1);
        }

        for (Entry<String, Integer> entry : map.entrySet()) { //   
            System.out.println(entry.getKey() + "- " + entry.getValue());
        }

    }

}
