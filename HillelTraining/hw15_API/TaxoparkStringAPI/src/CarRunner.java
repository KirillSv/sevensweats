
import java.util.ArrayList;
import java.util.List;

import cars.*;

public class CarRunner {
    public static void main(String[] args) {

        ArrayList<Car> cars = new ArrayList<Car>();

        cars.add(new BossCar("Ferrary", 10000, 18, 8, 250, true));
        cars.add(new BossCar("Mersedes", 5000, 28, 8, 250, false));
        cars.add(new BossCar("Pobeda", 500000, 18, 8, 250, true));
        cars.add(new ClientCar("Lanos", 5000, 6, 8, 5, 10));
        cars.add(new ClientCar("Reno", 1000, 6, 8, 5, 12));
        cars.add(new ClientCar("Ford", 6000, 6, 8, 5, 8));
        cars.add(new SpecificCars("T-34", 7000, 6, 8, 5, true));
        cars.add(new SpecificCars("IS-7", 9000, 6, 8, 5, false));
        cars.add(new TechnicalCar("Kamaz", 5000, 2, 8, 20, 2));
        cars.add(new TechnicalCar("Ural", 4000, 3, 8, 20, 5));

        TaxoPark tp = new TaxoPark(cars);
        System.out.println("Cost all taxopark = " + tp.calculateCostAllCar() + " $");

        System.out.println("\r\n" + "Sorted by fuel consumption taxopark looks like this: ");
        tp.sortingFuelConsumption();

        System.out.println("\r\n" + "The following cars fall in the range of maximum speed from 1 to 50 km / h: ");
        List<Car> result = tp.matchingSpeedRange(1, 50);
        for (Car i : result) {
            System.out.println(i);

        }
    }
}
