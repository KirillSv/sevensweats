package interfaces;

import java.util.ArrayList;
import java.util.List;
import cars.*;

public interface TaxoparkInterface {

    public double calculateCostAllCar();

    public void sortingFuelConsumption();

    public List<Car> matchingSpeedRange(int min, int max);

    public List<Car> getCars();

    public void setCars(ArrayList<Car> cars);

    public String toString();
}
