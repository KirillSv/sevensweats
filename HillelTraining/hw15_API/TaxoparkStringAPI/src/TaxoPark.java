import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cars.*;
import interfaces.*;

public class TaxoPark implements TaxoparkInterface {

    private List<Car> cars;

    public TaxoPark(ArrayList<Car> cars) {
        this.cars = cars;
    }

    public double calculateCostAllCar() {
        return cars.stream().mapToInt(cars -> cars.getpriceOfAuto()).sum();

//#2 
// return cars.stream()
//        .reduce(0, (sum, cars) -> sum +=cars.getpriceOfAuto(), (sum1, sum2) -> sum1 +sum2);        
    }

    public void sortingFuelConsumption() {
        cars.stream().sorted((a1, a2) -> a1.getFuelConsumption() - a2.getFuelConsumption()).collect(Collectors.toList())
                .forEach(System.out::println);

    }

    public List<Car> matchingSpeedRange(int min, int max) {

        return cars.stream().filter(car -> (min <= car.getSpeedMin() && max >= car.getSpeedMax()))
                .collect(Collectors.toList());

    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        String result = "";

        for (Car car : cars) {
            result += (car.toString() + "\n");
        }

        return result;
    }

}
