package cars;

import interfaces.CarInterface;

public class SpecificCars extends Car implements CarInterface {
    private boolean machineGun;

    public SpecificCars(String model, int priceOfAuto, int fuelConsumption, int speedMin, int speedMax,
            boolean machineGun) {
        super(model, priceOfAuto, fuelConsumption, speedMin, speedMax);
        this.machineGun = machineGun;
    }

    public boolean getmachineGun() {
        return machineGun;
    }

    public void setmachineGun(boolean machineGun) {
        this.machineGun = machineGun;
    }

    @Override
    public String toString() {
        setName(" This is a Specific Car                 " + getModel() + " " + getPriceOfAuto() + " $ "
                + " Fuel Consumption = " + getFuelConsumption() + " Max speed= " + getSpeedMax() + " Min speed= "
                + getSpeedMin() + " machine gun " + machineGun);
        return getName();
    }

    @Override
    public int getPriceOfAuto() {
        return 10;
    }

    @Override
    public void setPriceOfAuto(int priceOfAuto) {
    }

}
