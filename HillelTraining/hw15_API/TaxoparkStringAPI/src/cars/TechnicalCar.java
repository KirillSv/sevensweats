package cars;

public class TechnicalCar extends Car {
    private int loadCapacity;

    public TechnicalCar(String model, int priceOfAuto, int fuelConsumption, int speedMin, int speedMax,
            int loadCapacity) {
        super(model, priceOfAuto, fuelConsumption, speedMin, speedMax);
        this.loadCapacity = loadCapacity;
    }

    public int getLoadCapacity() {
        return loadCapacity;
    }

    public void setLoadCapacity(int loadCapacity) {
        this.loadCapacity = loadCapacity;
    }

    @Override
    public String toString() {
        setName(" This is a technical service machine  " + getModel() + " " + getpriceOfAuto() + " $ "
                + " Fuel Consumption = " + getFuelConsumption() + " Max speed= " + getSpeedMax() + " Min speed= "
                + getSpeedMin() + " load capacity- " + getLoadCapacity() + " tons");
        return getName();
    }

}