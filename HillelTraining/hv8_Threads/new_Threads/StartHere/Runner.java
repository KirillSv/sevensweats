package new_Threads.StartHere;

import java.util.Arrays;

import new_Threads.PrimeNumberFinderThread.PrimeNumberFinderThread;
import new_Threads.PrimeNumbersStorage.PrimeNumbersStorage;

public class Runner {

	public static void main(String[] args) {

		int minNumber = 1;
		int maxNumber = 100;
		int quantityStreams = 5;
		PrimeNumbersStorage storage = new PrimeNumbersStorage();
		Thread[] threads = new Thread[quantityStreams];

		// System.out.println("You are welcomed by the most powerful computing device of
		// our time."
		// + "\n Enter the minimum range number");
		// int minNumber = userChoice();
		// System.out.println("\n Enter the maximum range number");
		// int maxNumber = userChoice();
		// .out.println("\n Enter the number of threads: ");
		// int quantityStreams = userChoice();

		// int[] commonArr = new int[maxNumber];
		// System.out.println(
		// "\n Attention! Below you can observe the solemn conclusion of a single final
		// array of all primes of the entire specified range:");

		// Runnable[] r = new JThread[quantityStreams];

		int[] coordinate = new int[quantityStreams * 2];
		for (int i = 0; i < coordinate.length; i = i + 2) {
			if (i == 0) {
				coordinate[i] = minNumber;
				coordinate[i + 1] = minNumber + (int) ((maxNumber - minNumber) / quantityStreams);
			} else {
				coordinate[i] = coordinate[i - 1] + 1;
				coordinate[i + 1] = coordinate[i - 1] + (int) ((maxNumber - minNumber) / quantityStreams);
			}
		}
		coordinate[quantityStreams * 2 - 1] = maxNumber;

		for (int i = 0; i < quantityStreams; i++) {
			if (i == 0) {
				threads[i] = new Thread(new PrimeNumberFinderThread(coordinate[i], coordinate[i + 1], storage));
			} else {
				threads[i] = new Thread(new PrimeNumberFinderThread(coordinate[i * 2], coordinate[i * 2 + 1], storage));
			}
		}

		for (Thread thread : threads) {
			thread.start();
		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println(Arrays.toString(storage.getNumbers()));

	}

}
