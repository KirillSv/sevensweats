package new_Threads.PrimeNumberFinderThread;

import new_Threads.PrimeNumbersStorage.PrimeNumbersStorage;

public class PrimeNumberFinderThread implements Runnable {

	private int minNumber;
	private int maxNumber;
	private PrimeNumbersStorage storage;

	public PrimeNumberFinderThread(int minNumber, int maxNumber, PrimeNumbersStorage storage) {
		this.minNumber = minNumber;
		this.maxNumber = maxNumber;
		this.storage = storage;
	}

	@Override
	public void run() {
		int[] findedPrimeNumbers = {};
		int[] tempArr = new int[getMaxNumber()];
		int count = 0;

		for (int n = getMinNumber(); n <= getMaxNumber(); n++) {
			boolean isPrime = true;
			for (int i = 2; i < n; i++) {
				if ((n % i) == 0) {
					isPrime = false;
				}
			}
			if (isPrime & n > 1) {

				tempArr[count] = n;
				count++;

			} else
				System.out.print("");

		}

		findedPrimeNumbers = new int[count];

		for (int temp = 0; temp < count; temp++) {
			findedPrimeNumbers[temp] = tempArr[temp];
		}

		getStorage().addNumbers(findedPrimeNumbers);

	}

	public int getMinNumber() {
		return minNumber;
	}

	public void setMinNumber(int minNumber) {
		this.minNumber = minNumber;
	}

	public int getMaxNumber() {
		return maxNumber;
	}

	public void setMaxNumber(int maxNumber) {
		this.maxNumber = maxNumber;
	}

	public PrimeNumbersStorage getStorage() {
		return storage;
	}

	public void setStorage(PrimeNumbersStorage storage) {
		this.storage = storage;
	}

}
