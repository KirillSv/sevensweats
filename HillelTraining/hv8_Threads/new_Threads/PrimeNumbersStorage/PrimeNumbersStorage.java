package new_Threads.PrimeNumbersStorage;

public class PrimeNumbersStorage {
	private int[] numbers = new int[0];

	public synchronized void addNumbers(int[] numbersToAdd) {

		if (getNumbers().length == 0) {
			numbers = new int[numbersToAdd.length];
			for (int i = 0; i < numbersToAdd.length; i++) {

				numbers[i] = numbersToAdd[i];
			}
		} else {

			int[] temp = new int[getNumbers().length];
			for (int n = 0; n < getNumbers().length; n++) {
				temp[n] = numbers[n];
			}

			int newSizeNumbersArr = numbersToAdd.length + getNumbers().length;

			numbers = new int[newSizeNumbersArr];
			for (int n = 0; n < temp.length; n++) {
				numbers[n] = temp[n];
			}

			for (int k = temp.length; k < newSizeNumbersArr; k++) {
				numbers[k] = numbersToAdd[k - temp.length];
			}
		}

	}

	public int[] getNumbers() {
		return numbers;
	}

	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}
}
