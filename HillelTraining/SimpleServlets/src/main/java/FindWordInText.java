import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet("/FindWordInText")
public class FindWordInText extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		Enumeration<String> e = request.getParameterNames();
		String pname = (String) e.nextElement();
		String word = request.getParameter(pname);

		BufferedReader br = new BufferedReader(
				new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("/text")));
		int ch;
		String originalText = "";
		while ((ch = br.read()) != -1) {
			Character temp = new Character((char) ch);
			originalText = originalText + temp;
		}
		br.close();
		pw.println(originalText);
		pw.println();

		Pattern pat = Pattern.compile("\\b" + word + "\\b");
		Matcher mat = pat.matcher(originalText);
		int countRepetitions = 0;
		while (mat.find()) {

			countRepetitions++;
		}
		pw.println("Count repetitions " + word + " " + countRepetitions);

		pw.close();
	}
}