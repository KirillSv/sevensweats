import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

@WebServlet("/SortArrayServlet")
public class SortArrayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		Enumeration<String> b = request.getParameterNames();
		String pvalue = null;
		String pname = (String) b.nextElement();
		pvalue = request.getParameter(pname);

		String[] num = pvalue.split(",");

		for (int i = num.length - 1; i > 0; i--) {
			for (int j = 0; j < i; j++) {
				if (Integer.parseInt(num[j]) > Integer.parseInt(num[j + 1])) {
					String temp = num[j];
					num[j] = num[j + 1];
					num[j + 1] = temp;
				}
			}
		}

		for (String n : num) {
			pw.print(n + " ");
		}

		pw.close();
	}
}