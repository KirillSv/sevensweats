package interfaces;
import cars.*;

public interface TaxoparkInterface {

	public double calculateCostAllCar();

	public void sortingFuelConsumption();

	public Car[] matchingSpeedRange(int min, int max);

	public Car[] getCars();

	public void setCars(Car[] cars);

	public String toString();
}
