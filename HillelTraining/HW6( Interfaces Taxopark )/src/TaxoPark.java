import cars.*;
import interfaces.*;
public class TaxoPark implements TaxoparkInterface {

	private Car[] cars;

	public TaxoPark(Car[] cars) {
		this.cars = cars;
	}

	public double calculateCostAllCar() {
		double commonPrice = 0;
		for (int i = 0; i < cars.length; i++) {
			commonPrice = commonPrice + cars[i].getpriceOfAuto();

		}
		return commonPrice;
	}

	public void sortingFuelConsumption() {
		Car temp;
		for (int i = 0; i < cars.length; i++) {
			for (int j = i + 1; j < cars.length; j++) {
				if (cars[j].getFuelConsumption() < cars[i].getFuelConsumption()) {
					temp = cars[i];
					cars[i] = cars[j];
					cars[j] = temp;
				}
			}
		}
	}

	public Car[] matchingSpeedRange(int min, int max) {
		int count = 0, index = 0;
		for (Car car : cars) {
			if (min <= car.getSpeedMin() && max >= car.getSpeedMax()) {
				count++;
			}
		}
		Car[] result = new Car[count];
		for (Car car : cars) {
			if (min <= car.getSpeedMin() && max >= car.getSpeedMax()) {
				result[index++] = car;
			}
		}
		return result;
	}

	public Car[] getCars() {
		return cars;
	}

	public void setCars(Car[] cars) {
		this.cars = cars;
	}

	@Override
	public String toString() {
		String result = "";

		for (Car car : cars) {
			result += (car.toString() + "\n");
		}

		return result;
	}

}
