package cars;
public class Car {
	private int priceOfAuto;
	private double fuelConsumption;
	private int speedMax;
	private int speedMin;
	private String model;
	private String name;

	public Car(String model, int priceOfAuto, double fuelConsumption, int speedMin, int speedMax) {
		this.priceOfAuto = priceOfAuto;
		this.fuelConsumption = fuelConsumption;
		this.speedMax = speedMax;
		this.speedMin = speedMin;
		this.model = model;
	}

	public int getpriceOfAuto() {
		return priceOfAuto;
	}

	public void setpriceOfAuto(int priceOfAuto) {
		this.priceOfAuto = priceOfAuto;
	}

	public double getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(double fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public int getSpeedMax() {
		return speedMax;
	}

	public void setSpeedMax(int speedMax) {
		this.speedMax = speedMax;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpeedMin() {
		return speedMin;
	}

	public void setSpeedMin(int speedMin) {
		this.speedMin = speedMin;
	}

}