import java.sql.SQLException;

import dbContainer.DBWorker;

public class MainStarter {
    public static void main(String[] args) throws SQLException {

        DBWorker dbWorker = new DBWorker();

        dbWorker.findFilmsForCurrentAndPreviousYear();
        dbWorker.findActorsAsDirector();
        dbWorker.findActorsForFilm("Return of Budulay");
        dbWorker.findActorsForNFilms(2);
        dbWorker.deleteFilmsOlderGivenYears(1900);

    }
}
