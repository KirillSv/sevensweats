package comparator;

import java.util.Comparator;

import cars.Car;


public class CarFuelConsumptionBestComparator implements Comparator<Car>{

	@Override
	public int compare(Car o1, Car o2) {
		
		return (int) (o1.getFuelConsumption() - o2.getFuelConsumption());
	}

}
