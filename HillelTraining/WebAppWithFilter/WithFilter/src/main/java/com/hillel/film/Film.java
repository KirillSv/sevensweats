package com.hillel.film;

import java.util.List;

import com.hillel.person.Actor;
import com.hillel.person.Director;

public class Film {
    private int id;
    private String title;
    private Director director;
    private List<Actor> actors;
    private String date_release;
    private String country;

    public Film(int id, String title, List<Actor> actors, Director director, String date_release, String country) {
        this.id = id;
        this.title = title;
        this.actors = actors;
        this.director = director;
        this.date_release = date_release;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate_release() {
        return date_release;
    }

    public void setDate_release(String date_release) {
        this.date_release = date_release;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return id + " '" + title + "' " + "director: "+ director.getName() + " " +"date_release: " +date_release + " "+"country: " + country;
    }
}