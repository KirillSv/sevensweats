package com.hillel.person;

public abstract class Person {
    private int id;
    private String name;
    private String date;

    public Person(int id, String name, String date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName_of_director(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "id: " + id +  " name: " + name + " date_of_birth: " + date;
    }
}
