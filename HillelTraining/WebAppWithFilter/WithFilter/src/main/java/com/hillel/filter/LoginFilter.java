package com.hillel.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.hillel.dbcontainer.DBWorker;

public class LoginFilter implements Filter {
	
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.getRequestDispatcher("jsp/login.jsp").forward(request, response);		
    	chain.doFilter(request, response);

	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

	public void destroy() {
	}

}
