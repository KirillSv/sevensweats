package com.hillel.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbcontainer.DBWorker;
import com.hillel.film.Film;

@WebServlet("/FindFilmsForCurrentAndPreviousYear")
public class FindFilmsForCurrentAndPreviousYearServlet extends HttpServlet {
	private static final long serialVersionUID = 2L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("findFilmsForCurrentAndPreviousYear.jsp").forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DBWorker dbWorker = new DBWorker();
		List<Film> listFilmsForCurrentAndPreviousYear = dbWorker.findFilmsForCurrentAndPreviousYear();
		request.setAttribute("listFilmsForCurrentAndPreviousYear", listFilmsForCurrentAndPreviousYear);
		request.getRequestDispatcher("jsp/findFilmsForCurrentAndPreviousYear.jsp").forward(request, response);
	}

}
