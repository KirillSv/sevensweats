package com.hillel.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbcontainer.DBWorker;
import com.hillel.person.Actor;

@WebServlet("/FindActorsAsDirector")
public class FindActorsAsDirectorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DBWorker dbWorker = new DBWorker();
		List<Actor> listActorsAsDirector=dbWorker.findActorsAsDirector();
		request.setAttribute("listActorsAsDirector", listActorsAsDirector);
		request.getRequestDispatcher("jsp/findActorsAsDirector.jsp").forward(request, response);
	}

}
