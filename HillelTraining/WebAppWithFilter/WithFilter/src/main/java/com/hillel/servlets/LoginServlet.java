package com.hillel.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbcontainer.DBWorker;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 18L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		DBWorker dbWorker = new DBWorker();
		String loginner = dbWorker.loginner(username, password);

		if (loginner.equals("admin")) {
			request.getRequestDispatcher("jsp/deleteFilmsOlderGivenYears.jsp").forward(request, response);
		} else {
			request.setAttribute("errorMessage", "Try again!");
			request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
		}
	}

}
