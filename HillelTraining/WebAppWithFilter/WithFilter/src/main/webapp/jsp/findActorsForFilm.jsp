<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Find actors from this film</title>
</head>
<body>
	<c:import url="/style/style.css"></c:import>
	<c:import url="/style/header.jspf"></c:import>
	<div class='notebook'>
		Find actors from this film: <br> <br>

		<form name="Form1" method="post" action="FindActorsForFilm">
			<table>
				<tr>
					<td class="text">Input film name (Gremlins):</td>
					<td><input name="filmName" size="25"></td>
				</tr>
			</table>
			<input type=submit value=" Go! "> <br> <br>
		</form>

		<c:forEach var="actor" items="${listActorsFromSpecificFilm}">
			<br> ${actor} 
    </c:forEach>
	</div>
</body>
</html>