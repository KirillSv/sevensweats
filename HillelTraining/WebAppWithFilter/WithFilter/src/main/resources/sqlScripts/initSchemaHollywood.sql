
DROP TABLE IF EXISTS `films`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `films` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `director` int(11) NOT NULL,
  `date_release` date NOT NULL,
  `country` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
INSERT INTO `films` VALUES (1,'Star Wars',
1,'1981-01-01','USA'),(2,'Aliens',1,'1985-02-01','USA'),
(3,'Brave Heart',2,'2019-02-01','USA'),
(6,'Cheburachka',3,'2018-02-01','Mongolia'),
(7,'Snow White and the Seven Dwarfs',4,'2019-12-01','Germany'),
(8,'Return of Budulay',5,'1950-12-10','USSR'),
(9,'Gremlins',1,'1986-02-01','USA');

DROP TABLE IF EXISTS `actor`;
CREATE TABLE `actor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `name_of_birth_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `actor` VALUES 
(1,'Chubaka','5019-12-12'),
(2,'Ripli Smallboobs','1719-10-05'),
(3,'William Wallace','1619-03-08'),
(4,'Mel Gibson','1974-02-12'),
(5,'Hors William','1625-05-10'),
(6,'Arnold Schwarzneiger','1987-02-12'),
(7,'Angelina Jolie','1960-03-15'),
(8,'Elena Berkova','1981-12-22'),
(9,'Louis de FunГЁs','1960-10-10'),
(10,'R2D2','5022-09-12'),
(11,'Mister Bine','1970-03-22'),
(12,'Four Gnoms','1932-10-12'),
(13,'Budulay Budulaevich','1965-04-02'),
(14,'Hors Budulaevna','1919-03-01'),
(15,'Dog Budulaevich','1956-02-03'),
(16,'Gypsy Camp','1900-12-10');


DROP TABLE IF EXISTS `participation`;
CREATE TABLE `participation` (
  `id_actor` int(11) DEFAULT NULL,
  `id_film` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `participation` VALUES (1,1),(1,9),(2,2),(3,3),(4,3),(5,3),(6,6),(7,6),(8,6),(9,6),(9,7),(10,7),(10,1),(11,7),(12,1),(12,9),(13,8),(14,8),(15,8),(16,8);




DROP TABLE IF EXISTS `director`;
CREATE TABLE `director` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director`
--

LOCK TABLES `director` WRITE;
/*!40000 ALTER TABLE `director` DISABLE KEYS */;
INSERT INTO `director` VALUES (1,'Stiven Spilberg','1969-10-05'),(2,'Mel Gibson','1974-02-12'),(3,'Timur Bekmambetov','1974-02-12'),(4,'Pierre Woodman','1984-05-10'),(5,'Chif Gypsy','1964-02-12');
/*!40000 ALTER TABLE `director` ENABLE KEYS */;



DROP TABLE IF EXISTS `authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `authorization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorization`
--

LOCK TABLES `authorization` WRITE;
/*!40000 ALTER TABLE `authorization` DISABLE KEYS */;
INSERT INTO `authorization` VALUES (1,'1','1','admin'),(2,'2','2','user'),(3,'3','3','Hassaina prisol!!!');
/*!40000 ALTER TABLE `authorization` ENABLE KEYS */;
