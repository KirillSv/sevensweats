package com.hillel.dbcontainer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.hillel.entity.Role;
import javax.naming.NamingException;

import com.hillel.entity.User;
import com.hillel.film.Film;
import com.hillel.person.Actor;
import com.hillel.person.Director;

public class DBWorker implements com.hillel.homelibrary.HomeLibrarySource {

	public User getUser(String naming) throws NamingException {
		User user= null;
		Connection cn = null;
		try {
			cn = ConnectorDB.getConnection();
			Statement st = null;
			try {
				st = cn.createStatement();
				ResultSet rs = null;
				try {
					rs = st.executeQuery("SELECT * FROM hollywood.authorization WHERE login='" + naming + "'");

					while (rs.next()) {
						int id = rs.getInt(1);
						String login = rs.getString(2);
						String password = rs.getString(3);
						Role role = Role.valueOf(rs.getString(4));
						user = new User(id, login, password, role);

					}
					return user;
				} finally {
					if (rs != null) {
						rs.close();
					} else {
						System.err.println("error while reading DB");
					}
				}
			} finally {
				if (st != null) {
					st.close();
				} else {
					System.err.println("Statement was not created");
				}
			}
		} catch (SQLException e) {
			System.err.println("DB connection error: " + e);
		} finally {
			if (cn != null) {
				try {
					cn.close();
				} catch (SQLException e) {
					System.err.println("Сonnection close error: " + e);
				}
			}
		}
		
		return user;
	}

	public List<Actor> findActorsForFilm(String filmName) {
		List<Actor> listActorsFromSpecificFilm = new ArrayList<Actor>();
		String request = "SELECT id, name, data FROM hollywood.actor"
				+ " JOIN participation ON actor.id = participation.id_actor "
				+ "AND participation.id_film= (SELECT id FROM films WHERE title =?)";
		Connection connect = null;
		ResultSet rs = null;
		PreparedStatement preparedStatement = null;
		try {
			connect = ConnectorDB.getConnection();
			preparedStatement = connect.prepareStatement(request);
			preparedStatement.setString(1, filmName);
			rs = preparedStatement.executeQuery();

			while (rs.next()) {
				listActorsFromSpecificFilm.add(new Actor(rs.getInt(1), rs.getString(2), rs.getString(3)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		close(rs, connect, preparedStatement);

		return listActorsFromSpecificFilm;
	}

	public List<Actor> findActorsForNFilms(int filmsCount) {
		List<Actor> listActorsForNFilms = new ArrayList<Actor>();
		String request = "SELECT id, name, data from actor a JOIN participation p on a.id= p.id_actor "
				+ "GROUP BY id_actor HAVING count(id_film)=?";
		Connection connect = null;
		ResultSet rs = null;
		PreparedStatement preparedStatement = null;
		try {
			connect = ConnectorDB.getConnection();
			preparedStatement = connect.prepareStatement(request);
			preparedStatement.setInt(1, filmsCount);
			rs = preparedStatement.executeQuery();

			while (rs.next()) {
				listActorsForNFilms.add(new Actor(rs.getInt(1), rs.getString(2), rs.getString(3)));
			}

		} catch (SQLException error) {
			error.printStackTrace();
		}

		close(rs, connect, preparedStatement);

		return listActorsForNFilms;
	}

	public boolean deleteFilmsOlderGivenYears(int givenYears) {
		String request = "DELETE FROM films WHERE year(date_release)< " + givenYears;
		Connection connect = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try {
			connect = ConnectorDB.getConnection();
			preparedStatement = connect.prepareStatement(request);
			preparedStatement.executeUpdate(request);
		} catch (SQLException error) {
			error.printStackTrace();
			close(rs, connect, preparedStatement);
			return false;

		}
		close(rs, connect, preparedStatement);
		return true;
	}

	public List<Actor> findActorsAsDirector() {
		List<Actor> listActorsAsDirector = new ArrayList<Actor>();

		Connection connect = null;
		ResultSet rs = null;

		try {
			connect = ConnectorDB.getConnection();
			Statement st = connect.createStatement();
			rs = st.executeQuery("SELECT actor.id, actor.name, actor.data FROM actor, director "
					+ "WHERE actor.name = director.name and actor.data= director.data");
			while (rs.next()) {
				listActorsAsDirector.add(new Actor(rs.getInt(1), rs.getString(2), rs.getString(3)));

			}

		} catch (SQLException error) {
			error.printStackTrace();
		}

		close(rs, connect, null);
		return listActorsAsDirector;

	}

	public List<Film> findFilmsForCurrentAndPreviousYear() {
		List<Film> listFilmsForCurrentAndPreviousYear = new ArrayList<Film>();
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		String request = "SELECT f.id, title, director, date_release, country, d.id, d.name, d.data "
				+ "FROM films f JOIN director d ON YEAR(f.date_release)=? OR YEAR(f.date_release)=? "
				+ "HAVING director = d.id";
		Connection connect = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try {
			connect = ConnectorDB.getConnection();
			preparedStatement = connect.prepareStatement(request);
			preparedStatement.setInt(1, year);
			preparedStatement.setInt(2, year - 1);
			rs = preparedStatement.executeQuery();
			while (rs.next()) {

				int id = rs.getInt(1);
				String title = rs.getString(2);
				Director director = new Director(rs.getInt(6), rs.getString(7), rs.getString(8));
				String date_release = rs.getString(4);
				String country = rs.getString(5);

				listFilmsForCurrentAndPreviousYear.add(new Film(id, title, null, director, date_release, country));

			}

			addActorsInFilm(listFilmsForCurrentAndPreviousYear);

		} catch (SQLException error) {
			error.printStackTrace();
		}
		close(rs, connect, preparedStatement);

		return listFilmsForCurrentAndPreviousYear;

	}

	void addActorsInFilm(List<Film> film) {

		for (Film a : film) {
			List<Actor> listActorInThisFilm = new ArrayList<Actor>();

			String request = "Select a.id, a.name, a.data from participation JOIN actor a "
					+ "WHERE id_film=? and id_actor=id";
			Connection connect = null;
			PreparedStatement preparedStatement = null;
			ResultSet rs = null;
			try {
				connect = ConnectorDB.getConnection();
				preparedStatement = connect.prepareStatement(request);
				preparedStatement.setInt(1, a.getId());
				rs = preparedStatement.executeQuery();
				while (rs.next()) {
					listActorInThisFilm.add(new Actor(rs.getInt(1), rs.getString(2), rs.getString(3)));

				}

				a.setActors(listActorInThisFilm);

			} catch (SQLException error) {
				error.printStackTrace();
			}
			close(rs, connect, preparedStatement);
		}
	}

	private void close(ResultSet rs, Connection cn, PreparedStatement preparedStatement) {
		closeResultSet(rs);
		closeConnection(cn);
		closePreparedStatement(preparedStatement);
	}

	private void closeResultSet(ResultSet rs) {
		if (rs == null) {
			return;
		}

		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void closePreparedStatement(PreparedStatement preparedStatement) {
		if (preparedStatement == null) {
			return;
		}

		try {
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void closeConnection(Connection cn) {
		if (cn == null) {
			return;
		}

		try {
			cn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
