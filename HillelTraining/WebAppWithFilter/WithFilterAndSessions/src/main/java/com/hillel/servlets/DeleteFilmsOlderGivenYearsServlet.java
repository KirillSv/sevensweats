package com.hillel.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbcontainer.DBWorker;

@WebServlet("/DeleteFilmsOlderGivenYears")
public class DeleteFilmsOlderGivenYearsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/deleteFilmsOlder.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
try {
		
		
		int year = Integer.valueOf(request.getParameter("year"));
		
		DBWorker dbWorker = new DBWorker();
		boolean success = dbWorker.deleteFilmsOlderGivenYears(year);
		request.setAttribute("success", success);
		request.getRequestDispatcher("jsp/deleteFilmsOlder.jsp").forward(request, response);
	}
catch (NumberFormatException e) {	
	request.getRequestDispatcher("jsp/deleteFilmsOlder.jsp").forward(request, response);
}	
	}

}
