package com.hillel.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbcontainer.DBWorker;
import com.hillel.person.Actor;

@WebServlet("/FindActorsForNFilms")
public class FindActorsForNFilmsServlet extends HttpServlet {
	private static final long serialVersionUID = 4L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/findActorsForNFilms.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int count = Integer.valueOf(request.getParameter("count"));

			DBWorker dbWorker = new DBWorker();
			List<Actor> listActorsForNFilms = dbWorker.findActorsForNFilms(count);
			request.setAttribute("listActorsForNFilms", listActorsForNFilms);
			request.getRequestDispatcher("jsp/findActorsForNFilms.jsp").forward(request, response);
		} catch (NumberFormatException e) {
			request.getRequestDispatcher("jsp/findActorsForNFilms.jsp").forward(request, response);
		}
	}

}
