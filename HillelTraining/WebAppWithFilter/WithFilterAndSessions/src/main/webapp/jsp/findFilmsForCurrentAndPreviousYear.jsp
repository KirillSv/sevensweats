<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Find Films for current and previous year</title>
</head>


<body>
	<c:import url="/style/style.css"></c:import>
	<c:import url="/style/header.jspf"></c:import>
	<div class='notebook'>
		<br>
		<c:forEach var="film" items="${listFilmsForCurrentAndPreviousYear}">
      ${film} <br>
			<br>
		</c:forEach>
	</div>
</body>
</html>