<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Delete films older given year</title>
</head>
<body>
	<c:import url="/style/style.css"></c:import>
	<c:import url="/style/header.jspf"></c:import>
	<div class='notebook'>
		Delete films older given year: <br> <br>
		<form name="Form1" method="post" action="DeleteFilmsOlderGivenYears">
			<table>
				<tr>
					<td class="text">Input year:</td>
					<td><input name="year" size="25"></td>
				</tr>
			</table>
			<input type=submit value=" Go! "> <br>
		</form>
		<br> ${ success == true ? "Erase was successful!" : " "}

	</div>
</body>
</html>