
public class Car {
	int id, year, price;
	String brand, model, colour, regNumber, car[];

	public Car(int id, String brand, String model, int year, String colour, int price, String regNumber) {
		this.id = id;
		this.brand = brand;
		this.model = model;
		this.year = year;
		this.colour = colour;
		this.price = price;
		this.regNumber = regNumber;

	}

	public static void main(String[] args) {

		Car[] car = new Car[6];
		car[0] = new Car(1, "BMW", "X5", 2010, "green", 10000, "AX1212AX");
		car[1] = new Car(2, "Reno", "Kengoo", 2017, "white", 18400, "GOD OF JAVA");
		car[2] = new Car(3, "Volga", "GAZ24", 2015, "black", 30000, "0001AM");
		car[3] = new Car(4, "Zaporogec", "Gorbatiy", 2010, "pink", 100000, "AD666AD");
		car[4] = new Car(5, "Mersedes", "C100", 1990, "silver", 1000, "PAHAN777");
		car[5] = new Car(6, "Mersedes", "C100", 2017, "black", 2000, "X6575A");
		Processor.givenBrand(car, "Reno");
		Processor.givenModel(car, "C100", 5);
		Processor.yearPrice(car, 2017, 2000);

	}
}