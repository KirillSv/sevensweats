package com.hillel.RegularFunctions;

import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextHandler {

    public static void main(String[] args) {

        String myText = "В лесу родилась ёлочка,\r\n" + "В лесу она росла.\r\n" + "Зимой и летом стройная,\r\n"
                + "Зелёная была.\r\n" + "Метель ей пела песенку:\r\n" + "'Спи, ёлочка, бай-бай!'\r\n"
                + "Мороз снежком укутывал:\r\n" + "Смотри, не замерзай!\r\n" + "Трусишка зайка серенький\r\n"
                + "Под ёлочкой скакал.\r\n" + "Порою волк, сердитый волк,\r\n" + "Рысцою пробегал.\r\n"
                + "Чу! Снег по лесу частому\r\n" + "Под полозом скрипит.\r\n" + "Лошадка мохноногая\r\n"
                + "Торопится, бежит.\r\n" + "Везёт лошадка дровеньки,\r\n" + "На дровнях мужичок.\r\n"
                + "Срубил он нашу ёлочку\r\n" + "Под самый корешок.\r\n" + "Теперь ты здесь, нарядная,\r\n"
                + "На праздник к нам пришла\r\n" + "И много, много радости\r\n" + "Детишкам принесла.";

        String originalText = myText;
//1. Определить, сколько раз повторяется в тексте каждое слово, которое встречается в 
//нем.

        myText = myText.replaceAll("\\p{Punct}", "").replaceAll("\r\n", " ").toLowerCase();
        String[] myTextArr = myText.split(" ");
        HashMap<String, Integer> map = new HashMap<>();
        for (int i = 0; i < myTextArr.length; i++) {
            Pattern p = Pattern.compile(myTextArr[i]);
            Matcher m = p.matcher(myText);
            int count = 0;
            while (m.find()) {
                count++;
            }
            map.put(myTextArr[i], count);
        }

        for (Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + "- " + entry.getValue());
        }

//2. В каждом слове текста k-ю букву заменить заданным символом. Если k больше длины 
//слова, корректировку не выполнять

        final int K = 3;

        String regularExpression = "[^А-Яа-яеё][А-Яа-яеё]" + "{" + K + "}";
        Pattern p = Pattern.compile(regularExpression);

        StringBuilder fullOriginalSong = new StringBuilder(myText);

        Matcher m = p.matcher(fullOriginalSong);

        while (m.find()) {
            fullOriginalSong.setCharAt((m.start() + K), '@');

        }
        System.out.print(fullOriginalSong);

//3. Напечатать без повторения слова текста, у которых первая и последняя буквы 
//совпадают
        Set<String> set = new HashSet<String>();
        myText = myText.replaceAll("\\p{Punct}", "").replaceAll("\r\n", " ").toLowerCase();
        String[] myTextArr2 = myText.split(" ");
        for (int i = 0; i < myTextArr2.length; i++) {
            if (myTextArr2[i].matches("^(.).*\\1$"))
                set.add(myTextArr2[i]);
        }
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

//4. Подсчитать количество содержащихся в данном тексте знаков препинания

        Pattern pat = Pattern.compile("[$&+,:;=?@#|'<>.-^*()%!]");
        Matcher mat = pat.matcher(originalText);
        int countPunctuationMark = 0;
        while (mat.find()) {
            countPunctuationMark++;
        }
        System.out.println("Количество содержащихся в данном тексте знаков препинания- " + countPunctuationMark);
    }
}
