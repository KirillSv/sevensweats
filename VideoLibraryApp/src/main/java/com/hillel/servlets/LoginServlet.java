package com.hillel.servlets;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hillel.dbcontainer.DBWorker;
import com.hillel.entity.User;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 7L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("User");
		if (user == null) {
			request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("jsp/deleteFilmsOlder.jsp").forward(request, response);
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("User");
		if (user == null) {
			String username = request.getParameter("username");
			String password = request.getParameter("password");

			session.setAttribute("SessionPassword", password);
			session.setAttribute("SessionUsername", username);

			DBWorker dbWorker = new DBWorker();
			try {
				user = dbWorker.getUser(username);
			} catch (NamingException e) {
				e.printStackTrace();
			}
			session.setAttribute("User", user);

			if (user != null) {
				request.getRequestDispatcher("jsp/deleteFilmsOlder.jsp").forward(request, response);
			} else {
				request.setAttribute("errorMessage", "You don't exist, maybe died?");
				request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
			}

		} else {
			request.getRequestDispatcher("jsp/deleteFilmsOlder.jsp").forward(request, response);
		}
	}

}
