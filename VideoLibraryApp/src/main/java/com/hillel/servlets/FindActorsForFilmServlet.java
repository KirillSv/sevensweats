package com.hillel.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hillel.dbcontainer.DBWorker;
import com.hillel.person.Actor;

@WebServlet("/FindActorsForFilm")
public class FindActorsForFilmServlet extends HttpServlet {
	private static final long serialVersionUID = 3L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/findActorsForFilm.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String filmName = request.getParameter("filmName");

		DBWorker dbWorker = new DBWorker();
		List<Actor> listActorsFromSpecificFilm = dbWorker.findActorsForFilm(filmName);
		request.setAttribute("listActorsFromSpecificFilm", listActorsFromSpecificFilm);
		request.getRequestDispatcher("jsp/findActorsForFilm.jsp").forward(request, response);
	}

}
