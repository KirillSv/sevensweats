package com.hillel.homelibrary;

import java.util.List;

import com.hillel.film.Film;
import com.hillel.person.Actor;

public interface HomeLibrarySource {

    public List<Film> findFilmsForCurrentAndPreviousYear();

    public List<Actor> findActorsAsDirector();

    public List<Actor> findActorsForFilm(String filmName);

    public List<Actor> findActorsForNFilms(int filmsCount);

    public boolean deleteFilmsOlderGivenYears(int givenYears);

}
