package com.hillel.homelibrary;

public class HomeLibrary {
    private HomeLibrarySource source;

    public HomeLibrarySource getSource() {
        return source;
    }

    public void setSource(HomeLibrarySource source) {
        this.source = source;
    }

}
