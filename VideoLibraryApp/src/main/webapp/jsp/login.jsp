<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="language.text" />
<!DOCTYPE html>
<html>
<head>

<meta charset="ISO-8859-1">
<title>Login</title>
</head>

<body>
	<c:import url="/style/style.css"></c:import>
	<c:import url="/style/header.jspf"></c:import>
	<div class='notebook'>
		<form action="LoginServlet" method="post">
<fmt:message key="login.label.EnterYourPlanetNumberInTenture" var="EnterYourPlanetNumberInTenture" />
			${EnterYourPlanetNumberInTenture}: <input type="text"
				name="username" value="login" /> <input type="text" name="password"
				value="Password" /> 
			 <fmt:message key="login.button.GO" var="GO" />
			<input type="submit" name="GO" value="${GO}"> <br> <br>
		</form>
		${errorMessage}
	</div>

</body>

</html>