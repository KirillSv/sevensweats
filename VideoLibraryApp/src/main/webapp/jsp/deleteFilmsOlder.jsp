<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="language.text" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">

<title>Delete Films Older Given Year</title>
</head>
<body>
	<c:import url="/style/style.css"></c:import>
	<c:import url="/style/header.jspf"></c:import>
	<div class='notebook'>
	<fmt:message key="login.label.DeleteFilmsOlderGivenYear" var="DeleteFilmsOlderGivenYear" />
		${DeleteFilmsOlderGivenYear} : <br> <br>
		<form name="Form1" method="post" action="DeleteFilmsOlderGivenYears">
			<table>
				<tr>
	<fmt:message key="login.label.InputYear" var="InputYear" />			
					<td class="text">${InputYear}:</td>
					<td><input name="year" size="25"></td>
				</tr>
			</table>
			 <fmt:message key="login.button.GO" var="GO" />
			<input type="submit" name="GO" value="${GO}"> <br>
		</form>
	<fmt:message key="login.label.EraseWasSuccessful" var="EraseWasSuccessful" />		
		<br> ${ success == true ? "Erase was successfull!" : " "}

	</div>
</body>
</html>