<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="language.text" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Find actors from this film</title>
</head>
<body>
	<c:import url="/style/style.css"></c:import>
	<c:import url="/style/header.jspf"></c:import>
	<div class='notebook'>
<fmt:message key="login.label.FindActorsFromThisFilm" var="FindActorsFromThisFilm" />	
		${FindActorsFromThisFilm}: <br> <br>

		<form name="Form1" method="post" action="FindActorsForFilm">
			<table>
				<tr>
<fmt:message key="login.label.InputFilmName" var="InputFilmName" />					
					<td class="text">${InputFilmName} (Gremlins):</td>
					<td><input name="filmName" size="25"></td>
				</tr>
			</table>
			 <fmt:message key="login.button.GO" var="GO" />
			<input type="submit" name="GO" value="${GO}"> <br> <br> <br>
		</form>

		<c:forEach var="actor" items="${listActorsFromSpecificFilm}">
			<br> ${actor} 
    </c:forEach>
	</div>
</body>
</html>