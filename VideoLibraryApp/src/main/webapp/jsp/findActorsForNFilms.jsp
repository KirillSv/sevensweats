<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="language.text" />
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Find actors from N film</title>
</head>
<body>
	<c:import url="/style/style.css"></c:import>
	<c:import url="/style/header.jspf"></c:import>
	<div class='notebook'>
	<fmt:message key="login.label.FindActorsFromNilm" var="FindActorsFromNilm" />
		${FindActorsFromNilm} : <br> <br>

		<form name="Form1" method="post" action="FindActorsForNFilms">
			<table>
				<tr>
				<fmt:message key="login.label.InputCountFilms" var="InputCountFilms" />
					<td class="text">${InputCountFilms}:</td>
					<td><input name="count" size="25"></td>
				</tr>
			</table>
			 <fmt:message key="login.button.GO" var="GO" />
			<input type="submit" name="GO" value="${GO}"> <br> <br> <br>

		</form>
		<c:forEach var="actor" items="${listActorsForNFilms}">
			<br> ${actor} 
</c:forEach>

	</div>
</body>
</html>