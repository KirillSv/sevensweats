<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />
<!-- set language -->
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="login.label.card" /></title>
<link href="./CSS/style.css" rel="stylesheet">
</head>
<body>
<!-- form asked list confirmed payments-->
	<form id="forma" action="../pays/confpay" method="POST">
		<c:if test="${not empty sessionScope.incomeSuccess }">
			<c:remove var="incomeSuccess" />
		</c:if>
		<c:if test="${not empty sessionScope.paySuccess }">
			<c:remove var="paySuccess" />
		</c:if>
	    <fmt:message key="login.message.YouhaveNtAddedCard" var="noCards" />
		<fmt:message key="cart.label.prepNoPay" var="prepNoPay"/>
		<fmt:message key="login.message.YouDoSuccesPayment" var="confPayResultText" />
		<fmt:message key="login.label.logged" var="textLogged"/>
		
		<!-- button logout  user -->
		<div align="right">
			${textLogged}: ${pageContext.request.userPrincipal.name} <a
				href="logout"><fmt:message key="login.label.logout"/></a>
		</div>
		
		<%-- HEADER --%>
		<%@ include file="/WEB-INF/jspf/header.jspf"%>
		<%-- HEADER --%>
		<br><br>
		<c:if test="${empty sessionScope.payments}">
			<fmt:message key="cart.label.prepNoPay" var="prepNoPay" />
		${prepNoPay}
	</c:if>
	
	<!-- last stolbik in table whis button accept or delete concrete payment  -->
		<c:if test="${not empty sessionScope.payments}">
			<fmt:message key="cart.label.buttonAccept" var="buttonAccept" />
			<fmt:message key="cart.label.buttonDelete" var="buttonDelete" />
			<table class="spc" border="0" cellspacing="2" cellpadding="2"
				width="68%">
				<c:forEach var="item" items="${sessionScope.payments}">
					<tr>
						<td>${item.dtime}</td>
						<td>${item.account}</td>
						<td>${item.summary}</td>
						<td>${item.total}</td>
						<td>${item.balance}</td>
						<td><button name="confirm" value="${item.dtime}">${buttonAccept}</button></td>
						<td><button name="delete" value="${item.dtime}">${buttonDelete}</button></td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</form>
	
	<!-- message about confirm payment -->
	<c:if test="${sessionScope.confPayResult eq true}">
		<font color="green"> ${confPayResultText}</font>
	</c:if>
<br><br>
	<br><br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>