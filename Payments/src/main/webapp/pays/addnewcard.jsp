<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
	<!-- set language -->
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="mypays.button.addCard" /></title>
<link href="./CSS/style.css" rel="stylesheet">
</head>
<body>
<!-- login message -->
	<fmt:message key="login.label.logged" var="textLogged" />
	<div align="right">
		${textLogged}: ${pageContext.request.userPrincipal.name} <a
			href="./logout"><fmt:message key="login.label.logout" /></a>
	</div>
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<%-- HEADER --%>
	
	<!-- form add new card -->
	<form method="post" action="addnew">
		<div id="add-form">
			<input name="account" type="text"
			    placeholder="7777-7777-7777-7777"
				pattern="\d{4}\-\d{4}-\d{4}-\d{4}" 
				title=<fmt:message key="login.message.Enter12Diggits"/>
				 required> 
				 <br> <br>
			<select name="select"><option value="visa">VISA</option>
				<option value="master">MASTERCARD</option></select>
				<br>  <br>
				<input name="exp" type="text" 
				placeholder="01/19" 
				pattern="\d{2}\/\d{2}"
				title=<fmt:message key="login.message.EnterDateFormat"/>
				 required >
				  <br> <br>
			<input name="last" type="text" 
				placeholder="CVC2" 
				pattern="\d{3}"
				title=<fmt:message key="login.message.Enter3Digits"/>
				 required>
			<br> <br>
			<button type="submit" value="reg">
				<fmt:message key="login.button.Add" />
			</button>
		</div>
	</form>
	<img src="./imagesForJsp/verifiedVisa.jpg"/>
		<br><br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>