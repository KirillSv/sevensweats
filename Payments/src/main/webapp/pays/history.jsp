<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />

<!-- set language -->
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="login.label.History" /></title>

<!-- load script for sort column -->
<link href="./CSS/style.css" rel="stylesheet">

<script type="text/javascript" charset="utf-8" src="./CSS/script.js"></script>
</head>
<body>
	<fmt:message key="login.message.YouhaveNtAddedCard" var="noCards" />
	<fmt:message key="login.message.YouDoSuccesPayment" var="success" />
	<c:if test="${not empty sessionScope.success }">
		<c:remove var="success" />
	</c:if>
	<fmt:message key="login.label.logged" var="textLogged" />
	
	<!-- logout button -->
	<div align="right">
		${textLogged}: ${pageContext.request.userPrincipal.name} <a
			href="logout"><fmt:message key="login.label.logout" /></a>
	</div>
	
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<%-- HEADER --%>
	<br>
	<br>
	<div>
	
	<!-- form asked list card for concret user -->
		<form action="../pays/gettransactions" method="POST">
			<select name="selectAcc">
				<c:if test="${empty sessionScope.accounts}">
					<option>${noCards}</option>
				</c:if>
				<c:forEach var="item" items="${sessionScope.accounts}">
					<option value="${item.account}">${item.account}</option>
				</c:forEach>
			</select>
			<fmt:message key="mypays.button.viewHistory" var="buttonViewHistory" />
			<input type="submit" value="${buttonViewHistory}">
       </form>

<!-- if list transaction exsist show button for write file PDF writer and screenshot button-->
<c:if test="${not empty sessionScope.trancs}">	
<br>	
		<form action="../pays/pdf" method="POST">
			<input type="submit" value=<fmt:message key="login.button.makePDFreport" />>
		</form>
		<br>
		<form action="../pays/screenshot" method="GET">
			<input type="submit" value=<fmt:message key="login.button.makeScreenshot" />>
		</form>
		
<!-- if pdf report-file is exsist and  list transaction exsist show button for show file PDF and error message -->
    <c:if test="${not empty sessionScope.forButtonOpenPDF}">
		<h6 style="color:#5882FA">${pdfErrorMessage}</h6>
		<form action="../pays/openPdf" method="POST">
			<input type="submit" value= "<fmt:message key="login.button.ShowReport" /> ${forButtonOpenPDF}" >
		</form>
	</c:if>	
		
</c:if>		

		<br>
	</div>
	
	<!-- if list transaction exist this table  show all transaction concret card -->
	<c:if test="${not empty sessionScope.trancs }">
		<br>
		<div id="render_me">
			<table class="spc" id="history" border="0" cellspacing="2"
				cellpadding="2" width="98%">
				<tr>
					<td class="thd" >
					<!-- 2 sort button avers and revers-->
					
					
					<c:if test="${not empty sessionScope.whatNeedSort}">
							<form action="../pays/sortTransactions" method="post">
								<button name="whatNeedSort" value="Payment_data">
									<fmt:message key="login.button.Sort" />+
								</button>
							</form> 				
					</c:if>
			
					<c:if test="${empty sessionScope.whatNeedSort}">
							<form action="../pays/sortTransactions" method="post">
								<button name="whatNeedSort" value="Payment_dataRevers">
									<fmt:message key="login.button.Sort" />-
								</button>
							</form> 				
					</c:if>
					
					<!--  -->
						<fmt:message key="login.label.Data" />
						
						
						
					</td>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />>
						<fmt:message key="login.label.card" />
					</td>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />><fmt:message
							key="login.label.summary" /></td>
					<td class="thd" onclick="sort(this)"><fmt:message key="login.label.summa" /></td>
					<td class="thd" onclick="sort(this)"><fmt:message key="login.label.balance" /></td>
				</tr>
				<c:forEach var="item" items="${sessionScope.trancs}">
					<tr>
						<td>${item.dtime}</td>
						<td>${item.account}</td>
						<td>${item.summary}</td>
						<td>${item.total}</td>
						<td>${item.balance}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:if>
	<br><br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>