<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- set language -->
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="login.label.Translit" /></title>
<link href="./CSS/style.css" rel="stylesheet">
</head>
<body>
	<fmt:message key="login.message.YouhaveNtAddedCard"  var="noCards" />    
	<fmt:message key="login.message.YouDoSuccesPayment" var="paySuccess" />
	<fmt:message key="login.label.logged" var="textLogged" />
	
	<!-- logout button -->
	<div align="right">
		${textLogged}: ${pageContext.request.userPrincipal.name} <a
			href="logout"><fmt:message key="login.label.logout" /></a>
	</div>
	
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<%-- HEADER --%>
	<br><br>
	<div>
	<!--form make pay -->
		<form action="../pays/pays" method="POST">
			<c:if test="${empty sessionScope.accounts}">
				<select>
					<option>
						${noCards}
					</option>
				</select>
			</c:if>
			<fmt:message key="mypays.button.payFrom" var="payFrom" />
			<fmt:message key="mypays.button.payTo" var="payTo" />
			<fmt:message key="mypays.button.summa" var="summa" />
			<c:if test="${not empty sessionScope.accounts}">
				<div class="main">
					<div class="field">
						<label for="n">${payFrom}: </label> <select name="selectAcc1"
							id="n">
							<c:forEach var="item" items="${sessionScope.accounts}">
								<option>${item.account}</option>
							</c:forEach>
						</select>
					</div>
					<div class="field">
						<label for="n2">${payTo}: </label> <select name="selectAcc2"
							id="n2">
							<c:forEach var="item" items="${sessionScope.accounts}">
								<option>${item.account}</option>
							</c:forEach>
						</select>
					</div>
					<fmt:message key="mypays.button.paySubmit" var="ButtonPaySubmit" />
					<fmt:message key="mypays.button.payAttention" var="payAttention" />
					<div class="field">
						<label for="n3">${summa}: </label><input type="text" name="pays"
							pattern="(\d?)+\.?\d+" id="n3" required> <br>
					</div>
				</div>
				<br><br><br><br><br><br>
				<input type="submit" value="${ButtonPaySubmit}" />
				<br><br>
			</c:if>
		</form>
		
		<!--make message about send pay-->
	    <c:if test="${not empty sessionScope.AccountIsBlockedException}">
		<p>
		   <font color="red"> <fmt:message key="login.label.card" /> ${AccountIsBlockedException} <fmt:message key="login.label.isBlocked" /></font>
		</p>
</c:if>
		<c:if test="${sessionScope.paySuccess eq 'successfully'}">
			<p>
				<fmt:message key="payments.pay.successMoneySend"
					var="successMoneySend" />
				<font color="green">${successMoneySend}</font>
			</p>
		</c:if>
		<c:if test="${sessionScope.paySuccess eq 'negative'}">
			<p>
				<fmt:message key="payments.pay.noMoney" var="cardNoMoney" />
				<font color="red">${cardNoMoney}</font>
			</p>
		</c:if>
	</div>
	<br><br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>