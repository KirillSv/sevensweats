<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/jstl/isLocked.tld" prefix="xx"%>

<!-- set language -->
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="${language}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./CSS/style.css" rel="stylesheet">

<!-- load script for sort column -->
<script type="text/javascript" charset="utf-8" src="./CSS/script.js"></script>
<title><fmt:message key="login.label.UserPage" /></title>
</head>
<body>
	<!-- logout href and show user name -->
	<fmt:message key="login.label.logged" var="textLogged" />
	<div align="right">
		${textLogged}: ${pageContext.request.userPrincipal.name} <a
			href="logout"><fmt:message key="login.label.logout" /></a>
	</div>

	<c:if test="${not empty sessionScope.incomeSuccess }">
		<c:remove var="incomeSuccess" />
	</c:if>
	<c:if test="${not empty sessionScope.paySuccess }">
		<c:remove var="paySuccess" />
	</c:if>
	<c:if test="${sessionScope.confPayResult eq true}">
		<c:remove var="confPayResult" scope="session" />
	</c:if>
	<c:if test="${not empty sessionScope.resultOp}">
		<c:remove var="resultOp" />
	</c:if>
	
	
	<!-- show message with help my customer tag if user is blocked -->
		<c:if test="${sessionScope.customerStatus eq true}">
			<xx:isLocked value="true" />
     <!--  -->

	</c:if>
	<!-- admin button -->
	<c:if test="${sessionScope.isAdmin == true }">
		<input type="button" class="menu"
			value=<fmt:message key="login.label.Administration" />
			onclick="location.href='../admin/admin.jsp'" />
	</c:if>

	<c:if test="${sessionScope.isAdmin == false }">

		<%-- HEADER --%>
		<%@ include file="/WEB-INF/jspf/header.jspf"%>
		<%-- HEADER --%>

		<!-- message about empty cards list in this user -->
		<c:if test="${empty sessionScope.accounts}">
			<fmt:message key="login.label.noCards" var="noCards" />
			<p>${noCards}
			<p>
		</c:if>

		<!-- if user have card - show card data -->
		<c:if test="${not empty sessionScope.accounts}">
			<h3>
				<fmt:message key="login.label.ListAccounts" />
			</h3>
			<div class="div-table">
				<table class="spc" border="0" cellspacing="2" cellpadding="2"
					width="68%">
					<tr>
						<td class="thd">
							<!-- sort button -->
							<form action="../pays/sortAccount" method="post">
								<button name="whatNeedSort" value="Acc_name">
									<fmt:message key="login.button.Sort" />
								</button>
							</form> 
								<!--  -->
							<fmt:message key="login.label.card" />
						</td>

						<td class="thd">
                        <!-- sort button -->
							<form action="../pays/sortAccount" method="post">
								<button name="whatNeedSort" value="Acc_balance">
									<fmt:message key="login.button.Sort" />
								</button>
							</form> 
							<!--  -->
							<fmt:message key="login.label.balance" />
						</td>

						<td class="thd" onclick="sort(this)"
							title=<fmt:message key="login.message.pressForSortColumn" />>
							<fmt:message key="login.label.summary" />
						</td>
						<td class="thd"><fmt:message key="login.label.blockStatus" /></td>
						<td class="thd"><fmt:message key="login.label.blocked" /></td>
					</tr>
					<c:forEach var="item" items="${sessionScope.accounts}">
						<tr>
							<td>${item.account}</td>
							<td>${item.accBalance}</td>
							<td>${item.currency}</td>
							<td>${item.verified}</td>

							<!-- button on last table column for user can block card-->
							<td><c:if test="${item.verified eq false}">
									<form action="../pays/changeStatusUserAccounts" method="post">
										<button name="account" value="${item.account}">
											<fmt:message key="login.button.blockCard" />
										</button>
									</form>
								</c:if></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</c:if>
	</c:if>

	<c:if test="${sessionScope.isAdmin == false }">
		<br>
		<!-- form and button for send report to admin-->
	
		<form action="../pays/report/reportSender" method="post">
			<img wight='60' height='60' src="./imagesForJsp/callBack.jpg"/>
			<input type="text" name="report" pattern="[^.]{10,100}"
				title="min length 10.  max length 100 letters." required>
			<button name="report">
				<fmt:message key="login.buttun.SendMessageTAdmin" />
			</button>
		</form>
	</c:if>
	<br>
	<br>

	<!-- table show admin all report from database-->
	<c:if test="${sessionScope.isAdmin == true }">
		<c:if test="${not empty sessionScope.reports}">
			<h4>
				<fmt:message key="login.label.YouHaveReports" />
			</h4>

			<table class="spc" border="0" cellspacing="2" cellpadding="2"
				width="68%">
				<tr>
					<td class="thd" onclick="sort(this)"><fmt:message
							key="login.label.Data" /></td>
					<td class="thd" onclick="sort(this)"><fmt:message
							key="login.label.account" /></td>
					<td class="thd" onclick="sort(this)"><fmt:message
							key="login.label.report" /></td>
					<td class="thd" onclick="sort(this)"><fmt:message
							key="login.label.delete" /></td>
				</tr>
				<c:forEach var="itemReports" items="${sessionScope.reports}">
					<tr>
						<td>${itemReports.data}</td>
						<td>${itemReports.customerEmail}</td>
						<td>${itemReports.message}</td>
						<td>
							<!-- button on last column report table delete report from db-->
							<form action="../pays/report/reportDeleter" method="post">
								<button name="data" value="${itemReports.data}">
									<fmt:message key="login.button.deleteReport" />
								</button>
							</form>
						</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</c:if>
	<br>
	<br>
	<br>
	<br>
	<br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>