<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- set language-->
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="payments.pay.summa" /></title>
<link href="./CSS/style.css" rel="stylesheet">
</head>
<body>

	<c:if test="${not empty sessionScope.incomeSuccess }">
		<c:remove var="incomeSuccess" />
	</c:if>
	<c:if test="${not empty sessionScope.paySuccess }">
		<c:remove var="paySuccess" />
	</c:if>
	
	<fmt:message key="login.message.noCard" var="noCards"  />
	<fmt:message key="login.message.addPaymentTrue" var="successOp" />
	<fmt:message key="login.message.noAddPaymentcheckBalans" var="unSuccessOp" />
	<fmt:message key="login.label.logged" var="textLogged" />
	
	<!-- logout href-->
	<div align="right">
		${textLogged}: ${pageContext.request.userPrincipal.name} <a
			href="logout"><fmt:message key="login.label.logout"/></a>
	</div>
	
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<%-- HEADER --%>
	<br>
	<!-- title -->
	<fmt:message key="payments.pay.labelPay" var="textLabelPay" />
	<h3>${textLabelPay}</h3>
	
	<!-- form for make payment-->
	<form action="../pays/prepay" method="post">
		<c:if test="${empty sessionScope.accounts}">
			<select>
				<option>
					${noCards}
				</option>
			</select>
		</c:if>
		<c:if test="${not empty sessionScope.accounts}">
			<fmt:message key="payments.pay.fromPay" var="textFromPay" />	
			 ${textFromPay}: <select name="selectAcc1">
				<c:forEach var="item" items="${sessionScope.accounts}">
					<option>${item.account}</option>
				</c:forEach>
			</select>
			<br>
			<fmt:message key="payments.pay.toPay" var="textToPay" />
			${textToPay}:
			<input type="text" name="toPay"
				pattern="\d{5,12}" title=<fmt:message key="login.message.digits5max12" /> required>
			<br>
			<fmt:message key="payments.pay.purpose" var="textPurpose" />
			${textPurpose}:
			<input type="text" name="toPayInfo"
				pattern="[A-Za-zА-Яа-яЁё\s0-9]{3,50}"
				title=<fmt:message key="login.message.OnlySymbolandDigits50" /> required>
			<br>
			<fmt:message key="payments.pay.summa" var="textSumma" />
			${textSumma}:
			 <input type="text" name="money"
				pattern="\d{1,8}" title=<fmt:message key="login.message.OnlyDigits" /> required>
			<input type="submit"
				value=<fmt:message key="login.message.confirm" />>
			<br>
		</c:if>
	</form>
	
	<!-- make result input data in form payment  -->
	<br>
	
	<c:if test="${sessionScope.resultOp == 777}">
		<p>
			<fmt:message key="payments.pay.goodAddToCart" var="goodAddToCart" />
			   <font color="red"> <fmt:message key="login.label.card" /> ${AccountIsBlockedException} <fmt:message key="login.label.isBlocked" /></font>
		</p>
	</c:if>
	<c:if test="${sessionScope.resultOp == 1}">
		<p>
			<fmt:message key="payments.pay.goodAddToCart" var="goodAddToCart" />
			<font color="green">${goodAddToCart}</font>
		</p>
	</c:if>
	<c:if test="${sessionScope.resultOp == 11}">
		<p>
			<fmt:message key="payments.pay.badAddToCart" var="badAddToCart" />
			<font color="red">${badAddToCart}</font>
		</p>
	</c:if>
	<br><br><br>
	<br><br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>