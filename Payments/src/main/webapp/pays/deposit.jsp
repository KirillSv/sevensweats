<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- set language -->
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="mypays.label.Deposit" /></title>
<link href="./CSS/style.css" rel="stylesheet">
</head>
<body>
	<fmt:message key="login.message.noCard" var="noCards" />
	<fmt:message key="login.message.YouDoSuccesPayment"
		var="incomeSuccessText" />
	<c:if test="${not empty sessionScope.paySuccess }">
		<c:remove var="paySuccess" />
	</c:if>
	<fmt:message key="login.label.logged" var="textLogged" />
	<div align="right">
		${textLogged}: ${pageContext.request.userPrincipal.name} <a
			href="logout"><fmt:message key="login.label.logout" /></a>
	</div>
	<%-- HEADER --%>
	<%@ include file="/WEB-INF/jspf/header.jspf"%>
	<%-- HEADER --%>
	<br>
	<br>
	<div>
		<!-- form for put money on the card -->
		<form action="../pays/putdeposit" method="POST">
			<select name="selectAcc">
				<c:if test="${empty sessionScope.accounts}">
					<option>${noCards}</option>
				</c:if>
				<c:forEach var="item" items="${sessionScope.accounts}">
					<option>${item.account}</option>
				</c:forEach>
			</select> <br>
			<fmt:message key="deposit.label.buttonCredit" var="buttonCredit" />
			<input type="text" name="addMoney" pattern="(\d?)+\.?\d+"
				title="Only digits!" required> <input type="submit"
				value="${buttonCredit}">
		</form>
	</div>
	<!-- message about success put money on the card -->
	<br>
	<c:if test="${not empty sessionScope.AccountIsBlockedException}">
		<p>
			<font color="red"> <fmt:message key="login.label.card" />
				${AccountIsBlockedException} <fmt:message
					key="login.label.isBlocked" /></font>
		</p>
	</c:if>
	<c:if test="${sessionScope.incomeSuccess eq true}">
		<font color="green"> ${incomeSuccessText}</font>
	</c:if>
	<br>
	<br>
	<br>
	<br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>