<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="${language}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="login.button.label" /></title>

</head>
<body>

	<h4>
		<fmt:message key="login.button.label" />
		<br>
		<fmt:message key="login.button.label2" />
	</h4>
	<br>

	<form method="post" action="j_security_check" class="login">
		
				<label for="login"> <fmt:message key="login.label.username" />:
				</label> <input type="email" name="j_username" id="login" placeholder="name@example.com">
			<br>
				<label for="password"> <fmt:message key="login.label.password" />:
				</label> <input type="password" name="j_password" id="password" placeholder="********">		
		<br> <br>
		<fmt:message key="login.button.submit" var="buttonValue" />
		<fmt:message key="login.button.reg" var="buttonRegValue" />
		<p class="login-submit">
			<button type="submit" value="login" class="login-button">${buttonValue}</button>
			<input type="button" value="${buttonRegValue}"
				onclick="location.href='./unauth/regis'" />
		</p>
	</form>
	
<h6>	Prepared demo accounts:
<br> <br>
	
admin@admin.com<br>
user1@admin.com<br>
user2@admin.com<br>
password: 123456<br>
</h6>
  	<br><br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>

</body>
</html>