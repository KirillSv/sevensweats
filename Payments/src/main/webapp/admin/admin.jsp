<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- set language -->
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><fmt:message key="login.label.AdminArea" /></title>
<link href="../pays/CSS/style.css" rel="stylesheet">

<!-- sort script -->
<script type="text/javascript" charset="utf-8" src="../pays/CSS/script.js"></script>
</head>
<body>
<!-- logout href and show user name -->
	<fmt:message key="login.label.logged" var="textLogged" />	
	<div align="right">
		${textLogged}: ${pageContext.request.userPrincipal.name}
	</div>

<!-- back button -->
	<input type="button" class="menu"
		value=<fmt:message key="login.label.Back" />
		onclick="location.href='../pays/mypays.jsp'"/>
	<h3>
		<fmt:message key="login.label.IformOnAccount" />
	</h3>
	
	<!-- choise color for user login  red=blocked -->
		<c:if test="${sessionScope.customerStatus eq true}">
			<strong><font color="red">${sessionScope.userEmail}</font></strong>
		</c:if>
		<c:if test="${sessionScope.customerStatus eq false}">
			<strong><font color="green">${sessionScope.userEmail}</font></strong>
		</c:if>
	
	
	<!-- button  blocked user  if admin had already concret user-->
	<c:if test="${not empty sessionScope.userEmail}">
		<form action="../changeUserStatus" method="post">
			<button name="locked" value="${sessionScope.userEmail}">
				<fmt:message key="login.button.ChangeUserStatus" />
			</button>
		</form>
	</c:if>	
	<br>

	<!--  button send login(email) then get concrete user -->
	<form action="../pays/getaccounts" method="post">
	<select name="userEmail">
			<c:forEach var="item" items="${sessionScope.listAllCustomers}">
				<option value="${item.name}">${item.name}</option>
			</c:forEach>
	</select> 
		 <input type="submit" title=<fmt:message key="login.label.Send" />>
	</form>
	
	<br>
	
	<!-- button choice account from list user accounts and get his transactions -->
	<fmt:message key="login.message.YouhaveNtAddedCard" var="noCards" />
	<form action="../pays/gettransactions" method="post">
		<select name="selectAcc">
			<c:if test="${empty sessionScope.accounts}">
				<option>
					${noCards}
				</option>
			</c:if>
			<c:forEach var="item" items="${sessionScope.accounts}">
				<option value="${item.account}">${item.account}</option>
			</c:forEach>
		</select> <input type="submit" value=<fmt:message key="login.label.GetData" />>
	</form>
	
	<!--table whis data reports if report list is not empty -->
	<br>
	<c:if test="${not empty sessionScope.reports}">
	<h4><fmt:message key="login.label.YouHaveReports" /></h4>
		<table class="spc" border="0" cellspacing="2" cellpadding="2"
			width="68%">
			<tr>
				<td class="thd" onclick="sort(this)"><fmt:message key="login.label.Data" /></td>
				<td class="thd" onclick="sort(this)"><fmt:message key="login.label.account" /></td>
				<td class="thd" onclick="sort(this)"><fmt:message key="login.label.report" /></td>
				<td class="thd" onclick="sort(this)"><fmt:message key="login.label.delete" /></td>
			</tr>
			<c:forEach var="itemReports" items="${sessionScope.reports}">
				<tr>
					<td>${itemReports.data}</td>
					<td>${itemReports.customerEmail}</td>
					<td>${itemReports.message}</td>
					<td>
					<!-- admin button  delete report-->
						<form action="../pays/report/reportDeleter" method="post">
							<button name="data" value="${itemReports.data}">
								<fmt:message key="login.button.deleteReport" />
							</button>
						</form>
					</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
	
		<!-- message about empty cards list in this user -->
	<c:if test="${empty sessionScope.accounts}">
		<fmt:message key="login.label.noCards" var="noCards" />
		<p>${noCards}<p>
	</c:if>
	
	<!-- if user have card - show card data -->
	<c:if test="${not empty sessionScope.accounts}">
	<h3>
		<fmt:message key="login.label.ListAccounts" />
	</h3>
		<div class="div-table">
			<table class="spc" border="0" cellspacing="2" cellpadding="2"
				width="68%">
				<tr>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />>
						<fmt:message key="login.label.card" />
						</td>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />>
						<fmt:message key="login.label.balance" />
						</td>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />>
						<fmt:message key="login.label.summary" /></td>
					<td class="thd"><fmt:message key="login.label.blockStatus" /></td>
					<td class="thd"><fmt:message key="login.label.blocked" /></td>
				</tr>
				<c:forEach var="item" items="${sessionScope.accounts}">
					<tr>
						<td>${item.account}</td>
						<td>${item.accBalance}</td>
						<td>${item.currency}</td>
						<td>${item.verified}</td>
						
						<!-- button on last table column for user can block card-->
						<td>
								<form action="../pays/changeStatusUserAccounts" method="post">
									<button name="account" value="${item.account}">
										<fmt:message key="login.label.AdminBlocked" />
									</button>
								</form>
							</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:if>
	<br>
	<!-- admin table transaction concret user card -->
	<c:if test="${not empty sessionScope.trancs }">
		<br>
		<h3>
		<fmt:message key="login.label.HistoryOfTransactions" />
	</h3>
		<div id="render_me">
			<table class="spc" id="history" border="0" cellspacing="2"
				cellpadding="2" width="98%">
				<tr>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />><fmt:message
							key="login.label.Data" /></td>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />><fmt:message
							key="login.label.account" /></td>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />><fmt:message
							key="login.label.summary" /></td>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />><fmt:message
							key="login.label.Summa" /></td>
					<td class="thd" onclick="sort(this)"
						title=<fmt:message key="login.message.pressForSortColumn" />><fmt:message
							key="login.label.balance" /></td>
				</tr>
				<c:forEach var="item" items="${sessionScope.trancs}">
					<tr>
						<td>${item.dtime}</td>
						<td>${item.account}</td>
						<td>${item.summary}</td>
						<td>${item.total}</td>
						<td>${item.balance}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</c:if>
		<br><br><br>
		<br><br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>