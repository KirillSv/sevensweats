<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.nure.shvec.i18n.text" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="login.label.Registration" /></title>
</head>
<body>
	<h3><fmt:message key="login.label.RegistrationNewUser" /></h3>
	<form  method="post" action="regis">
				<div id="reg-form">
				<input name="name" type="text" placeholder="ФИО" pattern="[A-Za-zА-Яа-яЁё\s0-9]{10,50}" title="2 words first letter Uper Case"> <br>
				<input name="email" type="email" placeholder="e-mail"><br> 
				<input name="pass" type="password" placeholder="password" pattern="[A-Za-zА-Я0-9]{6,14}" title="Only symbols and digits max 50"> <br>
				<input name="repass" type="password" placeholder="password" pattern="[A-Za-zА-Я0-9]{6,14}" title="retype password"><br>
				<button type="submit" value="reg" ><fmt:message key="login.label.Registration" /></button>
				</div>
	</form>
		<br><br>
	<%-- Footer --%>
	<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%-- Footer --%>
</body>
</html>