package ua.nure.shvec.entity;

import java.io.Serializable;

/**
 * One of the main entities. Helps to get and keep DB rows and cells. Have
 * different constructions, which may be used depending on context.
 * 
 * @author Shvec K.
 * @version 1.0
 */
public class Transactions implements Serializable {
  private static final long serialVersionUID = 1L;

  private String dtime;
  private String category;
  private String account;
  private String summary;
  private double total;
  private double balance;

  public Transactions(String dtime, String category, String account, String summary, Double total, Double balance) {
    this.dtime = dtime;
    this.category = category;
    this.account = account;
    this.summary = summary;
    this.total = total;
    this.balance = balance;

  }
  
  public void setBalance(double balance) {
    this.balance = balance;
  }

  public Transactions() {

  }

  public String getDtime() {
    return dtime;
  }

  public String getCategory() {
    return category;
  }

  public String getAccount() {
    return account;
  }

  public String getSummary() {
    return summary;
  }

  public Double getTotal() {
    return total;
  }

  public Double getBalance() {
    return balance;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((account == null) ? 0 : account.hashCode());
    long temp;
    temp = Double.doubleToLongBits(balance);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    result = prime * result + ((category == null) ? 0 : category.hashCode());
    result = prime * result + ((dtime == null) ? 0 : dtime.hashCode());
    result = prime * result + ((summary == null) ? 0 : summary.hashCode());
    temp = Double.doubleToLongBits(total);
    result = prime * result + (int) (temp ^ (temp >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Transactions other = (Transactions) obj;
    if (account == null) {
      if (other.account != null) {
        return false;
      }
    } else if (!account.equals(other.account)) {
      return false;
    }
    if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance)) {
      return false;
    }
    if (category == null) {
      if (other.category != null) {
        return false;
      }
    } else if (!category.equals(other.category)) {
      return false;
    }
    if (dtime == null) {
      if (other.dtime != null) {
        return false;
      }
    } else if (!dtime.equals(other.dtime)) {
      return false;
    }
    if (summary == null) {
      if (other.summary != null) {
        return false;
      }
    } else if (!summary.equals(other.summary)) {
      return false;
    }
    if (Double.doubleToLongBits(total) != Double.doubleToLongBits(other.total)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Transactions [dtime=" + dtime + ", category=" + category + ", account=" + account + ","
        + " summary=" + summary + ", total=" + total + ", balance=" + balance + "]";
  }
}
