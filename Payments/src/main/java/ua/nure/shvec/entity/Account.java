package ua.nure.shvec.entity;

import java.io.Serializable;

/**
 * The main entity. Get and keep DB rows and cells contain customer's and
 * account's info.
 * 
 * @author Shvec.K.
 * @version 1.0
 */
public class Account implements Serializable {
  private static final long serialVersionUID = 1L;
  private String customerEmail;
  private String account;
  private String info;
  private String accBalance;
  private boolean verified;
  private boolean locked;
  private String currency;

  public Account(String customerEmail, String account, String info, String accBalance, 
      boolean verified,boolean locked) {
    this.customerEmail = customerEmail;
    this.account = account;
    this.info = info;
    this.accBalance = accBalance;
    this.verified = verified;
    this.locked = locked;
  }

  public Account(String account, boolean locked) {
    this.account = account;
    this.locked = locked;
  }

  public Account(boolean verified, String account) {
    this.account = account;
    this.verified = verified;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public boolean isLocked() {
    return locked;
  }

  public String getCustomerEmail() {
    return customerEmail;
  }

  public String getAccount() {
    return account;
  }

  public String getInfo() {
    return info;
  }

  public String getAccBalance() {
    return accBalance;
  }

  public void setAccBalance(String accBalance) {
    this.accBalance = accBalance;
  }

  public boolean isVerified() {
    return verified;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((accBalance == null) ? 0 : accBalance.hashCode());
    result = prime * result + ((account == null) ? 0 : account.hashCode());
    result = prime * result + ((currency == null) ? 0 : currency.hashCode());
    result = prime * result + ((customerEmail == null) ? 0 : customerEmail.hashCode());
    result = prime * result + ((info == null) ? 0 : info.hashCode());
    result = prime * result + (locked ? 1231 : 1237);
    result = prime * result + (verified ? 1231 : 1237);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Account other = (Account) obj;
    if (accBalance == null) {
      if (other.accBalance != null) {
        return false;
      }
    } else if (!accBalance.equals(other.accBalance)) {
      return false;
    }
    if (account == null) {
      if (other.account != null) {
        return false;
      }
    } else if (!account.equals(other.account)) {
      return false;
    }
    if (currency == null) {
      if (other.currency != null) {
        return false;
      }
    } else if (!currency.equals(other.currency)) {
      return false;
    }
    if (customerEmail == null) {
      if (other.customerEmail != null) {
        return false;
      }
    } else if (!customerEmail.equals(other.customerEmail)) {
      return false;
    }
    if (info == null) {
      if (other.info != null) {
        return false;
      }
    } else if (!info.equals(other.info)) {
      return false;
    }
    if (locked != other.locked) {
      return false;
    }
    if (verified != other.verified) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Account [customerEmail=" + customerEmail + ", account=" + account + ","
        + "" + " info=" + info + ", accBalance=" + accBalance + ", verified=" + verified
        + ", locked=" + locked + "," + " currency=" + currency + "]";
  }

}
