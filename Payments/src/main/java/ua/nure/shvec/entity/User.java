package ua.nure.shvec.entity;

import java.io.Serializable;
import java.util.List;

/**
 * The main entity. Helps to get and keep DB rows and cells contain customer's
 * info. Have different constructions, which may be used depending on context.
 * 
 * @author Shvec.K.
 * @version 1.0
 */
public class User implements Serializable {
  private static final long serialVersionUID = 1L;

  private String name;
  private String email;
  private String password;
  private String role = "user";
  private List<String> accounts;
  private Boolean lock;

  public User(String email) {
    this.email = email;
  }

  public User(String email, Boolean lock) {

    this.email = email;
    this.lock = lock;
  }

  public User(String name, String email, String password, String role, List<String> accounts, Boolean lock) {

    this.name = name;
    this.email = email;
    this.password = password;
    this.role = role;
    this.accounts = accounts;
    this.lock = lock;
  }

  public Boolean getLock() {
    return lock;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public User(String name, String email, String password) {
    this.name = name;
    this.email = email;
    this.password = password;
  }

  public User(String name, String email, String password, boolean lock) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.lock = lock;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<String> getAccounts() {
    return accounts;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((accounts == null) ? 0 : accounts.hashCode());
    result = prime * result + ((email == null) ? 0 : email.hashCode());
    result = prime * result + ((lock == null) ? 0 : lock.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((password == null) ? 0 : password.hashCode());
    result = prime * result + ((role == null) ? 0 : role.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    User other = (User) obj;
    if (accounts == null) {
      if (other.accounts != null) {
        return false;
      }
    } else if (!accounts.equals(other.accounts)) {
      return false;
    }
    if (email == null) {
      if (other.email != null) {
        return false;
      }
    } else if (!email.equals(other.email)) {
      return false;
    }
    if (lock == null) {
      if (other.lock != null) {
        return false;
      }
    } else if (!lock.equals(other.lock)) {
      return false;
    }
    if (name == null) {
      if (other.name != null) {
        return false;
      }
    } else if (!name.equals(other.name)) {
      return false;
    }
    if (password == null) {
      if (other.password != null) {
        return false;
      }
    } else if (!password.equals(other.password)) {
      return false;
    }
    if (role == null) {
      if (other.role != null) {
        return false;
      }
    } else if (!role.equals(other.role)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "User [name=" + name + ", email=" + email + ", password=" + password + ", role="
      + role + ", accounts=" + accounts + ", lock=" + lock + "]";
  }

}
