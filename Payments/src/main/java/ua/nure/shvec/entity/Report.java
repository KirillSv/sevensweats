/**
* @author Shvec.K.
* @version 1.0
*/
package ua.nure.shvec.entity;

import java.io.Serializable;

/**
 * The main entity. Helps to get and keep DB rows and cells contain reports
 * info.
 * 
 * @author Shvec.K.
 * @version 1.0
 */
public class Report implements Serializable {
  private static final long serialVersionUID = 1L;

  private String data;
  private String customerEmail;
  private String message;

  /**
   * Main method constructor.
   * 
   * @param data          data
   * @param customerEmail customer email
   * @param message       text report message
   */
  public Report(String data, String customerEmail, String message) {
    this.data = data;
    this.customerEmail = customerEmail;
    this.message = message;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((customerEmail == null) ? 0 : customerEmail.hashCode());
    result = prime * result + ((data == null) ? 0 : data.hashCode());
    result = prime * result + ((message == null) ? 0 : message.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Report other = (Report) obj;
    if (customerEmail == null) {
      if (other.customerEmail != null) {
        return false;
      }
    } else if (!customerEmail.equals(other.customerEmail)) {
      return false;
    }
    if (data == null) {
      if (other.data != null) {
        return false;
      }
    } else if (!data.equals(other.data)) {
      return false;
    }
    if (message == null) {
      if (other.message != null) {
        return false;
      }
    } else if (!message.equals(other.message)) {
      return false;
    }
    return true;
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  public String getCustomerEmail() {
    return customerEmail;
  }

  public void setCustomerEmail(String customerEmail) {
    this.customerEmail = customerEmail;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "Report [time_data=" + data + ", customerEmail=" + customerEmail + ", account="
      + message + "]";
  }

}
