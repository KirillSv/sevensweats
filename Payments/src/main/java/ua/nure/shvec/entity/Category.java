package ua.nure.shvec.entity;

/*
 * Support class contains enum category for payments
* @author Shvec.K.
* @version 1.0
*/
public enum Category {
	INCOME_FROM_CASH, INCOME_FROM_TRANSFER, PAYS, ACC_EXCHANGE
}