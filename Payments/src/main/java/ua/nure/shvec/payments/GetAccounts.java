package ua.nure.shvec.payments;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Account;
import ua.nure.shvec.entity.Report;
import ua.nure.shvec.entity.User;
import ua.nure.shvec.listeners.Listener;

/*
 * The main servlet. It all starts from this servlet Get's accounts info, put's
 * some attribute and other.
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/getaccounts")
public class GetAccounts extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(GetAccounts.class);
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    LOGGER.debug("/pays/getaccounts#doget started.");
    HttpSession session = request.getSession();

    List<Account> accounts = null;
    List<Report> reports = null;
    List<User> listAllCustomers = null;

    String userEmail = request.getUserPrincipal().getName();

    session.setAttribute("isAdmin", false);
    session.setAttribute("trancs", null);
    boolean status = false;

    if (request.isUserInRole("admin")) {
      session.setAttribute("isAdmin", true);
    }

    try {
      listAllCustomers = factoryDAO.getUserDao().getAllUsersData();
      reports = factoryDAO.getReportDao().getReports();
      accounts = factoryDAO.getAccountDao().getAccounts(userEmail);
      status = factoryDAO.getUserDao().isCustomerLocked(userEmail);
      LOGGER.debug("Have gotten accounts and status.");
      session.setAttribute("countVisitors", Listener.totalVisitor());
    } catch (SQLException e) {
      LOGGER.error("Cannot execute querry.", e);
    }

    if (accounts.size() == 0) {
      accounts = null;
    }
    session.setAttribute("reports", reports);
    if (session.getAttribute("balance") == null && accounts != null) {
      session.setAttribute("balance", accounts.get(0).getAccBalance());
    }
    session.setAttribute("pdfErrorMessage", null);
    session.setAttribute("forButtonOpenPDF", null);
    session.setAttribute("listAllCustomers", listAllCustomers);
    session.setAttribute("accounts", accounts);
    session.setAttribute("customerStatus", status);
    response.sendRedirect("pays/mypays.jsp");

  }

  @SuppressWarnings("unchecked")
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    LOGGER.debug("Servlet getAccounts#doPost start");

    boolean status = false;

    HttpSession session = request.getSession();
    String userEmail = request.getParameter("userEmail");

    List<Account> accounts = (List<Account>) session.getAttribute("accounts");

    if (request.isUserInRole("admin")) {
      session.setAttribute("isAdmin", true);
    }
    try {

      if (request.getParameter("verified") != null) {
        Account acc = factoryDAO.getAccountDao().getAccount(request.getParameter("verified"));
        System.out.println("acc.ver = " + acc.isVerified());
        Boolean bool = !(acc.isVerified());
        factoryDAO.getAccountDao().setVerified(request.getParameter("verified"), bool);
      }

      accounts = factoryDAO.getAccountDao().getAccounts(userEmail);
      status = factoryDAO.getUserDao().isCustomerLocked(userEmail);

    } catch (SQLException e) {
      LOGGER.error("Cannot execute querry.", e);
    }

    if (accounts.size() == 0) {
      accounts = null;
    }

    session.setAttribute("userEmail", userEmail);
    session.setAttribute("customerStatus", status);
    session.setAttribute("accounts", accounts);
    response.sendRedirect("../admin/admin.jsp");

  }

}
