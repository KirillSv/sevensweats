package ua.nure.shvec.payments;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Account;
import ua.nure.shvec.entity.Transactions;
/*
 * WebServlet. It help's to get payments from DB.
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/getpays")
public class GetPays extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();
  private static final Logger LOGGER = Logger.getLogger(GetPays.class);

  protected void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("/pays/getpays#doGet started");
    HttpSession session = request.getSession();
    String userEmail = request.getUserPrincipal().getName();

    List<Transactions> pays = null;
    List<Account> accounts = null;
    try {
      pays = factoryDAO.getTransactionsDao().getPayments(userEmail);
      accounts = factoryDAO.getAccountDao().getAccounts(userEmail);

    } catch (SQLException e) {
      e.printStackTrace();
    }
    LOGGER.debug("/pays/getpays#doGet finished success");
    session.setAttribute("accounts", accounts);
    session.setAttribute("payments", pays);
    response.sendRedirect("./cart.jsp");

  }

}
