package ua.nure.shvec.payments;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;

/*
 * WebServlet ConformPay. It add a new payments to DB.
 * 
 * @author Shvec K.
 * @version 1.0
 */
@WebServlet("/pays/confpay")
public class ConformPay extends HttpServlet {
  private static final Logger LOGGER = Logger.getLogger(ConformPay.class);
  private static final long serialVersionUID = 1L;
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();

  protected void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("/pays/confpay#dopost start");
    String confirm = request.getParameter("confirm");
    String dtime = request.getParameter("delete");

    LOGGER.debug("confirmed=" + confirm + " deleted=" + dtime);

    String userEmail = request.getUserPrincipal().getName();
    if (dtime != null) {

      Boolean delPayResult = factoryDAO.getTransactionsDao().delPayment(dtime);
      request.getSession().setAttribute("delPayResult", delPayResult);
      LOGGER.debug("payment was deleted.");
    }
    if (confirm != null) {

      Boolean confPayResult = factoryDAO.getTransactionsDao().confPayment(confirm, userEmail);
      factoryDAO.getTransactionsDao().delPayment(confirm);
      request.getSession().setAttribute("confPayResult", confPayResult);
      LOGGER.debug("payment was confirmed.");
    }

    response.sendRedirect("./getpays");
  }

}
