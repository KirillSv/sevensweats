package ua.nure.shvec.payments;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Account;

/*
 * Servlet to block one of his accounts by the user
 * 
 * @author Shvec K.
 * @version 1.0
 */
@WebServlet("/pays/changeStatusUserAccounts")
public class ChangeStatusUserAccounts extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();
  private static final Logger LOGGER = Logger.getLogger(GetAccounts.class);

  protected void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("/pays/changeStatusUserAccounts start account=" + request.getParameter("account"));

    HttpSession session = request.getSession();

    String userEmail = request.getUserPrincipal().getName();

    if (request.isUserInRole("admin")) {
      // neccessary for show account table in admin.jsp
      userEmail = (String) session.getAttribute("userEmail"); 
    }
    try {

      Account acc = factoryDAO.getAccountDao().getAccount(request.getParameter("account"));
      Boolean b = !(acc.isVerified());

      factoryDAO.getAccountDao().setVerified(request.getParameter("account"), b);

      List<Account> accounts = factoryDAO.getAccountDao().getAccounts(userEmail);

      session.setAttribute("accounts", accounts);

    } catch (SQLException e) {
      LOGGER.error("Cannot execute querry", e);
    }
    LOGGER.debug("/pays/changeStatusUserAccounts finished success");
    if (request.isUserInRole("admin")) {
      response.sendRedirect("../admin/admin.jsp");
    } else {
      response.sendRedirect("./mypays.jsp");
    }
  }

}
