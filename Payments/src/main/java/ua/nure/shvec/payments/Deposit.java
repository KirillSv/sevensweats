package ua.nure.shvec.payments;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Account;
import ua.nure.shvec.exceptions.AccountIsBlockedException;
/*
 * WebServlet deposit. It add new deposit to DB.
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/putdeposit")
public class Deposit extends HttpServlet {
  private static final Logger LOGGER = Logger.getLogger(Deposit.class);
  private static final long serialVersionUID = 1L;
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();

  protected void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("/pays/putdeposit#dopost started.");

    String account = request.getParameter("selectAcc");
    String userEmail = request.getUserPrincipal().getName();
    HttpSession session = request.getSession();
    boolean status = (boolean) session.getAttribute("customerStatus");
    if (status != true) {
      List<Account> accounts = null;
      boolean success = false;
      double income = Double.parseDouble(request.getParameter("addMoney"));

      try {
        try {
          success = factoryDAO.getTransactionsDao().addMoney(account, income);
        } catch (AccountIsBlockedException e) {
          session.setAttribute("AccountIsBlockedException", e.getLocalizedMessage());
        }
        request.getSession().setAttribute("incomeSuccess", success);
        accounts = factoryDAO.getAccountDao().getAccounts(userEmail);
      } catch (SQLException e) {
        LOGGER.error("Cannot execute querry.", e);
      }
      session.setAttribute("accounts", accounts);
      LOGGER.debug("/pays/putdeposit#dopost finished success.");
      response.sendRedirect("./deposit.jsp");
    } else {
      response.sendRedirect("./mypays.jsp");
    }
  }

}
