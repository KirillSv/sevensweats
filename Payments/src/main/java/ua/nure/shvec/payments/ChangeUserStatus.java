package ua.nure.shvec.payments;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;

/*
 * Servlet change User status. 
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/changeUserStatus")
public class ChangeUserStatus extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(ChangeUserStatus.class);
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    LOGGER.debug(request.getParameter("locked"));

    boolean status = false;

    HttpSession session = request.getSession();
    String userEmail = request.getParameter("locked");
    try {
      Boolean b = (factoryDAO.getUserDao().getCustomer(userEmail)).getLock();

      factoryDAO.getUserDao().setLocked(userEmail, !b);

      status = factoryDAO.getUserDao().isCustomerLocked(userEmail);

    } catch (SQLException e) {
      LOGGER.error("Cannot execute querry.", e);
    }

    session.setAttribute("userEmail", userEmail);
    session.setAttribute("customerStatus", status);
    response.sendRedirect("./admin/admin.jsp");

  }

}
