package ua.nure.shvec.payments;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Transactions;

/*
 * Servlet helps to get transactions from DB.
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/gettransactions")
public class GetTransactions extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();
  private static final Logger LOGGER = Logger.getLogger(GetTransactions.class);

  protected void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("/pays/gettransactions#dopost started.");

    HttpSession session = request.getSession();
    String account = request.getParameter("selectAcc");
    List<Transactions> trancs = null;
    try {

      trancs = factoryDAO.getTransactionsDao().getTransactions(account);
      LOGGER.warn("Transactions was getting.");

    } catch (SQLException e) {
      LOGGER.error("Cannot execute querry.", e);
    }
    session.setAttribute("trancs", trancs);
    session.setAttribute("pdfErrorMessage", null);
    session.setAttribute("forButtonOpenPDF", null);
    if (request.isUserInRole("admin")) {
      response.sendRedirect("../admin/admin.jsp");
    }
    else {
      response.sendRedirect("./history.jsp");
    }
  }

}
