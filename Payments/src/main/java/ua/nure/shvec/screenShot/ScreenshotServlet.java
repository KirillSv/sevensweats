package ua.nure.shvec.screenShot;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.filechooser.FileSystemView;
import org.apache.log4j.Logger;

/*
 * Servlet implementation class ScreenshotServlet servlet make FILE_TYPE file
 * screenshot and save him on FILE_PATH
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/screenshot")
public class ScreenshotServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(ScreenshotServlet.class);
  private static final String FILE_PATH = "screenHistory.jpg";
  private static final String FILE_TYPE = "jpg";

  protected void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("/pays/screenshot#doGet started");
    try {
      ImageIO.write(grabScreen(), FILE_TYPE, new File(getHomeDir(), FILE_PATH));
    } catch (IOException e) {
      LOGGER.error("/pays/screenshot#doGet error");
      e.printStackTrace();
    }
    LOGGER.debug("/pays/screenshot#doGet success");
    response.sendRedirect("./history.jsp");
  }

  private static File getHomeDir() {
    FileSystemView fsv = FileSystemView.getFileSystemView();
    return fsv.getHomeDirectory();
  }

  private static BufferedImage grabScreen() {
    try {
      return new Robot()
          .createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
    } catch (AWTException | SecurityException e) {
      e.printStackTrace();
    }
    return null;
  }

}