package ua.nure.shvec.exceptions;

/*
 * Class Exception. Trows exception when Account is blocked and some transactions is prohibited.
 * 
 * @author Shvec.K.
 * @version 1.0
 */
public class AccountIsBlockedException extends Exception {

  private static final long serialVersionUID = 1L;

  public AccountIsBlockedException(String message) {
    super(message);
  }

}