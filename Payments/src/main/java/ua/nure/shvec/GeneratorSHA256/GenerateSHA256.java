package ua.nure.shvec.GeneratorSHA256;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/*
 * Support class help's generate SHA256 for passwords.
 * 
 * @author Shvec K.
 * @version 1.0
 * @param UTF-8 string password.
 * @return SHA256 string password.
 */
public class GenerateSHA256 {
  public static String generate(String pass) {

    MessageDigest digest;
    String result = "";
    try {
      digest = MessageDigest.getInstance("SHA-256");
      digest.update(pass.getBytes("Cp1251"));

      byte[] hashArr = digest.digest();

      StringBuilder finale = new StringBuilder();
      StringBuilder hash = new StringBuilder();

      for (byte bt : hashArr) {
        char everyByte = 0xFF;
        finale.append(Integer.toHexString(everyByte & bt));
        if (finale.length() == 1) {
          hash.append('0');
        }
        hash.append(finale);
        finale.setLength(0);
      }

      result = hash.toString().toUpperCase(Locale.ENGLISH);
    } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return result;
  }

}
