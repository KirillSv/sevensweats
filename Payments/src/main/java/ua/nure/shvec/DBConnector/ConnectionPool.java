package ua.nure.shvec.DBConnector;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/*
* Class make Connection pool 
* @author Shvec.K.
* @version 1.0
*/
public class ConnectionPool {

  private ConnectionPool() {
  }

  private static ConnectionPool instance = null;

  /**
   * Method that ensures that the class has only one instance and provides a
   * global access point to it.
   * 
   * @return class instance
   */
  public static ConnectionPool getInstance() {
    if (instance == null) {
      instance = new ConnectionPool();
    }
    return instance;
  }

  /**
   *  Method get connection.
   * @return Connection
   */
  public Connection getConnection() {
    Connection con = null;
    try {

      Context initCtx = new InitialContext();
      Context envCtx = (Context) initCtx.lookup("java:comp/env");

      DataSource ds = (DataSource) envCtx.lookup("jdbc/paymentsdb");

      con = (Connection) ds.getConnection();
    } catch (NamingException | SQLException ex) {
      ex.printStackTrace();

      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      ex.printStackTrace(pw);
      System.out.println(sw);
    }
    return con;
  }

  /**
   * Support method for close connection database.
   */
  public static void close(Connection con) {
    if (con != null) {
      try {
        con.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Support method for close resultset.
   */
  public static void close(ResultSet rs) {
    if (rs != null) {
      try {
        rs.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Support method for close connection prepared statement.
   */
  public static void close(PreparedStatement ps) {
    if (ps != null) {
      try {
        ps.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}
