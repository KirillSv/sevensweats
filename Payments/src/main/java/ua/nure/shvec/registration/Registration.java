package ua.nure.shvec.registration;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import ua.nure.shvec.GeneratorSHA256.GenerateSHA256;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.User;

/*
 * Servlet check request and helps to get registration in DB.
 * 
 * @author Shvec K.
 * @version 1.0
 */
@WebServlet("/unauth/regis")
public class Registration extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();
  private static final Logger LOGGER = Logger.getLogger(Registration.class);

  protected void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    response.sendRedirect("registration.jsp");
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("/unauth/regis#dopost started.");

    String name = request.getParameter("name");
    String email = request.getParameter("email");
    String pass = request.getParameter("pass");
    boolean flag = false;
    if (!pass.equals((String) request.getParameter("repass"))) {
      LOGGER.debug("pass not eqals repass");
      response.sendRedirect("registration.jsp");
      flag = true;
    }
    if (!(request.getParameter("name").length() < 2 && request.getParameter("email").length() < 2
        && request.getParameter("pass").length() < 2)) {

      User user = new User(name, email, GenerateSHA256.generate(pass));

      try {
        factoryDAO.getUserDao().createUser(user);
        LOGGER.debug("User " + email + " created correctly.");
      } catch (SQLException e) {
        response.getWriter().println(e);
        LOGGER.error("User create failed.", e);
      }

    }
    if (!flag) {
      response.sendRedirect("../");
    }
  }

}
