package ua.nure.shvec.listeners;

import javax.servlet.http.HttpSessionActivationListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.log4j.Logger;
import ua.nure.shvec.payments.AddAccount;

/**
 * Application Lifecycle Listener implementation class Listener this listener
 * using for counts customer on server for all time when servers worked.
 * @author Shvec.K.
 * @version 1.0
 */
public class Listener implements HttpSessionListener, HttpSessionAttributeListener,
    HttpSessionActivationListener, HttpSessionBindingListener {
  private static final Logger LOGGER = Logger.getLogger(AddAccount.class);
  static int countVisitorsToday = 1;

  public static int totalVisitor() {
    return countVisitorsToday;
  }

  public void sessionCreated(HttpSessionEvent se) {
    LOGGER.warn("sessionCreated  Listener counted vistors. Everything ok");
    countVisitorsToday++;
  }

  @Override
  public void valueBound(HttpSessionBindingEvent event) {
  }

  @Override
  public void valueUnbound(HttpSessionBindingEvent event) {

  }

  @Override
  public void sessionWillPassivate(HttpSessionEvent se) {

  }

  @Override
  public void sessionDidActivate(HttpSessionEvent se) {
  }

  @Override
  public void attributeAdded(HttpSessionBindingEvent event) {
  }

  @Override
  public void attributeRemoved(HttpSessionBindingEvent event) {
  }

  @Override
  public void attributeReplaced(HttpSessionBindingEvent event) {
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent se) {
  }

}
