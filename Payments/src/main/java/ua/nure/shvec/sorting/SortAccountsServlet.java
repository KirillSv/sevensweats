package ua.nure.shvec.sorting;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.entity.Account;

/*
 * Servlet implementation class SortServlet 
 * class contains methods for sorting  Account table columns
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/sortAccount")
public class SortAccountsServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(SortAccountsServlet.class);

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    LOGGER.debug("/pays/SortAccountServlet#dopost started.");
    HttpSession session = request.getSession();
    String nameSortedColumn = request.getParameter("whatNeedSort");
    String returnPath = null;
    @SuppressWarnings("unchecked")
    List<Account> accounts = new ArrayList<Account>((List<Account>) session.getAttribute("accounts"));

    if (nameSortedColumn.equals("Acc_balance")) {

      Collections.sort(accounts, new Comparator<Account>() {
        public int compare(Account o1, Account o2) {
          return Integer.parseInt(o1.getAccBalance()) - Integer.parseInt(o2.getAccBalance());

        }
      });
      returnPath = "./mypays.jsp";
    }

    if (nameSortedColumn.equals("Acc_name")) {

      Collections.sort(accounts, new Comparator<Account>() {
        public int compare(Account o1, Account o2) {
          return o1.getAccount().compareTo(o2.getAccount());

        }
      });
      returnPath = "./mypays.jsp";
    }

    session.setAttribute("accounts", accounts);
    LOGGER.debug("/pays/SortAccountsServlet#dopost finished.");
    response.sendRedirect(returnPath);
  }

}
