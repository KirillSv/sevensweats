package ua.nure.shvec.sorting;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.entity.Transactions;

/*
 * Servlet implementation class SortServlet 
 * class contains methods for sorting Transactions table columns
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/sortTransactions")
public class SortTransactionsServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(SortAccountsServlet.class);

  protected void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("/pays/SortServlet#dopost started.");
    HttpSession session = request.getSession();
    String nameSortedColumn = request.getParameter("whatNeedSort");
    String returnPath = null;

    @SuppressWarnings("unchecked")
    List<Transactions> transactions = new ArrayList<Transactions>((List<Transactions>) session.getAttribute("trancs"));

    if (nameSortedColumn.equals("Payment_data")) {

      Collections.sort(transactions, new Comparator<Transactions>() {
        public int compare(Transactions o1, Transactions o2) {
          return o1.getDtime().compareTo(o2.getDtime());

        }
      });
      session.setAttribute("whatNeedSort", null);
      returnPath = "./history.jsp";
    }

    if (nameSortedColumn.equals("Payment_dataRevers")) {

      Collections.sort(transactions, new Comparator<Transactions>() {
        public int compare(Transactions o2, Transactions o1) {
          System.out.println(o1.getDtime() + " " + o2.getDtime());
          return o1.getDtime().compareTo(o2.getDtime());

        }

      });
      session.setAttribute("whatNeedSort", true);
      returnPath = "./history.jsp";
    }

    session.setAttribute("trancs", transactions);
    LOGGER.debug("/pays/SortTransactionsServlet#dopost finished.");
    response.sendRedirect(returnPath);
  }

}
