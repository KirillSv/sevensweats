package ua.nure.shvec.dao;

import java.sql.SQLException;
import java.util.List;
import ua.nure.shvec.entity.Account;
import ua.nure.shvec.exceptions.AccountIsBlockedException;

/*
 * DAO interface.
 * 
 * @author Shvec.K.
 * @version 1.0
 */
public interface AccountDao {

  Account getAccount(String account) throws SQLException;

  String sendMoney(String account1, String account2, double money) 
      throws SQLException, AccountIsBlockedException;

  void setVerified(String account, Boolean boolVerified) throws SQLException;

  void checkBlockAccount(String account) throws SQLException, AccountIsBlockedException;

  boolean addAccount(String currUserEmail, String account, String info) throws SQLException;

  List<Account> getAccounts(String userEmail) throws SQLException;

}
