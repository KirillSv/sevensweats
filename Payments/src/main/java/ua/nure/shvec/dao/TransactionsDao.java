package ua.nure.shvec.dao;

import java.sql.SQLException;
import java.util.List;
import ua.nure.shvec.entity.Transactions;
import ua.nure.shvec.exceptions.AccountIsBlockedException;
/*
* DAO interface.
* @author Shvec.K.
* @version 1.0
*/

public interface TransactionsDao {

  boolean addMoney(String account, double income) throws SQLException, AccountIsBlockedException;

  List<Transactions> getPayments(String userEmail) throws SQLException;

  boolean delPayment(String dtime);

  boolean confPayment(String dtime, String userEmail);

  List<Transactions> getTransactions(String account) throws SQLException;

  int addPayment(String account, String userEmail, String toPay, String toPayInfo, Double money)
      throws SQLException, AccountIsBlockedException;

}
