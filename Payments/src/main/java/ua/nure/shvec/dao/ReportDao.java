package ua.nure.shvec.dao;

import java.sql.SQLException;
import java.util.List;
import ua.nure.shvec.entity.Report;
/*
* DAO interface 
* @author Shvec.K.
* @version 1.0
*/

public interface ReportDao {

  void deleteReport(String data);

  void setReportfromUsertoAdmin(String userEmail, String inputText) throws SQLException;

  List<Report> getReports() throws SQLException;

}
