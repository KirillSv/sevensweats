package ua.nure.shvec.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import ua.nure.shvec.DBConnector.ConnectionPool;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.dao.TransactionsDao;
import ua.nure.shvec.entity.Category;
import ua.nure.shvec.entity.Transactions;
import ua.nure.shvec.exceptions.AccountIsBlockedException;

/**
 * DAO class implements AccountDao Get and keep DB rows and cells contain
 * transaction's info.
 * 
 * @author Shvec.K.
 * @version 1.0
 */

public class TransactionsDaoImpl implements TransactionsDao {

  private static final Logger LOGGER = Logger.getLogger(TransactionsDaoImpl.class);
  private static final double MAX_BALANCE_VALUE = 9999999.99d;
  private static final String SQL_ADD_PREP_PAYM = "INSERT INTO payments(customer_email, account,summary,comment,total ) values (?, ?, ?, ?, ?);";
  private static final String SQL_GET_TRANSACTION = "select * from transactions where account=?;";
  private static final String SQL_ADD_MONEY_HISTORY = "INSERT INTO transactions(category,account,summary,total,balance ) values (?, ?, ?, ?, ?);";
  private static final String SQL_GET_PREP_PAYM = "select * from payments where dtime = ? && customer_email=?;";
  private static final String SQL_GET_PAYMENT = "select * from payments where customer_email = ?;";
  private static final String SQL_DEL_PREP_PAYM = "DELETE from payments where dtime = ?;";
  private static final String SQL_GET_BALANCE = "select acc_balance from accounts WHERE account=?;";
  private static final String SQL_ADD_MONEY = "UPDATE accounts SET acc_balance=? WHERE account=?;";

  /*
   * Method take connection and trying to Execute query to add income for balance.
   * 
   * @return result boolean
   * 
   * @throws AccountIsBlockedException
   */
  @Override
  public boolean addMoney(String account, double income) throws SQLException, AccountIsBlockedException {
    LOGGER.debug("add Money() start");
    FactoryDAO factoryDAO = FactoryDAO.getInstance();
    factoryDAO.getAccountDao().checkBlockAccount(account);

    boolean result = false;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    double balance = 0d;
    double money = 0d;
    try {
      con = ConnectionPool.getInstance().getConnection();
      con.setAutoCommit(false);
    } catch (SQLException e1) {
      LOGGER.error("setAutoComit error", e1);
    }
    try {

      ps = con.prepareStatement(SQL_GET_BALANCE);
      LOGGER.debug(ps);
      ps.setString(1, account);
      System.out.println(ps);
      rs = ps.executeQuery();

      while (rs.next()) {
        balance = rs.getDouble("acc_balance");
      }
      ps.clearBatch();
      ps = con.prepareStatement(SQL_ADD_MONEY);
      LOGGER.debug(ps);
      if ((income + balance) < MAX_BALANCE_VALUE) {
        money = income + balance;
      } else {
        LOGGER.error("max value ballance is exceeded");
        return false;
      }
      ps.setDouble(1, money);
      ps.setString(2, account);
      System.out.println(ps);
      ps.executeUpdate();
      // block to add history
      ps.clearBatch();

      ps = con.prepareStatement(SQL_ADD_MONEY_HISTORY);
      LOGGER.debug(ps);
      ps.setString(1, Category.INCOME_FROM_CASH.toString());
      ps.setString(2, account);
      ps.setString(3, "income from cash");
      ps.setDouble(4, income);
      ps.setDouble(5, money);
      ps.executeUpdate();
      con.commit();
      result = true;

    } catch (SQLException e) {
      LOGGER.error(" error", e);
      con.rollback();
    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      con.setAutoCommit(true);
      ConnectionPool.close(con);
    }
    LOGGER.debug("addMoney() finished return " + result);
    return result;
  }

  /*
   * Class take connection and trying to Execute query to add payment to cart's
   * history
   * 
   * @return res is boolean value of result operation
   * 
   * @throws AccountIsBlockedException
   */
  @Override
  public int addPayment(String account, String userEmail, String toPay,
      String toPayInfo, Double money)throws SQLException, AccountIsBlockedException {
    LOGGER.debug("addPayment() start");
    FactoryDAO factoryDAO = FactoryDAO.getInstance();
    factoryDAO.getAccountDao().checkBlockAccount(account);
    int res = 1;
    String summary = "from " + account + " to " + toPay;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      con.setAutoCommit(false);
    } catch (SQLException e1) {
      LOGGER.error("setAutoComit error", e1);
    }

    try {
      ps = con.prepareStatement(SQL_ADD_PREP_PAYM);
      LOGGER.debug(ps);
      ps.setString(1, userEmail);
      ps.setString(2, account);
      ps.setString(3, summary);
      ps.setString(4, toPayInfo);
      ps.setDouble(5, money);
      ps.executeUpdate();
      con.commit();

    } catch (SQLException e) {
      LOGGER.error("error", e);
      con.rollback();
      res = 11;
    }

    finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      con.setAutoCommit(true);
      ConnectionPool.close(con);
    }
    LOGGER.debug("addPayment() finished return" + res);
    return res;
  }

  /*
   * Method take connection and trying to Execute query to get payments list
   * 
   * @param userEmail String customer email
   * 
   * @return List of Transactions
   */
  @Override
  public List<Transactions> getPayments(String userEmail) throws SQLException {
    LOGGER.debug("getPayment() start");
    PreparedStatement ps = null;
    ResultSet rs = null;
    List<Transactions> payments = new ArrayList<Transactions>();
    Transactions tranc;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_GET_PAYMENT);
      LOGGER.debug(ps);
      ps.setString(1, userEmail);
      rs = ps.executeQuery();
      while (rs.next()) {
        tranc = new Transactions(rs.getString(1), Category.PAYS.toString(), rs.getString(3),
            rs.getString(4) + " " + rs.getString(5), rs.getDouble(6), 0d);
        payments.add(tranc);
      }
    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    return payments;

  }

  /*
   * Class take connection and trying to Execute query to delete payment from cart
   * history.
   * 
   * @param dtime String date format 2016-08-25 15:45:59.0
   * 
   * @return res is boolean value of result operation
   */
  @Override
  public boolean delPayment(String dtime) {
    LOGGER.debug("delPayment() start");
    boolean res = false;
    PreparedStatement ps = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_DEL_PREP_PAYM);
      LOGGER.debug(ps);
      ps.setString(1, dtime);
      ps.execute();

    } catch (SQLException e) {
      LOGGER.error(" error", e);
      e.printStackTrace();
    } finally {
      try {
        ConnectionPool.close(ps);
        con.setAutoCommit(true);
        ConnectionPool.close(con);
      } catch (SQLException e) {
        LOGGER.error("block finally error", e);
        e.printStackTrace();
      }
    }
    LOGGER.debug("getPayment() finished return" + res);
    return res;
  }

  /*
   * Class take connection and trying to Execute query to confirm payment.
   * 
   * @param time payment and user email.
   * 
   * @return boolean result.
   * 
   */
  @SuppressWarnings("resource")
  @Override
  public boolean confPayment(String dtime, String userEmail) {
    LOGGER.debug("confPayment() start");
    boolean res = false;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    double balance = 0d;
    try {
      con = ConnectionPool.getInstance().getConnection();
      con.setAutoCommit(false);
    } catch (SQLException e1) {
      LOGGER.error("setAutoComit error.", e1);
      e1.printStackTrace();
    }

    Transactions tranc = new Transactions();
    try {
      ps = con.prepareStatement(SQL_GET_PREP_PAYM);
      LOGGER.debug(ps);
      ps.setString(1, dtime);
      ps.setString(2, userEmail);
      rs = ps.executeQuery();
      while (rs.next()) {
        tranc = new Transactions(rs.getString(1), Category.PAYS.toString(), rs.getString(3),
            rs.getString(4) + " " + rs.getString(5), rs.getDouble(6), 0d);
      }
      ps.clearBatch();
      // Get balance
      ps = con.prepareStatement(SQL_GET_BALANCE);
      LOGGER.debug(ps);
      ps.setString(1, tranc.getAccount());
      rs = ps.executeQuery();
      while (rs.next()) {
        balance = rs.getDouble("acc_balance");
      }

      System.out.println("BALANCE = " + balance);
      if ((balance - tranc.getTotal()) < 0) {

        throw new SQLException("not enough money from acc.");
      }
      tranc.setBalance(balance - tranc.getTotal());

      ps.clearBatch();
      ps = con.prepareStatement(SQL_ADD_MONEY);
      LOGGER.debug(ps);
      ps.setDouble(1, tranc.getBalance());
      ps.setString(2, tranc.getAccount());
      ps.executeUpdate();

      ps.clearBatch();
      ps = con.prepareStatement(SQL_ADD_MONEY_HISTORY);
      LOGGER.debug(ps);
      ps.setString(1, Category.PAYS.toString());
      ps.setString(2, tranc.getAccount());
      ps.setString(3, tranc.getSummary());
      ps.setDouble(4, tranc.getTotal());
      ps.setDouble(5, tranc.getBalance());
      ps.executeUpdate();
      ps.clearBatch();
      con.commit();
      res = true;
    } catch (SQLException e) {
      LOGGER.error("sql error", e);
      e.printStackTrace();
    } finally {
      try {
        ConnectionPool.close(rs);
        ConnectionPool.close(ps);
        con.setAutoCommit(true);
        ConnectionPool.close(con);
      } catch (SQLException e) {
        LOGGER.error("block finally error.", e);
        e.printStackTrace();
      }
    }
    LOGGER.debug("confPayment() fnished return" + res);
    return res;
  }

  /*
   * Method take connection and trying to Execute query to get list with
   * transactions Information from DB.
   * 
   * @param number of account.
   * 
   * @return transactions list.
   */
  @Override
  public List<Transactions> getTransactions(String account) throws SQLException {
    LOGGER.debug("getTransactions() start");
    PreparedStatement ps = null;
    ResultSet rs = null;
    List<Transactions> transactions = new ArrayList<Transactions>();
    Transactions tranc;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_GET_TRANSACTION);
      LOGGER.debug(ps);
      ps.setString(1, account);
      rs = ps.executeQuery();
      while (rs.next()) {
        tranc = new Transactions(rs.getString(1), rs.getString(2), rs.getString(3),
            rs.getString(4), rs.getDouble(5),rs.getDouble(6));
        transactions.add(tranc);
      }
    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    return transactions;

  }

}
