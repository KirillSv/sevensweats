package ua.nure.shvec.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import ua.nure.shvec.DBConnector.ConnectionPool;
import ua.nure.shvec.dao.AccountDao;
import ua.nure.shvec.entity.Account;
import ua.nure.shvec.entity.Category;
import ua.nure.shvec.exceptions.AccountIsBlockedException;
/**
* DAO class implements AccountDao 
* Get and keep DB rows and cells 
* contain accounts's info.

* @author Shvec.K.
* @version 1.0
*/
public class AccountDaoImpl implements AccountDao {

  private static final Logger LOGGER = Logger.getLogger(AccountDaoImpl.class);
  private static final double MAX_BALANCE_VALUE = 9999999.99d;
  private static final String SQL_GET_ACCOUNT_INFO = "SELECT accounts.*, customers.locked FROM accounts, customers where customers.customer_email = ? AND accounts.customer_email = ?; ";
  private static final String SQL_ADD_CARD = "INSERT INTO accounts(customer_email,account,info) values(?, ?, ?);";
  private static final String SQL_SET_ACCOUNT_VERIF = "UPDATE accounts SET verified=? WHERE account=?; ";
  private static final String SQL_GET_ACCOUNT = "SELECT * FROM accounts WHERE account=?;";
  private static final String SQL_GET_ACCOUNT_VERIFIED = "SELECT verified FROM accounts WHERE account=?;";
  private static final String SQL_ADD_MONEY = "UPDATE accounts SET acc_balance=? WHERE account=?;";
  private static final String SQL_ADD_MONEY_HISTORY = "INSERT INTO transactions(category,account,summary,total,balance ) values (?, ?, ?, ?, ?);";
  private static final String SQL_GET_BALANCE = "select acc_balance from accounts WHERE account=?;";

  /*
   * Method take connection and trying to Execute query to add account into DB.
   * 
   * @param
   * @return result boolean.
   */
  
  @Override
  public boolean addAccount(String currUserEmail, String account, String info) throws SQLException {
    LOGGER.debug("start addAccount()");
    boolean res = false;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_ADD_CARD);
      LOGGER.debug(ps);
      ps.setString(1, currUserEmail);
      ps.setString(2, account);
      ps.setString(3, info);
      ps.executeUpdate();

      res = true;

    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    LOGGER.debug("finish addAccount() return" + res);
    return res;

  }

  /*
   * Method take connection and trying to Execute query to get accounts
   * information.
   * 
   * @param
   * @return list accounts info
   */
  @Override
  public List<Account> getAccounts(String userEmail) throws SQLException {
    LOGGER.debug(" start getAccounts()");
    PreparedStatement ps = null;
    ResultSet rs = null;
    List<Account> accounts = new ArrayList<Account>();
    Account acc;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_GET_ACCOUNT_INFO);
      LOGGER.debug(ps);
      ps.setString(1, userEmail);
      ps.setString(2, userEmail);
      rs = ps.executeQuery();
      while (rs.next()) {
        acc = new Account(rs.getString("customer_email"), rs.getString("account"),
            rs.getString("info"),rs.getString("acc_balance"), rs.getBoolean("verified"),
            rs.getBoolean("locked"));
        acc.setCurrency(rs.getString("currency"));
        accounts.add(acc);
      }
    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    return accounts;

  }

  /*
   * Supporting method checks user account lockout before performing operations in
   * methods addMoney(), sendMoney(), addPayment()
   * 
   * @param
   * @return result boolean
   */
  @Override
  public void checkBlockAccount(String account) throws SQLException, AccountIsBlockedException {
    LOGGER.debug(" start checkBlockAccount() ");
    PreparedStatement ps = null;
    ResultSet rs = null;
    boolean result = true;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_GET_ACCOUNT_VERIFIED);
      LOGGER.debug(ps);
      ps.setString(1, account);

      rs = ps.executeQuery();
      while (rs.next()) {
        result = rs.getBoolean("verified");
      }
    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    LOGGER.debug(" checkBlockAccount() finished return=" + result);
    System.out.print("setBlock#finish set:" + result);

    if (result) {
      LOGGER.error("account +" + account + " is blocked.");
      throw new AccountIsBlockedException(" " + account + " ");
    }
  }

  /*
   * Method take connection and trying to Execute query to get account information
   * 
   * @param
   * @return type Account which contain addition info
   */
  @Override
  public Account getAccount(String account) throws SQLException {
    LOGGER.debug(" start getAccount() ");
    PreparedStatement ps = null;
    Account acc = null;
    ResultSet rs = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_GET_ACCOUNT);
      LOGGER.debug(ps);
      ps.setString(1, account);
      rs = ps.executeQuery();
      System.out.println(ps);
      while (rs.next()) {
        acc = new Account(rs.getBoolean("verified"), rs.getString("account"));
      }

    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    LOGGER.debug(" get Account finished. return " + acc);
    return acc;
  }

  /*
   * Method take connection and trying to Execute query to change balance between
   * accounts
   * 
   * @param
   * @return message can take "successfully" or "negative"
   * @throws AccountIsBlockedException
   */
  @SuppressWarnings("resource")
  @Override
  public String sendMoney(String account1, String account2, double money)
      throws SQLException, AccountIsBlockedException {
    LOGGER.debug(" start sendMoney() from " + account1 + " to " + account2 + " money=" + money);
    checkBlockAccount(account1);
    checkBlockAccount(account2);
    String message = "successfully";
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    double balance1 = 0d;
    double balance2 = 0d;
    double resBalance1 = 0d;
    double resBalance2 = 0d;
    try {
      con = ConnectionPool.getInstance().getConnection();
      con.setAutoCommit(false);
    } catch (SQLException e1) {
      LOGGER.error("setAutoComit error", e1);
    }
    try {
      // check balances possibility
      ps = con.prepareStatement(SQL_GET_BALANCE);
      LOGGER.debug(ps);
      ps.setString(1, account1);
      rs = ps.executeQuery();
      while (rs.next()) {
        balance1 = rs.getDouble("acc_balance");
      }
      ps.clearBatch();
      if (balance1 - money < 0) {
        LOGGER.error("Negative balance");
        return message = "negative";
      }
      ps = con.prepareStatement(SQL_GET_BALANCE);
      LOGGER.debug(ps);
      ps.setString(1, account2);
      rs = ps.executeQuery();
      while (rs.next()) {
        balance2 = rs.getDouble("acc_balance");
      }
      if (balance2 + money > MAX_BALANCE_VALUE) {
        LOGGER.error("balance overflow.");
        return message = "balance overflow.";
      }
      ps.clearBatch();
      ps = con.prepareStatement(SQL_ADD_MONEY);
      LOGGER.debug(ps);
      resBalance1 = balance1 - money;
      ps.setDouble(1, resBalance1);
      ps.setString(2, account1);
      ps.executeUpdate();
      resBalance2 = balance2 + money;
      ps.setDouble(1, resBalance2);
      ps.setString(2, account2);
      ps.executeUpdate();

      // block to add history
      ps.clearBatch();
      ps = con.prepareStatement(SQL_ADD_MONEY_HISTORY);
      LOGGER.debug(ps);
      ps.setString(1, Category.ACC_EXCHANGE.toString());
      ps.setString(2, account1);
      ps.setString(3, "exchange between own accounts " + account1 + " to " + account2);
      ps.setDouble(4, money);
      ps.setDouble(5, resBalance1);
      ps.executeUpdate();
      ps.clearBatch();
      ps = con.prepareStatement(SQL_ADD_MONEY_HISTORY);
      LOGGER.debug(ps);
      ps.setString(1, Category.INCOME_FROM_TRANSFER.toString());
      ps.setString(2, account2);
      ps.setString(3, "exchange between own accounts " + account1 + " to " + account2);
      ps.setDouble(4, money);
      ps.setDouble(5, resBalance2);
      ps.executeUpdate();
      con.commit();

    } catch (SQLException e) {
      LOGGER.error(" error", e);
      con.rollback();
    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      con.setAutoCommit(true);
      ConnectionPool.close(con);
    }
    LOGGER.debug("sendMoney() finished and return " + message);
    return message;
  }

  /*
   * Method take connection and trying to Execute query to set/upset verified flag
   * (block one from user`s accounts)
   * 
   * @param
   */
  @Override
  public void setVerified(String account, Boolean verified) throws SQLException {
    LOGGER.debug("setVerifed() start");
    PreparedStatement ps = null;
    Connection con = ConnectionPool.getInstance().getConnection();
    try {
      ps = con.prepareStatement(SQL_SET_ACCOUNT_VERIF);
      LOGGER.debug(ps);
      ps.setBoolean(1, verified);
      ps.setString(2, account);
      ps.executeUpdate();
      LOGGER.debug("setVerified#finish set:" + verified);
      System.out.print("setVerified#finish set:" + verified);
    } finally {
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
  }

}
