package ua.nure.shvec.dao.impl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import ua.nure.shvec.DBConnector.ConnectionPool;
import ua.nure.shvec.dao.UserDao;
import ua.nure.shvec.entity.User;

/**
* DAO class implements UserDao 
* Get and keep DB rows and cells 
* contain customer's and roles's info.

* @author Shvec.K.
* @version 1.0
*/

public class UserDaoImpl implements UserDao {

  private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class);

  private static final String SQL_ADD_USER = "INSERT INTO customers(customer_email,customer_name,password) values(?, ?, ?);";
  private static final String SQL_ADD_USER_ROLE = "INSERT INTO roles(customer_email, role_name) values(?, ?);";
  private static final String SQL_GET_CUSTOMER = "SELECT * FROM customers WHERE customer_email=?;";
  private static final String SQL_GET_CUSTOMER_LOCKED = "SELECT locked from customers WHERE customer_email=?;";
  private static final String SQL_SET_CUSTOMER_LOCKED = "UPDATE customers SET locked=? WHERE customer_email=?; ";
  private static final String SQL_GET_ALL_USERS = "SELECT * FROM customers;";

  /*
   * Method take connection and trying to Execute query to add new user into DB.
   * @param User object
   * @return result boolean
   */
  @Override
  public boolean createUser(User user) throws SQLException {
    LOGGER.debug("createUser() start");
    boolean res = false;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_ADD_USER);
      LOGGER.debug(ps);
      ps.setString(1, user.getEmail());
      ps.setString(2, user.getName());
      ps.setString(3, user.getPassword());
      System.out.println(ps);
      ps.executeUpdate();
      ps.clearBatch();
      ps = con.prepareStatement(SQL_ADD_USER_ROLE);
      LOGGER.debug(ps);
      ps.setString(1, user.getEmail());
      ps.setString(2, user.getRole());
      System.out.println(ps);
      ps.executeUpdate();
      res = true;

    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    LOGGER.debug("createUser() finished return " + res);
    return res;
  }

  /*
   * Method take connection and trying to get status Locked of customer.
   * 
   * @param userEmail to get access to database.
   * @return result boolean.
   */
  @Override
  public boolean isCustomerLocked(String userEmail) throws SQLException {
    LOGGER.debug("isCustomerLocked() start");
    boolean status = false;
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_GET_CUSTOMER_LOCKED);
      LOGGER.debug(ps);
      ps.setString(1, userEmail);
      rs = ps.executeQuery();
      while (rs.next()) {
        status = rs.getBoolean("locked");
        LOGGER.warn("isLocked " + userEmail + " is " + status);
      }

    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    LOGGER.debug("isCustomerLocked() finish return" + status);
    return status;
  }

  /*
   * Method take connection and trying to Execute query to set locked flag for
   * customer.
   * 
   * @param isLocked flag if equals try user is blocked and can`t make
   *                 transactions.
   */
  @Override
  public void setLocked(String customerEmail, Boolean isLocked) throws SQLException {
    LOGGER.debug("setLocked() start");
    PreparedStatement ps = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = (PreparedStatement) con.prepareStatement(SQL_SET_CUSTOMER_LOCKED);
      LOGGER.debug(ps);
      ps.setBoolean(1, isLocked);
      ps.setString(2, customerEmail);
      ps.executeUpdate();
      System.out.print("setLocked#finished");
      LOGGER.debug("setLocked#finished");
    } finally {
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
  }

  /*
   * Method take connection and trying to Execute query to get customer
   * information for administrator.
   * 
   * @param email is user`s email
   * @return user type User which contain addition info.
   */
  @Override
  public User getCustomer(String email) throws SQLException {
    LOGGER.debug("getCustomer() start");
    PreparedStatement ps = null;
    User user = null;
    ResultSet rs = null;
    Connection con = ConnectionPool.getInstance().getConnection();
    try {
      ps = con.prepareStatement(SQL_GET_CUSTOMER);
      LOGGER.debug(ps);
      ps.setString(1, email);
      rs = ps.executeQuery();
      while (rs.next()) {
        user = new User(rs.getString("customer_email"), rs.getBoolean("locked"));
      }

    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    LOGGER.debug("getCustomer() finished return" + user);
    return user;
  }

  /*
   * Method take connection and trying to Execute query to get customer
   * information for administrator.
   * 
   * @return List type User which contain addition info
   */
  @Override
  public List<User> getAllUsersData() throws SQLException {
    LOGGER.debug("getAllUserData() start");
    List<User> users = new ArrayList<User>();
    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = ConnectionPool.getInstance().getConnection();
    try {
      ps = con.prepareStatement(SQL_GET_ALL_USERS);
      LOGGER.debug(ps);
      rs = ps.executeQuery();
      while (rs.next()) {
        users.add(new User(rs.getString("customer_email"), rs.getString("customer_name"),
            rs.getString("password"),rs.getBoolean("locked")));
      }

    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }

    return users;
  }
}
