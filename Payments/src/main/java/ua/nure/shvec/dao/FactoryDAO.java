package ua.nure.shvec.dao;

import ua.nure.shvec.dao.impl.AccountDaoImpl;
import ua.nure.shvec.dao.impl.ReportDaoImpl;
import ua.nure.shvec.dao.impl.TransactionsDaoImpl;
import ua.nure.shvec.dao.impl.UserDaoImpl;

/*
 * Class Factory In order to keep the system independent of various types of
 * objects, the Factory Method pattern uses a polymorphism mechanism. This base
 * class defines a single interface through which the user will operate with
 * objects of finite types.
 * 
 * @author Shvec.K.
 * @version 1.0
 * 
 */
public class FactoryDAO {
  private static FactoryDAO factoryDAO;

  private FactoryDAO() {
  }

  /**
   * Method that ensures class has only one instance and provides a global access
   * point to it.
   * 
   * @return class instance
   */
  public static FactoryDAO getInstance() {
    if (factoryDAO == null) {
      factoryDAO = new FactoryDAO();
    }
    return factoryDAO;
  }

  public UserDao getUserDao() {
    return new UserDaoImpl();
  }

  public AccountDao getAccountDao() {
    return new AccountDaoImpl();
  }

  public ReportDao getReportDao() {
    return new ReportDaoImpl();
  }

  public TransactionsDao getTransactionsDao() {
    return new TransactionsDaoImpl();
  }

}
