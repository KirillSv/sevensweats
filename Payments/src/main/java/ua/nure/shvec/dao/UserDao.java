package ua.nure.shvec.dao;

import java.sql.SQLException;
import java.util.List;
import ua.nure.shvec.entity.User;

/*
* DAO interface.
* @author Shvec.K.
* @version 1.0
*/

public interface UserDao {

  boolean createUser(User user) throws SQLException;

  boolean isCustomerLocked(String userEmail) throws SQLException;

  void setLocked(String customerEmail, Boolean isLocked) throws SQLException;

  User getCustomer(String email) throws SQLException;

  List<User> getAllUsersData() throws SQLException;

}
