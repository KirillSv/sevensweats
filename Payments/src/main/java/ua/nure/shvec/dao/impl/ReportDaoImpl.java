package ua.nure.shvec.dao.impl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import ua.nure.shvec.DBConnector.ConnectionPool;
import ua.nure.shvec.dao.ReportDao;
import ua.nure.shvec.entity.Report;
/**
* DAO class implements ReportDao 
* Get and keep DB rows and cells 
* contain reports info.
* @author Shvec.K.
* @version 1.0
*/

public class ReportDaoImpl implements ReportDao {

  private static final Logger LOGGER = Logger.getLogger(ReportDaoImpl.class);
  private static final String SQL_DEL_REPORT = "DELETE from reports where data = ?;";
  private static final String SQL_SET_REPORT_FROM_USER = "INSERT INTO reports(customer_email, textreport) values(?, ?);";
  private static final String SQL_GET_ALL_REPORTS = "select * from reports;";

  /*
   * Method take connection and trying to Execute query to get reports information.
   * 
   * @param
   * @return list reports info.
   * @throws SQLException
   */
  @Override
  public List<Report> getReports() throws SQLException {
    LOGGER.debug("getReports() start");
    List<Report> reports = new ArrayList<Report>();

    PreparedStatement ps = null;
    ResultSet rs = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_GET_ALL_REPORTS);
      LOGGER.debug(ps);
      rs = ps.executeQuery();
      while (rs.next()) {
        reports.add(new Report(rs.getString("data"), rs.getString("customer_email"),
            rs.getString("textreport")));
      }
    } finally {
      ConnectionPool.close(rs);
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
    LOGGER.debug("getReports() finished");
    return reports;
  }

  /*
   * Method take connection and trying to Execute query set new report to database.
   * 
   * @param
   * @throws SQLException
   */
  @Override
  public void setReportfromUsertoAdmin(String userEmail, String inputText) throws SQLException {
    LOGGER.debug("setReportfromUsertoAdmin() start");
    PreparedStatement ps = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_SET_REPORT_FROM_USER);
      LOGGER.debug(ps);
      ps.setString(1, userEmail);
      ps.setString(2, inputText);
      System.out.println(ps);
      ps.executeUpdate();

    } finally {
      ConnectionPool.close(ps);
      ConnectionPool.close(con);
    }
  }

  /*
   * Method take connection and trying to Execute query delete report from database.
   * 
   * @param data - String contents string with sql data and time report.
   * @throws SQLException
   */
  @Override
  public void deleteReport(String data) {
    LOGGER.debug("delReport() start");
    PreparedStatement ps = null;
    Connection con = null;
    try {
      con = ConnectionPool.getInstance().getConnection();
      ps = con.prepareStatement(SQL_DEL_REPORT);
      LOGGER.debug(ps);
      ps.setString(1, data);
      ps.execute();

    } catch (SQLException e) {
      LOGGER.error(" error", e);
      e.printStackTrace();
    } finally {
      try {
        con.setAutoCommit(true);
        ConnectionPool.close(con);
        LOGGER.debug(ps);
        System.out.print("deleteReport#finish set:" + data);
      } catch (SQLException e) {
        LOGGER.error("block finally error.", e);
        e.printStackTrace();
      }
    }
  }

}
