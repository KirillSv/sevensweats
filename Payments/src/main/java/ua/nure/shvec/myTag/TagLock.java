package ua.nure.shvec.myTag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/*
 * Class to create and use custom JSTL tag.
 * 
 * @author Shvec K.
 * @version 1.0
 * @return warning colored message.
 */
public class TagLock extends TagSupport {
  private static final long serialVersionUID = 1L;
  private boolean value;

  public void setValue(boolean value) {
    this.value = value;
  }

  @Override
  public int doStartTag() throws JspException {
    try {
      if (this.value == true) {
        pageContext.getOut().print("<h6><strong><font color='red'>You can`t make transaction.</font></strong>"
            + "<img wight='30' height='30' src='./imagesForJsp/stop.gif'/></h6>");
      }
    } catch (IOException ioException) {
      throw new JspException("Error : " + ioException.getMessage());
    }
    return SKIP_BODY;
  }
}
