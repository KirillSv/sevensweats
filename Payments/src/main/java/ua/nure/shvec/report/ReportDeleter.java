package ua.nure.shvec.report;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Report;
import ua.nure.shvec.payments.GetAccounts;

/*
 * Servlet help administrator delete users report in DB.
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/report/reportDeleter")
public class ReportDeleter extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(GetAccounts.class);
	private FactoryDAO factoryDAO = FactoryDAO.getInstance();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		LOGGER.debug("/pays/report/reportDeleter#Post dataReport=" + request.getParameter("data"));
		HttpSession session = request.getSession();
		try {
			factoryDAO.getReportDao().deleteReport(request.getParameter("data"));
			List<Report> reports = factoryDAO.getReportDao().getReports();
			session.setAttribute("reports", reports);
		} catch (SQLException e) {
			LOGGER.error("Cannot execute querry", e);
		}
		response.sendRedirect("../mypays.jsp");

	}

}
