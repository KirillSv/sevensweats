package ua.nure.shvec.report;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.payments.GetAccounts;

/*
 * Servlet add users report to DB.
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/report/reportSender")
public class ReportSender extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(GetAccounts.class);
  private FactoryDAO factoryDAO = FactoryDAO.getInstance();

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String userEmail = request.getUserPrincipal().getName();
    LOGGER.debug("/pays/reportSender started account=" + userEmail + " text: " + request.getParameter("report"));
    try {
      factoryDAO.getReportDao().setReportfromUsertoAdmin(userEmail, request.getParameter("report"));
    } catch (SQLException e) {
      LOGGER.error("Cannot execute querry", e);
    }

    response.sendRedirect("../mypays.jsp");
  }

}
