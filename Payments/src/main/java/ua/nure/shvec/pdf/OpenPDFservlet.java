package ua.nure.shvec.pdf;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import ua.nure.shvec.filter.EncodingFilter;

/*
 * 
 * This Servet load file type FILE_TYPE report from desktop and open his.
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/openPdf")
public class OpenPDFservlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(EncodingFilter.class);
  private static final String FILEPATH = "D:\\ITextTest.pdf";
  private static final String FILE_TYPE = ".pdf";

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    LOGGER.debug("PDFopen#doPost starts");
    File file = new File(FILEPATH);
    if (file.toString().endsWith(FILE_TYPE)) {
      Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file);
    }
    else {
      Desktop desktop = Desktop.getDesktop();
      desktop.open(file);
    }
    LOGGER.debug("PDFopen#doPost finish");
    response.sendRedirect("./history.jsp");
  }

}
