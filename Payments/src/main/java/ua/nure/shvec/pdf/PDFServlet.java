package ua.nure.shvec.pdf;

import com.lowagie.text.Chapter;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Section;
import com.lowagie.text.pdf.CMYKColor;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import ua.nure.shvec.entity.Transactions;
import ua.nure.shvec.filter.EncodingFilter;
/*
 * 
 * This Servet make PDF report from history transaction and write file in the
 * file system.
 * 
 * @author Shvec K.
 * @version 1.0
 */

@WebServlet("/pays/pdf")
public class PDFServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = Logger.getLogger(EncodingFilter.class);
  private static final String FILEPATH = "D:\\Report.pdf";

  protected void doPost(HttpServletRequest request, HttpServletResponse response) 
      throws ServletException, IOException {
    LOGGER.debug("PDF servlet starts");
    HttpSession session = request.getSession();

    String cardNumber = null;
    try {

      @SuppressWarnings("unchecked")
      ArrayList<Transactions> transactions = (ArrayList<Transactions>) session.getAttribute("trancs");

      // Instantiation of document object
      Document document = new Document(PageSize.A4, 50, 50, 50, 50);

      // Creation of PdfWriter object

      PdfWriter.getInstance(document, new FileOutputStream(FILEPATH));
      document.open();

      // Creation of chapter object

      Paragraph title1 = new Paragraph("History of your transactions",
          FontFactory.getFont(FontFactory.HELVETICA, 20, Font.BOLDITALIC, new CMYKColor(0, 17, 255, 225)));
      Chapter chapter1 = new Chapter(title1, 1);
      chapter1.setNumberDepth(0);

      // Creation of section object
      Date date = new Date();
      Paragraph title11 = new Paragraph(date.toString(),
          FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD, new CMYKColor(0, 17, 255, 255)));
      Section section1 = chapter1.addSection(title11);

      // Creation of TopTable object

      PdfPTable t = new PdfPTable(5);
      t.setSpacingBefore(25);
      PdfPCell c1 = new PdfPCell(new Phrase("Data"));
      t.addCell(c1);
      PdfPCell c2 = new PdfPCell(new Phrase("Card"));
      t.addCell(c2);
      PdfPCell c3 = new PdfPCell(new Phrase("Summary"));
      t.addCell(c3);
      PdfPCell c4 = new PdfPCell(new Phrase("Sum"));
      t.addCell(c4);
      PdfPCell c5 = new PdfPCell(new Phrase("Balance"));
      t.addCell(c5);

      // Creation of BodyTable object
      PdfPTable t2 = new PdfPTable(5);

      section1.add(t);

      for (Transactions a : transactions) {
        t2.addCell(a.getDtime());
        t2.addCell(a.getAccount());
        t2.addCell(a.getSummary());
        t2.addCell(Double.toString(a.getTotal()));
        t2.addCell(Double.toString(a.getBalance()));
        cardNumber = a.getAccount();
      }
      section1.add(t2);
      // Addition of a chapter to the main document
      document.add(chapter1);
      document.close();
    } catch (DocumentException e1) {
      LOGGER.error(e1.getLocalizedMessage());
      e1.printStackTrace();
    }

    session.setAttribute("pdfErrorMessage",
        "PDF report for " + cardNumber + " successfully generated and saved to file: " + FILEPATH);

    session.setAttribute("forButtonOpenPDF", cardNumber);

    LOGGER.debug("PDF servlet finished");
    response.sendRedirect("./history.jsp");
  }

}
