package ua.nure.shvec.dao.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ua.nure.shvec.DBConnector.ConnectionPool;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Transactions;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConnectionPool.class)
public class TransactionsDaoImplTest {

	private FactoryDAO factoryDAO = FactoryDAO.getInstance();
	private static final String SQL_GET_TRANSACTION = "select * from transactions where account=?;";
	private static final String SQL_DEL_PREP_PAYM = "DELETE from payments where dtime = ?;";

	private PreparedStatement statementMock;
	private ResultSet rsMock;
	private Connection connectionMock;
	private ConnectionPool connectionPoolMock;
	private String dtime;
	private String category;
	private String account;
	private String summary;
	private double total;
	private double balance;

	@Before
	@Test
	public void init() {
		statementMock = mock(PreparedStatement.class);
		rsMock = mock(ResultSet.class);
		connectionMock = mock(Connection.class);
		connectionPoolMock = mock(ConnectionPool.class);
		dtime = "10:10_12-25";
		account = "1111-2222-3333-4444";
		summary = "summary";
		category = "PAYS";
		total = 100;
		balance = 0;
		PowerMockito.mockStatic(ConnectionPool.class);
		when(ConnectionPool.getInstance()).thenReturn(connectionPoolMock);
		when(connectionPoolMock.getConnection()).thenReturn(connectionMock);
	}

	@Test
	public void testDelPayment() throws SQLException {
		when(connectionMock.prepareStatement(SQL_DEL_PREP_PAYM)).thenReturn(statementMock);
		assertEquals(false, factoryDAO.getTransactionsDao().delPayment(dtime));
		verify(statementMock).setString(1, dtime);
	}

	@Test
	public void testGetTransactions() throws SQLException {
		Transactions transaction = new Transactions(dtime, category, account, summary, total, balance);
		List<Transactions> payments = new ArrayList<Transactions>();
		payments.add(transaction);

		when(connectionMock.prepareStatement(SQL_GET_TRANSACTION)).thenReturn(statementMock);
		when(statementMock.executeQuery()).thenReturn(rsMock);
		when(rsMock.next()).thenReturn(true).thenReturn(false);

		when(rsMock.getString(1)).thenReturn(dtime);
		when(rsMock.getString(2)).thenReturn(category);
		when(rsMock.getString(3)).thenReturn(account);
		when(rsMock.getString(4)).thenReturn(summary);

		when(rsMock.getDouble(5)).thenReturn(total);
		when(rsMock.getDouble(6)).thenReturn(balance);

		assertEquals(payments, factoryDAO.getTransactionsDao().getTransactions(account));
	}

}
