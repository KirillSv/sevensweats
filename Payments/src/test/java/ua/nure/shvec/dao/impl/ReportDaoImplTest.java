/**
* Test Class for ReportDaoImpl class
* @author Shvec.K.
* @version 1.0

*/
package ua.nure.shvec.dao.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ua.nure.shvec.DBConnector.ConnectionPool;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Report;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConnectionPool.class)
public class ReportDaoImplTest {

	private FactoryDAO factoryDAO = FactoryDAO.getInstance();
	private static final String SQL_DEL_REPORT = "DELETE from reports where data = ?;";
	private static final String SQL_SET_REPORT_FROM_USER = "INSERT INTO reports(customer_email, textreport) values(?, ?);";
	private static final String SQL_GET_ALL_REPORTS = "select * from reports;";

	private PreparedStatement statementMock;
	private ResultSet rsMock;
	private Connection connectionMock;
	private ConnectionPool connectionPoolMock;
	private String customer_email;
	private String data;
	private String message;

	@Before
	@Test
	public void init() {
		statementMock = mock(PreparedStatement.class);
		rsMock = mock(ResultSet.class);
		connectionMock = mock(Connection.class);
		connectionPoolMock = mock(ConnectionPool.class);
		customer_email = "user@mail.ru";
		data = "data";
		message = "some message";
		PowerMockito.mockStatic(ConnectionPool.class);
		when(ConnectionPool.getInstance()).thenReturn(connectionPoolMock);
		when(connectionPoolMock.getConnection()).thenReturn(connectionMock);
	}

	@Test
	public void testGetReports() throws SQLException {
		List<Report> reports = new ArrayList<Report>();

		Report report = new Report(data, customer_email, message);
		reports.add(report);

		when(connectionMock.prepareStatement(SQL_GET_ALL_REPORTS)).thenReturn(statementMock);
		when(statementMock.executeQuery()).thenReturn(rsMock);
		when(rsMock.next()).thenReturn(true).thenReturn(false);
		when(rsMock.getString("data")).thenReturn(data);
		when(rsMock.getString("customer_email")).thenReturn(customer_email);
		when(rsMock.getString("textreport")).thenReturn(message);

		assertEquals(reports, factoryDAO.getReportDao().getReports());

	}

	@Test
	public void testSetReportfromUsertoAdmin() throws SQLException {

		when(connectionMock.prepareStatement(SQL_SET_REPORT_FROM_USER)).thenReturn(statementMock);
		factoryDAO.getReportDao().setReportfromUsertoAdmin(customer_email, message);

		verify(statementMock).setString(1, customer_email);
		verify(statementMock).setString(2, message);
	}

	@Test
	public void testDeleteReport() throws SQLException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PrintStream old = System.out;
		output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		when(connectionMock.prepareStatement(SQL_DEL_REPORT)).thenReturn(statementMock);
		factoryDAO.getReportDao().deleteReport(data);

		String template = "deleteReport#finish set:" + data;
		assertEquals(template, output.toString());
		System.setOut(old);

	}

}
