/**
 * 
 */
package ua.nure.shvec.dao.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ua.nure.shvec.DBConnector.ConnectionPool;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.User;

/**
* @author Shvec.K.
* @version 1.0
*/
@RunWith(PowerMockRunner.class)
@PrepareForTest(ConnectionPool.class)
public class UserDaoImplTest {

	private FactoryDAO factoryDAO = FactoryDAO.getInstance();
	private static final String SQL_ADD_USER = "INSERT INTO customers(customer_email,customer_name,password) values(?, ?, ?);";
	private static final String SQL_ADD_USER_ROLE = "INSERT INTO roles(customer_email, role_name) values(?, ?);";
	private static final String SQL_GET_CUSTOMER = "SELECT * FROM customers WHERE customer_email=?;";
	private static final String SQL_GET_CUSTOMER_LOCKED = "SELECT locked from customers WHERE customer_email=?;";
	private static final String SQL_SET_CUSTOMER_LOCKED = "UPDATE customers SET locked=? WHERE customer_email=?; ";
	private static final String SQL_GET_ALL_USERS="SELECT * FROM customers;";
	
	private PreparedStatement statementMock;
	private ResultSet rsMock;
	private Connection connectionMock;
	private ConnectionPool connectionPoolMock;
	private String name;
	private String email;
	private String password;
	private String role;
	private List<String> accounts;
	private Boolean lock;

	@Before
	@Test
	public void init() {
		statementMock = mock(PreparedStatement.class);
		rsMock = mock(ResultSet.class);
		connectionMock = mock(Connection.class);
		connectionPoolMock = mock(ConnectionPool.class);

		name = "userName";
		email = "user@testmail.com";
		password = "password";
		role = "user";
		accounts = new ArrayList<String>();
		lock = false;

		PowerMockito.mockStatic(ConnectionPool.class);
		when(ConnectionPool.getInstance()).thenReturn(connectionPoolMock);
		when(connectionPoolMock.getConnection()).thenReturn(connectionMock);
	}

	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.UserDaoImpl#createUser(ua.nure.shvec.entity.User)}.
	 * 
	 * @throws SQLException
	 */
	@Test
	public void testCreateUser() throws SQLException {
		User user = new User(name, email, password, role, accounts, lock);
		when(connectionMock.prepareStatement(SQL_ADD_USER)).thenReturn(statementMock);
		when(connectionMock.prepareStatement(SQL_ADD_USER_ROLE)).thenReturn(statementMock);

		assertEquals(true, factoryDAO.getUserDao().createUser(user));
	}

	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.UserDaoImpl#isCustomerLocked(java.lang.String)}.
	 * 
	 * @throws SQLException
	 */
	@Test
	public void testIsCustomerLocked() throws SQLException {
		when(connectionMock.prepareStatement(SQL_GET_CUSTOMER_LOCKED)).thenReturn(statementMock);
		when(statementMock.executeQuery()).thenReturn(rsMock);
		when(rsMock.next()).thenReturn(true).thenReturn(false);
		when(rsMock.getBoolean("locked")).thenReturn(lock);
		assertEquals(false, factoryDAO.getUserDao().isCustomerLocked(email));
	}

	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.UserDaoImpl#setLocked(java.lang.String, java.lang.Boolean)}.
	 */
	@Test
	public void testSetLocked() throws SQLException {

		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PrintStream old = System.out;
		output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		when(connectionMock.prepareStatement(SQL_SET_CUSTOMER_LOCKED)).thenReturn(statementMock);
		factoryDAO.getUserDao().setLocked(email, lock);

		String template = "setLocked#finished";
		assertEquals(template, output.toString());
		System.setOut(old);

	}

	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.UserDaoImpl#getCustomer(java.lang.String)}.
	 * 
	 * @throws SQLException
	 */
	@Test
	public void testGetCustomer() throws SQLException {

		User user = new User(email, lock);

		when(connectionMock.prepareStatement(SQL_GET_CUSTOMER)).thenReturn(statementMock);
		when(statementMock.executeQuery()).thenReturn(rsMock);
		when(rsMock.next()).thenReturn(true).thenReturn(false);
		when(rsMock.getBoolean("locked")).thenReturn(lock);
		when(rsMock.getString("customer_email")).thenReturn(email);

		assertEquals(user, factoryDAO.getUserDao().getCustomer(email));

	}
	
	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.UserDaoImpl#getAllUsersData(java.lang.String)}.
	 * 
	 * @throws SQLException
	 */
	
	@Test
	public void testGetAllUsersData() throws SQLException {

		User user = new User(email, name, password, lock);
        List<User> users=new ArrayList<User>();
		when(connectionMock.prepareStatement(SQL_GET_ALL_USERS)).thenReturn(statementMock);
		when(statementMock.executeQuery()).thenReturn(rsMock);
		when(rsMock.next()).thenReturn(true).thenReturn(false);
		when(rsMock.getBoolean("locked")).thenReturn(lock);
		when(rsMock.getString("customer_email")).thenReturn(email);
		when(rsMock.getString("customer_name")).thenReturn(name);
		when(rsMock.getString("password")).thenReturn(password);
        users.add(user);
		assertEquals(users, factoryDAO.getUserDao().getAllUsersData());

	}
}


