/**
* Test Class for AccountDaoImpl class
* @author Shvec.K.
* @version 1.0

*/
package ua.nure.shvec.dao.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ua.nure.shvec.DBConnector.ConnectionPool;
import ua.nure.shvec.dao.FactoryDAO;
import ua.nure.shvec.entity.Account;
import ua.nure.shvec.exceptions.AccountIsBlockedException;

import static org.easymock.EasyMock.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ConnectionPool.class)
public class AccountDaoImplTest {

	private FactoryDAO factoryDAO = FactoryDAO.getInstance();
	private static final String SQL_GET_ACCOUNT_INFO = "SELECT accounts.*, customers.locked FROM accounts, customers where customers.customer_email = ? AND accounts.customer_email = ?; ";
	private static final String SQL_ADD_CARD = "INSERT INTO accounts(customer_email,account,info) values(?, ?, ?);";
	private static final String SQL_SET_ACCOUNT_VERIF = "UPDATE accounts SET verified=? WHERE account=?; ";
	private static final String SQL_GET_ACCOUNT = "SELECT * FROM accounts WHERE account=?;";
	private static final String SQL_GET_ACCOUNT_VERIFIED = "SELECT verified FROM accounts WHERE account=?;";

	private PreparedStatement statementMock;
	private ResultSet rsMock;
	private Connection connectionMock;
	private ConnectionPool connectionPoolMock;
	private String currUserEmail;
	private String account;
	private String info;
	private String ballance;
	private boolean verified;
	private boolean locked;

	@Before
	@Test
	public void init() {
		statementMock = mock(PreparedStatement.class);
		rsMock = mock(ResultSet.class);
		connectionMock = mock(Connection.class);
		connectionPoolMock = mock(ConnectionPool.class);
		currUserEmail = "user@mail.ru";
		account = "1111-2222-3333-4444";
		info = "card info";
		ballance = "1000";
		verified = false;
		locked = false;
		PowerMockito.mockStatic(ConnectionPool.class);
		when(ConnectionPool.getInstance()).thenReturn(connectionPoolMock);
		when(connectionPoolMock.getConnection()).thenReturn(connectionMock);
	}

	@Test
	public void testaddAccount() throws SQLException {

		when(connectionMock.prepareStatement(SQL_ADD_CARD)).thenReturn(statementMock);
		assertEquals(true, factoryDAO.getAccountDao().addAccount(currUserEmail, account, info));

		verify(statementMock).setString(1, currUserEmail);
		verify(statementMock).setString(2, account);
		verify(statementMock).setString(3, info);
	}

	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.AccountDaoImpl#getAccounts(java.lang.String)}.
	 */
	@Test
	public void testGetAccounts() throws SQLException {

		List<Account> accounts = new ArrayList<Account>();

		Account acc = new Account(currUserEmail, account, info, ballance, verified, locked);
		acc.setCurrency(null);
		accounts.add(acc);

		when(connectionMock.prepareStatement(SQL_GET_ACCOUNT_INFO)).thenReturn(statementMock);
		when(statementMock.executeQuery()).thenReturn(rsMock);
		when(rsMock.next()).thenReturn(true).thenReturn(false);
		when(rsMock.getString("customer_email")).thenReturn(currUserEmail);
		when(rsMock.getString("account")).thenReturn(account);
		when(rsMock.getString("info")).thenReturn(info);
		when(rsMock.getString("acc_balance")).thenReturn(ballance);
		when(rsMock.getBoolean("verified")).thenReturn(verified);
		when(rsMock.getBoolean("locked")).thenReturn(locked);

		assertEquals(accounts, factoryDAO.getAccountDao().getAccounts(currUserEmail));

	}

	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.AccountDaoImpl#checkBlockAccount(java.lang.String)}.
	 * @throws AccountIsBlockedException 
	 */
	
	@Test(expected = AccountIsBlockedException.class)
	public void testCheckBlockAccount() throws SQLException, AccountIsBlockedException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PrintStream old = System.out;
		output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));
		
		when(connectionMock.prepareStatement(SQL_GET_ACCOUNT_VERIFIED)).thenReturn(statementMock);
		when(statementMock.executeQuery()).thenReturn(rsMock);
		when(rsMock.next()).thenReturn(true).thenReturn(false);
		verified=true;                                          //set trows exception
		when(rsMock.getBoolean("verified")).thenReturn(verified);
		
		String template = "setBlock#finish set:" + verified;
		factoryDAO.getAccountDao().checkBlockAccount(account);
		
		assertEquals(template, output.toString());
		System.setOut(old);
	}
	

	//		ps.setString(1, account);


	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.AccountDaoImpl#getAccount(java.lang.String)}.
	 */
	@Test
	public void testGetAccount() throws SQLException {
		Account acc = new Account(account, verified);

		when(connectionMock.prepareStatement(SQL_GET_ACCOUNT)).thenReturn(statementMock);
		when(statementMock.executeQuery()).thenReturn(rsMock);
		when(rsMock.next()).thenReturn(true).thenReturn(false);
		when(rsMock.getString("account")).thenReturn(account);
		when(rsMock.getBoolean("verified")).thenReturn(verified);

		assertEquals(acc, factoryDAO.getAccountDao().getAccount("account"));
	}

	/**
	 * Test method for
	 * {@link ua.nure.shvec.dao.impl.AccountDaoImpl#setVerified(java.lang.String, java.lang.Boolean)}.
	 */
	@Test
	public void testSetVerified() throws SQLException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PrintStream old = System.out;
		output = new ByteArrayOutputStream();
		System.setOut(new PrintStream(output));

		when(connectionMock.prepareStatement(SQL_SET_ACCOUNT_VERIF)).thenReturn(statementMock);
		factoryDAO.getAccountDao().setVerified(account, verified);

		String template = "setVerified#finish set:" + verified;
		assertEquals(template, output.toString());
		System.setOut(old);
	}

}
