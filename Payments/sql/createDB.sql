--------------------------------------------------------
-- DB creation script for MySQL
----------------------------------------------------
DROP DATABASE IF EXISTS paymentsdb;
CREATE DATABASE paymentsdb;
USE paymentsdb;
----------------------------------------------------------------
-- CUSTOMER
----------------------------------------------------------------
CREATE TABLE customers(
`customer_email` VARCHAR(30) NOT NULL UNIQUE PRIMARY KEY,
`customer_name` VARCHAR(30) NOT NULL,	
`password` VARCHAR(100) NOT NULL, 
 `locked` BOOLEAN 
);
INSERT customers value("admin@admin.com", "adminName","8D969EEF6ECAD3C29A3A629280E686CF0C3F5D5A86AFF3CA12020C923ADC6C92", 0);
INSERT customers value("user1@admin.com", "user1Name","8D969EEF6ECAD3C29A3A629280E686CF0C3F5D5A86AFF3CA12020C923ADC6C92", 0);
INSERT customers value("user2@admin.com", "user2Name","8D969EEF6ECAD3C29A3A629280E686CF0C3F5D5A86AFF3CA12020C923ADC6C92", 0);
----------------------------------------------------------------
-- ROLES
----------------------------------------------------------------
CREATE TABLE roles(
`customer_email` VARCHAR(30) NOT NULL UNIQUE PRIMARY KEY,
`role_name` VARCHAR(30) NOT NULL,
FOREIGN KEY (`customer_email`) REFERENCES `customers` (`customer_email`) ON DELETE CASCADE
);

INSERT roles value("admin@admin.com", "admin");
INSERT roles value("user1@admin.com", "user");
INSERT roles value("user2@admin.com", "user");
----------------------------------------------------------------
-- ACCOUNTS
----------------------------------------------------------------
CREATE TABLE accounts(
`customer_email` VARCHAR(30)  NOT NULL,
`account` VARCHAR(30) unique  NOT NULL PRIMARY KEY,
`info` VARCHAR(30),
`acc_balance` INTEGER(10) default 0,
`verified` BOOLEAN not null default 0,
`currency` VARCHAR(10),
FOREIGN KEY (`customer_email`) REFERENCES `customers` (`customer_email`) ON DELETE CASCADE
);

INSERT accounts value("user1@admin.com", "1111-1111-1111-1111","info", "1000", 0, "dollar");
INSERT accounts value("user1@admin.com", "1111-2222-2222-2222","info", "1000", 0, "dollar");
INSERT accounts value("user1@admin.com", "1111-3333-3333-3333","info", "1000", 0, "dollar");

INSERT accounts value("user2@admin.com", "2222-1111-1111-1111","info", "1000", 0, "dollar");
INSERT accounts value("user2@admin.com", "2222-2222-2222-2222","info", "1000", 0, "dollar");
INSERT accounts value("user2@admin.com", "2222-3333-3333-3333","info", "1000", 0, "dollar");
------------------------------------------------------------------
-- TRANSACTIONS
------------------------------------------------------------------
CREATE TABLE transactions(
`dtime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`category` VARCHAR(20) NOT NULL,
`account` VARCHAR(20) NOT NULL,
`summary` VARCHAR(100),
`total` INTEGER (10) default 0,
`balance`INTEGER(10) default 0,
PRIMARY KEY (`dtime`,`account`),
FOREIGN KEY (`account`) REFERENCES `accounts` (`account`) ON DELETE CASCADE
);
----------------------------------------------------------------
-- PAYMENTS
----------------------------------------------------------------
CREATE TABLE payments(
`dtime` TIMESTAMP unique NOT NULL DEFAULT CURRENT_TIMESTAMP PRIMARY KEY,
`customer_email` VARCHAR(30) NOT NULL,
`account` VARCHAR(30) NOT NULL,
`summary` VARCHAR(60),
`comment` VARCHAR(30), 
`total` INTEGER(10) default 0,
FOREIGN KEY (`customer_email`) REFERENCES `customers` (`customer_email`) ON DELETE CASCADE,
FOREIGN KEY (`account`) REFERENCES `accounts` (`account`) ON DELETE CASCADE
);
---------------------------------------
-- REPORTS
----------------------------------------
CREATE TABLE reports(
`data` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP PRIMARY KEY,
`customer_email` VARCHAR(30) NOT NULL,
`textreport` VARCHAR(100) NOT NULL,
FOREIGN KEY (`customer_email`) REFERENCES `customers` (`customer_email`) ON DELETE CASCADE
);
INSERT reports value(DEFAULT,"user1@admin.com", "Я что-то нажала и все исчезло!!!");
