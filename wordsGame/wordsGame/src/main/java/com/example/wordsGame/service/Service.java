package com.example.wordsGame.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

@Component
public class Service {

	final String PATH = "src/main/resources/word_rus.txt";
	final char[] INVALID_CHARACTERS = new char[] { 'ё', 'ъ', 'ь', 'ы' };

	/**
	 * The method reads a set of words from the base and puts it into the set.
	 * 
	 * @return set words
	 */
	public Set<String> readWordFromBase() {
		Set<String> setWords = new HashSet<>();
		try {
			Stream<String> lines = Files.lines(Paths.get(PATH));
			setWords = lines.collect(Collectors.toSet());
			lines.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return setWords;
	}

	/**
	 * The method selects last playable letter.
	 * 
	 * @return last playable letter.
	 */
	public char checkWordLastLetter(String citySuggestedSystem) {
		char lastSystemLetter = citySuggestedSystem.charAt(citySuggestedSystem.length() - 1);
		List<Character> valid_characters = new ArrayList<Character>();

		for (int i = 0; i < INVALID_CHARACTERS.length; i++) {
			valid_characters.add(INVALID_CHARACTERS[i]);
		}

		if (valid_characters.contains((lastSystemLetter))) {
			lastSystemLetter = citySuggestedSystem.charAt(citySuggestedSystem.length() - 2);
		}

		return lastSystemLetter;
	}

	/**
	 * The method selects a new word that the system responds to the player.
	 * 
	 * @return word or message "notFound".
	 */
	public String findNewSystemWord(Set<String> setWords, char lastUserLetter) {
		String s = String.valueOf(lastUserLetter);
		Set<String> sortedSetWords = new TreeSet<String>(setWords);

		Optional<String> findFirst = sortedSetWords.stream().filter(word -> word.startsWith(s)).findFirst();
		return findFirst.orElse("notFound");

	}

	public void removeAdd(Set<String> setWords, Set<String> antiDoubleSet, String word) {
		setWords.remove(word);
		antiDoubleSet.add(word);
	}

}
