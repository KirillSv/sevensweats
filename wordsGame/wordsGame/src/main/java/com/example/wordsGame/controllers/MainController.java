package com.example.wordsGame.controllers;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import com.example.wordsGame.service.Service;

@Controller
@SessionAttributes({ "citySuggestedSystem", "wordSet", "antiDoubleSet" })
public class MainController {
	final String FIRST_LETTER_WRONG = "Первая буква вашего слова не та что нужно. Введите другое слово";
	final String DOUBLE_SITY = "Этот город был. Выберете какой-то другой";
	final String YOU_WIN = "Вы выиграли! В моей базе закончились города на букву ";
	final String GOOD_BYE = "Спасибо за игру";
	final String WORD_NOT_EXIST = "Такого слова нет в словаре русского языка. Нужно выбрать другое";

	@Autowired
	Service service;

	@GetMapping("/begin")
	public String starter(Model model) {
		model.addAttribute("title", "Start");

		Set<String> setWords = service.readWordFromBase();
		Set<String> antiDoubleSet = new HashSet<String>();

		Iterator<String> iterator = setWords.iterator();
		iterator.hasNext();
		String citySuggestedSystem = iterator.next();

		service.removeAdd(setWords, antiDoubleSet, citySuggestedSystem);

		model.addAttribute("wordSet", setWords);
		model.addAttribute("antiDoubleSet", antiDoubleSet);
		model.addAttribute("citySuggestedSystem", citySuggestedSystem);
		return "begin";
	}

	@GetMapping("/next")
	public String customerInputNewWorld(@RequestParam String word, Model model, SessionStatus status) {
		word = word.toLowerCase();
		// get word set and systemWorld from session
		@SuppressWarnings("unchecked")
		Set<String> antiDoubleSet = (HashSet<String>) model.getAttribute("antiDoubleSet");
		@SuppressWarnings("unchecked")
		Set<String> setWords = (HashSet<String>) model.getAttribute("wordSet");

		String citySuggestedSystem = (String) model.getAttribute("citySuggestedSystem");

		// find system's_word last letter
		char lastSystemLetter = service.checkWordLastLetter(citySuggestedSystem);
		// check user's_word first letter
		char firstUserLetter = word.charAt(0);
		if (!(firstUserLetter == lastSystemLetter)) {
			model.addAttribute("errorMessage", FIRST_LETTER_WRONG);
			return "begin";
		}

		// check exist word
		if (!(setWords.contains(word))) {
			// check users's_word for duplicate
			if (antiDoubleSet.contains(word)) {
				model.addAttribute("errorMessage", DOUBLE_SITY);
				return "begin";
			}
			model.addAttribute("errorMessage", WORD_NOT_EXIST);
			return "begin";
		}
		service.removeAdd(setWords, antiDoubleSet, word);
		// find last user's_word letter
		char lastUserLetter = service.checkWordLastLetter(word);

		// find new system_word
		String newSystemWord = service.findNewSystemWord(setWords, lastUserLetter);

		if (!newSystemWord.equals("notFound")) {
			model.addAttribute("citySuggestedSystem", newSystemWord);
			service.removeAdd(setWords, antiDoubleSet, newSystemWord);
			return "begin";
		}

		status.setComplete();
		model.addAttribute("errorMessage", YOU_WIN + lastUserLetter);
		return "begin";
	}

	@PostMapping("/end")
	public String customerInputNewWorld(Model model, SessionStatus status) {
		model.addAttribute("title", "The End");
		status.setComplete();
		model.addAttribute("errorMessage", GOOD_BYE);
		return "end";

	}
}
