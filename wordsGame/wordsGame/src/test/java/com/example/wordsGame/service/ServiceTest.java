package com.example.wordsGame.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class) // — запускает тесты с «запускателем» Спринга
@SpringBootTest
public class ServiceTest {

	@Autowired
	Service service;

	@Test
	public void readWordFromBaseTest() throws Exception {
		assertThat(service).isNotNull();
		Set<String> setWords = service.readWordFromBase();
		assertThat(setWords.size()).isNotNull();
		assertEquals(setWords.size(), 34012);
	}

	@Test
	public void checkWordLastLetterTest() throws Exception {
		String test1 = "Вася";
		String test2 = "Васяъ";
		char expected = 'я';
		char result1 = service.checkWordLastLetter(test1);
		char result2 = service.checkWordLastLetter(test2);
		assertEquals(result1, expected);
		assertEquals(result2, expected);

	}

	@Test
	public void findNewSystemWordTest() throws Exception {
		Set<String> setWords = new HashSet<String>();
		setWords.add("алабама");
		setWords.add("баран");
		setWords.add("тест");

		char lastUserLetter1 = 'а';
		char lastUserLetter2 = 'т';
		char lastUserLetter3 = 'я';

		String result1 = service.findNewSystemWord(setWords, lastUserLetter1);
		String result2 = service.findNewSystemWord(setWords, lastUserLetter2);
		String result3 = service.findNewSystemWord(setWords, lastUserLetter3);
		assertEquals(result1, "алабама");
		assertEquals(result2, "тест");
		assertEquals(result3, "notFound");
	}
}
