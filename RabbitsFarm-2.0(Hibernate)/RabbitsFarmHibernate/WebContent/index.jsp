<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>Авторизация</title>
<c:import url="jsp/style.jsp"></c:import>
</head>

<body>
	<div class='login'>
		<h2>Необходима авторизация</h2>

		<form name="Form" method="post" action="./loginServlet">
			<input type="text" name="username" placeholder="Login" required /> <br />
			<input type="text" name="password" placeholder="*********" required />
			<br /> <br /> <input type="submit" name="Go" value="  Ввод "
				class="button"> <br /> <br />
		</form>
		<br /> <br /> ${loginErrorMessage}
		<h4>
			ADMIN "admin" "admin" - может создавать и удалять записи в БД <br />
			USER "1" "1" - может делать записи в базу данных, но не удалять их.<br />
			любой другой пароль зайдет как гость. Можно только смотреть.<br />
		</h4>
	</div>
</body>
</html>