<style>
body {
	margin: 0;	
	background: url(../img/background.jpg) ;	
}

.center-img {
  display: block;
  margin: 0 auto;
}
table {
    padding: .3em 1em;
	font-family: "Courier New",monospace;
	font-size: 20px;	
	text-align: center;
}
.spc {border-collapse: collapse;}
.spc td, .tbord {
	border: 1px solid;
	font-family: Tahoma, Courier, monospace;	
	font-size: 14px;
	color:#646464;
}
.spc tr:hover {background-color: #DEFECB;}
.thd { 
	border: 1px solid;	
	text-align: center;
	font-family: Tahoma, Courier, monospace;	
	font-size: 11px;
	color:#646464;
}
.login{
	background: url(img/background.jpg) ;
	box-shadow: 10px 10px 10px 0px rgba(0,0,0, 1);
    border: 1px solid green;
 	border-radius: 10px;
    background: #C1F5F4;
    position: absolute;
	text-align: center;
	padding: 10px;
	text-align: center;	
	margin-top:20%;
	margin-left:30%;
}
	
.accessDenied{
	background: url(../img/background.jpg) ;
	box-shadow: 10px 10px 10px 0px rgba(0,0,0, 1);
    border: 1px solid green;
 	border-radius: 10px;
    background: #FF4500;
    position: absolute;
	text-align: center;
	padding: 10px;
	text-align: center;	
	margin-top:20%;
	margin-left:30%;
}

.twoStaticTables {
	box-shadow: 10px 10px 10px 0px rgba(0,0,0, 1);
    border: 1px solid green;
 	border-radius: 10px;
	position: absolute;
	background: #9ACD32;
	right: 15px;
	margin-top:20px;
	padding: 10px;
}
.header {	
    margin-top:5px;
	box-shadow: 10px 10px 10px 0px rgba(0,0,0, 1);
    border: 1px solid green;
 	border-radius: 20px;
	background: #C1F5F4;
	text-align: center;
	font-family: 'URW Chancery L, cursive';
	
}
.headerImg{
	border-radius: 200px;
}
.menu {
	box-shadow: 10px 10px 10px 0px rgba(0,0,0, 1);
    margin-top:20px;
    position: absolute;
    left: 15px;
    border: 1px solid green;
 	border-radius: 10px;
	background: #C1F5F4;
	text-align: center;
	padding: 20px;
}

.addsomethingJSP {
	box-shadow: 10px 10px 10px 0px rgba(0,0,0, 1);
    margin-top:400px;
    position: absolute;
    left: 15px;
    border: 1px solid green;
 	border-radius: 10px;
	background: #C1F5F4;
	text-align: center;
	padding: 20px;
}
.delsomethingJSP {
	box-shadow: 10px 10px 10px 0px rgba(0,0,0, 1);
    margin-top:400px;
    position: absolute;
    left: 15px;
    border: 1px solid green;
 	border-radius: 10px;
	background: #FF4500;
	text-align: center;
	padding: 20px;
}
.button{
border-radius: 12px;
}
.button:hover {
  background-color: #BDBDBD;
}

</style>