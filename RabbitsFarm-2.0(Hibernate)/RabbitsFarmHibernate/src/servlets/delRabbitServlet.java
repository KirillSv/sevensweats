package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import processor.dao.FactoryDao;

@WebServlet("/delRabbitServlet")
public class delRabbitServlet extends HttpServlet {
	private static final long serialVersionUID = 6L;
	private FactoryDao factoryDao = FactoryDao.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("selectionMenu", "delRabbit");
		response.sendRedirect("jsp/start.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (!(session.getAttribute("role").equals("ADMIN"))) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

		}

		String name = request.getParameter("name");
		String errorMessage = factoryDao.getRabbitDao().deleteRabbit(name);
		
		session.setAttribute("errorMessage", errorMessage);

		response.sendRedirect("ControllerServlet");
	}

}
