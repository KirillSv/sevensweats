package servlets;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import entity.Note;
import entity.Rabbit;
import processor.dao.FactoryDao;

@WebServlet("/addNoteServlet")
public class addNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 2L;
	private FactoryDao factoryDao = FactoryDao.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("selectionMenu", "addNote");
		response.sendRedirect("jsp/start.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		session.setAttribute("selectionMenu", "addNote");

		if (session.getAttribute("role").equals("GUEST")) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);
		}
		String name = request.getParameter("name");
		String text = request.getParameter("text");

		String dateOfNoteMonth = request.getParameter("dateOfNoteMonth");
		String dateOfNoteDay = request.getParameter("dateOfNoteDay");

		String dateOfNote = request.getParameter("dateOfNoteYear") + dateOfNoteMonth + dateOfNoteDay;

		@SuppressWarnings("unchecked")
		ArrayList<Rabbit> listRabbit = (ArrayList<Rabbit>) session.getAttribute("listRabbit");
		Rabbit rabbitFromThisNote=new Rabbit();
		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getName().equals(name)) {
				rabbitFromThisNote=rabbit;
				break;
			}
		}

		Note newNote = new Note(rabbitFromThisNote, text, dateOfNote);
		String errorMessage = factoryDao.getNoteDao().addNote(newNote);

		session.setAttribute("errorMessage", errorMessage);

		response.sendRedirect("ControllerServlet");
	}

}
