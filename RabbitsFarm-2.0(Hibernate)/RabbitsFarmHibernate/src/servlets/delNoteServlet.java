package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import processor.dao.FactoryDao;

@WebServlet("/delNoteServlet")
public class delNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 5L;
	private FactoryDao factoryDao = FactoryDao.getInstance();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String errorMessage = factoryDao.getNoteDao().deleteNote(request.getParameter("id"));
		session.setAttribute("errorMessage", errorMessage);
		response.sendRedirect("ControllerServlet");
	}

}
