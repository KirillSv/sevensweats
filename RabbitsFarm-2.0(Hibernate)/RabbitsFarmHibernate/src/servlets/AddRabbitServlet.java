package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Rabbit;
import processor.dao.FactoryDao;

@WebServlet("/AddRabbitServlet")
public class AddRabbitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FactoryDao factoryDao = FactoryDao.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("selectionMenu", "addRabbit");
		response.sendRedirect("jsp/start.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("GUEST")) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

		}
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		int count = Integer.valueOf(request.getParameter("count"));
		String dateOfBirthMonth = request.getParameter("dateOfBirthMonth");
		String dateOfBirthDay = request.getParameter("dateOfBirthDay");

		String dateOfBirth = request.getParameter("dateOfBirthYear") + dateOfBirthMonth + dateOfBirthDay;

		Rabbit newRabbit = new Rabbit(name, sex, dateOfBirth, count);
		String errorMessage = factoryDao.getRabbitDao().addRabbit(newRabbit);
		request.setAttribute("errorMessage", errorMessage);

		response.sendRedirect("ControllerServlet");
	}

}
