package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import entity.Note;
import processor.dao.FactoryDao;

@WebServlet("/EditNoteServlet")
public class EditNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FactoryDao factoryDao = FactoryDao.getInstance();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("selectionMenu", "editNote");
		
		String id = request.getParameter("id");
		
		@SuppressWarnings("unchecked")
		ArrayList<Note> notes=(ArrayList<Note>) session.getAttribute("listNote");
		for (Note note: notes) {
			if(note.getId()==Integer.parseInt(id)) {
				session.setAttribute("note", note);
				break;
			}
		}
		
		response.sendRedirect("jsp/start.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		Note note= (Note) session.getAttribute("note");
        int id = note.getId();
		String text = request.getParameter("text");
		String data = request.getParameter("data");
		String errorMessage = factoryDao.getNoteDao().editNote(id, text, data);
		session.setAttribute("errorMessage", errorMessage);
		
		response.sendRedirect("ControllerServlet");
	}

}
