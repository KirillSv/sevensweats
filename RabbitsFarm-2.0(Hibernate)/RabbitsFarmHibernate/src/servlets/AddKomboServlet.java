package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import entity.Note;
import entity.Rabbit;
import processor.dao.FactoryDao;

@WebServlet("/addKomboServlet")
public class AddKomboServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FactoryDao factoryDao = FactoryDao.getInstance();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("selectionMenu", "addKombo");
		response.sendRedirect("jsp/start.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("GUEST")) {
			response.sendRedirect("jsp/accessDenied.jsp");
		}
		
		String name = request.getParameter("name");
		String sex = "3";
		int count = Integer.valueOf(request.getParameter("count"));
		String dateOfBirthMonth = request.getParameter("dateOfBirthMonth");
		String dateOfBirthDay = request.getParameter("dateOfBirthDay");
		String mothersName = request.getParameter("mothersName");
		String dateOfBirthYear = request.getParameter("dateOfBirthYear");

		String dateOfBirth = request.getParameter("dateOfBirthYear") + dateOfBirthMonth + dateOfBirthDay;

		
		@SuppressWarnings("unchecked")
		ArrayList<Rabbit> rabbitsList=(ArrayList<Rabbit>) session.getAttribute("listRabbit");
		
		Rabbit vivodok=new Rabbit(name, sex, dateOfBirth, count);
		factoryDao.getRabbitDao().addRabbit(vivodok);

		//make note zaboi vivodok
		String dateOfNoteZaboi = formCorrectDataFromZaboi(dateOfBirthMonth, dateOfBirthYear) + dateOfBirthDay;
		factoryDao.getNoteDao().addNote(new Note(vivodok, "�����", dateOfNoteZaboi));

		Rabbit mothersRabbit=null;
		for (Rabbit rabbit: rabbitsList) {
			if(rabbit.getName().equals(mothersName)) {
				mothersRabbit=rabbit;
				break;
			}
		}
			
		String dateOfNoteVyazka = formCorrectDataFromVyazka(dateOfBirthMonth, dateOfBirthYear) + dateOfBirthDay;
		factoryDao.getNoteDao().addNote(new Note(mothersRabbit, "������", dateOfNoteVyazka));

		factoryDao.getRabbitDao().addMessageInPersonDiary(mothersRabbit,
				dateOfBirth + " ������ " +name+" "+ count + " ����, ����� �� �����:  " + dateOfNoteVyazka);

		response.sendRedirect("ControllerServlet");
	}

	private String formCorrectDataFromZaboi(String dateOfBirthMonth, String dateOfNoteYear) {
		int month = Integer.parseInt(dateOfBirthMonth);
		int year = Integer.parseInt(dateOfNoteYear);
		month = month + 4;
		if (month > 12) {
			month = month - 12;
			year = year + 1;
			return "" + year + formCorrectMonth(month);
		}
		String temp = "" + year + formCorrectMonth(month);
		return temp;
	}

	private String formCorrectDataFromVyazka(String dateOfBirthMonth, String dateOfNoteYear) {
		int month = Integer.parseInt(dateOfBirthMonth);
		int year = Integer.parseInt(dateOfNoteYear);
		String temp = null;
		month++;
		if (month > 12) {
			month = month - 12;
			year = year + 1;
			temp = "" + year + formCorrectMonth(month);
			return temp;
		}
		temp = "" + year + formCorrectMonth(month);
		return temp;
	}

	private String formCorrectMonth(int month) {
		String correctMonth = null;
		if (month < 10) {
			correctMonth = "0" + month;
		}
		return correctMonth;
	}
}
