package servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import entity.Note;
import entity.Rabbit;
import processor.dao.FactoryDao;

@WebServlet("/ControllerServlet")
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 3L;
	private FactoryDao factoryDao = FactoryDao.getInstance();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		ArrayList<Rabbit> rabbitList = factoryDao.getRabbitDao().readRabbitFromBase();
		for(Rabbit rabbit:  rabbitList) {
			rabbit.setAge(calculateRabbitAge(rabbit));	
		}
		
		ArrayList<Note> noteList = factoryDao.getNoteDao().readNoteFromBase();
		for(Note note:  noteList) {
			note.setDaysleft(findDaysLeft(note));	
		}
		Collections.sort(noteList);

		int[] sumRabbit = findSumRabbit(rabbitList);
		double sumFood = findSumFood(rabbitList);
		double sumAddMeat = findSumAddMeat(rabbitList);

		HttpSession session = request.getSession();

	//	session.setAttribute("role", "ADMIN"); 

		session.setAttribute("listRabbit", rabbitList);
		session.setAttribute("listNote", noteList);
		session.setAttribute("sumAddMeat", sumAddMeat);
		session.setAttribute("sumFood", sumFood);
		session.setAttribute("sumOldRabbit", sumRabbit[0]);
		session.setAttribute("sumLitleRabbit", sumRabbit[1]);

		response.sendRedirect("jsp/start.jsp");
	}
	
	private int calculateRabbitAge(Rabbit rabbit) {
		Calendar birth = Calendar.getInstance();
		java.sql.Date birthDateSQL =java.sql.Date.valueOf(rabbit.getDateOfBirth());
		birth.setTime(birthDateSQL);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.MONTH) - birth.get(Calendar.MONTH);
		int ageYear = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
		if (ageYear > 0) {
			age = age + ageYear * 12;
		}
		return age;
	}
	
	private int findDaysLeft(Note n) {
		Calendar note = Calendar.getInstance();
		Date date1=null;
		try {
			date1 = new SimpleDateFormat("yyyy-MM-dd").parse(n.getDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		note.setTime(date1);
		Calendar today = Calendar.getInstance();
		int daysLeft = today.get(Calendar.DAY_OF_YEAR) - note.get(Calendar.DAY_OF_YEAR);
		if ((note.get(Calendar.YEAR) - today.get(Calendar.YEAR)) > 0) {
			daysLeft -= 180;
		}
		return -daysLeft;
	}
	
	private int[] findSumRabbit(ArrayList<Rabbit> rabbitList) {// result [count old rabbit , count little rabbit]
		int[] sum = { 0, 0 };
		for (Rabbit rabbit : rabbitList) {

			if (rabbit.getAge() > 4) {
				sum[0] += rabbit.getCount();
			} else {
				sum[1] += rabbit.getCount();
			}
		}
		return sum;
	}

	private double findSumFood(ArrayList<Rabbit> rabbitList) {
		double sum = 0;
		for (Rabbit rabbit : rabbitList) {
			if (rabbit.getAge() > 4) {
				sum += (5 * rabbit.getCount());
			}
			if (rabbit.getAge() > 1 | rabbit.getAge() < 4) {
				sum += (2 * rabbit.getCount());
			}

		}
		sum = sum * 0.1;
		return Math.round(sum * 100d) / 100d;
	}

	private double findSumAddMeat(ArrayList<Rabbit> rabbitList) {
		double sum = 0;
		for (Rabbit rabbit : rabbitList) {
			if (rabbit.getAge() < 4) {
				sum += (rabbit.getAge() * rabbit.getCount());
			}
		}
		sum = (sum * 0.4) / 30;
		return Math.round(sum * 100d) / 100d;
	}

}
