package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "rabbit_table")
public class Rabbit implements Serializable {
	private static final long serialVersionUID = -5795273499307778884L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "sex")
	private String sex;

	@Column(name = "date_of_birth")
	private String dateOfBirth;

	@Column(name = "count")
	private int count;

	@Column(name = "person_diary")
	private String personDiary;

	@Transient
	private int age;

	public Rabbit(int id, String name, String sex, String dateOfBirth, int count, String personDiary) {
		this.name = name;
		this.id = id;
		this.sex = sex;
		this.dateOfBirth = dateOfBirth;
		this.count = count;
		this.personDiary = personDiary;
	}

	public Rabbit() {
	}

	public Rabbit(String name, String sex, String dateOfBirth, int count) {
		this.name = name;
		this.sex = sex;
		this.dateOfBirth = dateOfBirth;
		this.count= count;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPersonDiary() {
		return personDiary;
	}

	public void setPersonDiary(String personDiary) {
		this.personDiary = personDiary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + count;
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((personDiary == null) ? 0 : personDiary.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rabbit other = (Rabbit) obj;
		if (age != other.age)
			return false;
		if (count != other.count)
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (personDiary == null) {
			if (other.personDiary != null)
				return false;
		} else if (!personDiary.equals(other.personDiary))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + ")  '" + name + "'  " + sex + "     age: " + age + " month     " + count + "  count";
	}

}