package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "note_table")
public class Note implements Comparable<Note>, Serializable {

	private static final long serialVersionUID = 3103375249562698846L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@ManyToOne
	@JoinColumn(name = "name", nullable = false)
	private Rabbit rabbit;

	@Column(name = "text")
	private String text;

	@Column(name = "date_note")
	private String date;

	@Transient
	private int daysleft;

	public Note() {
	}

	public Note(Rabbit rabbit, String text, String date) {
		this.rabbit = rabbit;
		this.text = text;
		this.date = date;
	}

	public int getDaysleft() {
		return daysleft;
	}

	public void setDaysleft(int daysleft) {
		this.daysleft = daysleft;
	}

	public Rabbit getRabbit() {
		return rabbit;
	}

	public void setRabbit(Rabbit rabbit) {
		this.rabbit = rabbit;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Note other = (Note) obj;
		if (id != other.id)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + ")  '" + rabbit.getName() + "' " + text + " data: " + date + " left  " + daysleft + " days";
	}

	public int compareTo(Note note) {
		return -(note.getDaysleft()-getDaysleft());

	}

}