package processor.dao;

import java.util.ArrayList;
import entity.User;

public interface UserDao {

	ArrayList<User> readUserFromBase();

}
