package processor.dao;

import java.util.ArrayList;

import entity.Rabbit;

public interface RabbitDao {

	String addMessageInPersonDiary(Rabbit rabbit, String string);

	ArrayList<Rabbit> readRabbitFromBase();

	String deleteRabbit(String name);

	String addRabbit(Rabbit rabbit);

	String editRabbit(int newId, String newName, String newSex, String newDate, int newCount);

}
