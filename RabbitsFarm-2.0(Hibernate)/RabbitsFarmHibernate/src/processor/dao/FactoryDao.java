package processor.dao;

import processor.hibernateImplementation.NoteDaoHibernateImpl;
import processor.hibernateImplementation.RabbitDaoHibernateImpl;
import processor.hibernateImplementation.UserDaoHibernateImpl;

public class FactoryDao {
	private static FactoryDao factoryDao;

	private FactoryDao() {
	}

	public static FactoryDao getInstance() {
		if (factoryDao == null) {
			factoryDao = new FactoryDao();
		}
		return factoryDao;
	}
	
	
	public UserDao getUserDao() {
		return new UserDaoHibernateImpl();
	}

	public NoteDao getNoteDao() {
		return new NoteDaoHibernateImpl();
	}
	
	public RabbitDao getRabbitDao() {
		return new RabbitDaoHibernateImpl();
	}

}
