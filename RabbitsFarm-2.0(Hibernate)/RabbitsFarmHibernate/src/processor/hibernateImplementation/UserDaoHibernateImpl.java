package processor.hibernateImplementation;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import entity.Note;
import entity.User;
import processor.dao.UserDao;

public class UserDaoHibernateImpl implements UserDao{

	private final String READ_USER_LIST = "from User order by id";
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<User> readUserFromBase() {
		ArrayList<User> listUser = null;
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			listUser = (ArrayList<User>) session.createQuery(READ_USER_LIST).list();
			session.getTransaction().commit();
		}
		return listUser;

	}

}

