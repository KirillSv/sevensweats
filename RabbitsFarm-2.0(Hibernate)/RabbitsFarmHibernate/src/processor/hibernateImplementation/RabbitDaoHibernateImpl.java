package processor.hibernateImplementation;

import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import entity.Rabbit;
import processor.dao.RabbitDao;

public class RabbitDaoHibernateImpl implements RabbitDao {

	private final String OK = "All perfectly :)";
	private final String READ_LIST_RABBIT = "from Rabbit order by id";
	private final String DEL_RABBIT_BY_NAME = "delete from Rabbit where name= :delName";
	private final String UPDATE_RABBIT = "UPDATE Rabbit r SET r.name= :newName, r.sex= :newSex, r.dateOfBirth= :newDate, r.count= :newCount WHERE r.id= :newId";
	private final String UPDATE_DIARY = "UPDATE Rabbit r SET r.personDiary = :newPersonDiary WHERE r.name= :rabbitName";

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Rabbit> readRabbitFromBase() {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			ArrayList<Rabbit> listRabbit = (ArrayList<Rabbit>) session.createQuery(READ_LIST_RABBIT).list();
			session.getTransaction().commit();
			return listRabbit;
		}

	}

	@Override
	public String addRabbit(Rabbit rabbit) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			session.save(rabbit);
			session.getTransaction().commit();
		}
		return OK;
	}

	@Override
	public String deleteRabbit(String delName) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			session.createQuery(DEL_RABBIT_BY_NAME).setParameter("delName", delName).executeUpdate();
			session.getTransaction().commit();

		} catch (Exception o) {
			return o.getMessage();
		}
		return OK;
	}

	@Override
	public String editRabbit(int newId, String newName, String newSex, String newDate, int newCount) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			session.createQuery(UPDATE_RABBIT).setParameter("newId", newId).setParameter("newName", newName)
					.setParameter("newSex", newSex).setParameter("newDate", newDate).setParameter("newCount", newCount)
					.executeUpdate();
			session.getTransaction().commit();

		} catch (Exception o) {
			return o.getMessage();
		}
		return OK;
	}

	@Override
	public String addMessageInPersonDiary(Rabbit rabbit, String newMessage) {

		String message = rabbit.getPersonDiary() + "#" + newMessage;

		SessionFactory factory = new Configuration().configure().buildSessionFactory();

		try (Session session = factory.openSession()) {
			System.out.println(message + rabbit.getName());
			session.beginTransaction();
			session.createQuery(UPDATE_DIARY).setParameter("newPersonDiary", message)
					.setParameter("rabbitName", rabbit.getName()).executeUpdate();
			session.getTransaction().commit();
		} catch (Exception o) {
			return o.getMessage();
		}

		return OK;
	}

}
