package processor.hibernateImplementation;

import java.util.ArrayList;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import entity.Note;
import processor.dao.NoteDao;

public class NoteDaoHibernateImpl implements NoteDao {

	private final String OK = "All perfectly :)";
	private final String READNOTELIST = "from Note order by id";
	private final String DELNOTEBYID = "delete from Note where id= :newId";
	private final String UPDATE_NOTE="UPDATE Note n SET n.text= :newText, n.date= :newDate WHERE n.id= :newId";
	
	@Override
	public String addNote(Note note) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			session.save(note);
			session.getTransaction().commit();
		}
		return OK;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Note> readNoteFromBase() {
		ArrayList<Note> listNote = null;
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			listNote = (ArrayList<Note>) session.createQuery(READNOTELIST).list();
			session.getTransaction().commit();
		}
		return listNote;

	}

	@Override
	public String deleteNote(String delID) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			int newId = Integer.parseInt(delID);
			session.beginTransaction();
			session.createQuery(DELNOTEBYID).setParameter("newId", newId).executeUpdate();
			session.getTransaction().commit();

		} catch (Exception o) {
			return o.getMessage();
		}
		return OK;
	}

	@Override
	public String editNote(int id, String text, String data) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			session.createQuery(UPDATE_NOTE).setParameter("newId", id).setParameter("newText", text).setParameter("newDate", data)
					.executeUpdate();
			session.getTransaction().commit();

		} catch (Exception o) {
			return o.getMessage();
		}
		return OK;
	}

}
