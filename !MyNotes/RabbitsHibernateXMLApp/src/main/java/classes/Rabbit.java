package classes;

import java.sql.Date;
import java.util.Calendar;

public class Rabbit {
	private int id;
	private String name;
	private int sex;
	private Date dateOfBirth;
	private int count;
	private String personDiary;
	
	private int age;
	private String sexText;

	public Rabbit(String name, int sex, Date dateOfBirth, int count, String personDiary) {
		this.name = name;
		this.sex = sex;
		this.dateOfBirth = dateOfBirth;
		this.count = count;
		this.personDiary = personDiary;		
	}

	public Rabbit() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public int getAge() {
		return calculateRabbitAge(dateOfBirth);
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getPersonDiary() {
		return personDiary;
	}

	public void setSexText(String sexText) {
		this.sexText=sexText;
	}

	public String getSexText() {
		return sex == 1 ? "wooman" : sex == 3 ? "little rabbit" : "man";
	}

	public void setPersonDiary(String personDiary) {
		this.personDiary = personDiary;
	}
	
	private int calculateRabbitAge(Date dateOfBirth) {
		Calendar birth = Calendar.getInstance();
		birth.setTime(dateOfBirth);
		Calendar today = Calendar.getInstance();
		age = today.get(Calendar.MONTH) - birth.get(Calendar.MONTH);
		int ageYear = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
		if (ageYear > 0) {
			age = age + ageYear * 12;
		}
		return age;
	}

	@Override
	public String toString() {
		return "'" + name + "'" + "  " + sexText +" "
				+ age + " month     " + count + "  count";
	}

}