package classes;

import java.sql.Date;
import java.util.Calendar;

public class Note implements Comparable<Note> {
	private int id;
	private int rabbitName;
	private String text;
	private Date date;
	private Rabbit rabbit;
	private int leftDays;

	public Note(int rabbitName, String text, Date date) {
		this.rabbitName = rabbitName;
		this.text = text;
		this.date = date;
	}

	public Note() {
	}

	public int getRabbitName() {
		return rabbitName;
	}

	public void setRabbitName(int rabbitName) {
		this.rabbitName = rabbitName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Rabbit getRabbit() {
		return rabbit;
	}

	public void setRabbit(Rabbit rabbit) {
		this.rabbit = rabbit;
	}

	public int getLeftDays() {
		return findDaysLeft();
	}

	public void setLeftDays(int leftDays) {
		this.leftDays = leftDays;
	}

	private int findDaysLeft() {
		Calendar note = Calendar.getInstance();
		note.setTime(date);
		Calendar today = Calendar.getInstance();
		int leftDays = today.get(Calendar.DAY_OF_YEAR) - note.get(Calendar.DAY_OF_YEAR);
		if ((note.get(Calendar.YEAR) - today.get(Calendar.YEAR)) > 0) {
			leftDays -= 180;
		}
		return Math.abs(leftDays);
	}

	@Override
	public int compareTo(Note note) {
		return (leftDays - note.leftDays);
	}

	@Override
	public String toString() {
		return id + ")  '" + rabbit.getName() + "' " + text + " data: " + date + " left " + leftDays + " day";
	}

}