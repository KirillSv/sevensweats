package Processor;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import accessOrganization.User;
import classes.Note;
import classes.Rabbit;
import dao.DAO;
import dao.FarmerDAO;

public class DBmetods {


	public String addRabbit(String name, String sexString, String dateOfBirthString, int count) throws Exception {
		InteriorMetods processor = new InteriorMetods();
		boolean checkName = processor.checkValidName(name);
		if (checkName) {
			return "A rabbit with that name is already in the database. Come up with a new";
		}
		int sexInt = Integer.parseInt(sexString);

		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date parsed = format.parse(dateOfBirthString);
		java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());

		Rabbit newRabbit = new Rabbit(name, sexInt, sqlDate, count, "0");
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		DAO farmerDAO = new FarmerDAO(factory);
		farmerDAO.addRabbit(newRabbit);

		return "Record successfully added to database";
	}

	public ArrayList<Rabbit> readRabbitFromBase() {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		DAO farmerDAO = new FarmerDAO(factory);
		List<Rabbit> listRabbit = farmerDAO.readRabbitFromBase();
		return (ArrayList<Rabbit>) listRabbit;
	}

	public String addNote(String rabbitName, String text, String dateOfNote) throws Exception {
		int NoteName = -1;

		ArrayList<Rabbit> listRabbit = readRabbitFromBase();
		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getName().equals(rabbitName)) {
				NoteName = rabbit.getId();
			}
		}
		if (NoteName == -1) {
			return "A rabbit with that name is absent in the database. First you need to add it to the database";
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date parsed = format.parse(dateOfNote);
		java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
		Note newNote = new Note(NoteName, text, sqlDate);
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		DAO farmerDAO = new FarmerDAO(factory);
		farmerDAO.addNote(newNote);

		return "Recording was successful";
	}

	public ArrayList<Note> readNoteFromBase() {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		DAO farmerDAO = new FarmerDAO(factory);
		List<Note> listNote = farmerDAO.readNoteFromBase();
		return (ArrayList<Note>) listNote;

	}

	public User loginChecker(String username, String password) {
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		DAO farmerDAO = new FarmerDAO(factory);
		List<User> listUser = farmerDAO.loginChecker();
		for (User user : listUser) {
			System.out.println(listUser);
			if (user.getPassword().equals(password) & (user.getLogin().equals(username)))
				return user;
		}
		return new User("0", "0", "GUEST");
	}

	public String deleteRabbit(String nameRabbit) {
	    InteriorMetods processor = new InteriorMetods();
		boolean checkName = processor.checkValidName(nameRabbit);
		Rabbit ourRabbit = null;
		if (!checkName) {
			return "Rabbit with name " + nameRabbit + " is absent in the database";
		}
		ArrayList<Rabbit> listRabbit = readRabbitFromBase();
		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getName().equals(nameRabbit)) {
				ourRabbit = rabbit;
				break;
			}
		}
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		DAO farmerDAO = new FarmerDAO(factory);
		farmerDAO.deleteRabbit(ourRabbit);

		return "Everything is fine. Rabbit removed";
	}

	public String deleteNote(String numberNote) {
		Note ourNote = null;
		try {

			int numberNoteInt = Integer.valueOf(numberNote);
			boolean checkName = false;
			ArrayList<Note> listNote = readNoteFromBase();
			for (Note note : listNote) {
				if ((note.getId() == numberNoteInt)) {
					checkName = true;
					ourNote = note;
					break;

				}
			}

			if (!checkName) {
				return "Note with number " + numberNoteInt + " is absent in database";
			}

		} catch (Exception e) {
			return "Enter the record number from the table and not this-" + numberNote;

		}
		SessionFactory factory = new Configuration().configure().buildSessionFactory();
		DAO farmerDAO = new FarmerDAO(factory);
		farmerDAO.deleteNote(ourNote);

		return "Note delete successful";
	}

	public String addMessageInPersonDiary(String nameRabbit, String newMessage) {
		List<Rabbit> listRabbit = readRabbitFromBase();
		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getName().contentEquals(nameRabbit)) {
				 InteriorMetods processor = new InteriorMetods();
				rabbit.setPersonDiary(processor.readPersonDiary(nameRabbit) + "#" + newMessage);
				SessionFactory factory = new Configuration().configure().buildSessionFactory();
				DAO farmerDAO = new FarmerDAO(factory);
				farmerDAO.addRabbit(rabbit);

			}

		}

		return "Recording was successful";
	}

}
