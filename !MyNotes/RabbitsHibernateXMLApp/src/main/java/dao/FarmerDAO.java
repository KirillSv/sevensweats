package dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import accessOrganization.User;
import classes.Note;
import classes.Rabbit;

public class FarmerDAO implements DAO {

	private final SessionFactory factory;

	public FarmerDAO(final SessionFactory factory) {
		this.factory = factory;
	}

	public SessionFactory getFactory() {
		return factory;
	}

	public void addRabbit(Rabbit rabbit) {
		try (final Session session = factory.openSession()) {
			session.beginTransaction();
			session.saveOrUpdate(rabbit);
			session.getTransaction().commit();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Rabbit> readRabbitFromBase() {
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			List<Rabbit> listRabbit = (List<Rabbit>) session.createQuery("from Rabbit order by id").list();
			session.getTransaction().commit();
			return listRabbit;
		}

	}

	@Override
	public void addNote(Note note) {
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			session.save(note);
			session.getTransaction().commit();

		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Note> readNoteFromBase() {
		List<Note> listNote = null;
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			listNote = (List<Note>) session.createQuery("from Note order by id").list();
			session.getTransaction().commit();
			if (listNote != null) {
				for (Note note : listNote) {
					if (note != null) {
						Hibernate.initialize(note.getRabbit().getName());
					}
				}

			}
		}
		return listNote;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> loginChecker() {
		List<User> listUser = null;

		try (Session session = factory.openSession()) {
			Transaction transaction = session.beginTransaction();
			listUser = (List<User>) session.createQuery("from  User").list();
			transaction.commit();
		}
		return listUser;
	}

	@Override
	public void deleteRabbit(Rabbit rabbit) {
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			session.delete(rabbit);
			session.getTransaction().commit();
		}
	}

	@Override
	public void deleteNote(Note note) {
		try (Session session = factory.openSession()) {
			session.beginTransaction();
			session.delete(note);
			session.getTransaction().commit();
		}

	}

}
