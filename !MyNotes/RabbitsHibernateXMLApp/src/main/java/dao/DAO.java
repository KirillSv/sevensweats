package dao;

import java.util.List;

import accessOrganization.User;
import classes.Note;
import classes.Rabbit;

public interface DAO {

	public void addRabbit(Rabbit rabbit);
	public List<Rabbit> readRabbitFromBase();
	public void addNote(Note note);
	public List <Note> readNoteFromBase();
	public void deleteNote(Note note);
	List<User> loginChecker();
	void deleteRabbit(Rabbit rabbit);
	}
	

