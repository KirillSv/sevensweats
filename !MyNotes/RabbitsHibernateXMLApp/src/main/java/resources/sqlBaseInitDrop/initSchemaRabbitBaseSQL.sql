CREATE DATABASE  IF NOT EXISTS `rabbitbase_hibernate` ;
USE `rabbitbase_hibernate2`;
SET NAMES utf8 ;


DROP TABLE IF EXISTS `access_table`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `access_table` (
  `id` int(11) NOT NULL,
  `login` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
LOCK TABLES `access_table` WRITE;
INSERT INTO `access_table` VALUES (1,'admin','admin','ADMIN'),(2,'1','1','USER'),(3,'0','0','GUEST');
UNLOCK TABLES;

DROP TABLE IF EXISTS `rabbit_table`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rabbit_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sex` tinyint(10) DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `count` tinyint(50) NOT NULL,
  `person_diary` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;
LOCK TABLES `rabbit_table` WRITE;
INSERT INTO `rabbit_table` VALUES 
(1,'����',1,'2019-03-07',1,'0#344#03,11 ������ 11 ����'),
(2,'���',1,'2019-03-07',1,'0#����� 2 �� ������'),
(3,'���',1,'2019-03-07',1,'0#�������   9 �������#04.11 ������ ������ �������������#05.11 ������� �����.'),
(4,'�����',2,'2018-05-24',1,'0#������� ������#1'),
(30,'����1 �������',3,'2019-07-25',7,NULL),
(31,'���1 �������',3,'2019-07-25',5,NULL),
(32,'���2 �������',3,'2019-10-01',5,NULL),
(33,'����2 �������',3,'2019-09-10',5,NULL),
(35,'����3 �������',2,'2019-11-03',11,'0');
UNLOCK TABLES;

DROP TABLE IF EXISTS `note_table`;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `note_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `text` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `date_note` date NOT NULL,
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `DelNoteifDeletedRabbit_idx` (`name`),
  CONSTRAINT `DelNoteIfRabbitDel` FOREIGN KEY (`name`) REFERENCES `rabbit_table` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
LOCK TABLES `note_table` WRITE;
INSERT INTO `note_table` VALUES 
(30,30,'�����','2019-11-23'),
(31,31,'�����','2019-11-23'),
(38,32,'�����','2020-02-01'),
(39,33,'�����','2020-01-09'),
(46,2,'����','2019-12-02'),
(47,1,'������','2019-12-03'),
(48,35,'�����','2020-03-03'),
(50,3,'����(��������� �������)','2019-12-08');
UNLOCK TABLES;
