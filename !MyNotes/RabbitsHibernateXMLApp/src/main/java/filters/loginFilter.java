package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class loginFilter implements Filter {

	public void destroy() {
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		if (username.length() < 1) {
			request.setAttribute("loginErrorMessage", "�� �� ����� �����");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		} else if (password.length() < 1) {
			request.setAttribute("loginErrorMessage", "�� �� ����� ������");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}

		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {

	}
}
