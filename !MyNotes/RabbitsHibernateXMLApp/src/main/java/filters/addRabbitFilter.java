package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class addRabbitFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		String errorMessage = null;
		String name = request.getParameter("name");
		int Icount = 0, Isex = 0, IdateOfBirthYear = 0, IdateOfBirthMonth = 0, IdateOfBirthDay = 0;
		try {

			Icount = Integer.valueOf(request.getParameter("count"));
			Isex = Integer.valueOf(request.getParameter("sex"));
			IdateOfBirthYear = Integer.valueOf(request.getParameter("dateOfBirthYear"));
			IdateOfBirthMonth = Integer.valueOf(request.getParameter("dateOfBirthMonth"));
			IdateOfBirthDay = Integer.valueOf(request.getParameter("dateOfBirthDay"));

		} catch (NumberFormatException e) {
			e.printStackTrace();
			errorMessage = "���������� ��������� ��������� �����";
			request.setAttribute("errorMessage", errorMessage);
			request.getRequestDispatcher("jsp/addRabbit.jsp").forward(request, response);
		}

		if (Icount < 1 || Icount > 15) {
			errorMessage = "�� ����� ������������ ���������� ��������";

		} else {
			if (name.length() < 2) {
				errorMessage = "�� �� ����� ��� �������";

			} else {
				if (IdateOfBirthYear < 2019 || IdateOfBirthYear > 2025 || IdateOfBirthMonth < 1
						|| IdateOfBirthMonth > 12 || IdateOfBirthDay < 1 || IdateOfBirthDay > 31) {
					errorMessage = "�� ����� ������������ ����. ������: 12/12/2018 ";

				} else {
					if (Isex == 0) {
						errorMessage = "�� �� ������� ��� �������";
					}
				}

			}
		}

		if (errorMessage != null) {
			request.setAttribute("errorMessage", errorMessage);
			request.getRequestDispatcher("jsp/addRabbit.jsp").forward(request, response);
		} else {
			chain.doFilter(request, response);

		}

	}

	public void init(FilterConfig fConfig) throws ServletException {

	}

}
