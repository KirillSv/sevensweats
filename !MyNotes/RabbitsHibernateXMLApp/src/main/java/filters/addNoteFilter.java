package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class addNoteFilter implements Filter {

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		String errorMessage = null;
		String name = request.getParameter("name");
		String text = request.getParameter("text");
		int IdateOfNoteYear = 0, IdateOfNoteMonth = 0, IdateOfNoteDay = 0;
		try {
			IdateOfNoteYear = Integer.valueOf(request.getParameter("dateOfNoteYear"));
			IdateOfNoteMonth = Integer.valueOf(request.getParameter("dateOfNoteMonth"));
			IdateOfNoteDay = Integer.valueOf(request.getParameter("dateOfNoteDay"));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			errorMessage = "���������� ��������� ��������� �����";
			request.setAttribute("errorMessage", errorMessage);
			request.getRequestDispatcher("jsp/addNote.jsp").forward(request, response);
		}
		if (text.length() < 2) {
			errorMessage = "�� �� ����� �������� �����������";

		} else {
			if (IdateOfNoteYear < 2019 || IdateOfNoteYear > 2030 || IdateOfNoteMonth < 1 || IdateOfNoteMonth > 12
					|| IdateOfNoteDay < 1 || IdateOfNoteDay > 31) {
				errorMessage = "�� ����� ������������ ����. ������: 12/12/2018 ";

			} else {
				if (name.length() < 2) {
					errorMessage = "�� �� ����� ��� �������";

				}

			}
		}

		if (errorMessage != null) {
			request.setAttribute("errorMessage", errorMessage);
			request.getRequestDispatcher("jsp/addNote.jsp").forward(request, response);
		} else {
			chain.doFilter(request, response);

		}

	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
