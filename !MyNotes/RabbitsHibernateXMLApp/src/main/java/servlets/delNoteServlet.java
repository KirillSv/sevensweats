package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Processor.DBmetods;

@WebServlet("/delNoteServlet")
public class delNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 5L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/delNote.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("ADMIN")) {

			DBmetods dBmetods = new DBmetods();
			String errorMessage = dBmetods.deleteNote(request.getParameter("number"));
			request.setAttribute("errorMessage", errorMessage);

			request.setAttribute("sumOldRabbit", session.getAttribute("sumOldRabbit"));
			request.setAttribute("sumLitleRabbit", session.getAttribute("sumLitleRabbit"));
			request.setAttribute("sumFood", session.getAttribute("sumFood"));
			request.setAttribute("sumAddMeat", session.getAttribute("sumAddMeat"));
			request.setAttribute("listRabbit", session.getAttribute("listRabbit"));
			request.setAttribute("listNote", session.getAttribute("listNote"));

			request.getRequestDispatcher("jsp/delNote.jsp").forward(request, response);
		}
		request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

	}

}
