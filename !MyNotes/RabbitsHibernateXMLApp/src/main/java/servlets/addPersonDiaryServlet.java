package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Processor.DBmetods;
import Processor.InteriorMetods;

@WebServlet("/addPersonDiaryServlet")
public class addPersonDiaryServlet extends HttpServlet {
	private static final long serialVersionUID = 7L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (!(session.getAttribute("role").equals("USER") || session.getAttribute("role").equals("ADMIN"))) {

			String newMessage = request.getParameter("text");
			DBmetods db = new DBmetods();
			String name = (String) session.getAttribute("name");
			db.addMessageInPersonDiary(name, newMessage);
			InteriorMetods processor = new InteriorMetods();
			String listDataRabbit = processor.readPersonDiary(name);
			String[] listDataRabbitArr = listDataRabbit.split("#");

			request.setAttribute("sumOldRabbit", session.getAttribute("sumOldRabbit"));
			request.setAttribute("sumLitleRabbit", session.getAttribute("sumLitleRabbit"));
			request.setAttribute("sumFood", session.getAttribute("sumFood"));
			request.setAttribute("sumAddMeat", session.getAttribute("sumAddMeat"));
			request.setAttribute("listRabbit", session.getAttribute("listRabbit"));
			request.setAttribute("listNote", session.getAttribute("listNote"));

			request.setAttribute("listDataRabbit", listDataRabbitArr);
			request.getRequestDispatcher("jsp/personDiary.jsp").forward(request, response);
		}
		request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

	}
}
