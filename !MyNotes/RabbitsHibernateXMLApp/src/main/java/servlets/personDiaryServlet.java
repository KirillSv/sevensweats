package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Processor.InteriorMetods;

@WebServlet("/personDiaryServlet")
public class personDiaryServlet extends HttpServlet {
	private static final long serialVersionUID = 8L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/personDiary.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("USER") || session.getAttribute("role").equals("ADMIN")) {

			String name = request.getParameter("name");
			InteriorMetods processor = new InteriorMetods();
			String listDataRabbit = processor.readPersonDiary(name);
			request.setAttribute("rabbitName", name);
			if (listDataRabbit.equals("Rabbit with name " + name + " is absent in the database")) {
				request.setAttribute("rabbitName", "Rabbit with name " + name + " is absent in our database.");
				request.setAttribute("listDataRabbit", null);
				request.getRequestDispatcher("jsp/personDiary.jsp").forward(request, response);
			}

			String[] listDataRabbitArr = listDataRabbit.split("#");
			session.setAttribute("name", name);
			session.setAttribute("listDataRabbitArr", name);

			request.setAttribute("listDataRabbit", listDataRabbitArr);

			request.setAttribute("sumOldRabbit", session.getAttribute("sumOldRabbit"));
			request.setAttribute("sumLitleRabbit", session.getAttribute("sumLitleRabbit"));
			request.setAttribute("sumFood", session.getAttribute("sumFood"));
			request.setAttribute("sumAddMeat", session.getAttribute("sumAddMeat"));
			request.setAttribute("listRabbit", session.getAttribute("listRabbit"));
			request.setAttribute("listNote", session.getAttribute("listNote"));

			request.getRequestDispatcher("jsp/personDiary.jsp").forward(request, response);
		}

		request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

	}

}
