package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Processor.DBmetods;

@WebServlet("/addNoteServlet")
public class addNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 2L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/addNote.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("USER") || session.getAttribute("role").equals("ADMIN")) {

			String name = request.getParameter("name");
			String text = request.getParameter("text");

			DBmetods DBmetods = new DBmetods();

			String dateOfNoteMonth = request.getParameter("dateOfNoteMonth");
			String dateOfNoteDay = request.getParameter("dateOfNoteDay");

			if (dateOfNoteMonth.length() < 2) {
				dateOfNoteMonth = "0" + dateOfNoteMonth;
			}
			if (dateOfNoteDay.length() < 2) {
				dateOfNoteDay = "0" + dateOfNoteDay;
			}

			String dateOfNote = request.getParameter("dateOfNoteYear") + dateOfNoteMonth + dateOfNoteDay;

			String errorMessage = "";
			try {
				errorMessage = DBmetods.addNote(name, text, dateOfNote);
			} catch (Exception e) {
				e.printStackTrace();
			}
			request.setAttribute("errorMessage", errorMessage);

			request.setAttribute("sumOldRabbit", session.getAttribute("sumOldRabbit"));
			request.setAttribute("sumLitleRabbit", session.getAttribute("sumLitleRabbit"));
			request.setAttribute("sumFood", session.getAttribute("sumFood"));
			request.setAttribute("sumAddMeat", session.getAttribute("sumAddMeat"));
			request.setAttribute("listRabbit", session.getAttribute("listRabbit"));
			request.setAttribute("listNote", session.getAttribute("listNote"));

			request.getRequestDispatcher("jsp/addNote.jsp").forward(request, response);
		}
		request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

	}

}
