package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Processor.DBmetods;

@WebServlet("/addRabbitServlet")
public class addRabbitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/addRabbit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("USER") || session.getAttribute("role").equals("ADMIN")) {

			DBmetods DBmetods = new DBmetods();
			String name = request.getParameter("name");
			String sex = request.getParameter("sex");
			int count = Integer.valueOf(request.getParameter("count"));
			String dateOfBirthMonth = request.getParameter("dateOfBirthMonth");
			String dateOfBirthDay = request.getParameter("dateOfBirthDay");

			if (dateOfBirthMonth.length() < 2) {
				dateOfBirthMonth = "0" + dateOfBirthMonth;
			}
			if (dateOfBirthDay.length() < 2) {
				dateOfBirthDay = "0" + dateOfBirthDay;
			}

			String dateOfBirth = request.getParameter("dateOfBirthYear") + dateOfBirthMonth + dateOfBirthDay;

			String errorMessage;
			try {
				errorMessage = DBmetods.addRabbit(name, sex, dateOfBirth, count);
			} catch (Exception e) {
				errorMessage = "Database problem";
				e.printStackTrace();

			}

			request.setAttribute("sumOldRabbit", session.getAttribute("sumOldRabbit"));
			request.setAttribute("sumLitleRabbit", session.getAttribute("sumLitleRabbit"));
			request.setAttribute("sumFood", session.getAttribute("sumFood"));
			request.setAttribute("sumAddMeat", session.getAttribute("sumAddMeat"));
			request.setAttribute("listRabbit", session.getAttribute("listRabbit"));
			request.setAttribute("listNote", session.getAttribute("listNote"));

			request.setAttribute("errorMessage", errorMessage);
			request.getRequestDispatcher("jsp/addRabbit.jsp").forward(request, response);

		}
		request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

	}

}
