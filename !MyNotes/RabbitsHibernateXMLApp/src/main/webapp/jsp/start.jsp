<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ферма "Веселый кролик"</title>
<c:import url="style.jsp"></c:import>
</head>
<body>
	<div class='header'>
		<h1>Ферма "Веселый кролик" <img src="/img/rabbit.gif" width="60" height="60" class='headerImg'></h1>		
		<p> "Кролики — это не только ценный мех, но и три — четыре килограмма диетического, легкоусвояемого мяса"(c)</p>
		<p>Вы зашли как  ${role}<p>
	</div>
<div class='twoStaticTables'>
<br />
	<c:import url="tableRabbit.jsp"></c:import>	
	<br />
	<c:import url="tableNote.jsp"></c:import>	
	<br />
	<h3>Всего кроликов : взрослых (>4мес) ${sumOldRabbit} штук, маленьких ${sumLitleRabbit} штук<br>
    Среднемесячное потребление кормов: ${sumFood} кг<br>
    Ежесуточный прирост в пересчете на чистое мясо: ${sumAddMeat} кг</h3>
    
    <form action="/startServlet">
			<input type="submit" name="refresh"
				value=" Обновить " style="width: 500px; " class="button"/>
    </form>
</div>
	<div class='menu'>
	<br />
		<form action="/addNoteServlet" > 
			<input type="submit" name="addNote" value="+ Добавить запись"
				style="width: 300px" onclick="addNote()" class="button"/><br />
		</form>

		<form action="/addRabbitServlet">
			<input type="submit" name="addRabbit" value="+ Добавить кролика(-ов)"
				style="width: 300px" class="button"/> <br />
		</form>
		<br />

		<form action="/delRabbitServlet">
			<input type="submit" name="deleteRabbit"
				value="- Удалить кролика(-ов)" style="width: 300px" class="button"/>
		</form>

		<form action="/delNoteServlet">
			<input type="submit" name="deleteNote" value="- Удалить запись"
				style="width: 300px" class="button"/>
		</form>
		<br />
		<form action="/personDiaryServlet">
			<input type="submit" name="personDiary"
				value=" Посмотреть личное дело " style="width: 300px" class="button"/>
		</form>
		</div>
</body>
</html>