<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Удалить кролика</title>
</head>
<body>
	<c:import url="start.jsp"></c:import>
	<div class='delsomethingJSP'>
		<h3>Если удалить кролика из базы данных.</h3>
		<h3> Восстановить его будет невозможно!</h3>
		<form name="Form2" method="post" action="/delRabbitServlet">
			<input name="name" placeholder="Имя кролика" size="25"><br />
			<br /> <br /> <input type="submit" name="Go"
				value="    Удалить  " class="button"> <br /> <br />
		</form>
		<br /> ${errorMessage}
	</div>
</body>
</html>