<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h3>Сейчас на вашей ферме счастливо обитают такие кролики:</h3>

	<table border="1"  cellpadding="5" bgcolor="#00FF00">
		<tr>
			<th> Имя </th>
			<th> Пол </th>
			<th> Возраст(мес) </th>
			<th> штук </th>
		</tr>
		<c:forEach var="Rabbit" items="${listRabbit}">
			<tr>
				<td>${Rabbit.name}</td>
				<td>${Rabbit.sexText}</td>
				<td>${Rabbit.age}</td>
				<td>${Rabbit.count}</td>
			</tr>

		</c:forEach>
	</table>
</body>
</html>