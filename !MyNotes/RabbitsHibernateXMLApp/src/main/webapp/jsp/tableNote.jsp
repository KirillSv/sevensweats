<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h3>Запланированые мероприятия:</h3>
	<table border="1" cellpadding="5" bgcolor="#00FF00">
		<tr>
			<th>Id</th>
			<th>Имя кролика</th>
			<th>Мероприятие</th>
			<th>Когда</th>
			<th>Осталось дней:</th>
		</tr>
		<c:forEach var="Note" items="${listNote}">
			<tr>
				<td>${Note.id}</td>
				<td>${Note.rabbit.name}</td>
				<td>${Note.text}</td>
				<td>${Note.date}</td>
				<td>${Note.leftDays}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>