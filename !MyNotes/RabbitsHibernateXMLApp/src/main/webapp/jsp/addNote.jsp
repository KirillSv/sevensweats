<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Добавить запись</title>
</head>
<body>
	<c:import url="start.jsp"></c:import>
	<div class='addsomethingJSP'>
		<br /> <br />
		<h3>Добавление записи в базу данных:</h3>
		<form name="Form2" method="post" action="/addNoteServlet">

			<input name="name" placeholder="Имя кролика" size="25"><br />
			<input name="dateOfNoteDay" placeholder="День" size="5">
			<input name="dateOfNoteMonth" placeholder="Месяц"  size="4">
			<input name="dateOfNoteYear" placeholder="Год"  size="5"><br />
			<input name="text" placeholder="Мероприятие" size="25"><br />
			<br /> <br /> <input type="submit" name="Go"
				value="    Добавить в базу   " class="button"> <br /> <br />
		</form>
		<br /> ${errorMessage}
	</div>
</body>
</html>