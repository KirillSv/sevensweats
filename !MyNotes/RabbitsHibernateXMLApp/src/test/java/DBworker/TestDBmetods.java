package DBworker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import Processor.DBmetods;
import Processor.InteriorMetods;
import classes.Note;

public class TestDBmetods {

	private DBmetods db = new DBmetods();
	private InteriorMetods processor = new InteriorMetods();

	@Test
	public void testReadNoteFromBase() {
		ArrayList<Note> listNotes = db.readNoteFromBase();
		assertNotNull(listNotes);
		System.out.println(listNotes);
	}

	@Test
	public void testAddRabbit() throws Exception {
		db.addRabbit("TestAddRabbit", "1", "20010101", 1);
		assertTrue(processor.checkValidName("TestAddRabbit"));
		db.deleteRabbit("TestAddRabbit");
	}

	@Test
	public void testDeleteRabbit() throws Exception {
		db.addRabbit("TestRabbit777", "1", "20010101", 1);
		assertTrue(processor.checkValidName("TestRabbit777"));
		db.deleteRabbit("TestRabbit777");
		assertFalse(processor.checkValidName("TestRabbit777"));
	}

	@Test
	public void testAddNote() throws Exception {
		String expectedText = "textTest";
		String receivedText = "";
		String rabbitName = "TestRabbit11";
		db.addRabbit(rabbitName, "1", "20010101", 1);
		db.addNote(rabbitName, expectedText, "20191010");
		ArrayList<Note> testListNote = db.readNoteFromBase();
		for (Note note : testListNote) {
			if (note.getText().contentEquals(expectedText)) {
				receivedText = note.getText();
			}

		}

		assertEquals(receivedText, expectedText);
		db.deleteRabbit(rabbitName); // Note delete automatically
	}

	@Test
	public void testDeleteNote() throws Exception {
		String testId = "";
		db.addRabbit("TestRabbit", "1", "20010101", 1);
		db.addNote("TestRabbit", "textTest", "20191010");
		ArrayList<Note> testListNote = db.readNoteFromBase();
		for (Note note : testListNote) {
			if (note.getText().contentEquals("textTest")) {
				testId = String.valueOf(note.getId());
			}
		}
		assertEquals(db.deleteNote(testId), "Note delete successful");
		db.deleteRabbit("TestRabbit"); 

	}

	@Test
	public void testAddMessageInPersonDiary() throws Exception {
		String RabbitName = "TestRabt1";
		String newMessage = "TestMessage";
		String expectedMessage = "0" + "#" + "TestMessage" + "#" + "TestMessage";
		db.addRabbit(RabbitName, "1", "20010101", 1);
		db.addMessageInPersonDiary(RabbitName, newMessage);
		db.addMessageInPersonDiary(RabbitName, newMessage); // need check double add
		assertEquals(processor.readPersonDiary(RabbitName), expectedMessage);
		db.deleteRabbit(RabbitName);
	}

}