package DBworker;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import Processor.DBmetods;
import accessOrganization.User;
import edu.emory.mathcs.backport.java.util.Arrays;

@RunWith(Parameterized.class)
public class ParameterizedTestClass {

	String login, password, role, expResultRole;

	public ParameterizedTestClass(String login, String password, String expResultRole) {
		this.login = login;
		this.password = password;
		this.expResultRole = expResultRole;
	}

	@Test
	public void testloginCheckerTest_returnGuestUser() {
		DBmetods db = new DBmetods();
		User actual = db.loginChecker(login, password);
		assertEquals(expResultRole, actual.getRole());
	}

	@Parameterized.Parameters
	public static List<?> members() {
		return Arrays.asList(new Object[][] {
					{ "1", "1", "USER" }, 
					{ "admin", "admin", "ADMIN" }, 
					{ "1", "e", "GUEST" },
					{ "e", "admin", "GUEST" },
					{ "vasya", "pupkin", "GUEST" }, 
					{ "something", "else", "GUEST" } });
	}
}
