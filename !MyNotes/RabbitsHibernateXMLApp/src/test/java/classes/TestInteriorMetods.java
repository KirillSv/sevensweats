package classes;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.annotation.processing.Processor;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import Processor.DBmetods;
import Processor.InteriorMetods;

public class TestInteriorMetods {

	private ArrayList<Rabbit> testRabbitList;
	private InteriorMetods processor;
	private DBmetods db = new DBmetods();

	@Ignore
	@Before
	public void testPrepareListRabbitForTest() throws Exception {
		processor = new InteriorMetods();
		testRabbitList = new ArrayList<Rabbit>();
		String sDate1 = "20010101";
		String sDate2 = "20191008"; // here must be today date minus 1 months. Because rabbits up to 1 months don't
									// consume food
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date parsed1 = format.parse(sDate1);
		Date parsed2 = format.parse(sDate2);
		java.sql.Date sql1 = new java.sql.Date(parsed1.getTime());
		java.sql.Date sql2 = new java.sql.Date(parsed2.getTime());

		testRabbitList.add(new Rabbit("OneOldRabbit", 1, sql1, 1, "firstDiary"));
		testRabbitList.add(new Rabbit("little", 1, sql2, 10, "SecondDiary"));
	}

	@Ignore
	@Test
	public void testFindSumRabbit() {
		int[] expected = { 1, 10 }; // result [count old rabbit , count little rabbit]
		assertArrayEquals(processor.findSumRabbit(testRabbitList), expected);
	}

	@Ignore
	@Test
	public void testFindSumFood() {
		double expected = 2.7;
		assertEquals(processor.findSumFood(testRabbitList), expected, 0.1);
	}

	@Ignore
	@Test
	public void testFindSumAddMeat() {
		double expected = 0.13;
		assertEquals(processor.findSumAddMeat(testRabbitList), expected, 0.1);
	}

	@Ignore
	@Test
	public void testReadPersonDiary() throws Exception {
		db.addRabbit("TestRabbit", "1", "20191010", 1);
		db.addMessageInPersonDiary("TestRabbit", "ExpectedTestNote");
		assertEquals(processor.readPersonDiary("TestRabbit"), "0#" + "ExpectedTestNote");
		db.deleteRabbit("TestRabbit");
	}

	@Ignore
	@Test
	public void testCheckValidName() throws Exception {
		db.addRabbit("TestRabbit1", "1", "20191010", 1);
		assertTrue(processor.checkValidName("TestRabbit1"));
		assertFalse(processor.checkValidName("NoNameFantasticRabbit"));
		db.deleteRabbit("TestRabbit1");
	}
}
