package com.example.next.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.next.domain.User;

public interface UserRepo extends JpaRepository<User, Long> {
    User findByUsername(String username);
}