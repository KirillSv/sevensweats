package com.example.next.securingweb;

	import org.springframework.context.annotation.Bean;
	import org.springframework.context.annotation.Configuration;
	import org.springframework.security.config.annotation.web.builders.HttpSecurity;
	import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
	import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
	import org.springframework.security.core.userdetails.User;
	import org.springframework.security.core.userdetails.UserDetails;
	import org.springframework.security.core.userdetails.UserDetailsService;
	import org.springframework.security.provisioning.InMemoryUserDetailsManager;

	@Configuration
	@EnableWebSecurity
	public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http
				.authorizeRequests()                 //avtorizaciya
				.antMatchers("/").permitAll()    //na glavnuyu stranicu daem polniy dostup
				.anyRequest().authenticated()    // ostalnim autentification
			.and()
				.formLogin()                      //vkluchaem formu logina
				.loginPage("/login")               //ukazivaem maping logina
				.permitAll()                      //razreshaem etim polzovatsya vsem
			.and()
				.logout()                       //vkluchaem logaut
				.permitAll();                    //razreshaem etim polzovatsya vsem
		}

		@Bean
		@Override       //sozdaet v pamyati menedger obslugivayusiy uchetnie zapisi
		public UserDetailsService userDetailsService() {
			UserDetails user =
				 User.withDefaultPasswordEncoder()
					.username("user")
					.password("password")
					.roles("USER")
					.build();

			return new InMemoryUserDetailsManager(user);
		}
	}