package com.itproger.blog.Repo;

import org.springframework.data.repository.CrudRepository;
import com.itproger.blog.Models.Post;

public interface PostRepository extends CrudRepository<Post, Long> {

}
