package com.example.next.servingwebcontent;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.next.repository.MessageRepository;
import com.example.next.domain.Message;

@Controller
public class GreetingController {
	@Autowired
	@Qualifier("messageRepository")
	private MessageRepository messageRepository;

	@GetMapping
	public String main(Map<String, Object> model) {
		Iterable<Message> messages = messageRepository.findAll();

		model.put("messages", messages);
		return "main";
	}

	@GetMapping("/greeting")
	public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name,
			Map<String, Object> model) {
		model.put("name", name);
		return "greeting";
	}

	@PostMapping // output
	public String add(@RequestParam String text, @RequestParam String tag, Map<String, Object> model) {
		Message message = new Message(text, tag);
		messageRepository.save(message); // save to repository

		Iterable<Message> messages = messageRepository.findAll(); // taken from repository

		model.put("messages", messages); // put in model

		return "main"; // get to user

	}

	@PostMapping("filter") // naidet po peredannomu tegu vse messages
	public String filter(@RequestParam String filter, Map<String, Object> model) {
		Iterable<Message> messages;
		
		if(filter !=null && !filter.isEmpty()) {
			messages=messageRepository.findByTag(filter);
		} else {
			messages =messageRepository.findAll();
		}

		model.put("messages", messages);

		return "main";
	}
}