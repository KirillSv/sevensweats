package letscode.javalearn;

import letscode.javalearn.domain.Department;
import letscode.javalearn.domain.Employee;
import letscode.javalearn.domain.Event;
import letscode.javalearn.domain.Position;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.*;

public class Streams {
    private List<Employee> emps = List.of(
            new Employee("Michael", "Smith",   243,  43, Position.CHEF),
            new Employee("Jane",    "Smith",   523,  40, Position.MANAGER),
            new Employee("Jury",    "Gagarin", 6423, 26, Position.MANAGER),
            new Employee("Jack",    "London",  5543, 53, Position.WORKER),
            new Employee("Eric",    "Jackson", 2534, 22, Position.WORKER),
            new Employee("Andrew",  "Bosh",    3456, 44, Position.WORKER),
            new Employee("Joe",     "Smith",   723,  30, Position.MANAGER),
            new Employee("Jack",    "Gagarin", 7423, 35, Position.MANAGER),
            new Employee("Jane",    "London",  7543, 42, Position.WORKER),
            new Employee("Mike",    "Jackson", 7534, 31, Position.WORKER),
            new Employee("Jack",    "Bosh",    7456, 54, Position.WORKER),
            new Employee("Mark",    "Smith",   123,  41, Position.MANAGER),
            new Employee("Jane",    "Gagarin", 1423, 28, Position.MANAGER),
            new Employee("Sam",     "London",  1543, 52, Position.WORKER),
            new Employee("Jack",    "Jackson", 1534, 27, Position.WORKER),
            new Employee("Eric",    "Bosh",    1456, 32, Position.WORKER)
    );

    private List<Department> deps = List.of(
            new Department(1, 0, "Head"),
            new Department(2, 1, "West"),
            new Department(3, 1, "East"),
            new Department(4, 2, "Germany"),
            new Department(5, 2, "France"),
            new Department(6, 3, "China"),
            new Department(7, 3, "Japan")
    );

    @Test
    public void creation() throws IOException {
		//стрим содержит список строк из файла
        Stream<String> lines = Files.lines(Paths.get("some.txt"));
		// список обьектов в дирректории
        Stream<Path> list = Files.list(Paths.get("./"));
			// список обьектов в дирректории с глубиной дерева в 3
        Stream<Path> walk = Files.walk(Paths.get("./"), 3);

// стрим из списка из перечисляемых типов
        IntStream intStream = IntStream.of(1, 2, 3, 4);
        DoubleStream doubleStream = DoubleStream.of(1.2, 3.4);
		
		// range- диапазон (от и до)
        IntStream range = IntStream.range(10, 100); // от 10 .. до99
        IntStream intStream1 = IntStream.rangeClosed(10, 100); // 10 .. 100

        int[] ints = {1, 2, 3, 4};
        IntStream stream = Arrays.stream(ints); //создание стрима из массива скаляров

        Stream<String> stringStream = Stream.of("1", "2", "3");
		
		//если тип обьектов в стриме разный то наследуемся от сериалайзбл
        Stream<? extends Serializable> stream1 = Stream.of(1, "2", "3");

        Stream<String> build = Stream.<String>builder()
                .add("Mike")
                .add("joe")
                .build();
				
// засовываем в стрим любую коллекцию
        Stream<Employee> stream2 = emps.stream();
		//обработка стрима в несколько потоков для больших коллекций
        Stream<Employee> employeeStream = emps.parallelStream();

//генератор последовательностей (тут- событие с  ID, дата, пустое описание)
        Stream<Event> generate = Stream.generate(() ->
                new Event(UUID.randomUUID(), LocalDateTime.now(), "")
        );

// список лет с 1950 и через каждые 3 года 1950,1953,1956,1959
        Stream<Integer> iterate = Stream.iterate(1950, val -> val + 3);

//сложить 2 стрима stringStream и build последовательно каждый элемент с каждым
        Stream<String> concat = Stream.concat(stringStream, build);
    }

    @Test
    public void terminate() {
		
        Stream<Employee> stream = emps.stream();
//считаем количество элементов в стриме
        stream.count();
//берем каждого эмплоя и выводим в консоль его возраст
        emps.stream().forEach(employee -> System.out.println(employee.getAge()));
		//либо так чтоб не создавать лишний обьект стрима
        emps.forEach(employee -> System.out.println(employee.getAge()));
//выведет все возраста сортированно
        emps.stream().forEachOrdered(employee -> System.out.println(employee.getAge()));
//стрим в любую коллецию преобразуем
        emps.stream().collect(Collectors.toList());
		//или в массив
        emps.stream().toArray();
		
		//или в мэп. нужно задать что будет ключем ( Employee::getId,) а что значением
        Map<Integer, String> collect = emps.stream().collect(Collectors.toMap(
                Employee::getId,
                emp -> String.format("%s %s", emp.getLastName(), emp.getFirstName())
        ));

//сложить стрим в одно число (в лефт будет сумма в райт передается следующее число)
        IntStream intStream = IntStream.of(100, 200, 300, 400);
        intStream.reduce((left, right) -> left + right).orElse(0);

   //     System.out.println(deps.stream().reduce(this::reducer));


//подсчет статистики
        IntStream.of(100, 200, 300, 400).average();
        IntStream.of(100, 200, 300, 400).max();
        IntStream.of(100, 200, 300, 400).min();
        IntStream.of(100, 200, 300, 400).sum();
//вся статистика одной строчкой
        IntStream.of(100, 200, 300, 400).summaryStatistics();

//находим максимальный возраст сотрудников
        emps.stream().max(Comparator.comparingInt(Employee::getAge));
//получить первый элемент из стрима
        emps.stream().findAny();
        emps.stream().findFirst(); //для параллельных стримов

//НЕ ОДИН сотрудник не иметт возраст больше 60 лет
        emps.stream().noneMatch(employee -> employee.getAge() > 60); // true
//ХОТЯБЫ ОДИН из этого списка  соответствует условию()		
        emps.stream().anyMatch(employee -> employee.getPosition() == Position.CHEF); // true
//ВСЕ сотрудники старше 18		
        emps.stream().allMatch(employee -> employee.getAge() > 18); // true
    }

    @Test
    public void transform() {  //все операции трансформации ленивые, отрабатывают только при вызове терминального метода.
		//стринг примитивов в стринг обьектов
        LongStream longStream = IntStream.of(100, 200, 300, 400).mapToLong(Long::valueOf);
        IntStream.of(100, 200, 300, 400).mapToObj(value ->
                new Event(UUID.randomUUID(), LocalDateTime.of(value, 12, 1, 12, 0), "")
        );

//исключение дубликатов
        IntStream.of(100, 200, 300, 400, 100, 200).distinct(); // 100, 200, 300, 400
//получить стрим всех сотрудников кто НЕ является шефом
        Stream<Employee> employeeStream = emps.stream().filter(employee -> employee.getPosition() != Position.CHEF);

        emps.stream()
                .skip(3) //пропустить N элементов с начала стрима
                .limit(5); // первые 5 записий стрима


        emps.stream()
                .sorted(Comparator.comparingInt(Employee::getAge))  //сортируем по возрасту
                .peek(emp -> emp.setAge(18))  //устанавливаем всем сотрудникам возраст 18
                .map(emp -> String.format("%s %s", emp.getLastName(), emp.getFirstName()));

// takeWhile работает пока не выполнится условие в скобках. Берем сотрудников пока есть те чей возраст больше 30
//отсекает все элементы которые идут после первого элемента несоответствующего условию
        emps.stream().takeWhile(employee -> employee.getAge() > 30).forEach(System.out::println);
        System.out.println();
		//наоборот. отбрасывает элементы пока они не начнут соответствовать условию и выводит все что осталось
        emps.stream().dropWhile(employee -> employee.getAge() > 30).forEach(System.out::println);

        System.out.println();

//дан стрим а мы хотим поменять шаг элементов и выводить через 50 а не через 100
        IntStream.of(100, 200, 300, 400)
                .flatMap(value -> IntStream.of(value - 50, value))
                .forEach(System.out::println);
    } //выдаст 50 100 150 200 250....

    @Test
    public void real() {
		
		//получаем список сотрудников возраст которых меньше или = 30 и при этом они не worker
        Stream<Employee> empl = emps.stream()
                .filter(employee ->
                        employee.getAge() <= 30 && employee.getPosition() != Position.WORKER
                )
                .sorted(Comparator.comparing(Employee::getLastName));

        print(empl);

//старше 40ка первые четверо отсортированные по возрасту
        Stream<Employee> sorted = emps.stream()
                .filter(employee -> employee.getAge() > 40)
                .sorted((o1, o2) -> o2.getAge() - o1.getAge())
                .limit(4);

        print(sorted);

//берем сотрудников всех и считаем по их возрастам  статистику
        IntSummaryStatistics statistics = emps.stream()
                .mapToInt(Employee::getAge)
                .summaryStatistics();

        System.out.println(statistics);
    } //выдаст макс мин и средний возраст сотрудников а также их суммарный возраст

    private void print(Stream<Employee> stream) {
        stream
                .map(emp -> String.format(
                        "%4d | %-15s %-10s age %s %s",
                        emp.getId(),
                        emp.getLastName(),
                        emp.getFirstName(),
                        emp.getAge(),
                        emp.getPosition()
                ))
                .forEach(System.out::println);

        System.out.println();
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Department reducer(Department parent, Department child) {
        if (child.getParent() == parent.getId()) {
            parent.getChild().add(child);
        } else {
            parent.getChild().forEach(subParent -> reducer(subParent, child));
        }

        return parent;
    }
}

https://www.youtube.com/watch?v=RzEiCguFZiY