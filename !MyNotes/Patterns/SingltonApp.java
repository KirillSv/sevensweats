
public class SingltonApp {

	public static void main(String[] args) {

		for (int i = 0; i < 10; i++) {
			Singl singl1 = Singl.getSingl();
			System.out.println(singl1);
		}
	}
}

class Singl {
	public static Singl singl;

	private Singl() {
	}

	public static Singl getSingl() {
		if (singl == null) {
			singl = new Singl();
		}
		return singl;
	}
}
