
public class DelegateApp {

	public static void main(String[] args) {
		Painter painter = new Painter();
		painter.setDrow(new Triangle());
		painter.drowing();
	}

}

interface Drow {
	public void drower();
}

class Triangle implements Drow {
	@Override
	public void drower() {
		System.out.println("������ �����������");

	}
}

class Sqare implements Drow {
	@Override
	public void drower() {
		System.out.println("������ �������");

	}
}

class Circle implements Drow {
	@Override
	public void drower() {
		System.out.println("������ ����");
	}
}

class Painter {
	Drow drow;

	public void setDrow(Drow o) {
		drow = o;
	}
	
	void drowing() {
		drow.drower();
	}

}