
public class AbstractFactory {
	public static void main(String[] args) {
		MakercompFactory comfactory = new CompFactory();
		ConcreteFactory f = comfactory.createComp("RU");

		Comp keyb= f.runKeyboard();
		Comp mouse=f.runMouse();
		Comp cd=f.runCd();

		keyb.run();
		mouse.run();
		cd.run();
	}

}

interface Comp {
	void run();
}

class RUMouse implements Comp {
	public void run() {
		System.out.println("������� ����");
	}
}

class RUSd implements Comp {
	public void run() {
		System.out.println("������� ��");
	}
}

class RUKeyboard implements Comp {
	public void run() {
		System.out.println("������� �����");
	}
}

class ENMouse implements Comp {
	public void run() {
		System.out.println("English mouse");
	}
}

class ENSd implements Comp {
	public void run() {
		System.out.println("EnglishCD");
	}
}

class ENKeyboard implements Comp {
	public void run() {
		System.out.println("English Keyboard");
	}
}

interface MakercompFactory {
	ConcreteFactory createComp(String lang);
}

class CompFactory implements MakercompFactory {
	@Override
	public ConcreteFactory createComp(String lang) {
		switch (lang) {
		case "EN":
			return new EnglishFactory();
		case "RU":
			return new RussianFactory();
		default:
			throw new RuntimeException("Unsupported language");
		}

	}
}

interface ConcreteFactory {

	Comp runMouse();
	Comp runKeyboard();
	Comp runCd();

};

class RussianFactory implements ConcreteFactory {

	@Override
	public Comp runMouse() {
		return new RUMouse();
		
	}

	@Override
	public Comp runKeyboard() {
		return new RUKeyboard();
		
	}

	@Override
	public Comp runCd() {
		return new RUSd();
		
	}
}

class EnglishFactory implements ConcreteFactory {

	@Override
	public Comp runMouse() {
		return new ENMouse();
		
	}

	@Override
	public Comp runKeyboard() {
		return new ENKeyboard();
		
	}

	@Override
	public Comp runCd() {
		return new ENSd();
		
	}
}
