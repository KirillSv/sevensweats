import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ObserverMeteoStation {
	public static void main(String[] args) {
		MeteoStation meteostation = new MeteoStation();
		meteostation.addObserver(new RUObserver());
		meteostation.addObserver(new ENObserver());
	//	meteostation.addObserver(new FileObserver());
		
		meteostation.setMeteoData(1, 5);
	}

}

interface Observable {
	void addObserver(Observer o);

	void removeObserver(Observer o);

	void notifyObserver();
}

interface Observer {
	void handleEvent(int t, int s);
}

class MeteoStation implements Observable {

	int temperatura;
	int speed;

	List<Observer> observ = new ArrayList<Observer>();

	void setMeteoData(int t, int s) {
		temperatura = t;
		speed = s;
		notifyObserver();
	}

	@Override
	public void addObserver(Observer o) {
		observ.add(o);

	}

	@Override
	public void removeObserver(Observer o) {
		observ.remove(o);

	}

	@Override
	public void notifyObserver() {
		for (Observer o : observ) {
			o.handleEvent(temperatura, speed);
		}

	}

}

class RUObserver implements Observer {
	@Override
	public void handleEvent(int t, int s) {
		System.out.println("����������� �� ������: " + t + " ��������: " + s);
	}

}

class ENObserver implements Observer {
	@Override
	public void handleEvent(int t, int s) {
		System.out.println("temperature overboard: " + t + " speed: " + s);

	}
}
	class FileObserver implements Observer {
		public void handleEvent(int temp, int presser) {
			File f;
			try {
				f = File.createTempFile("TempPressure", "_txt");
				PrintWriter pw = new PrintWriter(f);
				pw.print("������ ����������. ����������� = " + temp + ", �������� = " + presser + ".");
				pw.println();
				pw.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}