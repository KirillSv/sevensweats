//����������� ������ ��������������,  ���������� �����������
//����� �������� �������, 
//�� ��������� � ������� ������������, �������� ������� � ���,
//����� ������ ������ ���������, �� �����������.

public class Factory {

	public static void main(String[] args) {
		Maker maker = makerMaker("Electro"); // ��� ��� ������������ ����� ��� ����� ������
		Watch watch = maker.createWatch();
		watch.showTime();

	}

	public static Maker makerMaker(String w) {
		if (w.equals("Electro"))
			return new MakerElectronicWatch();
		else
			return new MakerRomeWatch();
	}
}

interface Watch {
	void showTime();
}

class ElectronicWatch implements Watch {
	public void showTime() {
		System.out.println("I'm Electronic watch");

	}

}

class RomeWatch implements Watch {
	public void showTime() {
		System.out.println("I'm Rome Watch");

	}

}

interface Maker {
	Watch createWatch();
}

class MakerElectronicWatch implements Maker {

	@Override
	public Watch createWatch() {
		return new ElectronicWatch();

	}

}

class MakerRomeWatch implements Maker {

	@Override
	public Watch createWatch() {
		return new RomeWatch();

	}
}
