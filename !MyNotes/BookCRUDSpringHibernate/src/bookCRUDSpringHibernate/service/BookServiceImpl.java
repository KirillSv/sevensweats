package bookCRUDSpringHibernate.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import bookCRUDSpringHibernate.dao.BookDao;
import bookCRUDSpringHibernate.model.Book;

@Service
public class BookServiceImpl implements BookService{

	private BookDao bookDao;
	

	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}
	
	@Override
	@Transactional
	public void addBook(Book book) {
		this.bookDao.addBook(book);
		
	}

	@Override
	@Transactional
	public void updateBook(Book book) {
		this.bookDao.updateBook(book);
		
	}

	@Override
	@Transactional
	public void removeBook(int id) {
		this.bookDao.removeBook(id);
		
	}

	@Override
	@Transactional
	public Book getBookById(int id) {
		return this.bookDao.getBookById(id);
	
	}

	@Override
	@Transactional
	public List<Book> listBooks() {
		return this.bookDao.listBooks();
	}


}
