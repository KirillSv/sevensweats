DROP DATABASE IF EXISTS BookManager;
CREATE DATABASE BookManager;
USE BookManager;

CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_title` varchar(225) NOT NULL,
  `book_author` varchar(225) NOT NULL,
   `book_price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ;