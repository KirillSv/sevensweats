import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//����� � ���������� 20 ���� � ������. ��������, ������� ��� ������ �� ��� ����������� � ������.
//��������� ����� ����������� � ���� ������� Map<String, Integer>, ��� ������ �������� - ���������� �����, � ������ - ����� ���, ������� ��� ������ ����� ����������� � ������.
//
//������ ���������� ������� �� �����.
//� ������ ������� (�������/��������� �����) ������ �� ���������.
//
//����������:
//+����� countWords ������ ��������� � ���������������� HashMap � ����� ��������� <String, Integer>.
//+����� countWords ������ ���������� ��������� �������.
//����� countWords ������ ��������� � ������� �����, ��������������� ���������� ������, � �������� �� ���� ������, ������������, ������� ��� ����������� �����.
//��������� ������ �������� �� ����� ���������� �������.
//+����� main ������ �������� ����� countWords.

public class MapCountWord {
	public static ArrayList<String> listWord;
    public static final int COUNT=20;
	public static void main(String[] args) {

	//	initWord();
		initWordFromConsole();
		System.out.println(countWords());
	}

	private static ArrayList<String> initWordFromConsole() {
		listWord = new ArrayList<String>();
		try (Scanner sc = new Scanner(System.in)) {
			for (int i=0; i< COUNT; i++) {
				listWord.add(sc.nextLine());
			}
		}
		return listWord;
		
	}

	public static ArrayList<String> initWord() {
		listWord = new ArrayList<String>();
		listWord.add("som");
		listWord.add("som");
		listWord.add("wer");
		listWord.add("ert");
		listWord.add("list");
		listWord.add("list");

		return listWord;
	}

	private static Map<String, Integer> countWords() {

		Map<String, Integer> mapa = new HashMap<String, Integer>();

		for (String word : listWord) {
			if (!mapa.containsKey(word))
				mapa.put(word, 1);
			else
				mapa.put(word, mapa.get(word) + 1);
		}

		return mapa;

	}
}
