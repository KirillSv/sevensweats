package ru.shvec.springcourse;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

// sozdaem analog vot etogo
//<context:component-scan base-package="ru.shvec.springcourse"/>
//<context:property-placeholder location="classpath:musicPlayer.properties"/>    

@Configuration
@PropertySource("classpath:musicPlayer.properties")
public class SpringConfig {
    @Bean
    public ClassicalMusic classicalMusic() {
        return new ClassicalMusic();
    }

    @Bean
    public RockMusic rockMusic() {
        return new RockMusic();
    }
    
    @Bean
    public RapMusic rapMusic() {
        return new RapMusic();
    }
    
    @Bean
    public List<Music> musicList() {
        // ���� ���� ������������ (immutable)
        return Arrays.asList(classicalMusic(), rockMusic(), rapMusic());
    }
    
    @Bean
    public MusicPlayer musicPlayer() {
        return new MusicPlayer(musicList());
    }

    @Bean
    public Computer computer() {
        return new Computer(musicPlayer());
    }
}