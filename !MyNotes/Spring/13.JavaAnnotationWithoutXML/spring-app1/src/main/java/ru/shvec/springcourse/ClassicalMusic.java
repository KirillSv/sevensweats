package ru.shvec.springcourse;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class ClassicalMusic implements Music {

    @PostConstruct
    public void doMyInit() {
        System.out.println("Doing my initialization");
    }

    // ��� Prototype ����� �� ���������� destroy-�����!
    @PreDestroy
    public void doMyDestroy() {
        System.out.println("Doing my destruction");
    }

	public String getSong() {
		 return "Hungarian Rhapsody";
	}

 
}