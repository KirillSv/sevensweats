package ru.shvec.springcourse;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class ClassicalMusic implements Music {

	private List<String> classicList = new ArrayList<String>();;

	{
		classicList.add("1.classicSong");
		classicList.add("2.classicSong");
		classicList.add("3.classicSong");

	}

	public List<String> getSong() {

		return classicList;
	}

}
