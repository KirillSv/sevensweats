package ru.shvec.springcourse;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class RockMusic implements Music {

	private List<String> rockList = new ArrayList<String>();;

	{
		rockList.add("1.rockSong");
		rockList.add("2.rockSong");
		rockList.add("3.rockSong");

	}

	public List<String> getSong() {
		return rockList;
	}

}
