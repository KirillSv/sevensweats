package ru.shvec.springcourse;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class MusicPlayer {
	private ClassicalMusic classicalMusic;
	private RockMusic rockMusic;

	@Autowired
	public MusicPlayer(ClassicalMusic classicalMusic, RockMusic rockMusic) {
		this.classicalMusic = classicalMusic;
		this.rockMusic = rockMusic;
	}

	public void playMusic(EnumClass genre) {
		Random random = new Random();
		int randomNumber = random.nextInt(3);

		if (genre == EnumClass.Classical) {
			System.out.println(classicalMusic.getSong().get(randomNumber));
		} else {
			System.out.println(rockMusic.getSong().get(randomNumber));
		}
	}
}