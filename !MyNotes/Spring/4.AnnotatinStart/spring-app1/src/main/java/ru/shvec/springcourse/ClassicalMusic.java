package ru.shvec.springcourse;

import org.springframework.stereotype.Component;

@Component
public class ClassicalMusic implements Music {

	public void doMyInit() {
		System.out.println("Doing my initialization");
	}
	
	public void doMyDestroy() {
		System.out.println("Doing my distraction");
	}
	
	
	
	public String getSong() {
		return "Hungarian Rapsody - Classical Music";
	}

	
	
	
	
}
