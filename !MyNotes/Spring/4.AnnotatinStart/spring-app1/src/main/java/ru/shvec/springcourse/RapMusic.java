package ru.shvec.springcourse;

import org.springframework.stereotype.Component;

@Component
public class RapMusic implements Music {

	public String getSong() {

		return "Kasta - Rap music";
	}

}
