package ru.shvec.springcourse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		Music music = context.getBean("musicBean", Music.class);
		MusicPlayer musicPlayer = new MusicPlayer(music);
	
		Music music2 = context.getBean("classicalMusic", Music.class);
		MusicPlayer musicPlayer2 = new MusicPlayer(music2);
			
		Music music3 = context.getBean("rapMusic", Music.class);
		MusicPlayer musicPlayer3 = new MusicPlayer(music3);
		
		
		musicPlayer.playMusic();
		musicPlayer2.playMusic();
		musicPlayer3.playMusic();
		context.close();
	}

}
