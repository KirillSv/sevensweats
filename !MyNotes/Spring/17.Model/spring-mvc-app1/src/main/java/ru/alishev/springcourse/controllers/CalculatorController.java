package ru.alishev.springcourse.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CalculatorController {

	@GetMapping("/calculator")
	public String calc(@RequestParam(value = "firstNumber", required = false) int firstNumber,
			@RequestParam(value = "secondNumber", required = false) int secondNumber,
			@RequestParam(value = "operator", required = false) String operator, Model model) {
		double result;

		switch (operator) {
		case "+":
			result = firstNumber + secondNumber;
			break;
		case "-":
			result =firstNumber - secondNumber;
			break;
		case "*":
			result = firstNumber * secondNumber;
			break;
		case "/":
			result = firstNumber / (double)secondNumber;
			break;
		default:
			result = 0;
		}

		model.addAttribute("result", result);
		return "/result";

	}
}
