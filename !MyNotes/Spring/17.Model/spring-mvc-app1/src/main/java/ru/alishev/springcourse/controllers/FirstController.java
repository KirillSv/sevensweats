package ru.alishev.springcourse.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
//@RequestMapping("/first")   //dobavit etot prefics pered adresom cogda pereidet po zaprosu
public class FirstController {

	@GetMapping("/hello")
	public String helloPage() {
		return "first/hello";
	}
	
	@GetMapping("/goodbye")
	public String goodBuePage() {
		return "first/goodbye";
	}
	
	
}
