package ru.alishev.springcourse.config;

//analog web.xml
//
//<display-name>spring-mvc-app1</display-name>
//
//<absolute-ordering/>
//
//<servlet>
//	<servlet-name>dispatcher</servlet-name>
//	<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
//	<init-param>
//		<param-name>contextConfigLocation</param-name>
//		<param-value>/WEB-INF/applicationContextMVC.xml</param-value>
//	</init-param>
//	<load-on-startup>1</load-on-startup>
//</servlet>
//
//<servlet-mapping>
//	<servlet-name>dispatcher</servlet-name>
//	<url-pattern>/</url-pattern>
//</servlet-mapping>
//
//</web-app>

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MySpringMvcDispatcherServletInitializer  extends AbstractAnnotationConfigDispatcherServletInitializer{

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {  //gde nahoditsya javaConfig
		return new Class[] {SpringConfig.class};
	}

	
	
	
	
	
	@Override
	protected String[] getServletMappings() {
		return new String[] {"/"};
	}
	//<servlet-mapping>
//	<servlet-name>dispatcher</servlet-name>
//	<url-pattern>/</url-pattern>
//</servlet-mapping>
//

}
