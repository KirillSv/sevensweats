package ru.shvec.springcourse;

import java.util.ArrayList;
import java.util.List;

public class MusicPlayer {
	private List<Music> musicList = new ArrayList<Music>();

	private String name;
	private int volume;

	// Ioc
//	public MusicPlayer(Music music) {
//		this.music = music;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public MusicPlayer() {

	}

	public void playMusic() {

		for (Music a : musicList) {
			System.out.println("Playing: " + a.getSong());
		}
	}

	public List<Music> getMusicList() {
		return musicList;
	}

	public void setMusicList(List<Music> musicList) {
		this.musicList = musicList;
	}

}