package a2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class a2 {
// ввести число, занести его цифры в стек, вывести число у которого цифры идут в обратном порядке
	public static void main(String[] args) {

	Scanner	sc = new Scanner(System.in);
	System.out.println("Enter a long number: ");
	int number = sc.nextInt();
	
	
	List<Integer> nums = new ArrayList<>();
	int count = 0;
	while (number != 0) {
	  nums.add(number % 10);
	  number /= 10;
	  count += 1;
	}
	
	for (int i = 0; i<count;i++) {
		System.out.print(nums.get(i));
	} 
}
}