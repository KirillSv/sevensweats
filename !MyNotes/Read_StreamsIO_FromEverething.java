import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;

public class Read_StreamsIO_FromEverething{

    public static void main(String[] args) throws IOException {
        InputStream inFile = new FileInputStream("c:/wasya");
        readFullyByByte(inFile);
        System.out.print("\n\n\n");

        InputStream inUrl = new URL("http://googl.com").openStream();
        readFullyByByte(inUrl);
        System.out.print("\n\n\n");

        InputStream inArray = new ByteArrayInputStream(new byte[] { 25, 56, 58, 45, 78 });
        readFullyByByte(inArray);
        System.out.print("\n\n\n");

    }

    public static void readFullyByByte(InputStream in) throws IOException {
        while (true) {
            int oneByte = in.read();
            if (oneByte != -1) {
                System.out.println((char) oneByte);
            } else {
                System.out.println("\n" + "end");
                break;
            }
        }
    }


// second variant with privat Exception

    public static void main2(String[] args) throws IOException {
        String filename = "/tmp";

        InputStream inFile = null;
        try {
            inFile = new FileInputStream(filename);
            readFullyByByte(inFile);
        } catch (IOException e) {
            throw new IOException("Exception when open and read file" + filename);
        } finally {
            if(inFile !=null){
               try {
                   inFile.close();
                   
               }catch (IOException ignore) {
                   // lovim and  ignor exception closed
               }
                
            }
        }
    }

    
    
    
    // read and print odnovremenno
    
    public static void read1FullyByte(InputStream in) throws IOException{
        int oneByte;
        while ((oneByte= in.read()) != -1) {
            System.out.print((char) oneByte);
        }
    }
    

//read nuznoe kol-vo bait

public static void readFullBufferArray2(InputStream in) throws IOException {
    byte[] buff = new byte[5];
    while(true){
    int count =in.read(buff);
    if (count != -1) {
        System.out.println("count =" + count
            + ", buff = " + Arrays.toString(buff)
            + ", str = " + new String(buff, 0, count, "UTF -8"));
}else {
    break;
}
}    
}

// nakopit vse schitannie bytes v odnom baitovom massive

public static byte[] iwerifirfh(InputStream in) throws IOException {
int oneByte;
ByteArrayOutputStream out = new ByteArrayOutputStream();
while ((oneByte = in.read()) != -1) {
    out.write(oneByte);
}
return out.toByteArray();
}


//read and write odnowremenno

public static byte[] reqadwrite(InputStream in) throws IOException {
ByteArrayOutputStream out = new ByteArrayOutputStream();
byte[] buff = new byte[5];
int count;          
while ((count = in.read(buff)) != -1) {
    
    //cheto delaem
    
    out.write(buff, 0, count);  //write etot kusok
}
return out.toByteArray();
}
    
}


