package testtest;


//Задача 6. Используя функциональный интерфейс из задачи 5 
//написать лямбда-выражение, которое возвращает результат операции a * b^c.
//
//Chislo d = ()->(a*(int)Math. pow(b, c));

//Задача 5. Написать функциональный интерфейс с методом, который принимает три 
//дробных числа: a, b, c и возвращает тоже дробное число. Написать реализацию 
//такого интерфейса в виде лямбда-выражения, которое возвращает дискриминант.
//Кто забыл, D = b^2 — 4ac.

public class test{
	interface Chislo {
		public double discriminant();
	}
		
	public static void main(String[] args) {
	   double a = 2.3, b = 3.5, c = 5.2;
	    Chislo d = ()->(b*b - 4*a*c);
	    System.out.println(d.discriminant());
	}
	    
	
	}

//Задача 3. Написать функциональный интерфейс с методом, который принимает число
//и возвращает булево значение. Написать реализацию такого
//интерфейса в виде лямбда-выражения, которое возвращает true 
//если переданное число делится без остатка на 13.

//public class test{
//	interface Chislo {
//		public boolean chislo();
//	}
//		
//	public static void main(String[] args) {
//	    int x= 10;
//	    Chislo a = () -> (x % 13) == 0;
//	    System.out.println(a.chislo());
//	}
//	    
//	
//	}

//Задача 4. Написать функциональный
//интерфейс с методом, который принимает две строки 
//и возвращает тоже строку. Написать реализацию такого
//интерфейса в виде лямбды, которая возвращает ту строку, которая длиннее.

//public class test{
//	interface Chislo {
//		public String viborStroki();
//	}
//		
//	public static void main(String[] args) {
//	    String a = "Vasymm";
//	    String b = "Petya";
//	    Chislo c = () ->{
//	    		if(a.length() > b.length())
//	    			return a;
//	    		return b;
//	    		};
//	    System.out.println(c.viborStroki());
//	}
//	    
//	
//	}