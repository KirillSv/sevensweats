package com.hillel.sessionandfilter;

public enum ClientType {
	GUEST, USER, ADMINISTRATOR
}