package com.hillel.sessionandfilter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/controller")
public class CookieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		CookieAction.setCookie(response);
		request.setAttribute("messages", CookieAction.addToRequest(request));
		request.getRequestDispatcher("/jsp/maincookie.jsp").forward(request, response);
	}

}