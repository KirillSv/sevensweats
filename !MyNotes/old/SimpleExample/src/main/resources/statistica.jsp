<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Statistica</title>
<link href="../pays/CSS/style.css" rel="stylesheet">
<script type="text/javascript" charset="utf-8" src="../pays/CSS/script.js"></script>
</head>
<body>
	<c:if test="${not empty stat}">
		<table class="spc" border="0" cellspacing="2" cellpadding="2" width="68%">
		<tr >
		<td class="thd">Name</td>
		<td class="thd">Count</td>

		<c:forEach var="item" items="${sessionScope.stat}">
				
			<tr>
			<tr><td>${item.account}</td><td>${item.balance}</td> 
			</tr>
		
		</c:forEach>
		</table>
	</c:if>
</body>
</html>