package ua.nure.shvec.payments;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.shvec.DAO.Transactions;
import ua.nure.shvec.db.DBManager;

@WebServlet("/pays/statistica")
public class StatisticaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(GetAccounts.class); 
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LOGGER.debug("statistic servlet started");
		Connection con = DBManager.getDBCon();
       
		List <Transactions>statistica = null;
		try {
			statistica = DBManager.getStatistica(con);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession session = request.getSession();
		
		session.setAttribute("stat", statistica);
		response.sendRedirect("./statistica.jsp");
	}

}
