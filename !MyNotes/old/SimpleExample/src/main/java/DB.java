import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ua.nure.shvec.DAO.Transactions;
import ua.nure.shvec.DBConnector.ConnectionPool;

public class DB {
	
	public static List<Transactions> getStatistica(Connection con) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con= null;
  List <Transactions> transaction = new ArrayList <Transactions>(); 
		try {
			con = ConnectionPool.getInstance().getConnection();
			ps = (PreparedStatement) con.prepareStatement("select account, balance from transactions");
			rs = ps.executeQuery();
			while (rs.next()) {
				transaction.add(
						new Transactions(rs.getString("account"), rs.getDouble("balance")));
			}
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
		}
		return transaction;
	}
	

}
