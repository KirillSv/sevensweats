<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
<h3>Сейчас на вашей ферме счастливо обитают такие кролики:</h3>

    <table class="spc" border="1" cellspacing="2" cellpadding="2" width="98%">
				<tr>
					<td class="thd" onclick="sort(this)">Имя</td>
					<td class="thd" onclick="sort(this)">Пол</td>
					<td class="thd" onclick="sort(this)">Возраст (мес)</td>
					<td class="thd" onclick="sort(this)">Количество</td> 
				    <td class="thd" onclick="sort(this)">Дата рождения</td>
				</tr>
				<c:forEach var="item" items="${sessionScope.listRabbit}">
					<tr>
						<td>${item.name}</td>
						<td>${item.sex}</td>
						<td>${item.age}</td>
						<td>${item.count}</td>
						<td>${item.dateOfBirth}</td>
					</tr>
				</c:forEach>
	</table>

</body>
</html>