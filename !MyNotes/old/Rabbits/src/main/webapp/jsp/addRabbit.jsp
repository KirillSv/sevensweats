<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<title>Добавить кролика</title>
</head>
<body>
<c:import url="start.jsp"></c:import>
	<div class='addsomethingJSP'>
		<br /> <br />
		<h3>Добавление кролика в базу данных:</h3>
		<form name="form" method="post" action="/addRabbitServlet">
			<input name="name" placeholder="Имя"  size="25"><br />
		    <select name="dateOfBirthDay" required>
				<option>День</option>
				<option>01</option>
				<option>02</option>
				<option>03</option>
				<option>04</option>
				<option>05</option>
				<option>06</option>
				<option>07</option>
				<option>08</option>
				<option>09</option>
				<option>10</option>
				<option>11</option>
				<option>12</option>
				<option>13</option>
				<option>14</option>
				<option>15</option>
				<option>16</option>
				<option>17</option>
				<option>18</option>
				<option>19</option>
				<option>20</option>
				<option>21</option>
				<option>22</option>
				<option>23</option>
				<option>24</option>
				<option>25</option>
				<option>26</option>
				<option>27</option>
				<option>28</option>
				<option>29</option>
				<option>30</option>
				<option>31</option>
			</select> <select name="dateOfBirthMonth" required>
				<option>Месяц</option>
				<option>01</option>
				<option>02</option>
				<option>03</option>
				<option>04</option>
				<option>05</option>
				<option>06</option>
				<option>07</option>
				<option>08</option>
				<option>09</option>
				<option>10</option>
				<option>11</option>
				<option>12</option>
			</select> <select name="dateOfBirthYear" required>
				<option>Год</option>
				<option>2020</option>
				<option>2021</option>
				<option>2022</option>
			</select>
			<select name="count" required>
				<option>Количество</option>
				<option>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
				<option>5</option>
				<option>6</option>
				<option>7</option>
				<option>8</option>
				<option>9</option>
				<option>10</option>
				<option>11</option>
				<option>12</option>
			</select> 
			<br /> <br />
		   	<input type="radio"  name="sex" id="male" value="1" /><label for="male">Самка</label>
			 <input type="radio" name="sex" id="female" value="3" id="sex" /><label for="famale">Окрол</label> 
			 <input type="radio" name="sex" id="children" value="2" id="sex" /><label for="children">Самец</label> <br /> <br /> 
			 <input type="submit" name="Go" value="    Добавить в базу   " class="button"> <br /> <br />
		</form>
		<br> ${errorMessage}
	</div>
</body>
</html>
