<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>

<body>	
	<h3>Запланированые мероприятия: </h3>

    <table class="spc" border="1" cellspacing="2" cellpadding="2" width="98%">
				<tr>
					<td class="thd" onclick="sort(this)">Id</td>
					<td class="thd" onclick="sort(this)">Имя</td>
					<td class="thd" onclick="sort(this)">Событие</td>
					<td class="thd" onclick="sort(this)">Дата</td>
					<td class="thd" onclick="sort(this)">Осталось дней</td>
					<td class="thd">-</td>
				</tr>
				<c:forEach var="item" items="${sessionScope.listNote}">
					<tr>
						<td>${item.id}</td>
						<td>${item.rabbitName}</td>
						<td>${item.text}</td>
						<td>${item.date}</td>
						<td>${item.daysleft}</td>
						<td>
					
						<c:if test="${sessionScope.role.equals('ADMIN')}">
								<form action="/delNoteServlet" method="post">
									<button name="number" value="${item.id}">
										       Удалить
									</button>
								</form>
						</c:if>
						</td>
					</tr>
				</c:forEach>
	</table>
</body>
</html>