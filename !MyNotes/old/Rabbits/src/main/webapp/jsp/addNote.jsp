<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Добавить запись</title>
</head>
<body>
	<c:import url="start.jsp"></c:import>
	<div class='addsomethingJSP'>
		<br /> <br />
		<h3>Добавление записи в базу данных:</h3>
		<form name="Form2" method="post" action="/addNoteServlet">
			<select name="name">
				<c:forEach var="item" items="${sessionScope.listRabbit}">
					<option value="${item.name}">${item.name}</option>
				</c:forEach>
			</select>
			<br /> 
			<select name="dateOfNoteDay" required>
				<option>День</option>
				<option>01</option>
				<option>02</option>
				<option>03</option>
				<option>04</option>
				<option>05</option>
				<option>06</option>
				<option>07</option>
				<option>08</option>
				<option>09</option>
				<option>10</option>
				<option>11</option>
				<option>12</option>
				<option>13</option>
				<option>14</option>
				<option>15</option>
				<option>16</option>
				<option>17</option>
				<option>18</option>
				<option>19</option>
				<option>20</option>
				<option>21</option>
				<option>22</option>
				<option>23</option>
				<option>24</option>
				<option>25</option>
				<option>26</option>
				<option>27</option>
				<option>28</option>
				<option>29</option>
				<option>30</option>
				<option>31</option>
			</select> <select name="dateOfNoteMonth" required>
				<option>Месяц</option>
				<option>01</option>
				<option>02</option>
				<option>03</option>
				<option>04</option>
				<option>05</option>
				<option>06</option>
				<option>07</option>
				<option>08</option>
				<option>09</option>
				<option>10</option>
				<option>11</option>
				<option>12</option>
			</select> <select name="dateOfNoteYear" required>
				<option>Год</option>
				<option>2020</option>
				<option>2021</option>
				<option>2022</option>
			</select> 
			<input name="text" placeholder="Мероприятие" size="25" required><br />
			<br /> <br /> <input type="submit" name="Go"
				value="    Добавить в базу   " class="button"> <br /> <br />
		</form>
		<br /> ${errorMessage}
	</div>
</body>
</html>