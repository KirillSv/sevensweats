package classes;

import java.sql.Date;
import java.util.Calendar;

public class Note implements Comparable<Note> {
	private int id;
	private String rabbitName;
	private String text;
	private Date date;
	private int daysleft;

	public Note(int id, String rabbitName, String text, Date date) {
		this.id = id;
		this.rabbitName = rabbitName;
		this.text = text;
		this.date = date;
		this.daysleft = findDaysLeft();
	}

	public int getDaysleft() {
		return daysleft;
	}


	public void setDaysleft(int daysleft) {
		this.daysleft = daysleft;
	}

	public Note() {
	}

	public String getRabbitName() {
		return rabbitName;
	}

	public void setRabbitName(String rabbitName) {
		this.rabbitName = rabbitName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	int findDaysLeft() {
		Calendar note = Calendar.getInstance();
		note.setTime(date);
		Calendar today = Calendar.getInstance();
		int daysLeft = today.get(Calendar.DAY_OF_YEAR) - note.get(Calendar.DAY_OF_YEAR);
		if ((note.get(Calendar.YEAR) - today.get(Calendar.YEAR)) > 0) {
			daysLeft -= 180;
		}
		return -daysLeft;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + id;
		result = prime * result + ((rabbitName == null) ? 0 : rabbitName.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Note other = (Note) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id != other.id)
			return false;
		if (rabbitName == null) {
			if (other.rabbitName != null)
				return false;
		} else if (!rabbitName.equals(other.rabbitName))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + ")  '" + rabbitName + "' " + text + " data: " + date + " left  " + daysleft + " days";
	}

	public int compareTo(Note note) {
		return (findDaysLeft() - note.findDaysLeft());

	}

}