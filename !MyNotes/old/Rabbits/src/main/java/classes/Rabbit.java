package classes;

import java.sql.Date;
import java.util.Calendar;

public class Rabbit {
	private int id;
	private String name;
	private String sex;
	private Date dateOfBirth;
	private int count;
	private int age;
	private String personDiary;

	public Rabbit(int id, String name, String sex, Date dateOfBirth, int count, String personDiary) {
		this.name = name;
		this.id = id;
		this.sex = sex;
		this.dateOfBirth = dateOfBirth;
		this.count = count;
		this.personDiary = personDiary;
		age = calculateRabbitAge(dateOfBirth);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPersonDiary() {
		return personDiary;
	}

	public void setPersonDiary(String personDiary) {
		this.personDiary = personDiary;
	}

	private int calculateRabbitAge(Date birthDate) {
		Calendar birth = Calendar.getInstance();
		birth.setTime(birthDate);
		Calendar today = Calendar.getInstance();
		age = today.get(Calendar.MONTH) - birth.get(Calendar.MONTH);
		int ageYear = today.get(Calendar.YEAR) - birth.get(Calendar.YEAR);
		if (ageYear > 0) {
			age = age + ageYear * 12;
		}
		return age;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + count;
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((personDiary == null) ? 0 : personDiary.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rabbit other = (Rabbit) obj;
		if (age != other.age)
			return false;
		if (count != other.count)
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (personDiary == null) {
			if (other.personDiary != null)
				return false;
		} else if (!personDiary.equals(other.personDiary))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + ")  '" + name + "'  " + sex + "     age: " + age + " month     " + count + "  count";
	}

}