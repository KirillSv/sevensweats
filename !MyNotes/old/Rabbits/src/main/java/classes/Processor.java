package classes;

import classes.Rabbit;
import java.util.ArrayList;

import DBworker.DBmetods;

public class Processor {

	public int[] findSumRabbit(ArrayList<Rabbit> rabbitList) {// result [count old rabbit , count little rabbit]
		int[] sum = { 0, 0 };
		for (Rabbit rabbit : rabbitList) {

			if (rabbit.getAge() > 4) {
				sum[0] += rabbit.getCount();
			} else {
				sum[1] += rabbit.getCount();
			}
		}
		return sum;
	}

	public double findSumFood(ArrayList<Rabbit> rabbitList) {
		double sum = 0;
		for (Rabbit rabbit : rabbitList) {
			if (rabbit.getAge() > 4) {
				sum += (5 * rabbit.getCount());
			}
			if (rabbit.getAge() > 1 | rabbit.getAge() < 4) {
				sum += (2 * rabbit.getCount());
			}

		}
		sum = sum * 0.1;
		return Math.round(sum * 100d) / 100d;
	}

	public double findSumAddMeat(ArrayList<Rabbit> rabbitList) {
		double sum = 0;
		for (Rabbit rabbit : rabbitList) {
			if (rabbit.getAge() < 4) {
				sum += (rabbit.getAge() * rabbit.getCount());
			}
		}
		sum = (sum * 0.4) / 30;
		return Math.round(sum * 100d) / 100d;
	}

	public String readPersonDiary(String nameRabbit) {
		DBmetods db = new DBmetods();
		ArrayList<Rabbit> listRabbit = db.readRabbitFromBase();
		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getName().equals(nameRabbit)) {
				return rabbit.getPersonDiary();
			}
		}
		return "Rabbit with name " + nameRabbit + " is absent in our base.";
	}

	public boolean checkValidName(String name) {
		DBmetods db = new DBmetods();
		boolean checkName = false;
		ArrayList<Rabbit> listRabbit = db.readRabbitFromBase();
		for (Rabbit rabbit : listRabbit) {
			if (rabbit.getName().equals(name)) {
				checkName = true;
				break;
			}
		}
		return checkName;
	}

}
