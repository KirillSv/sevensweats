package DBworker;

import DBworker.ConnectorDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import accessOrganization.User;
import classes.Note;
import classes.Processor;
import classes.Rabbit;
import edu.emory.mathcs.backport.java.util.Collections;

public class DBmetods {

	private Processor processor = new Processor();

	public User loginChecker(String username, String password) {
		User user = null;

		String request = "Select * from rabbitbase.access_table where login='" + username + "' and password='"
				+ password + "'";
		Connection connect = null;
		ResultSet rs = null;
		PreparedStatement preparedStatement = null;

		try {
			connect = ConnectorDB.getConnection();
			preparedStatement = connect.prepareStatement(request);
			rs = preparedStatement.executeQuery();

			while (rs.next()) {
				user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		close(rs, connect, preparedStatement);
		if (user == null) {
			user = new User(0, "0", "0", "GUEST");
		}
		return user;
	}

	public String addMessageInPersonDiary(String nameRabbit, String newMessage) {
		String oldMessage = processor.readPersonDiary(nameRabbit);
		String request = "UPDATE rabbit_table set person_diary='" + (oldMessage + "#" + newMessage) + "'WHERE  name='"
				+ nameRabbit + "'";
		return simpleReqest(request);
	}

	public String deleteRabbit(String nameRabbit) {
		boolean checkName = processor.checkValidName(nameRabbit);
		if (!checkName) {
			return "Rabbit with name " + nameRabbit + " is absent in our database.";
		}

		String request = "DELETE FROM rabbitbase.rabbit_table WHERE name='" + nameRabbit + "'";
		return simpleReqest(request);
	}

	public String deleteNote(String numberNote) {
		try {
			int numberNoteInt = Integer.valueOf(numberNote);
			boolean checkName = false;
			ArrayList<Note> listNote = readNoteFromBase();
			for (Note note : listNote) {
				if ((note.getId() == numberNoteInt)) {
					checkName = true;
					break;
				}
			}

			if (!checkName) {
				return "Note with number " + numberNoteInt + " is absent in database";
			}

		} catch (Exception e) {
			return "Enter number note. Not it -" + numberNote;

		}

		String request = "DELETE FROM rabbitbase.note_table WHERE id=" + numberNote;
		return simpleReqest(request);
	}

	
	public String addRabbit(String name, String sex, String dateOfBirth, int count) {
		boolean checkName = processor.checkValidName(name);
		if (checkName) {
			return "Rabbit with thise name has already in base. Change name";
		}
		int sexInt = Integer.parseInt(sex);
		String request = "insert rabbitbase.rabbit_table (name, sex, date_of_birth, count, person_diary) VALUE ('"
				+ name + "'," + sexInt + "," + dateOfBirth + "," + count + ",'0')";
		Connection connect = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try {
			connect = ConnectorDB.getConnection();
			preparedStatement = connect.prepareStatement(request);
			preparedStatement.executeUpdate(request);
		} catch (SQLException error) {
			error.printStackTrace();
			close(rs, connect, preparedStatement);
			return "Error when was  record from base SQL";
		}
		close(rs, connect, preparedStatement);
		return "Rabbit succesfull added to base";
	}

	public String addNote(String name, String text, String dateOfNote) {
		boolean checkName = processor.checkValidName(name);
		if (!checkName) {
			return "Rabbit whis this name is absent in base.";
		}
		String request = "insert rabbitbase.note_table (name, text, date_note) VALUE ((Select id from rabbit_table where name='"
				+ name + "'),'" + text + "'," + dateOfNote + ")";
		return simpleReqest(request);
	}

	public ArrayList<Rabbit> readRabbitFromBase() {
		ArrayList<Rabbit> listRabbit = new ArrayList<Rabbit>();
		try {
			String request = "SELECT id, name, sex, date_of_birth, count, person_diary FROM rabbitbase.rabbit_table";
			Connection connect = null;
			ResultSet rs = null;
			PreparedStatement preparedStatement = null;
			try {
				connect = ConnectorDB.getConnection();
				preparedStatement = connect.prepareStatement(request);
				rs = preparedStatement.executeQuery();

				while (rs.next()) {
					String sex;
					int intSex = Integer.valueOf(rs.getString(3));
					switch (intSex) {
					case (1):
						sex = "Самка";
						break;
					case (3):
						sex = "Выводок";
						break;
					default:
						sex = "Самец";
					}
					listRabbit.add(new Rabbit(rs.getInt(1), rs.getString(2), sex, rs.getDate(4), rs.getInt(5),
							rs.getString(6)));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			close(rs, connect, preparedStatement);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		return listRabbit;
	}

	public ArrayList<Note> readNoteFromBase() {
		ArrayList<Note> listNote = new ArrayList<Note>();
		try {
			String request = "select n.id, r.name, text, date_note from note_table n JOIN rabbit_table r on n.name=r.id";
			Connection connect = null;
			ResultSet rs = null;
			PreparedStatement preparedStatement = null;
			try {
				connect = ConnectorDB.getConnection();
				preparedStatement = connect.prepareStatement(request);
				rs = preparedStatement.executeQuery();

				while (rs.next()) {
					listNote.add(new Note(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4)));
				}

			} catch (SQLException e) {
				e.printStackTrace();
			}

			close(rs, connect, preparedStatement);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		Collections.sort(listNote);
		return listNote;
	}

	private String simpleReqest(String request) {
		Connection connect = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		try {
			connect = ConnectorDB.getConnection();
			preparedStatement = connect.prepareStatement(request);
			preparedStatement.executeUpdate(request);
		} catch (SQLException error) {
			error.printStackTrace();
			close(rs, connect, preparedStatement);
			return "Error connection SQL";

		}
		close(rs, connect, preparedStatement);
		return "Everything OK";
	}

	private void close(ResultSet rs, Connection cn, PreparedStatement preparedStatement) {
		closeResultSet(rs);
		closeConnection(cn);
		closePreparedStatement(preparedStatement);
	}

	private void closeResultSet(ResultSet rs) {
		if (rs == null) {
			return;
		}

		try {
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void closePreparedStatement(PreparedStatement preparedStatement) {
		if (preparedStatement == null) {
			return;
		}

		try {
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void closeConnection(Connection cn) {
		if (cn == null) {
			return;
		}
	}

}
