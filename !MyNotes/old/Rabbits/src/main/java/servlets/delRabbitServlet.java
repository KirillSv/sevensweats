package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DBworker.DBmetods;

@WebServlet("/delRabbitServlet")
public class delRabbitServlet extends HttpServlet {
	private static final long serialVersionUID = 6L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/delRabbit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (!(session.getAttribute("role").equals("ADMIN"))) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

		}
		DBmetods dBmetods = new DBmetods();
		String name = request.getParameter("name");
		String errorMessage = dBmetods.deleteRabbit(name);
		request.setAttribute("errorMessage", errorMessage);

		response.sendRedirect("jsp/delRabbit.jsp");
	}

}
