package servlets;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DBworker.DBmetods;
import classes.Note;
import classes.Processor;
import classes.Rabbit;

@WebServlet("/startServlet")
public class startServlet extends HttpServlet {
	private static final long serialVersionUID = 3L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		DBmetods dBmetods = new DBmetods();
		ArrayList<Rabbit> rabbitList = dBmetods.readRabbitFromBase();
		ArrayList<Note> noteList = dBmetods.readNoteFromBase();

		request.setAttribute("listRabbit", rabbitList);
		request.setAttribute("listNote", noteList);

		Processor processor = new Processor();
		int[] sumRabbit = processor.findSumRabbit(rabbitList);
		double sumFood = processor.findSumFood(rabbitList);
		double sumAddMeat = processor.findSumAddMeat(rabbitList);

		HttpSession session = request.getSession();
		
		session.setAttribute("role", "ADMIN");   //убрать
		
		
		session.setAttribute("listRabbit", rabbitList);
		session.setAttribute("listNote", noteList);
		session.setAttribute("sumAddMeat", sumAddMeat);
		session.setAttribute("sumFood", sumFood);
		session.setAttribute("sumOldRabbit", sumRabbit[0]);
		session.setAttribute("sumLitleRabbit", sumRabbit[1]);

		response.sendRedirect("jsp/start.jsp");
	}

}
