package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DBworker.DBmetods;

@WebServlet("/delNoteServlet")
public class delNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 5L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DBmetods dBmetods = new DBmetods();
		String errorMessage = dBmetods.deleteNote(request.getParameter("number"));
		request.setAttribute("errorMessage", errorMessage);
		request.getRequestDispatcher("/loginServlet").forward(request, response);
	}

}
