package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DBworker.DBmetods;
import accessOrganization.User;

@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 9L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		DBmetods dbmetods = new DBmetods();
		HttpSession session = request.getSession();

		String username = request.getParameter("username");
		String password = request.getParameter("password");

	//	User user = dbmetods.loginChecker(username, password);

	//	session.setAttribute("role", user.getRole());
	

		request.getRequestDispatcher("/startServlet").forward(request, response);
	}
}
