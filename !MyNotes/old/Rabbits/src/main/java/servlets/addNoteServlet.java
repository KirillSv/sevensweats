package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DBworker.DBmetods;

@WebServlet("/addNoteServlet")
public class addNoteServlet extends HttpServlet {
	private static final long serialVersionUID = 2L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("jsp/addNote.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("GUEST")) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);
		}
		String name = request.getParameter("name");
		String text = request.getParameter("text");

		DBmetods DBmetods = new DBmetods();

		String dateOfNoteMonth = request.getParameter("dateOfNoteMonth");
		String dateOfNoteDay = request.getParameter("dateOfNoteDay");

		String dateOfNote = request.getParameter("dateOfNoteYear") + dateOfNoteMonth + dateOfNoteDay;

		String errorMessage = DBmetods.addNote(name, text, dateOfNote);
		request.setAttribute("errorMessage", errorMessage);

		response.sendRedirect("jsp/addNote.jsp");
	}

}
