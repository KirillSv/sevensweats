package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DBworker.DBmetods;

@WebServlet("/addRabbitServlet")
public class addRabbitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/addRabbit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("GUEST")) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

		}
		DBmetods DBmetods = new DBmetods();
		String name = request.getParameter("name");
		String sex = request.getParameter("sex");
		int count = Integer.valueOf(request.getParameter("count"));
		String dateOfBirthMonth = request.getParameter("dateOfBirthMonth");
		String dateOfBirthDay = request.getParameter("dateOfBirthDay");

		String dateOfBirth = request.getParameter("dateOfBirthYear") + dateOfBirthMonth + dateOfBirthDay;

		String errorMessage = DBmetods.addRabbit(name, sex, dateOfBirth, count);

		request.setAttribute("errorMessage", errorMessage);
		response.sendRedirect("jsp/addRabbit.jsp");
	}

}
