package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DBworker.DBmetods;
import org.apache.log4j.Logger;

@WebServlet("/addKomboServlet")
public class AddKomboServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(AddKomboServlet.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("jsp/addKombo.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("GUEST")) {
			response.sendRedirect("jsp/accessDenied.jsp");
		}
		DBmetods dBmetods = new DBmetods();
		String name = request.getParameter("name");
		String sex = "3";
		int count = Integer.valueOf(request.getParameter("count"));
		String dateOfBirthMonth = request.getParameter("dateOfBirthMonth");
		String dateOfBirthDay = request.getParameter("dateOfBirthDay");
		String mothersName = request.getParameter("mothersName");
		String dateOfBirthYear = request.getParameter("dateOfBirthYear");

		String dateOfBirth = request.getParameter("dateOfBirthYear") + dateOfBirthMonth + dateOfBirthDay;
		LOGGER.debug(name + " " + sex + " " + dateOfBirth + " " + count);

		dBmetods.addRabbit(name, sex, dateOfBirth, count);

		String dateOfNoteZaboi = formCorrectDataFromZaboi(dateOfBirthMonth, dateOfBirthYear) + dateOfBirthDay;
		dBmetods.addNote(name, "забой", dateOfNoteZaboi);

		String dateOfNoteVyazka = formCorrectDataFromVyazka(dateOfBirthMonth, dateOfBirthYear) + dateOfBirthDay;
		dBmetods.addNote(mothersName, "случка", dateOfNoteVyazka);

		// запись в дневник самки
		dBmetods.addMessageInPersonDiary(mothersName,
				dateOfBirth + "родила" +name+" "+ count + "штук, вязка по плану:  " + dateOfNoteVyazka);

		response.sendRedirect("jsp/addKombo.jsp");
	}

	private String formCorrectDataFromZaboi(String dateOfBirthMonth, String dateOfNoteYear) {
		int month = Integer.parseInt(dateOfBirthMonth);
		int year = Integer.parseInt(dateOfNoteYear);
		month = month + 4;
		if (month > 12) {
			month = month - 12;
			year = year + 1;
			return "" + year + formCorrectMonth(month);
		}
		String temp = "" + year + formCorrectMonth(month);
		return temp;
	}

	private String formCorrectDataFromVyazka(String dateOfBirthMonth, String dateOfNoteYear) {
		int month = Integer.parseInt(dateOfBirthMonth);
		int year = Integer.parseInt(dateOfNoteYear);
		String temp = null;
		month++;
		if (month > 12) {
			month = month - 12;
			year = year + 1;
			temp = "" + year + formCorrectMonth(month);
			return temp;
		}
		temp = "" + year + formCorrectMonth(month);
		return temp;
	}

	private String formCorrectMonth(int month) {
		String correctMonth = null;
		if (month < 10) {
			correctMonth = "0" + month;
		}
		return correctMonth;
	}
}
