package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import classes.Processor;
@WebServlet("/personDiaryServlet")
public class personDiaryServlet extends HttpServlet {
	private static final long serialVersionUID = 8L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("jsp/personDiary.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("GUEST")) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

		}
		String name = request.getParameter("name");
		Processor processor = new Processor();
		String listDataRabbit = processor.readPersonDiary(name);
		request.setAttribute("rabbitName", name);

		String[] listDataRabbitArr = listDataRabbit.split("#");
		session.setAttribute("name", name);
		session.setAttribute("listDataRabbitArr", name);

		request.setAttribute("listDataRabbit", listDataRabbitArr);
		request.getRequestDispatcher("jsp/personDiary.jsp").forward(request, response);
	}

}
