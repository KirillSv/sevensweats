package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DBworker.DBmetods;
import classes.Processor;

@WebServlet("/addPersonDiaryServlet")
public class addPersonDiaryServlet extends HttpServlet {
	private static final long serialVersionUID = 7L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("role").equals("GUEST")) {
			request.getRequestDispatcher("jsp/accessDenied.jsp").forward(request, response);

		}
		String newMessage = request.getParameter("text");
		DBmetods db = new DBmetods();
		String name = (String) session.getAttribute("name");
		db.addMessageInPersonDiary(name, newMessage);
		Processor processor= new Processor();
		String listDataRabbit = processor.readPersonDiary(name);
		String[] listDataRabbitArr = listDataRabbit.split("#");


		request.setAttribute("listDataRabbit", listDataRabbitArr);
		response.sendRedirect("jsp/personDiary.jsp");
	}

}
