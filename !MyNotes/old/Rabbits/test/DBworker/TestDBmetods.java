package DBworker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import org.junit.Test;
import DBworker.DBmetods;
import accessOrganization.User;
import classes.Note;
import classes.Processor;

public class TestDBmetods {

	private DBmetods db = new DBmetods();
	private Processor processor = new Processor();

	@Test
	public void testloginCheckerTest_returnNullNewUser() {
		User expectedNullUser = new User(0, "0", "0", "GUEST");
		assertEquals(db.loginChecker("any other login except admin or user", "anyPassword"), expectedNullUser);
	}

	@Test
	public void testloginCheckerTest_returnAdminNewUser() {
		User expectedAdminUser = new User(1, "admin", "admin", "ADMIN");
		assertEquals(db.loginChecker("admin", "admin"), expectedAdminUser);
	}

	@Test
	public void testAddRabbit() throws Exception {
		db.addRabbit("TestAddRabbit", "1", "20010101", 1);
		assertTrue(processor.checkValidName("TestAddRabbit"));
		db.deleteRabbit("TestAddRabbit");
	}

	@Test
	public void testDeleteRabbit() throws Exception {
		db.addRabbit("TestRabbit777", "1", "20010101", 1);
		assertTrue(processor.checkValidName("TestRabbit777"));
		db.deleteRabbit("TestRabbit777");
		assertFalse(processor.checkValidName("TestRabbit777"));
	}

	@Test
	public void testAddNote() {
		String expectedText = "textTest";
		String receivedText = "";
		db.addRabbit("TestRabbit", "1", "20010101", 1);
		db.addNote("TestRabbit", expectedText, "20191010");
		ArrayList<Note> testListNote = db.readNoteFromBase();
		for (Note note : testListNote) {
			if (note.getText().contentEquals(expectedText)) {
				receivedText = note.getText();
			}

		}
		assertEquals(receivedText, expectedText);
		db.deleteRabbit("TestRabbit");

	}

	@Test
	public void testAddMessageInPersonDiary() {
		String newMessage = "TestMessage";
		String expectedMessage = "0#" + "TestMessage" + "#" + "TestMessage";
		db.addRabbit("TestRabbit", "1", "20010101", 1);
		db.addMessageInPersonDiary("TestRabbit", newMessage);
		db.addMessageInPersonDiary("TestRabbit", newMessage); // need check double add
		assertEquals(processor.readPersonDiary("TestRabbit"), expectedMessage);
		db.deleteRabbit("TestRabbit");
	}

	@Test
	public void testDeleteNote() throws Exception {
		String testId = "";
		db.addRabbit("TestRabbit", "1", "20010101", 1);
		db.addNote("TestRabbit", "textTest", "20191010");
		ArrayList<Note> testListNote = db.readNoteFromBase();
		for (Note note : testListNote) {
			if (note.getText().contentEquals("textTest")) {
				testId = String.valueOf(note.getId());
			}
		}
		assertEquals(db.deleteNote(testId), "Everything OK");
		db.deleteRabbit("TestRabbit");

	}
}