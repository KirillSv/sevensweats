package classes;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import DBworker.DBmetods;

public class TestProcessor {

	private ArrayList<Rabbit> testRabbitList;
	private Processor processor;
	private DBmetods db = new DBmetods();

	@Before
	public void prepareListRabbitForTest() throws Exception {
		processor = new Processor();
		testRabbitList = new ArrayList<Rabbit>();
		String sDate1 = "20010101";
		String sDate2 = "20191008"; // here must be today date minus 1 months. Because rabbits up to 1 months don't
									// consume food
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Date parsed1 = format.parse(sDate1);
		Date parsed2 = format.parse(sDate2);
		java.sql.Date sql1 = new java.sql.Date(parsed1.getTime());
		java.sql.Date sql2 = new java.sql.Date(parsed2.getTime());

		testRabbitList.add(new Rabbit(1, "OneOldRabbit", 1, sql1, 1, "firstDiary"));
		testRabbitList.add(new Rabbit(2, "little", 1, sql2, 10, "SecondDiary"));
	}

	@Test
	public void testFindSumRabbit() {
		int[] expected = { 1, 10 }; // result [count old rabbit , count little rabbit]
		assertArrayEquals(processor.findSumRabbit(testRabbitList), expected);
	}

	@Test
	public void testFindSumFood() {
		double expected = 2.7;
		assertEquals(processor.findSumFood(testRabbitList), expected, 0.1);
	}

	@Test
	public void testFindSumAddMeat() {
		double expected = 0.13;
		assertEquals(processor.findSumAddMeat(testRabbitList), expected, 0.1);
	}

	@Test
	public void testReadPersonDiary() {
		db.addRabbit("TestRabbit", "1", "20191010", 1);
		db.addMessageInPersonDiary("TestRabbit", "ExpectedTestNote");
		assertEquals(processor.readPersonDiary("TestRabbit"), "0#" + "ExpectedTestNote");
		db.deleteRabbit("TestRabbit");
	}

	@Test
	public void testCheckValidName() {
		db.addRabbit("TestRabbit1", "1", "20191010", 1);
		assertTrue(processor.checkValidName("TestRabbit1"));
		assertFalse(processor.checkValidName("NoNameFantasticRabbit"));
		db.deleteRabbit("TestRabbit1");
	}
}
