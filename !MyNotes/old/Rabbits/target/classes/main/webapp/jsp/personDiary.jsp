<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Дело кролика</title>
</head>
<body>
<c:import url="start.jsp"></c:import>
<div class='addsomethingJSP'>
	<h2>Выберите кролика:</h2>
	<form name="Form2" method="post" action="/personDiaryServlet">
		<select name="name">
				<c:forEach var="item" items="${sessionScope.listRabbit}">
					<option value="${item.name}">${item.name}</option>
				</c:forEach>
		</select>
		<br />
		<br /> <br /> <input type="submit" name="Go" value=" Искать  " class="button" >
		<br /> <br />
	</form>
	<br /> ${errorMessage}


	<h3>${rabbitName}</h3>
	<c:forEach var="note" items="${listDataRabbit}">
		<br> ${note} 
    </c:forEach>

   <c:if test = "${not empty listDataRabbit}">
   <form name="Form3" method="post" action="/addPersonDiaryServlet">
		<input name="text" placeholder="Добавить запись" size="50"><br />
		<br /> <br /> <input type="submit" name="Go" value=" Добавить  " class="button" >
		<br /> <br />
	</form>
	</c:if>
	</div>
</body>
</html>