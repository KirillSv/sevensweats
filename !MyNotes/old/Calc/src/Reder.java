import javax.swing.JFrame;

public class Reder {
    public static void main(String args[]) {
        Calc r = new Calc("Best Calculator 2019");
        r.setVisible(true);
        r.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        r.setSize(200, 200);
        r.setResizable(false);
        r.setLocationRelativeTo(null);
    }
}
