import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Calc extends JFrame {
    private static final long serialVersionUID = 6232820682626499392L;
    JButton b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16;
    JLabel l1;
    long numer1, x;
    int znak = 0;
    String y = "";
    JTextField t1;

    eHandler handler = new eHandler();

    public Calc(String s) {
        super(s);
        setLayout(new FlowLayout());
        b1 = new JButton("1");
        b2 = new JButton("2");
        b3 = new JButton("3");
        b4 = new JButton("4");
        b5 = new JButton("5");
        b6 = new JButton("6");
        b7 = new JButton("7");
        b8 = new JButton("8");
        b9 = new JButton("9");
        b10 = new JButton("0");
        b11 = new JButton("/");
        b12 = new JButton("*");
        b13 = new JButton("+");
        b14 = new JButton("-");
        b15 = new JButton("=");
        b16 = new JButton("C");
        l1 = new JLabel("");
        t1 = new JTextField(15);
        add(l1);
        add(t1);
        add(b1);
        add(b2);
        add(b3);
        add(b4);
        add(b5);
        add(b6);
        add(b7);
        add(b8);
        add(b9);
        add(b10);
        add(b11);
        add(b12);
        add(b13);
        add(b14);
        add(b15);
        add(b16);
        b1.addActionListener(handler);
        b2.addActionListener(handler);
        b3.addActionListener(handler);
        b4.addActionListener(handler);
        b5.addActionListener(handler);
        b6.addActionListener(handler);
        b7.addActionListener(handler);
        b8.addActionListener(handler);
        b9.addActionListener(handler);
        b10.addActionListener(handler);
        b11.addActionListener(handler);
        b12.addActionListener(handler);
        b13.addActionListener(handler);
        b14.addActionListener(handler);
        b15.addActionListener(handler);
        b16.addActionListener(handler);

    }

    public class eHandler implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == b1) {
                x = 1;
                y = y + String.valueOf(x);
            }

            if (e.getSource() == b2) {
                x = 2;
                y = y + String.valueOf(x);

            }
            if (e.getSource() == b3) {
                x = 3;
                y = y + String.valueOf(x);

            }
            if (e.getSource() == b4) {
                x = 4;
                y = y + String.valueOf(x);

            }
            if (e.getSource() == b5) {
                x = 5;
                y = y + String.valueOf(x);

            }
            if (e.getSource() == b6) {
                x = 6;
                y = y + String.valueOf(x);

            }
            if (e.getSource() == b7) {
                x = 7;
                y = y + String.valueOf(x);

            }
            if (e.getSource() == b8) {
                x = 8;
                y = y + String.valueOf(x);

            }
            if (e.getSource() == b9) {
                x = 9;
                y = y + String.valueOf(x);

            }
            if (e.getSource() == b10) {
                x = 0;
                y = y + String.valueOf(x);

            }

            if (e.getSource() == b16) {
                x = 0;
                y = "";
            }

            if (e.getSource() == b11) {

                znak = 1;
                numer1 = Integer.parseInt(y);
                y = y + "/";
            }
            if (e.getSource() == b12) {

                znak = 2;
                numer1 = Integer.parseInt(y);
                y = y + "*";
            }
            if (e.getSource() == b13) {

                znak = 3;
                numer1 = Integer.parseInt(y);
                y = y + "+";
            }
            if (e.getSource() == b14) {
                znak = 4;
                numer1 = Integer.parseInt(y);
                y = y + "-";
            }

            if (e.getSource() == b15) {
                if (znak == 0)
                    ; // ������ ����� �� �=�
                else { // ������ ����� �� ���� ��� ����-�� �����

                    if (znak == 1)
                        x = numer1 / x;
                    if (znak == 2)
                        x = numer1 * x;
                    if (znak == 3)
                        x = numer1 + x;
                    if (znak == 4)
                        x = numer1 - x;
                    y = String.valueOf(x);
                }

            }

            t1.setText(y);
        }
    }
}
