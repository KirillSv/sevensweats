package kirill.com.parser;

import java.util.ArrayList;
import java.util.List;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class ParsWorker {

	public String oneValue(String searchUrl, String xpath) {
		String result = "error";
		WebClient client = new WebClient();
		try {
			client.getOptions().setCssEnabled(false);
			client.getOptions().setJavaScriptEnabled(false);
			HtmlPage page = client.getPage(searchUrl);
			// System.out.println(page.asXml());
			HtmlElement items = (HtmlElement) page.getFirstByXPath(xpath);
			result = items.asText();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			client.close();
		}
		return result;

	}

	public List<String> weekParsing() {
		WebClient client = new WebClient();
		client.getOptions().setCssEnabled(false);
		client.getOptions().setJavaScriptEnabled(false);
		try {
			String searchUrl = "https://obmenka.ua/";
			HtmlPage page = client.getPage(searchUrl); // html document
			@SuppressWarnings("unchecked")
			List<HtmlElement> items = (ArrayList<HtmlElement>) page.getByXPath("//span[@class='reserves-right']");
			@SuppressWarnings("unchecked")
			List<HtmlElement> names = (ArrayList<HtmlElement>) page.getByXPath("//span[@class='reserves-left']");
			if (items.isEmpty()) {
				System.out.println("No items found !");
			} else {
				for (int i = 0; i < items.size(); i++) {
					String itemPrice = items.get(i) == null ? "0.0" : items.get(i).asText();
					String itemName = names.get(i) == null ? "0.0" : names.get(i).asText();
					System.out.println(String.format(itemName + ": " + itemPrice));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			client.close();
		}
		return null;
	}

	public void arrayParsing() {
		WebClient client = new WebClient();
		client.getOptions().setCssEnabled(false);
		client.getOptions().setJavaScriptEnabled(false);
		try {
			String baseUrl = "https://newyork.craigslist.org/search/sss?sort=rel&query=iphone+6s";
			HtmlPage page = client.getPage(baseUrl);
			@SuppressWarnings("unchecked")
			List<HtmlElement> items = (List<HtmlElement>) page.getByXPath("//div[@class='result-info']");
			if (items.isEmpty()) {
				System.out.println("No items found !");
			} else {
				for (HtmlElement item : items) {

					HtmlAnchor itemAnchor = ((HtmlAnchor) item.getFirstByXPath(".//a"));
					String itemName = itemAnchor.asText();
					String itemUrl = itemAnchor.getHrefAttribute();
					HtmlElement spanPrice = ((HtmlElement) item.getFirstByXPath(".//span[@class='result-price']"));
					String itemPrice = spanPrice == null ? "no price" : spanPrice.asText();
					System.out.println(String.format("Name : %s Url : %s Price : %s", itemName, itemPrice, itemUrl));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			client.close();
		}
	}
}
