package kirill.com.parser;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		String searchUrl, xpath;
		ParsWorker  parsWorker  = new ParsWorker();
		
 		searchUrl = "https://sinoptik.ua/%D0%BF%D0%BE%D0%B3%D0%BE%D0%B4%D0%B0-%D0%BB%D0%B8%D0%BF%D1%86%D1%8B";
 		xpath = "//p[@class='today-temp']";
 		
 		String dayTemperatura = parsWorker.oneValue(searchUrl, xpath);
		System.out.println("Прямо сейчас в Липцах температура воздуха: " + dayTemperatura);

		
		searchUrl = "https://myfin.by/crypto-rates/bitcoin";
		xpath = "/html/body/div[6]/div/div/div/div[1]/div/div/div/div/div/div[4]/div/article/div[1]/div[2]/div[1]/div/div[3]/div[1]/div";
		
		String dayBit = parsWorker.oneValue(searchUrl, xpath);
		System.out.println("Котировка Bitcoin: " + dayBit);
		parsWorker.weekParsing();
		
		parsWorker.arrayParsing();	
		
	}
}
