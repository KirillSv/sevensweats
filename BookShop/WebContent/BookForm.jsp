<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Books Store Application</title>
</head>
<body>
	<c:if test="${book != null}">
		<form action="update" method="post">
	</c:if>

	<c:if test="${book == null}">
		<form action="insert" method="post">
	</c:if>

	<table border="1">

		<h3>
			<c:if test="${book != null}">
                        Edit
                    </c:if>
			<c:if test="${book == null}">
                        Add New Book
                    </c:if>
		</h3>

		<c:if test="${book != null}">
			<input type="hidden" name="id" value="<c:out value='${book.id}' />" />
		</c:if>
		<tr>
			<th>Title:</th>
			<td><input type="text" name="title" size="40"
				value="<c:out value='${book.title}' />" />
			</td>
		</tr>
		<tr>
			<th>Author:</th>
			<td><input type="text" name="author" size="40"
				value="<c:out value='${book.author}' />" /></td>
		</tr>

		<tr>
			<td><input type="submit" value="Save" /></td>
		</tr>
	</table>
	<br>
	Menu
	<br>
	<h3>
		<a href="./new">Add Book</a>
		 <br> 
		 <a href="./list">List All Books</a>
	</h3>
</body>
</html>