<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Books Shop</title>
</head>
<body>
	<table border="2" cellpadding="2">
			<h3>List all Books</h3>
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Author</th>
			<th>Action</th>
		</tr>
		<c:forEach var="book" items="${listBook}">
			<tr>
				<td><c:out value="${book.id}" /></td>
				<td><c:out value="${book.title}" /></td>
				<td><c:out value="${book.author}" /></td>
				<td><a href="./edit?id=<c:out value='${book.id}' />">Edit</a> ;
					<a href="./delete?id=<c:out value='${book.id}' />">Remove</a></td>
			</tr>
		</c:forEach>
	</table>
<br><br>
Menu
<br>
	<a href="./new"> Add Books</a>
<br>
	<a href="./list"> Show All Books</a>
</body>
</html>