DROP DATABASE IF EXISTS Bookshop;
CREATE DATABASE Bookshop;
USE Bookshop;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `author` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ;

INSERT book values(1, "Golden key", "Tolstoy");
INSERT book values(2, "Kolobok", "Narodnaya");