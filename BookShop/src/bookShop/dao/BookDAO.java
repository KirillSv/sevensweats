package bookShop.dao;

import bookShop.entity.Book;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class BookDAO {

	private static final String SELECT_BOOK = "SELECT * FROM book WHERE id = ?";
	private static final String SELECT_ALL_BOOK = "SELECT * FROM book";
	private static final String INSERT_BOOK = "INSERT INTO book (title, author) VALUES (?, ?)";
	private static final String DELETE_BOOK = "DELETE FROM book where id = ?";
	private static final String UPDATE_BOOK = "UPDATE book SET title = ?, author = ?";

	private String jdbcURL;
	private String jdbcUser;
	private String jdbcPass;
	private Connection jdbcConnection;

	public BookDAO(String jdbcURL, String jdbcUser, String jdbcPass) {
		this.jdbcURL = jdbcURL;
		this.jdbcUser = jdbcUser;
		this.jdbcPass = jdbcPass;
	}

	public boolean insertBook(Book book) throws SQLException {
		connectDatabase();
		PreparedStatement st = jdbcConnection.prepareStatement(INSERT_BOOK);
		st.setString(1, book.getTitle());
		st.setString(2, book.getAuthor());
		boolean res = st.executeUpdate() > 0;
		st.close();
		closeConnect();
		return res;
	}

	public List<Book> listAllBooks() throws SQLException {
		List<Book> listBook = new ArrayList<>();
		connectDatabase();
		Statement st = jdbcConnection.createStatement();
		ResultSet rs = st.executeQuery(SELECT_ALL_BOOK);
		while (rs.next()) {
			int id = rs.getInt("id");
			String title = rs.getString("title");
			String author = rs.getString("author");

			Book book = new Book(id, title, author);
			listBook.add(book);
		}

		rs.close();
		st.close();
		closeConnect();
		return listBook;
	}

	public boolean deleteBook(Book book) throws SQLException {
		connectDatabase();
		PreparedStatement st = jdbcConnection.prepareStatement(DELETE_BOOK);
		st.setInt(1, book.getId());
		boolean res = st.executeUpdate() > 0;
		st.close();
		closeConnect();
		return res;
	}

	public boolean updateBook(Book book) throws SQLException {
		connectDatabase();
		PreparedStatement st = jdbcConnection.prepareStatement(UPDATE_BOOK);
		st.setString(1, book.getTitle());
		st.setString(2, book.getAuthor());
		st.setInt(4, book.getId());

		boolean res = st.executeUpdate() > 0;
		st.close();
		closeConnect();
		return res;
	}

	public Book getBook(int id) throws SQLException {
		Book book = null;
		connectDatabase();
		PreparedStatement st = jdbcConnection.prepareStatement(SELECT_BOOK);
		st.setInt(1, id);
		ResultSet rs = st.executeQuery();
		if (rs.next()) {
			String title = rs.getString("title");
			String author = rs.getString("author");
			book = new Book(id, title, author);
		}
		rs.close();
		st.close();
		return book;
	}

	protected void connectDatabase() throws SQLException {
		if (jdbcConnection == null || jdbcConnection.isClosed()) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				throw new SQLException(e);
			}

			Properties prop = new Properties();
			prop.put("user", jdbcUser);
			prop.put("password", jdbcPass);
			prop.put("useSSL", "false");
			prop.put("serverTimezone", "UTC");
			prop.put("characterEncoding", "UTF-8");

			jdbcConnection = DriverManager.getConnection(jdbcURL, prop);
		}
	}

	protected void closeConnect() throws SQLException {
		if (jdbcConnection != null && !jdbcConnection.isClosed()) {
			jdbcConnection.close();
		}
	}
}